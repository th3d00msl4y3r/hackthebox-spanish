# OpenKeyS

**OS**: OpenBSD \
**Dificultad**: Medio \
**Puntos**: 30

## Resumen

- CVE-2019-19521: Authentication bypass
- CVE-2019-19520: Local privilege escalation via xlock

## Nmap Scan

`nmap -Pn -sV -sC -p- -10.10.10.199`

```
Nmap scan report for 10.10.10.199
Host is up (0.058s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.1 (protocol 2.0)
| ssh-hostkey: 
|   3072 5e:ff:81:e9:1f:9b:f8:9a:25:df:5d:82:1a:dd:7a:81 (RSA)
|   256 64:7a:5a:52:85:c5:6d:d5:4a:6b:a7:1a:9a:8a:b9:bb (ECDSA)
|_  256 12:35:4b:6e:23:09:dc:ea:00:8c:72:20:c7:50:32:f3 (ED25519)
80/tcp open  http    OpenBSD httpd
|_http-title: Site doesn't have a title (text/html).
```

## Enumeración

Enumerando el puerto 80 podemos ver un panel de login, al cual no podemos acceder, ya que no tenemos credenciales.

![1](img/1.png)

Usando gobuster para encontrar directorios, podemos ver uno llamado **/includes**.

`gobuster dir -u http://10.10.10.199/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -x php,txt,html -t 40`

![2](img/2.png)

Se visualiza un archivo interesante llamado **auth.php.swp**.

`http://10.10.10.199/includes/`

![3](img/3.png)

Utilizamos wget para descargarlo y haciendo uso del comando **strings** vemos que contiene un nombre de usuario llamado **jennifer** y un path **../auth_helpers/check_auth**.

- `wget http://10.10.10.199/includes/auth.php.swp`
- `strings auth.php.swp`

![4](img/4.png)

Accediendo desde el browser se encuentra un archivo llamado **check_auth**, el cual descargamos.

`http://10.10.10.199/auth_helpers/`

![5](img/5.png)

Repitiendo el proceso con el comando strings, a simple vista no se logran ver cosas interesantes.

`strings check_auth`

![6](img/6.png)

### CVE-2019-19521: Authentication bypass

Investigando llegamos a este [articulo](https://www.qualys.com/2019/12/04/cve-2019-19521/authentication-vulnerabilities-openbsd.txt) que nos lista varias vulnerabilidades de OpenBSD entre ellas una para hacer bypass de la autenticación.

![7](img/7.png)

Usando **-schallenge** como usuario y un password cualquiera podemos ver que es posible acceder pero nos regresa un mensaje que dice **OpenSSH key not found for user -schallenge**.

![8](img/8.png)

![9](img/9.png)

Si recordamos el archivo **auth.php.swp** es necesario agregar **username=jennifer** en la cookie. Para eso utilizamos **Burpsuite**, capturamos la petición y modificamos la cookie.

![10](img/10.png)

Veremos que nos regresa la llave del usuario jennifer.

![11](img/11.png)

Copiamos la llave y le cambiamos los permisos para acceder mediante SSH.

- `nano id_rsa`
- `chmod 400 id_rsa`
- `ssh -i id_rsa jennifer@10.10.10.199`

![12](img/12.png)

## Escalada de Privilegios

Usando como base el [articulo](https://www.qualys.com/2019/12/04/cve-2019-19521/authentication-vulnerabilities-openbsd.txt) mencionado anteriormente se puede ver que hay una vulnerabilidad que nos permite escalar privilegios.

![13](img/13.png)

### CVE-2019-19520: Local privilege escalation via xlock

Descargamos él [exploit](https://github.com/bcoles/local-exploits/blob/master/CVE-2019-19520/openbsd-authroot) y lo subimos a la maquina.

`scp -i id_rsa exploit.sh jennifer@10.10.10.199:/home/jennifer`

![14](img/14.png)

Le damos permisos de ejecución al exploit, lo ejecutamos y solo tendríamos que escribir el password que nos arroja el exploit para escalar a root.

- `chmod +x exploit.sh`
- `./exploit.sh`

![15](img/15.png)

## Referencias
https://www.qualys.com/2019/12/04/cve-2019-19521/authentication-vulnerabilities-openbsd.txt \
https://github.com/bcoles/local-exploits/blob/master/CVE-2019-19520/openbsd-authroot