# Delivery

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Mattermost
- osTicket
- MySQL
- Hashcat Rules

## Nmap Scan

`nmap -sV -sC -p- -oN nmap.txt 10.10.10.222`

```
Nmap scan report for 10.10.10.222
Host is up (0.065s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 9c:40:fa:85:9b:01:ac:ac:0e:bc:0c:19:51:8a:ee:27 (RSA)
|   256 5a:0c:c0:3b:9b:76:55:2e:6e:c4:f4:b9:5d:76:17:09 (ECDSA)
|_  256 b7:9d:f7:48:9d:a2:f2:76:30:fd:42:d3:35:3a:80:8c (ED25519)
80/tcp   open  http    nginx 1.14.2
|_http-server-header: nginx/1.14.2
|_http-title: Welcome
8065/tcp open  unknown
| fingerprint-strings: 
|   GenericLines, Help, RTSPRequest, SSLSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 200 OK
|     Accept-Ranges: bytes
|     Cache-Control: no-cache, max-age=31556926, public
|     Content-Length: 3108
|     Content-Security-Policy: frame-ancestors 'self'; script-src 'self' cdn.rudderlabs.com
|     Content-Type: text/html; charset=utf-8
|     Last-Modified: Thu, 14 Jan 2021 21:40:58 GMT
|     X-Frame-Options: SAMEORIGIN
|     X-Request-Id: arec7jcjuf81upz414skzhdjzw
|     X-Version-Id: 5.30.0.5.30.1.57fb31b889bf81d99d8af8176d4bbaaa.false
|     Date: Thu, 14 Jan 2021 22:06:44 GMT
|     <!doctype html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0"><meta name="robots" content="noindex, nofollow"><meta name="referrer" content="no-referrer"><title>Mattermost</title><meta name="mobile-web-app-capable" content="yes"><meta name="application-name" content="Mattermost"><meta name="format-detection" content="telephone=no"><link re
|   HTTPOptions: 
|     HTTP/1.0 405 Method Not Allowed
|     Date: Thu, 14 Jan 2021 22:06:46 GMT
|_    Content-Length: 0
```

## Enumeración

En el puerto 80 logramos ver un subdominio **helpdesk.delivery.htb**, el cual agregamos a nuestro archivo hosts.

![1](img/1.png)

##### /etc/hosts
```
10.10.10.222    delivery.htb helpdesk.delivery.htb
```

Accediendo al subdominio vemos una aplicación para levantar tickets **osTicket**.

![2](img/2.png)

Mientras que en el puerto 8065 encontramos una aplicación llamada **Mattermost** donde podemos registrarnos.

![3](img/3.png)

### Mattermost / osTicket

Si intentamos registrar un usuario en la página Mattermost nos enviaran un correo de validación y en osTicket al momento de levantar un ticket nos genera un correo y un número para después dar seguimiento del ticket. Combinando esas funciones obtenemos lo siguiente:

Creamos un ticket nuevo.

![4](img/4.png)

![5](img/5.png)

Con el correo que se nos genera crearemos una cuenta en Mattermost.

![6](img/6.png)

Después de crear la cuenta iremos al apartado de **Check Ticket Status** e ingresaremos el correo que se puso al momento de generar el ticket y también el número de ticket.

![7](img/7.png)

Veremos que recibimos el correo de validación.

![8](img/8.png)

Accederemos al link y se validará correctamente la cuenta.

![9](img/9.png)

Ahora solo es cuestión de iniciar sesión y obtendremos un usuario y password.

![10](img/10.png)

```
maildeliverer : Youve_G0t_Mail!
```

Con las credenciales accedemos por SSH.

![11](img/11.png)

## Escalada de Privilegios

Enumerando encontramos los archivos de configuración de la aplicación Mattermost y podemos ver usuario y password para conectarnos a MySQL.

`cat /opt/mattermost/config/config.json`

![12](img/12.png)

```
mmuser : Crack_The_MM_Admin_PW
```

Accediendo a la base de datos encontramos varios usuarios pero el más importante **root** y su hash.

- `mysql -u mmuser -pCrack_The_MM_Admin_PW -D mattermost`
- `SELECT Username,Password FROM Users;`

![13](img/13.png)

### Hashcat Rules

Si recordamos los mensajes que están en Mattermost, nos mencionan lo siguiente:

```
PleaseSubscribe! may not be in RockYou but if any hacker manages to get our hashes, they can use hashcat rules to easily crack all variations of common words or phrases.
```

Podemos intentar descifrar el hash usando reglas con la palabra **PleaseSubscribe!**.

`hashcat -m 3200 hash.txt password.txt -r /usr/share/hashcat/rules/best64.rule --quiet`

![14](img/14.png)

Obtenemos el password y con él podemos iniciar sesión como root.

```
root : PleaseSubscribe!21
```

`su root`

![15](img/15.png)

## Referencias
https://medium.com/intigriti/how-i-hacked-hundreds-of-companies-through-their-helpdesk-b7680ddc2d4c \
https://hashcat.net/wiki/doku.php?id=example_hashes